<?php
$config = array(
    'manager/food' => array(
        // array(
        //         'field' => 'lang',
        //         'label' => 'نوع غذا',
        //         'rules' => 'required|max_length[50]'
        // ),
        array(
                'field' => 'name',
                'label' => 'نام غذا',
                'rules' => 'required|max_length[200]'
        ),
        array(
                'field' => 'price',
                'label' => 'قیمت',
                'rules' => 'required|numeric'
        ),
        array(
                'field' => 'description',
                'label' => 'توضیحات',
                'rules' => 'required|max_length[200]'
        ),
        array(
                'field' => 'discount',
                'label' => 'درصد تخفیف',
                'rules' => 'numeric'
        ),
        array(
                'field' => 'type',
                'label' => 'نوع خوراکی',
                'rules' => 'required|numeric|is_natural'
        ),
        array(
                'field' => 'categoryId',
                'label' => 'دسته غذا',
                'rules' => 'required'
        ),
    ),
    'manager/foodcategory' => array(
        array(
                'field' => 'name',
                'label' => 'نام غذا',
                'rules' => 'required|min_length[2]|max_length[250]'
        ),
        array(
                'field' => 'lang',
                'label' => 'نوع غذا',
                'rules' => 'required'
        ),
    ),
    'manager/postcategory' => array(
        array(
                'field' => 'name',
                'label' => 'نام بلاگ',
                'rules' => 'required|min_length[2]|max_length[250]'
        ),
        array(
                'field' => 'lang',
                'label' => 'نوع بلاگ',
                'rules' => 'required'
        ),
    ),

    'manager/sendChat' => array(
        array(
                'field' => 'text',
                'label' => 'نام بلاگ',
                'rules' => 'required|min_length[2]'
        ),
        array(
                'field' => 'userId',
                'label' => 'نوع بلاگ',
                'rules' => 'required'
        ),
    ),
    'post' => array(
        array(
                'field' => 'subject',
                'label' => 'موضوع پیام',
                'rules' => 'required'
        ),
        // array(
        //         'field' => 'lang',
        //         'label' => 'زبان',
        //         'errors' => array(
        //             'required' => '%s را انتخاب کنید.',
        //         ),
        //         'rules' => 'required'
        // ),
        array(
                'field' => 'categoryId',
                'label' => 'دسته',
                'errors' => array(
                    'is_natural_no_zero' => '%s را انتخاب کنید.',
                ),
                'rules' => 'required|numeric|is_natural_no_zero'
        ),
        array(
                'field' => 'text',
                'label' => 'متن',
                'rules' => 'required|min_length[10]'
        ),
    ),
    'offer' => array(
        array(
                'field' => 'subject',
                'label' => 'موضوع',
                'rules' => 'required|max_length[250]'
        ),
        array(
                'field' => 'text',
                'label' => 'متن',
                'rules' => 'required|min_length[10]'
        ),
        array(
                'field' => 'userId',
                'label' => 'نوع پیام',
                'rules' => 'required'
        ),
    ),
    'comment' => array(
        array(
                'field' => 'text',
                'label' => 'متن',
                'rules' => 'required|min_length[10]'
        ),
        array(
                'field' => 'postId',
                'label' => '',
                'rules' => 'required|numeric'
        ),
        array(
                'field' => 'userId',
                'label' => 'نوع پیام',
                'rules' => 'required|numeric'
        ),
    ),
    'survey' => array(
        array(
                'field' => 'surveyId',
                'label' => 'موضوع',
                'rules' => 'required|numeric'
        ),
        array(
                'field' => 'answer',
                'label' => 'پاسخ',
                'rules' => 'required|numeric'
        ),
    ),
    'manager/mediacategory' => array(
        array(
                'field' => 'name',
                'label' => 'نام دسته مدیا',
                'rules' => 'required|min_length[2]|max_length[250]'
        ),
        array(
                'field' => 'lang',
                'label' => 'نوع دسته مدیا',
                'rules' => 'required'
        ),
        array(
                'field' => 'type',
                'label' => 'نوع رسانه',
                'errors' => array(
                    'required' => '%s را انتخاب کنید',
                ),
                'rules' => 'required'
        ),
    ),
    'manager/mediasubcategory' => array(
        array(
                'field' => 'name',
                'label' => 'نام دسته مدیا',
                'rules' => 'required|min_length[2]|max_length[250]'
        ),
        array(
                'field' => 'categoryId',
                'label' => 'دسته اصلی',
                'rules' => 'required'
        ),
    ),
    'selectSubCategory' => array(
        array(
                'field' => 'categoryId',
                'label' => 'دسته',
                'rules' => 'required|numeric'
        ),
    ),
    'foodbasket' => array(
        array(
                'field' => 'foodId',
                'label' => 'دسته',
                'rules' => 'required|numeric'
        ),
    ),
    'orderedbasket' => array(
        array(
                'field' => 'orderId',
                'label' => 'شماره سفارش',
                'rules' => 'required|numeric'
        ),
    ),
    'order' => array(
        // array(
        //         'field' => 'name',
        //         'label' => 'نام لیست',
        //         'rules' => 'required|max_length[5]'
        // ),
        array(
                'field' => 'desk',
                'label' => 'شماره میز',
                'rules' => 'required|numeric'
        ),
        array(
                'field' => 'chair',
                'label' => 'شماره صندلی',
                'rules' => 'required|numeric'
        ),
    ),
    'medialike' => array(
        array(
                'field' => 'mediaId',
                'label' => '',
                'rules' => 'required|numeric'
        ),
    ),
    'postlike' => array(
        array(
                'field' => 'postId',
                'label' => '',
                'rules' => 'required|numeric'
        ),
    ),
    'media' => array(
        array(
                'field' => 'subject',
                'label' => 'موضوع پیام',
                'rules' => 'required'
        ),
        array(
                'field' => 'type',
                'label' => 'نوع خوراکی',
                'errors' => array(
                    'required' => '%s را انتخاب کنید.',
                ),
                'rules' => 'required|numeric'
        ),
        array(
                'field' => 'lang',
                'label' => 'زبان',
                'errors' => array(
                    'required' => '%s را انتخاب کنید.',
                ),
                'rules' => 'required'
        ),
        array(
                'field' => 'categoryId',
                'label' => 'دسته',
                'errors' => array(
                    'is_natural_no_zero' => '%s را انتخاب کنید.',
                ),
                'rules' => 'required|numeric|is_natural_no_zero'
        ),
        array(
                'field' => 'subCategoryId',
                'label' => 'زیر دسته',
                'errors' => array(
                    'is_natural_no_zero' => '%s را انتخاب کنید.',
                ),
                'rules' => 'required|numeric|is_natural_no_zero'
        ),

    ),
    'mediaupdate' => array(
        array(
                'field' => 'subject',
                'label' => 'موضوع پیام',
                'rules' => 'required'
        ),
        array(
                'field' => 'categoryId',
                'label' => 'دسته',
                'errors' => array(
                    'is_natural_no_zero' => '%s را انتخاب کنید.',
                ),
                'rules' => 'required|numeric|is_natural_no_zero'
        ),
        array(
                'field' => 'subCategoryId',
                'label' => 'زیر دسته',
                'errors' => array(
                    'is_natural_no_zero' => '%s را انتخاب کنید.',
                ),
                'rules' => 'required|numeric|is_natural_no_zero'
        ),

    ),
    'sendMessage' => array(
        array(
                'field' => 'userId',
                'label' => '',
                'rules' => 'required|numeric'
        ),
        array(
                'field' => 'text',
                'label' => 'متن پیام',
                'rules' => 'required'
        ),
        array(
                'field' => 'subject',
                'label' => 'موضوع پیام',
                'rules' => 'required'
        ),
    ),
    'customer' => array(
        array(
                'field' => 'userId',
                'label' => '',
                'rules' => 'required|numeric'
        ),
        array(
                'field' => 'text',
                'label' => 'متن پیام',
                'rules' => 'required'
        ),
        array(
                'field' => 'subject',
                'label' => 'موضوع پیام',
                'rules' => 'required'
        ),
    ),
    'manager/survey' => array(
        array(
                'field' => 'question',
                'label' => 'سوال',
                'rules' => 'required'
        ),
        array(
                'field' => 'answer1',
                'label' => 'پاسخ ۱',
                'rules' => 'required|max_length[250]'
        ),
        array(
                'field' => 'answer2',
                'label' => 'پاسخ ۲',
                'rules' => 'required|max_length[250]'
        ),
    ),

);
