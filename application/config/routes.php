<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['manage']='manager';
$route['register']='user_acts/register';
$route['picklang']='home/picklang';
$route['basket']='home/basket';
$route['mybasket']='home/mybasket';
$route['menu']='home/menu';
$route['home']='home/index';
// $route['order']='home/order';
$route['orderedlists']='home/orderedlists';
$route['offer']='home/offer';
$route['survey']='home/survey';
$route['messages']='home/messages';

$route['picturegallery']='home/picturegallery';
$route['videogallery']='home/videogallery';
$route['musics']='home/musics';
$route['games']='home/games';
$route['texts']='home/texts';
$route['medialike']='home/medialike';
$route['mediaseen']='home/mediaseen';
$route['mediadownload']='home/mediadownload';
$route['download']='home/download';

$route['text/(:any)-(:num)']='home/text/$1/$2';
$route['game/(:any)-(:num)']='home/game/$1/$2';
$route['video/(:any)-(:num)']='home/video/$1/$2';
$route['picture/(:any)-(:num)']='home/picture/$1/$2';
$route['music/(:any)-(:num)']='home/music/$1/$2';

$route['default_controller'] = 'home/menu';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
