<?php

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');
if ( ! function_exists('manager_url'))
{
    function manager_url($url=''){
       return base_url('manager/'.$url);
    }
}
if ( ! function_exists('manager_url'))
{
    function log_site($zone='members')
    {
    //stuff that uses CI
    	$CI =& get_instance();
    	$CI->load->library('user_agent');
    	if($CI->agent->is_robot())
		{
			return FALSE;
		}
		else
		{

		    if ($CI->agent->is_browser())
		    {
		        $agent = $CI->agent->browser().' '.$CI->agent->version();
		    }
		    elseif ($CI->agent->is_mobile())
		    {
		        $agent = $CI->agent->mobile();
		    }
		    else
		    {
		        $agent = 'Unidentified User Agent';
		    }

		    $browser = $CI->agent->browser();
		    $browser_version = $CI->agent->version();
		    $os = $CI->agent->platform();


		    //$zone (frontend / admin / members)
		    $general = $CI->uri->segment( 1 );        //products
		    $specific = $CI->uri->segment( 2 );        //products/shoes
		    $item = $CI->uri->segment( 3 );        //products/view_shoe/34
		    $session = $CI->db_session->userdata( 'session_id' );
		    $ip = $CI->db_session->userdata( 'ip_address' );
		    $user_id = getUserProperty( 'id' );      //if user is logged in
		    $user_agent = $CI->db_session->userdata( 'user_agent' );

		    //use a model for inserting data
		    $CI->load->model('Logmodel');
		    $CI->Logmodel->log_site($zone, $general, $specific, $item, $session, $ip, $user_id, $user_agent, $browser, $browser_version, $os, $agent);
		}
	}
}

if ( ! function_exists('this_url'))
{
    function this_url($url=''){
       return "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }
}

if ( ! function_exists('url_maker'))
{
    function url_maker($action,$id){
    	if(preg_match("/$action/", $_SERVER["REQUEST_URI"]))
    	{
			$myUrl=preg_replace("/$action=.*/", "$action=".$id, $_SERVER["REQUEST_URI"]);
    		// if(preg_match("/&$action=.*/", $_SERVER["REQUEST_URI"]))
    		// {
	    	// 	// echo '---'.$myUrl.'---';
	    	// 	$queryString=$_SERVER["QUERY_STRING"]!=""?$myUrl."&$action=".$id:$myUrl."?$action=".$id;
	    	// }
    		// elseif($myUrl=preg_replace("/$action=.*/", '', $_SERVER["REQUEST_URI"]))
    		// {
    		// 	echo '11111111111111111111111111111111111111111111111111';
		    // 	$queryString=$_SERVER["QUERY_STRING"]!=""?$myUrl."?$action=".$id:$myUrl."?$action=".$id;
    		// }

	    	$queryString=$myUrl;
    	}
    	else
	    	$queryString=$_SERVER["QUERY_STRING"]!=""?$_SERVER["REQUEST_URI"]."&$action=".$id:$_SERVER["REQUEST_URI"]."?$action=".$id;

    	return $queryString;
    }
}
if ( ! function_exists('pgPerPage'))
{
    function pgPerPage(){
    	$opt=(isset($_COOKIE['stuffAtPage']) && $_COOKIE['stuffAtPage']!='')?"<option value='".$_COOKIE['stuffAtPage']."'>$_COOKIE[stuffAtPage]</option>":'';
    	return '
    	<div class="dataTables_length" id="example1_length">
	        <label>نمایش
	            <select name="example1_length" id="numberAtPage" aria-controls="example1" class="form-control input-sm" onchange="numberAtPagee()">
	            '.$opt.'
	                <option value="10">10</option>
	                <option value="5">5</option>
	                <option value="25">25</option>
	                <option value="50">50</option>
	                <option value="100">100</option>
	            </select> مطلب در صفحه
	        </label>
	    </div>
	    <script>
	    function setCookie(name,value)
        {
            var date = new Date();
            date.setTime(date.getTime() + (31 * 24 * 60 * 60 * 1000));
            var expire = "; expires=" + date.toGMTString();
            document.cookie = name + "=" + value + "" + expire + "; path=/";
        }
        function getCookie(name)
        {
            var cookiename = name + "=";
            var ca = document.cookie.split(";");
            for(var i=0;i < ca.length;i++)
            {
                var c = ca[i];
                while (c.charAt(0)==" ") c = c.substring(1,c.length);
                if (c.indexOf(cookiename) == 0) return c.substring(cookiename.length,c.length);
            }
            return null;
        }function numberAtPagee()
		{
			select=document.getElementById("numberAtPage");
			setCookie("stuffAtPage",select.value);
			//window.location="?page=1";
			href=window.location.href;
			// var str   = "asd-0.testing";
			var regex = /page=\d/;
			newHref = href.replace(regex, "page=1");
			location.href=newHref;
		}</script>
    	';
    }
}


if ( ! function_exists('flashdata'))
{
    function flashdata($field,$message){
    	$CI =& get_instance();
       return $CI->session->set_flashdata($field,$message);
    }
}

if ( ! function_exists('lang_date'))
{
    function lang_date($pat='H:i:s Y/m/d',$time=''){
    	global $timezones;
		$time=empty($time)?time():$time;
    	$CI =& get_instance();
    	switch ($_SESSION['usLang']) {
    		case 'persian':
    		case 'arabic':
		       		return $CI->jdf->jdate($pat,$time);
    			break;
    		case 'english':
		       		return date($pat,$time);
    			break;

    		default:
		       		return $CI->jdf->jdate($pat,$time);
    			break;
    	}
    }
}

if ( ! function_exists('asset_url'))
{
    function asset_url(){
    //    echo base_url();
       return base_url().'assets/';
    }
}

if ( ! function_exists('message'))
{
    function message($type,$message){
    //    echo base_url();
       return "<span class='text-$type'>$message</span>";
    }
}
if ( ! function_exists('messageuser'))
{
    function messageuser($type,$message){
    //    echo base_url();
       return "<span class='uk-text-$type uk-h5'>$message</span>";
    }
}
/**
 * CodeIgniter Short Input Posts Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Hassan Shojaei
 */

// ------------------------------------------------------------------------

if ( ! function_exists('posts'))
{

	function posts($field='')
	{
		$CI =& get_instance();
		if(empty($field))
	        return $CI->input->post()?$CI->input->post():false;
	    else
	        return $CI->input->post($field)?$CI->input->post($field):false;
	}
}
if ( ! function_exists('servers'))
{

	function servers($field='')
	{
		$CI =& get_instance();
		if(empty($field))
	        return $CI->input->server();
	    else
	        return $CI->input->server($field);
	}
}
if ( ! function_exists('langDirection'))
{
	function langDirection()
	{
        global $langDirection;
        if(isset($_SESSION['usLang']) && !empty($_SESSION['usLang']))
            return $langDirection[$_SESSION['usLang']];
        else
            return $langDirection['persian'];
	}
}
/**
 * CodeIgniter Short Input Posts Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Hassan Shojaei
 */

// ------------------------------------------------------------------------

if ( ! function_exists('gets'))
{
	/**
	 * Get "now" time
	 *
	 * Returns time() based on the timezone parameter or on the
	 * "time_reference" setting
	 *
	 * @param	string
	 * @return	int
	 */
	function gets($field='')
	{
		$CI =& get_instance();
       if(empty($field))
	        return $CI->input->get()?$CI->input->get():false;
	    else
	        return $CI->input->get($field)?$CI->input->get($field):false;
	}
}
/**
 * CodeIgniter Short Uri Segments Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Hassan Shojaei
 */

// ------------------------------------------------------------------------

if ( ! function_exists('params'))
{
	/**
	 * Get "now" time
	 *
	 * Returns time() based on the timezone parameter or on the
	 * "time_reference" setting
	 *
	 * @param	string
	 * @return	int
	 */
	function params($field='')
	{
		$CI =& get_instance();
        return $CI->uri->segment($field);
	}
}
