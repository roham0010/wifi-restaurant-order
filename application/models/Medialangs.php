<?php
class Medialangs extends CI_Model
{
    // --- ASSOCIATIONS ---
    // generateAssociationEnd : 

    // --- ATTRIBUTES ---

    /**
     * Short description of attribute mmlId
     *
     * @access public
     * @var Integer
     */
    public $mlId = null;

    /**
     * Short description of attribute mmlMediaId
     *
     * @access public
     * @var Integer
     */
    public $mlMediaId = null;

    /**
     * Short description of attribute mmlMediaId
     *
     * @access public
     * @var Integer
     */
    public $mlCategoryId = null;

    /**
     * Short description of attribute mmlMediaId
     *
     * @access public
     * @var Integer
     */
    public $mlSubCategoryId = null;

    /**
     * Short description of attribute meSubject
     *
     * @access public
     * @var string
     */
    public $mlSubject = '';

    /**
     * Short description of attribute mlText
     *
     * @access public
     * @var String
     */
    public $mlText = null;

    /**
     * Short description of attribute mlText
     *
     * @access public
     * @var String
     */
    public $mlLang = null;

    // --- OPERATIONS ---
    public function select_langs($meId)
    {
         if($medialangs=$this->db->where('mlMediaId',$meId)->get('medialangs',3,0))
            return $medialangs->result();
        else
            return false;
    }
    public function select_lang($mlId)
    {
        
        $this->db->join('medias','meId=mlMediaId');
        // $this->db->join('mediauser','meId=muMediaId','left');
        $this->db->join('(select * from `mediauser` order by field(muUserId,'.$_SESSION['usId'].') desc) as muser ','meId=muser.muMediaId','left outer');
        if($medialang=$this->db->where('mlId',$mlId)->get('medialangs',1,0))
            return $medialang->result();
        else
            return false;
    }
    public function set_value($params,$mediaId)
    {
        $this->mlId=(isset($params['mlId']) && (isset($params['mlId'])))?$params['mlId']:$this->mlId;
        $this->mlMediaId=(isset($mediaId) && (isset($mediaId)))?$mediaId:$this->mlMediaId;
        $this->mlCategoryId=(isset($params['categoryId']) && (isset($params['categoryId'])))?$params['categoryId']:$this->mlCategoryId;
        $this->mlSubCategoryId=(isset($params['subCategoryId']) && (isset($params['subCategoryId'])))?$params['subCategoryId']:$this->mlSubCategoryId;
        $this->mlSubject=(isset($params['subject']) && (isset($params['subject'])))?$params['subject']:$this->mlSubject;
        $this->mlText=(isset($params['text']) && (isset($params['text'])))?$params['text']:$this->mlText;
        $this->mlLang=(isset($params['lang']) && (isset($params['lang'])))?$params['lang']:$this->mlLang;
    }
    public function insert()
    {
        if($this->db->insert('medialangs', $this))
            return $this->db->insert_id();
        else
            return false;
    }
    public function update()
    {
        if((posts('mlId')))
        {
            if($this->db->insert('medialangs', $this))
                return $this->db->insert_id();
            else
                return false;
        }
        else
        {
            if($this->db->where('mlId',$this->mlId)->update('medialangs', $this))
                return true;
            else
                return false;
        }
    }
} /* end of class mediaMultiLang */

?>