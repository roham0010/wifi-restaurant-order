<?php
/**
 * Short description of class mediaFiles
 *
 * @access public
 * @author firstname and lastname of author, <author@example.org>
 */
class Mediafiles extends CI_Model
{
    // --- ASSOCIATIONS ---


    // --- ATTRIBUTES ---

    /**
     * Short description of attribute mfId
     *
     * @access public
     * @var Integer
     */
    public $mfId = null;

    /**
     * Short description of attribute mfMediaId
     *
     * @access public
     * @var Integer
     */
    public $mfMediaId = null;

    /**
     * Short description of attribute mfSize
     *
     * @access public
     * @var varchar(250)
     */
    public $mfName = null;

    /**
     * Short description of attribute mfSize
     *
     * @access public
     * @var varchar(250)
     */
    public $mfRealName = null;

    /**
     * Short description of attribute mfSize
     *
     * @access public
     * @var Integer
     */
    public $mfSize = null;

    /**
     * Short description of attribute mfSuffix
     *
     * @access public
     * @var String
     */
    public $mfSuffix = null;

    // --- OPERATIONS ---

    /**
     * Short description of method select_all
     *
     * @access public
     * @author Hassan Shojaei
     * @return array
     */
    public function select_files($meId)
    {
        global $mediaTypes,$langsShow;
        

        if($mediafiles=$this->db->where('mfMediaId',$meId)->get('mediafiles'))
            return $mediafiles->result();
        else
            return false;
    }
    /**
     * Short description of method selectone
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $mfId
     * @return array
     */
    public function select_one($mfId)
    {
         if($mediafile=$this->db->where('mfId',$mfId)->get('mediafiles',1,0))
            return $mediafile->result();
        else
            return false;
    }
    public function select_file($mfId)
    {
        
        $this->db->join('medias','meId=mfMediaId');
        if($mediafile=$this->db->where('mfId',$mfId)->get('mediafiles',1,0))
            return $mediafile->result();
        else
            return false;
    }
    public function set_value($params,$array)
    {
        $this->mfId=(isset($params['id']) && (isset($params['id'])))?$params['id']:$this->mfId;
        $this->mfMediaId=(isset($params['mediaId']) && (isset($params['mediaId'])))?$params['mediaId']:$this->mfMediaId;
        $this->mfName=(isset($array['name']) && (isset($array['name'])))?$array['name']:$this->mfName;
        $this->mfRealName=(isset($array['realName']) && (isset($array['realName'])))?$array['realName']:$this->mfRealName;
        // $this->mfSeen=(isset($array['seen']) && (isset($array['seen'])))?$array['seen']:$this->mfSeen;
        // $this->mfRate=(isset($array['rate']) && (isset($array['rate'])))?$array['rate']:$this->mfRate;
        // $this->mfDownloaded=(isset($array['downloaded']) && (isset($array['downloaded'])))?$array['downloaded']:$this->mfDownloaded;
        $this->mfSize=(isset($array['size']) && (isset($array['size'])))?$array['size']:$this->mfSize;
        $this->mfSuffix=(isset($array['suffix']) && (isset($array['suffix'])))?$array['suffix']:$this->mfSuffix;
    }

    public function insert()
    {
        if($this->db->insert('mediafiles', $this))
            return $this->db->insert_id();
        else
            return false;
    }
    /**
     * Short description of method delete
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $mfId
     * @return boolean
     */
    public function delete($mfId,$type)
    {
        $mediafile=$this->select_one($mfId);
        if(sizeof($mediafile)>0)
        {
            $mediafile=$mediafile[0];

            switch ($type) {
                case '1':
                    $folder='images';
                    break;
                case '2':
                    $folder='videos';
                    break;
                case '3':
                    $folder='games';
                    break;
                
                default:
                    $folder='';
                    break;
            }
            if($folder!='') @unlink(FCPATH."upload/".$folder.'/'.$mediafile->mfName);
            if($this->db->where('mfId',$mfId)->delete('mediafiles'))
            {
                // echo $mfId;
                // echo $this->db->where('mfId',$mfId)->delete('mediafiles');
                // exit;
                return true;
            }
            else
                return false;
        }
        else 
            return true;
    }    
    /**
     * Short description of method delete
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $meId
     * @param  $type
     * @return boolean
     */
    public function delete_medias($meId,$type)
    {
        $mediafiles=$this->db->where('mfMediaId',$meId)->get('mediafiles');
        $mediafiles=$mediafiles->result();
        if(sizeof($mediafiles)>0)
        {
            // $mediafiles=$mediafiles[0];

            switch ($type) {
                case '1':
                    $folder='images';
                    break;
                case '2':
                    $folder='videos';
                    break;
                case '3':
                    $folder='games';
                    break;
                
                default:
                    $folder='';
                    break;
            }
            $flag=false;
            foreach ($mediafiles as $mediafile) 
            {
                if($folder!='') @unlink(FCPATH."upload/".$folder.'/'.$mediafile->mfName);
                if(!$this->db->where('mfMediaId',$meId)->delete('mediafiles'))
                    $flag=true;
            }
            if(!$flag)
            {
                // echo $mfId;
                // echo $this->db->where('mfId',$mfId)->delete('mediafiles');
                // exit;
                return true;
            }
            else
                return false;
        }
        else 
            return true;
    }
} /* end of class mfdiaFiles */

?>