<?php
/**
 * untitledModel - class.mediasubcategorys.php
 *
 * $Id$
 *
 * This file is part of untitledModel.
 *
 * Automatically generated on 04.05.2016, 19:45:53 with ArgoUML PHP module 
 * (last revised $Date: 2010-01-12 20:14:42 +0100 (Tue, 12 Jan 2010) $)
 *
 * @author Hassan Shojaei
 */

/**
 * Short description of class mediasubcategorys
 *
 * @access public
 * @author Hassan Shojaei
 */
class Mediasubcategory extends CI_Model 
{
    // --- ASSOCIATIONS ---


    // --- ATTRIBUTES ---

    /**
     * Short description of attribute mscId
     *
     * @access public
     * @var Integer
     */
    public $mscId = null;

    /**
     * Short description of attribute mscName
     *
     * @access public
     * @var String
     */
    public $mscName = null;

    /**
     * Short description of attribute mscName
     *
     * @access public
     * @var String
     */
    public $mscCategoryId = null;

    // --- OPERATIONS ---

    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Short description of method select
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $start
     * @param  $limit
     * @return array
     */
    public function select($start, $limit)
    {
        if($mediasubcategorys=$this->db->get('mediasubcategory',$limit,$start))
            return $mediasubcategorys->result();
        else
            return false;
    }

    /**
     * Short description of method select_all
     *
     * @access public
     * @author Hassan Shojaei
     * @return array
     */
    public function select_all()
    {
        global $mediaTypes,$langsShow;
        $this->db->from('mediacategory');
        $this->db->join('mediasubcategory','mcId=mscCategoryId');
        if((gets('lang')) && gets('lang')!='all')
        {
            $this->db->where('mcLang',$langsShow[gets('lang')]);
        }
        if((gets('categoryId')) && gets('categoryId')!='all')
        {
            $this->db->where('mcId',gets('categoryId'));
        }
        if(posts('action') && posts('action')=='selectSubCategory' && $this->input->is_ajax_request())
        {
            $this->db->where('mscCategoryId',posts('categoryId'));
        }
        if((gets('mediatype')) && gets('mediatype')!='all')
            $this->db->where('mctype',$mediaTypes[gets('mediatype')]);
        // echo $this->db->get_compiled_select();
        $this->db->order_by('mcId asc');
        if($mediasubcategorys=$this->db->get())
            return $mediasubcategorys->result();
        else
            return false;
    }
    /**
     * Short description of method select_all
     *
     * @access public
     * @author Hassan Shojaei
     * @return array
     */
    public function select_all_user($lang,$type)
    {
        global $mediaTypes,$langsShow;
        $this->db->from('mediacategory');
        $this->db->join('mediasubcategory','mcId=mscCategoryId');

        $this->db->where('mcLang',$lang);
        $this->db->where('mcType',$type);

        if((gets('category')) && gets('category')!='all')
        {
            $this->db->where('mcId',gets('category'));
        }

        // echo $this->db->get_compiled_select();
        $this->db->order_by('mscId desc');
        if($mediasubcategorys=$this->db->get())
            return $mediasubcategorys->result();
        else
            return false;
    }
    /**
     * Short description of method select_one_edit
     *
     * @access public
     * @author Hassan Shojaei
     * @return array
     */
    public function select_one_edit($categoryId)
    {
        global $mediaTypes,$langsShow;
        $this->db->from('mediacategory');
        $this->db->join('mediasubcategory','mcId=mscCategoryId');
        
        $this->db->where('mscCategoryId',$categoryId);
        
        // echo $this->db->get_compiled_select();
        $this->db->order_by('mcId asc');
        if($mediasubcategorys=$this->db->get())
            return $mediasubcategorys->result();
        else
            return false;
    }

    /**
     * Short description of method selectone
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $mscId
     * @return array
     */
    public function select_one($mscId)
    {
         if($mediasubcategory=$this->db->where('mscId',$mscId)->get('mediasubcategory',1,0))
            return $mediasubcategory->result_array();
        else
            return false;
    }
    /**
     * Short description of method set_value
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $mscId
     * @return boolean
     */
    public function set_value($params)
    {
        $this->mscId=(isset($params['id']) && (isset($params['id'])))?$params['id']:'';
        $this->mscName=(isset($params['name']) && (isset($params['name'])))?$params['name']:$this->mscName;
        $this->mscCategoryId=(isset($params['categoryId']) && (isset($params['categoryId'])))?$params['categoryId']:$this->mscCategoryId;
    }

    /**
     * Short description of method update
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $mscId
     * @return boolean
     */
    public function update()
    {
        if($this->db->set('mscName',$this->mscName)->where('mscId',$this->mscId)->update('mediasubcategory'))
            return true;
        else
            return false;
    }

    /**
     * Short description of method update
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $mscId
     * @return boolean
     */
    public function active($mscId)
    {
        if($this->db->set('mscActive','NOT mscActive',false)->where('mscId',$mscId)->update('mediasubcategory'))
            return true;
        else
            return false;
    }

    /**
     * Short description of method insert
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $mscId
     * @return boolean
     */
    public function insert()
    {
        if($this->db->insert('mediasubcategory', $this))
            return $this->db->insert_id();
        else
            return false;
    }

    /**
     * Short description of method delete
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $mscId
     * @return boolean
     */
    public function delete($mscId)
    {
        if($this->db->where('mscId',$mscId)->delete('mediasubcategory'))
            return true;
        else
            return false;
    }


} /* end of class mediasubcategorys */

?>