<?php

/**
 * Short description of class userOrder
 *
 * @access public
 * @author firstname and lastname of author, <author@example.org>
 */
class Userorders extends CI_Model
{
    // --- ASSOCIATIONS ---
    // generateAssociationEnd :     // generateAssociationEnd : 

    // --- ATTRIBUTES ---

    /**
     * Short description of attribute uoId
     *
     * @access public
     * @var Integer
     */
    public $uoId = null;

    /**
     * Short description of attribute uoName
     *
     * @access public
     * @var Integer
     */
    public $uoName = null;

    /**
     * Short description of attribute $uoDesk
     *
     * @access public
     * @var Integer
     */
    public $uoDesk = null;

    /**
     * Short description of attribute$uoChair
     *
     * @access public
     * @var Integer
     */
    public $uoChair = null;

    /**
     * Short description of attribute$uoPrice
     *
     * @access public
     * @var Integer
     */
    public $uoPrice = null;

    /**
     * Short description of attribute$uoState
     *
     * @access public
     * @var Integer
     */
    public $uoState = null;

    // --- OPERATIONS ---
        /**
     * Short description of method selectone
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $uoId
     * @return array
     */
    public function select_one($uoId)
    {
        $this->db->from('userorderlog');
        $this->db->join('userorders','uoId=uolUserOrderId');
        $this->db->join('userlogs','ulId=uolUserLogId');
        $this->db->join('users','ulUserId=usId');
        $this->db->limit(1,0);

        // echo $this->db->limit($limit,$start)->get_compiled_select();
         if($food=$this->db->where('uoId',$uoId)->get())
            return $food->result();
        else
            return false;
    }
    /**
     * Short description of method select_join
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $start
     * @param  $limit
     * @return array
     */
    public function select_join($start, $limit,$usId='')
    {
        $this->db->from('userorderlog');
        $this->db->join('userorders','uoId=uolUserOrderId');
        $this->db->join('userlogs','ulId=uolUserLogId');
        $this->db->join('users','ulUserId=usId');
        // $this->db->join('orderitems','uoId=otUserOrderId');
        $this->db->order_by('uolId desc');
        if(!empty($usId))
            $this->db->where('ulUserId',$usId);
        // $this->conditions();
        // echo $this->db->limit($limit,$start)->get_compiled_select();
        if($userorderlog=$this->db->limit($limit,$start)->get())
            return $userorderlog->result();
        else
            return false;
    }

    /**
     * Short description of method select_join
     *
     * @access public
     * @author Hassan Shojaei
     * @return int
     */
    public function select_join_num($usId='')
    {
        $this->db->from('userorderlog');
        $this->db->join('userorders','uoId=uolUserOrderId');
        $this->db->join('userlogs','ulId=uolUserLogId');
        // $this->db->join('orderitems','uoId=otUserOrderId');
        $this->db->order_by('uolId desc');
        if(!empty($usId))
            $this->db->where('ulUserId',$usId);
        // $this->conditions();
        // echo $this->db->get_compiled_select();
        if($userorderlogNum=$this->db->count_all_results())
            return $userorderlogNum;   
        else
            return false;
    }

    public function set_value($params)
    {
        $this->uoName=(isset($params['name']) && (isset($params['name'])))?$params['name']:$this->uoName;
        $this->uoDesk=(isset($params['desk']) && (isset($params['desk'])))?$params['desk']:$this->uoDesk;
        $this->uoChair=(isset($params['chair']) && (isset($params['chair'])))?$params['chair']:$this->uoChair;
    }

    public function insert()
    {
        $price=0;
        foreach ((array)@$_SESSION['basket'] as $key => $basket) 
        {
            $price+=$basket['price'];
        }
        $this->uoPrice=$price;
        if($this->db->insert('userorders',$this))
            return $this->db->insert_id();
        else
            return false;
    }
        /**
     * Short description of method state
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $uoId
     * @param  $val
     * @return boolean
     */
    public function state($uoId,$val)
    {
        if($this->db->set('uoState',$val)->where('uoId',$uoId)->update('userorders'))
            return true;
        else
            return false;
    }
} /* end of class userOrder */

?>