<?php
/**
 * untitledModel - class.foodCategorys.php
 *
 * $Id$
 *
 * This file is part of untitledModel.
 *
 * Automatically generated on 04.05.2016, 19:45:53 with ArgoUML PHP module 
 * (last revised $Date: 2010-01-12 20:14:42 +0100 (Tue, 12 Jan 2010) $)
 *
 * @author Hassan Shojaei
 */

/**
 * Short description of class foodCategorys
 *
 * @access public
 * @author Hassan Shojaei
 */
class Foodcategory extends CI_Model 
{
    // --- ASSOCIATIONS ---


    // --- ATTRIBUTES ---

    /**
     * Short description of attribute fcId
     *
     * @access public
     * @var Integer
     */
    public $fcId = null;

    /**
     * Short description of attribute fcName
     *
     * @access public
     * @var String
     */
    public $fcName = null;

    /**
     * Short description of attribute fcName
     *
     * @access public
     * @var String
     */
    public $fcLang = null;

    // --- OPERATIONS ---

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }
    /**
     * Short description of method select
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $start
     * @param  $limit
     * @return array
     */
    public function select($start, $limit)
    {
        if($foodCategorys=$this->db->get('foodcategory',$limit,$start))
            return $foodCategorys->result();
        else
            return false;
    }

    /**
     * Short description of method select_all
     *
     * @access public
     * @author Hassan Shojaei
     * @return array
     */
    public function select_all()
    {
        if($foodCategorys=$this->db->order_by('fcId asc')->get('foodcategory'))
            return $foodCategorys->result();
        else
            return false;
    }

    /**
     * Short description of method selectone
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $fcId
     * @return array
     */
    public function select_one($fcId)
    {
         if($foodCategory=$this->db->where('fcId',$fcId)->get('foodcategory',1,0))
            return $foodCategory->result_array();
        else
            return false;
    }
    /**
     * Short description of method set_value
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $fcId
     * @return boolean
     */
    public function set_value($params)
    {
        $this->fcId=(isset($params['catid']) && (isset($params['catid'])))?$params['catid']:'';
        $this->fcName=(isset($params['name']) && (isset($params['name'])))?$params['name']:$this->fcName;
        $this->fcLang=(isset($params['lang']) && (isset($params['lang'])))?$params['lang']:$this->fcLang;
    }

    /**
     * Short description of method update
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $fcId
     * @return boolean
     */
    public function update()
    {
        if($this->db->set('fcName',$this->fcName)->set('fcLang',$this->fcLang)->where('fcId',$this->fcId)->update('foodcategory'))
            return true;
        else
            return false;
    }

    /**
     * Short description of method update
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $fcId
     * @return boolean
     */
    public function active($fcId)
    {
        if($this->db->set('fcActive','NOT fcActive',false)->where('fcId',$fcId)->update('foodcategory'))
            return true;
        else
            return false;
    }

    /**
     * Short description of method insert
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $fcId
     * @return boolean
     */
    public function insert()
    {
        if($this->db->insert('foodcategory', $this))
            return $this->db->insert_id();
        else
            return false;
    }

    /**
     * Short description of method delete
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $fcId
     * @return boolean
     */
    public function delete($fcId)
    {
        if($this->db->where('fcId',$fcId)->delete('foodcategory'))
            return true;
        else
            return false;
    }


} /* end of class foodCategorys */

?>