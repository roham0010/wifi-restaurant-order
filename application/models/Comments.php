<?php

class Comments extends CI_Model
{

    /**
     * Short description co attribute coId
     *
     * @access public
     * @var Integer
     */
    public $coId = null;

    /**
     * Short description co attribute coUserId
     *
     * @access public
     * @var Integer
     */
    public $coUserId = null;

    /**
     * Short description co attribute coText
     *
     * @access public
     * @var String
     */
    public $coText = null;

    // --- OPERATIONS ---
    public function set_value($params)
    {
        $this->coPostId=(isset($params['postId']) && (isset($params['postId'])))?$params['postId']:$this->coPostId;
        $this->coUserId=(isset($params['userId']) && (isset($params['userId'])) && $params['userId']==$_SESSION['usId'])?$params['userId']:$this->coUserId;
        $this->coText=(isset($params['text']) && (isset($params['text'])))?$params['text']:$this->coText;
    }
    public function insert()
    {
        if($this->db->insert('comments', $this))
            return $this->db->insert_id();
        else
            return false;
    }
    /**
     * Short description co method select_join
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $start
     * @param  $limit
     * @return array
     */
    public function select($poId)
    {
        $this->db->from('comments');
        $this->db->join('posts','poId=coPostId');
        $this->db->join('users','usId=coUserId');
        $this->db->where('coPostId',$poId);
        $this->db->order_by('coId asc');
        // echo $this->db->get_compiled_select();
        if($comments=$this->db->get())
            return $comments->result();
        else
            return false;
    }

    /**
     * Short description co method select
     *
     * @access public
     * @author Hassan Shojaei
     * @return int
     */
    public function select_num()
    {
        $this->db->from('comments');
        $this->db->order_by('coId asc');
        // echo $this->db->get_compiled_select();
        if($commentsNum=$this->db->count_all_results())
            return $commentsNum;   
        else
            return false;
    }
}

