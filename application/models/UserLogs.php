<?php
/**
 * Short description of class UserLogs
 *
 * @access public
 * @author firstname and lastname of author, <author@example.org>
 */
class UserLogs
{
    // --- ASSOCIATIONS ---
    // generateAssociationEnd : 

    // --- ATTRIBUTES ---

    /**
     * Short description of attribute ulId
     *
     * @access public
     * @var Integer
     */
    public $ulId = null;

    /**
     * Short description of attribute ulOrderId
     *
     * @access public
     * @var Integer
     */
    public $ulOrderId = null;

    /**
     * Short description of attribute ulIn
     *
     * @access public
     * @var DateTime
     */
    public $ulIn = null;

    /**
     * Short description of attribute ulOut
     *
     * @access public
     * @var DateTime
     */
    public $ulOut = null;

    /**
     * Short description of attribute ulUserId
     *
     * @access public
     * @var Integer
     */
    public $ulUserId = null;

    // --- OPERATIONS ---

} /* end of class UserLogs */

?>