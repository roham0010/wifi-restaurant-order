<?php

error_reporting(E_ALL);

/**
 * untitledModel - class.offers.php
 *
 * $Id$
 *
 * This file is part of untitledModel.
 *
 * Automatically generated on 04.05.2016, 19:45:53 with ArgoUof PHP module 
 * (last revised $Date: 2010-01-12 20:14:42 +0100 (Tue, 12 Jan 2010) $)
 *
 * @author firstname and lastname of author, <author@example.org>
 */
/* user defined includes */
// section 127-0-1-1--4c9faab0:154441ae362:-8000:0000000000000C05-includes begin
// section 127-0-1-1--4c9faab0:154441ae362:-8000:0000000000000C05-includes end

/* user defined constants */
// section 127-0-1-1--4c9faab0:154441ae362:-8000:0000000000000C05-constants begin
// section 127-0-1-1--4c9faab0:154441ae362:-8000:0000000000000C05-constants end

/**
 * Short description of class offers
 *
 * @access public
 * @author firstname and lastname of author, <author@example.org>
 */
class offers extends CI_Model
{
    // --- ASSOCIATIONS ---


    public function __construct()
    {
        parent::__construct();
    }

    // --- ATTRIBUTES ---

    /**
     * Short description of attribute ofId
     *
     * @access public
     * @var Integer
     */
    public $ofId = null;

    /**
     * Short description of attribute ofUserId
     *
     * @access public
     * @var Integer
     */
    public $ofUserId = null;

    /**
     * Short description of attribute ofSubject
     *
     * @access public
     * @var String
     */
    public $ofSubject = null;

    /**
     * Short description of attribute ofText
     *
     * @access public
     * @var String
     */
    public $ofText = null;

    // --- OPERATIONS ---
    public function set_value($params)
    {
        $this->ofUserId=(isset($params['userId']) && (isset($params['userId'])) && $params['userId']==$_SESSION['usId'])?$params['userId']:$this->ofUserId;
        $this->ofSubject=(isset($params['subject']) && (isset($params['subject'])))?$params['subject']:$this->ofSubject;
        $this->ofText=(isset($params['text']) && (isset($params['text'])))?$params['text']:$this->ofText;
    }
    public function insert()
    {
        if($this->db->insert('offers', $this))
            return $this->db->insert_id();
        else
            return false;
    }
    /**
     * Short description of method select_join
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $start
     * @param  $limit
     * @return array
     */
    public function select($start, $limit)
    {
        $this->db->from('offers');
        $this->db->order_by('ofId asc');
        // echo $this->db->get_compiled_select();
        if($offers=$this->db->limit($limit,$start)->get())
            return $offers->result();
        else
            return false;
    }

    /**
     * Short description of method select
     *
     * @access public
     * @author Hassan Shojaei
     * @return int
     */
    public function select_num()
    {
        $this->db->from('offers');
        $this->db->order_by('ofId asc');
        // echo $this->db->get_compiled_select();
        if($offersNum=$this->db->count_all_results())
            return $offersNum;   
        else
            return false;
    }
} /* end of class offers */

?>