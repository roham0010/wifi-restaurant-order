<?php

error_reporting(E_ALL);

/**
 * untitledModel - class.users.php
 *
 * $Id$
 *
 * This file is part of untitledModel.
 *
 * Automatically generated on 04.05.2016, 19:45:53 with ArgoUML PHP module 
 * (last revised $Date: 2010-01-12 20:14:42 +0100 (Tue, 12 Jan 2010) $)
 *
 * @author firstname and lastname of author, <author@example.org>
 */
/* user defined includes */
// section 127-0-1-1--4c9faab0:154441ae362:-8000:0000000000000B5A-includes begin
// section 127-0-1-1--4c9faab0:154441ae362:-8000:0000000000000B5A-includes end

/* user defined constants */
// section 127-0-1-1--4c9faab0:154441ae362:-8000:0000000000000B5A-constants begin
// section 127-0-1-1--4c9faab0:154441ae362:-8000:0000000000000B5A-constants end

/**
 * Short description of class users
 *
 * @access public
 * @author firstname and lastname of author, <author@example.org>
 */
class users extends CI_Model
{
    // --- ASSOCIATIONS ---


    // --- ATTRIBUTES ---

    /**
     * Short description of attribute usId
     *
     * @access public
     * @var Integer
     */
    public $usId = null;

    /**
     * Short description of attribute usName
     *
     * @access public
     * @var String
     */
    public $usName = null;

    /**
     * Short description of attribute usPhone
     *
     * @access public
     * @var Integer
     */
    public $usPhone = null;

    /**
     * Short description of attribute usPass
     *
     * @access public
     * @var Integer
     */
    public $usPass = null;

    /**
     * Short description of attribute usLang
     *
     * @access public
     * @var varchar
     */
    public $usLang = null;

    /**
     * Short description of attribute usBirthday
     *
     * @access public
     * @var Date
     */
    public $usBirthday = null;

    /**
     * Short description of attribute usIsAdmin
     *
     * @access public
     * @var String
     */
    public $usIsAdmin = null;

    /**
     * Short description of attribute usAnnivarsary
     *
     * @access public
     * @var Date
     */
    public $usAnniversary = null;

    /**
     * Short description of attribute usImportance
     *
     * @access public
     * @var Integer
     */
    public $usImportance = null;

    /**
     * Short description of attribute usPoint
     *
     * @access public
     * @var Integer
     */
    public $usPoints = null;

    // --- OPERATIONS ---

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }
    public function conditions()
    {
        if((gets('search')))
        {
            $search=gets('search');
            $this->db->group_start();
            $this->db->or_like('usName',$search);
            $this->db->or_like('usLang',$search);
            $this->db->or_like('usPhone',$search);
            $this->db->or_like('uspoints',$search);
            $this->db->or_like('usBirthday',$search);
            $this->db->or_like('usAnniversary',$search);
            $this->db->or_like('usId',$search);
            $this->db->group_end();
        }
        if((gets('lang')))
        {
            $lang=gets('lang');
            if($lang!='all')
                $this->db->where('uslang',$lang);
        }
    }
    public function select($start, $limit,$where='')
    {
        $this->db->from('users');
        $this->db->order_by('usId asc');
        !empty($where)?$this->db->where($where):'';
        $this->users->conditions();
        // echo $this->db->get_compiled_select();
        if($users=$this->db->limit($limit,$start)->get())
            return $users->result();
        else
            return false;
    }

    public function select_join_num()
    {
        $this->db->from('users');
        $this->db->order_by('usId asc');

        $this->users->conditions();
        // echo $this->db->get_compiled_select();
        if($usersNum=$this->db->count_all_results())
            return $usersNum;   
        else
            return false;
    }
    public function select_one($usId)
    {
        if($user=$this->db->where('usId',$usId)->get('users',1,0))
            return $user->result();
        else
            return false;
    }
    
    public function importance($usId)
    {
        if($this->db->set('usImportance','NOT usImportance',false)->where('usId',$usId)->update('users'))
            return true;
        else
            return false;
    }

    public function check_mac($mac)
    {
        if($this->db->where('usMac',$mac)->count_all_results('users')>0)
            return true;
        else
            return false;
    }
    public function login_mac($mac)
    {
        if($user=$this->db->where('usMac',$mac)->get('users',1,0))
            return $user->result();
        else
            return false;
    }
    function logout()
    {
        $this->session->sess_destroy();
        redirect();
    }
    public function set_value($params)
    {
        $this->usName=(isset($params['name']) && (isset($params['name'])))?$params['name']:$this->usName;
        $this->usPhone=(isset($params['phone']) && (isset($params['phone'])))?$params['phone']:$this->usPhone;
        $this->usPass=(isset($params['pass']) && (isset($params['pass'])))?$params['pass']:$this->usPass;
        $this->usLang=(isset($params['lang']) && (isset($params['lang'])))?$params['lang']:$this->usLang;
        $this->usBirthday=(isset($params['birthday']) && (isset($params['birthday'])))?$params['birthday']:$this->usBirthday;
        $this->usHash=(isset($params['hash']) && (isset($params['hash'])))?$params['hash']:$this->usHash;
    }

    public function insert()
    {
        if($this->db->insert('users', $this))
            return $this->db->insert_id();
        else
            return false;
    }
    
    public function order()
    {
        $this->db->trans_begin();
        //insert an ordered list in userOrders
        $this->load->model('userorders');
        $this->userorders->set_value(posts());
        $userOrderId=$this->userorders->insert();
        
        //insert order items
        $this->load->model('orderitems');
        $this->orderitems->insert($userOrderId);

        //get log id from userLogs
        $userLog=$this->select_log();
        // print_r($userLog);

        // insert order id and log id in userOrderLog
        $userLogId=$userLog[0]->ulId;
        $this->load->model('userorderlog');
        $this->userorderlog->insert($userOrderId,$userLogId);
    }

    public function new_mac($mac,$myLang)
    {
        $this->usMac = $mac;
        $this->usLang = $myLang;
        if($this->db->insert('users', $this))
            return true;
        else
            return false;
    }   

    public function select_log()
    {
        $outTime=time()-3600;
        $userLog=$this->db->where('ulUserId',$_SESSION['usId'])->where('ulOrderId',NULL)->where('UNIX_TIMESTAMP(ulOut) >',$outTime)->where('UNIX_TIMESTAMP(ulIn) >=','(UNIX_TIMESTAMP(ulOut)-10800)',FALSE);
        // echo $this->db->get_compiled_select('userlogs',1,0);
        $userLog=$this->db->get('userlogs',1,0);
        $userLog=$userLog->result();

        if(sizeof($userLog)>0)
        {
            $this->refresh_log($userLog[0]->ulId);
        }
        else
        {
            // echo '2222';exit;
            $this->new_log();
        }
        // exit;
        if($userlog=$this->db->where('ulUserId',$_SESSION['usId'])->where('ulOrderId',NULL)->where('UNIX_TIMESTAMP(ulOut) >',$outTime)->where('UNIX_TIMESTAMP(ulIn) >=','(UNIX_TIMESTAMP(ulOut)-10800)',FALSE)->get('userlogs',1,0))
            return $userlog->result();
        else
            return false;
    }
    
    public function select_online()
    {
        $outTime=time()-300;
        $userLog=$this->db->where('ulOrderId',NULL)->where('UNIX_TIMESTAMP(ulOut) >',$outTime)->where('UNIX_TIMESTAMP(ulIn) >=','(UNIX_TIMESTAMP(ulOut)-10800)',FALSE);
        // echo $this->db->get_compiled_select('userlogs',1,0);
        $this->db->join('users','usId=ulUserId');
        $userLog=$this->db->get('userlogs');
        $userLog=$userLog->result();
        return $userLog;
    }
    public function edit_lang($myLang)
    {
        $this->usLang = $myLang;
        if($this->db->set('usLang',$myLang)->where('usMac',$_SESSION['usMac'])->update('users'))
            return true;
        else
            return false;
    }
    public function new_log()
    {
        if($this->db->set('ulUserId',$_SESSION['usId'])->insert('userlogs'))
            return true;
        else
            return false;
    }
    public function refresh_log($ulId)
    {
        if($this->db->set('ulOut', 'NOW()', FALSE)->where('ulId',$ulId)->update('userlogs'))
            return true;
        else
            return false;
    }

    public function first_come()
    {
        
        // print_r($_SESSION);
        // unset($_SESSION['usLang']);
        // unset($_SESSION['usMac']);
        // unset($_SESSION);
        // exit;

        if(isset($_SESSION['usLang']) && !empty($_SESSION['usLang']))
        {
            $this->select_log();
            return;
            
        }

        
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $macAddr=false;
        $arp=`arp -a $ipAddress`;
        $lines=explode("\n", $arp);
        $cols=preg_split('/\s+/', trim($lines[0]));
        $macAddr=$cols[3];
        $_SESSION['usMac']=$macAddr;
        if('picklang'!==params(1))
        {
            if(!$this->users->check_mac($macAddr))
            {
                echo "1";
                echo "1";
                $_SESSION['usMac']=$macAddr;
                // $_SESSION['usLang']='english';
                redirect('picklang');

            }
            else
            {
                echo "2222222222";
                $user=$this->users->login_mac($macAddr);
                $user=$user[0];
                $this->user=$user;
                $user =  (array) $user;
                $this->session->set_userdata($user);
                $this->select_log();
                // print_r($user);
                // print_r($_SESSION);
                // exit;
                if(empty($user['usLang']))
                {
                    // $_SESSION['usLang']='english';
                    redirect('picklang');                
                }
            }
        }
    }
    public function update()
    {
            
            $this->db->update('users', $this, array('id' => $this->input->post('id')));
    }
    public function delete($usId)
    {
        if($this->db->where('usId',$usId)->delete('users'))
            return true;
        else
            return false;
    }
} /* end of class users */

?>