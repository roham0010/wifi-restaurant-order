<?php

/**
 * Short description of class foods
 *
 * @access public
 * @author Hassan Shojaei
 */
class foods extends CI_Model
{
    // --- ASSOCIATIONS ---

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }
    // --- ATTRIBUTES ---

    /**
     * Short description of attribute foId
     *
     * @access public
     * @var Integer
     */
    public $foId = null;

    /**
     * Short description of attribute foCategoryId
     *
     * @access public
     * @var Integer
     */
    public $foCategoryId = null;

    /**
     * Short description of attribute foName
     *
     * @access public
     * @var String
     */
    public $foName = null;

    /**
     * Short description of attribute foPrice
     *
     * @access public
     * @var Integer
     */
    public $foPrice = null;

    /**
     * Short description of attribute foDescription
     *
     * @access public
     * @var String500
     */
    public $foDescription = null;

    /**
     * Short description of attribute foSpecial
     *
     * @access public
     * @var Boolean
     */
    public $foSpecial = null;

    /**
     * Short description of attribute foDiscount
     *
     * @access public
     * @var Integer
     */
    public $foDiscount = null;

    /**
     * Short description of attribute chefFood
     *
     * @access public
     * @var Boolean
     */
    public $foChefFood = null;

    /**
     * Short description of attribute chefFood
     *
     * @access public
     * @var Boolean
     */
    public $foType = null;

    /**
     * Short description of attribute foLang
     *
     * @access public
     * @var String
     */
    public $foLang = null;

    // --- OPERATIONS ---

    /**
     * Short description of method select
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $start
     * @param  $limit
     * @return array
     */
    public function select($start, $limit)
    {
        if($foods=$this->db->get('foods',$limit,$start))
            return $foods->result();
        else
            return false;
    }
    public function conditions()
    {
        if((gets('search')))
        {
            // $where=array_merge(array('foName like' => "%".gets('search')."%", ),$where);
            $search=gets('search');
            $this->db->group_start();
            $this->db->like('foName',$search);
            $this->db->or_like('fcName',$search);
            $this->db->or_like('fcLang',$search);
            $this->db->group_end();
        }
        if((gets('lang')))
        {
            $lang=gets('lang');
            if($lang!='all')
                $this->db->where('fclang',$lang);
        }
    }
    /**
     * Short description of method select_join
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $start
     * @param  $limit
     * @return array
     */
    public function select_join($start, $limit)
    {
        $this->db->from('foods');
        $this->db->join('foodcategory','fcId=foCategoryId');
        $this->db->order_by('foId desc');
        $this->db->where('foActive',1);
        // $this->db->where('foType',1);
        $this->conditions();
        // echo $this->db->get_compiled_select();
        if($foods=$this->db->limit($limit,$start)->get())
            return $foods->result();
        else
            return false;
    }

    /**
     * Short description of method select_join
     *
     * @access public
     * @author Hassan Shojaei
     * @return int
     */
    public function select_join_num()
    {
        $this->db->from('foods');
        $this->db->join('foodcategory','fcId=foCategoryId');
        $this->db->order_by('foId asc');
        $this->db->where('foActive',1);
        $this->db->where('foType',1);
        $this->conditions();
        // echo $this->db->get_compiled_select();
        if($foodsNum=$this->db->count_all_results())
            return $foodsNum;   
        else
            return false;
    }

    /**
     * Short description of method select_all
     *
     * @access public
     * @author Hassan Shojaei
     * @return array
     */
    public function select_all()
    {
        global $langsShow;
        if((gets('foodtype')))
            $foodType=gets('foodtype');
        else
            $foodType=$langsShow['persian'];
        $this->db->where('fcLang',$foodType);
        $this->db->where('foActive',1);
        $this->db->where('foType',1);
        $this->db->join('foodcategory','fcId=foCategoryId');
        if($foods=$this->db->order_by('fcName asc')->order_by('foId asc')->get('foods'))
            return $foods->result();
        else
            return false;
    }

    /**
     * Short description of method select_all_deserts
     *
     * @access public
     * @author Hassan Shojaei
     * @return array
     */
    public function select_all_deserts()
    {
        global $langsShow;
        if((gets('foodtype')))
            $foodType=gets('foodtype');
        else
            $foodType=$langsShow['persian'];
        $this->db->where('fcLang',$foodType);
        $this->db->where('foActive',1);
        $this->db->where('foType', NULL);
        $this->db->join('foodcategory','fcId=foCategoryId');
        if($foods=$this->db->order_by('fcName asc')->order_by('foId asc')->get('foods'))
            return $foods->result();
        else
            return false;
    }
    /**
     * Short description of method select_cats
     *
     * @access public
     * @author Hassan Shojaei
     * @return array
     */
    // public function select_cats()
    // {
    //     global $langsShow;
    //     if((gets('foodtype')))
    //         $foodType=gets('foodtype');
    //     else
    //         $foodType=$langsShow['persian'];
    //     if($foods=$this->db->order_by('foId asc')->get('foods'))
    //         return $foods->result_array();
    //     else
    //         return false;
    // }

    /**
     * Short description of method selectone
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $foId
     * @return array
     */
    public function select_one($foId)
    {
         if($food=$this->db->where('foId',$foId)->get('foods',1,0))
            return $food->result();
        else
            return false;
    }
    /**
     * Short description of method set_value
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $foId
     * @return boolean
     */
    public function set_value($params)
    {
        $this->foId=(isset($params['id'])) && ($params['id'])?$params['id']:$this->foId;
        $this->foLang=(isset($params['lang'])) && ($params['lang'])?$params['lang']:$this->foLang;
        $this->foCategoryId=(isset($params['categoryId'])) && ($params['categoryId'])?$params['categoryId']:$this->foCategoryId;
        $this->foName=(isset($params['name'])) && ($params['name'])?$params['name']:$this->foName;
        $this->foPrice=(isset($params['price'])) && ($params['price'])?$params['price']:$this->foPrice;
        $this->foDescription=(isset($params['description'])) && ($params['description'])?$params['description']:$this->foDescription;
        $this->foSpecial=(isset($params['special'])) && ($params['special'])?$params['special']:$this->foSpecial;
        $this->foDiscount=(isset($params['discount'])) && ($params['discount'])?$params['discount']:$this->foDiscount;
        $this->foChefFood=(isset($params['chefFood'])) && ($params['chefFood'])?$params['chefFood']:$this->foChefFood;
        $this->foType=(isset($params['type'])) && ($params['type'])?$params['type']:$this->foType;
    }

    /**
     * Short description of method update
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $foId
     * @return boolean
     */
    public function update()
    {
        if($this->db->where('foId',$this->foId)->update('foods',$this))
            return true;
        else
            return false;
    }

    /**
     * Short description of method update
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $foId
     * @return boolean
     */
    public function active($foId)
    {
        if($this->db->set('foActive','NOT foActive',false)->where('foId',$foId)->update('foods'))
            return true;
        else
            return false;
    }
    /**
     * Short description of method insert
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $foId
     * @return boolean
     */
    public function insert()
    {
        if($this->db->insert('foods', $this))
            return $this->db->insert_id();
        else
            return false;
    }

    /**
     * Short description of method delete
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $foId
     * @return boolean
     */
    public function delete($foId)
    {
        if($this->db->where('foId',$foId)->delete('foods'))
            return true;
        else
            return false;
    }

} /* end of class foods */

?>