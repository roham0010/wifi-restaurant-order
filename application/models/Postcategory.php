<?php
/**
 * untitledModel - class.postcategorys.php
 *
 * $Id$
 *
 * This file is part of untitledModel.
 *
 * Automatically generated on 04.05.2016, 19:45:53 with ArgoUML PHP module 
 * (last revised $Date: 2010-01-12 20:14:42 +0100 (Tue, 12 Jan 2010) $)
 *
 * @author Hassan Shojaei
 */

/**
 * Short description of class postcategorys
 *
 * @access public
 * @author Hassan Shojaei
 */
class Postcategory extends CI_Model 
{
    // --- ASSOCIATIONS ---


    // --- ATTRIBUTES ---

    /**
     * Short description of attribute pcId
     *
     * @access public
     * @var Integer
     */
    public $pcId = null;

    /**
     * Short description of attribute pcName
     *
     * @access public
     * @var String
     */
    public $pcName = null;

    /**
     * Short description of attribute pcName
     *
     * @access public
     * @var String
     */
    public $pcLang = null;

    // --- OPERATIONS ---

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }
    /**
     * Short description of method select
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $start
     * @param  $limit
     * @return array
     */
    public function select($start, $limit)
    {
        if($postcategorys=$this->db->get('postcategory',$limit,$start))
            return $postcategorys->result();
        else
            return false;
    }

    /**
     * Short description of method select_all
     *
     * @access public
     * @author Hassan Shojaei
     * @return array
     */
    public function select_all()
    {
        global $langsShow;
        if((gets('lang')))
            $this->db->where('pcLang',$langsShow[gets('lang')]);
        if($postcategorys=$this->db->order_by('pcId desc')->get('postcategory'))
            return $postcategorys->result();
        else
            return false;
    }

    /**
     * Short description of method selectone
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $pcId
     * @return array
     */
    public function select_one($pcId)
    {
         if($postcategory=$this->db->where('pcId',$pcId)->get('postcategory',1,0))
            return $postcategory->result_array();
        else
            return false;
    }
    /**
     * Short description of method set_value
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $pcId
     * @return boolean
     */
    public function set_value($params)
    {
        $this->pcId=(isset($params['catid']) && (isset($params['catid'])))?$params['catid']:'';
        $this->pcName=(isset($params['name']) && (isset($params['name'])))?$params['name']:$this->pcName;
        $this->pcLang=(isset($params['lang']) && (isset($params['lang'])))?$params['lang']:$this->pcLang;
    }

    /**
     * Short description of method update
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $pcId
     * @return boolean
     */
    public function update()
    {
        if($this->db->set('pcName',$this->pcName)->set('pcLang',$this->pcLang)->where('pcId',$this->pcId)->update('postcategory'))
            return true;
        else
            return false;
    }

    /**
     * Short description of method update
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $pcId
     * @return boolean
     */
    public function active($pcId)
    {
        if($this->db->set('pcActive','NOT pcActive',false)->where('pcId',$pcId)->update('postcategory'))
            return true;
        else
            return false;
    }

    /**
     * Short description of method insert
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $pcId
     * @return boolean
     */
    public function insert()
    {
        if($this->db->insert('postcategory', $this))
            return $this->db->insert_id();
        else
            return false;
    }

    /**
     * Short description of method delete
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $pcId
     * @return boolean
     */
    public function delete($pcId)
    {
        if($this->db->where('pcId',$pcId)->delete('postcategory'))
            return true;
        else
            return false;
    }


} /* end of class postcategorys */

?>