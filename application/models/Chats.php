<?php

/**
 * Short description of class userMessage
 *
 * @access public
 * @author firstname and lastname of author, <author@example.org>
 */
class Chats extends CI_Model
{
    // --- ASSOCIATIONS ---


    // --- ATTRIBUTES ---

    /**
     * Short description of attribute chId
     *
     * @access public
     * @var Integer
     */
    public $chId = null;

    /**
     * Short description of attribute chSendBy
     *
     * @access public
     * @var Integer
     */
    public $chSendBy = null;


    public $chSendByName = null;

    /**
     * Short description of attribute chReceiveBy
     *
     * @access public
     * @var String
     */
    public $chReceiveBy = null;

    /**
     * Short description of attribute chText
     *
     * @access public
     * @var Text
     */
    public $chText = null;

    /**
     * seen of attribute chSeen
     *
     * @access public
     * @var Text
     */
    public $chSeen = null;

    // --- OPERATIONS ---

    /**
     * Short description of method select_join
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $start
     * @param  $limit
     * @return array
     */
    public function select($start=0, $limit=0)
    {
        
        $this->db->from('chats');
        if($limit!=0)
            $this->db->limit($limit,$start);
        if((posts('userId')))
        {
            $this->db->order_by('chId asc');
            $this->db->group_start();        
                $this->db->where('chReceiveBy',$_SESSION['usId']);
                $this->db->where('chSeen',NULL);
            $this->db->group_end();
            $this->db->or_group_start();        
                $this->db->where('chSendBy',$_SESSION['usId']);
                $this->db->where('chSeen',NULL);
            $this->db->group_end();
        }
        if((posts('userId')))// && posts('userId')!=$_SESSION['usId']
        {
            $this->db->group_start();
                $this->db->group_start();
                    $this->db->where('chSendBy',$_SESSION['usId']);
                    $this->db->where('chReceiveBy',posts('userId'));
                $this->db->group_end();
                $this->db->or_group_start();
                    $this->db->where('chSendBy',posts('userId'));
                    $this->db->where('chReceiveBy',$_SESSION['usId']);
                $this->db->group_end();
            $this->db->group_end();
            $this->db->where('chSeen','1');
            $this->db->where('chId <',posts('limit'));
            $this->db->limit(15);
            $this->db->order_by('chId desc');
            
        }
            
        // echo $this->db->get_compiled_select();
        if($chats=$this->db->get())
        {
            $chats=$chats->result();
            $ids=array();
            foreach ($chats as $chat) 
            {
                $ids[]=$chat->chId;
            }
            // if(sizeof($ids) > 0)
            //     $this->db->set('chSeen',1)->where_in('chId',$ids)->where('chSeen','0')->update('chats');
            // print_r($chats);
            return $chats;
        }
        else
            return false;
    }
    /**
     * Short description of method select
     *
     * @access public
     * @author Hassan Shojaei
     * @return int
     */
    public function select_nch()
    {
        $this->db->from('chats');
        $this->db->where('chSendBy',$_SESSION['usId']);
        // echo $this->db->count_all_results();
        if($chatsNch=$this->db->count_all_results())
            return $chatsNch;   
        else
            return false;
    }
    public function set_value($params)
    {
        $this->chSendBy=$_SESSION['usId'];
        $this->chSendByName=!empty($_SESSION['usName'])?$_SESSION['usName']:$_SESSION['usId'];
        $this->chReceiveBy=(isset($params['userId'])) && ($params['userId'])?$params['userId']:$this->chReceiveBy;
        $this->chText=(isset($params['text'])) && ($params['text'])?$params['text']:$this->chText;
    }
    /**
     * Short description of method insert
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $foId
     * @return boolean
     */
    public function insert()
    {
        if($this->db->insert('chats', $this))
            return $this->db->insert_id();
        else
            return false;
    }
} /* end of class userMessage */

?>