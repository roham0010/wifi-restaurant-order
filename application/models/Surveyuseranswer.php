<?php

/**
 * Short description of class surveyUserAnswer
 *
 * @access public
 * @author firstname and lastname of author, <author@example.org>
 */
class Surveyuseranswer extends CI_Model
{
    // --- ASSOCIATIONS ---


    // --- ATTRIBUTES ---

    /**
     * Short description of attribute suaId
     *
     * @access public
     * @var Integer
     */
    public $suaId = null;

    /**
     * Short description of attribute suaAnswerId
     *
     * @access public
     * @var Integer
     */
    public $suaAnswer = null;

    /**
     * Short description of attribute suaUserId
     *
     * @access public
     * @var Integer
     */
    public $suaUserId = null;

    // --- OPERATIONS ---
    public function insert($params)
    {
        $answer=$params['answer'];
        $surveyId=$params['surveyId'];
// print_r($answer);exit;
        $this->db->set('suaAnswer',$answer);
        $this->db->set('suaSurveyId',$surveyId);
        $this->db->set('suaUserId',$_SESSION['usId']);
        
        if($this->db->insert('surveyuseranswer'))
            return $this->db->insert_id();
        else
            return false;
    }

} /* end of class surveyUserAnswer */

?>