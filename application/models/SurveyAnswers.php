<?php

error_reporting(E_ALL);

/**
 * untitledModel - class.surveyAnswers.php
 *
 * $Id$
 *
 * This file is part of untitledModel.
 *
 * Automatically generated on 04.05.2016, 19:45:53 with ArgoUML PHP module 
 * (last revised $Date: 2010-01-12 20:14:42 +0100 (Tue, 12 Jan 2010) $)
 *
 * @author firstname and lastname of author, <author@example.org>
 */

if (0 > version_compare(PHP_VERSION, '5')) {
    die('This file was generated for PHP 5');
}

/* user defined includes */
// section 127-0-1-1--4c9faab0:154441ae362:-8000:0000000000000BF1-includes begin
// section 127-0-1-1--4c9faab0:154441ae362:-8000:0000000000000BF1-includes end

/* user defined constants */
// section 127-0-1-1--4c9faab0:154441ae362:-8000:0000000000000BF1-constants begin
// section 127-0-1-1--4c9faab0:154441ae362:-8000:0000000000000BF1-constants end

/**
 * Short description of class surveyAnswers
 *
 * @access public
 * @author firstname and lastname of author, <author@example.org>
 */
class surveyAnswers
{
    // --- ASSOCIATIONS ---


    // --- ATTRIBUTES ---

    /**
     * Short description of attribute saId
     *
     * @access public
     * @var Integer
     */
    public $saId = null;

    /**
     * Short description of attribute saSurveyId
     *
     * @access public
     * @var Integer
     */
    public $saSurveyId = null;

    /**
     * Short description of attribute saAnswer
     *
     * @access public
     * @var String
     */
    public $saAnswer = null;

    // --- OPERATIONS ---

    /**
     * Short description of method newOperation
     *
     * @access public
     * @author firstname and lastname of author, <author@example.org>
     * @return mixed
     */
    public function newOperation()
    {
        // section 127-0-1-1--39723a6a:1547af1e9f7:-8000:0000000000000BF4 begin
        // section 127-0-1-1--39723a6a:1547af1e9f7:-8000:0000000000000BF4 end
    }

} /* end of class surveyAnswers */

?>