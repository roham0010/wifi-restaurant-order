<?php

/**
 * Short description of class orderItems
 *
 * @access public
 * @author firstname and lastname of author, <author@example.org>
 */
class Userorderlog extends CI_Model
{
    // --- ASSOCIATIONS ---


    // --- ATTRIBUTES ---

    /**
     * Short description of attribute uolId
     *
     * @access public
     * @var Integer
     */
    public $uolId = null;

    /**
     * Short description of attribute uolUserOrderId
     *
     * @access public
     * @var Integer
     */
    public $uolUserOrderId = null;

    /**
     * Short description of attribute uolUserOrderId
     *
     * @access public
     * @var Integer
     */
    public $uolUserLogId = null;

    // --- OPERATIONS ---
    
    public function set_value($userOrderId,$userLogId)
    {
        $this->uolUserOrderId=(isset($userOrderId) && !empty($userOrderId))?$userOrderId:'';
        $this->uolUserLogId=(isset($userLogId) && !empty($userLogId))?$userLogId:'';
    }

    public function insert($userOrderId,$userLogId)
    {
        $this->set_value($userOrderId,$userLogId);
        $this->db->insert('userorderlog',$this);
    }
} /* end of class orderItems */

?>