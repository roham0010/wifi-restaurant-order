<?php
/**
 * Short description of class medias
 *
 * @access public
 * @author firstname and lastname of author, <author@example.org>
 */
class medias extends CI_Model
{
    // --- ASSOCIATIONS ---
    // generateAssociationEnd :     // generateAssociationEnd : 

    // --- ATTRIBUTES ---

    /**
     * Short description of attribute meId
     *
     * @access public
     * @var Integer
     */
    public $meId = null;

    /**
     * Short description of attribute meRate
     *
     * @access public
     * @var Integer
     */

    /**
     * Short description of attribute meType
     *
     * @access public
     * @var Integer
     */
    public $meType = null;
    /**
     * Short description of attribute mfDownloaded
     *
     * @access public
     * @var Integer
     */
    public $meDownloaded = null;

    /**
     * Short description of attribute mfSeen
     *
     * @access public
     * @var Integer
     */
    public $meSeen = null;

    /**
     * Short description of attribute mfRate
     *
     * @access public
     * @var Integer
     */
    public $meRate = null;

    // --- OPERATIONS ---
    /**
     * Short description of method selectone
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $meId
     * @return array
     */
    public function select_one_lang($meId)
    {
        global $mediaTypes,$langsShow;
        $this->db->from('mediacategory');
        $this->db->join('mediasubcategory','mcId=mscCategoryId');
        $this->db->join('medialangs','mscId=mlSubCategoryId');
        $this->db->join('medias','meId=mlMediaId');

        $this->db->order_by('meId asc');
        $this->db->group_by('meId');
        if((gets('mediatype')))
        {
            $this->db->where('meType',$mediaTypes[gets('mediatype')]);
        }
        if((gets('lang')))
        {
            $this->db->where('mlLang',$langsShow[gets('lang')]);
        }
        // echo $this->db->get_compiled_select();
        if($mediafile=$this->db->where('meId',$meId)->get())
            return $mediafile->result();
        else
            return false;
    }
    public function conditions()
    {
        global $mediaTypes;
        if((gets('search')))
        {
            $search=gets('search');
            $this->db->group_start();
            $this->db->or_like('mlSubject',$search);
            $this->db->or_like('mcName',$search);
            $this->db->or_like('mscName',$search);
            $this->db->group_end();
        }
        if((gets('lang')))
        {
            $lang=gets('lang');
            if($lang!='alllang')
                $this->db->where('mclang',$lang);
        }
        if((gets('mediatype')))
        {
            $mediatype=gets('mediatype');
            if($mediatype!='all')
                $this->db->where('mcType',$mediaTypes[$mediatype]);
        }

    }
    public function select($start, $limit)
    {
        $this->db->from('mediacategory');
        $this->db->join('mediasubcategory','mcId=mscCategoryId');
        $this->db->join('medialangs','mscId=mlSubCategoryId');
        $this->db->join('medias','meId=mlMediaId');
        $this->db->join('mediafiles','meId=mfMediaId','left');
        $this->db->order_by('meId DESC');
        $this->db->group_by('meId');
        
        $this->medias->conditions();
        // echo $this->db->get_compiled_select();
        if($medias=$this->db->limit($limit,$start)->get())
            return $medias->result();
        else
            return false;
    }

    public function select_join_num()
    {

        $this->db->select('meId');
        $this->db->distinct('meId');
        $this->db->from('mediacategory');
        $this->db->join('mediasubcategory','mcId=mscCategoryId');
        $this->db->join('medialangs','mscId=mlSubCategoryId');
        $this->db->join('medias','meId=mlMediaId');
        // $this->db->group_by('meId');
        $this->medias->conditions();
        // echo $this->db->get_compiled_select();
        if($mediasNum=$this->db->count_all_results())
        {
            return $mediasNum;   
        }
        else
            return false;
    }



    public function conditionsuser()
    {
        global $mediaTypes;
        if((gets('search')))
        {
            $search=gets('search');
            $this->db->group_start();
            $this->db->or_like('mlSubject',$search);
            $this->db->or_like('mcName',$search);
            $this->db->or_like('mscName',$search);
            $this->db->group_end();
        }
        if((gets('lang')))
        {
            $lang=(gets('lang'))?$_SESSION['usLang']:gets('lang');
            // if($lang!='alllang')
            $this->db->where('mcLang',$lang);
        }
        if((gets('category')) && gets('category')!='all')
        {
            $this->db->where('mcId',gets('category'));

        }
        if((gets('subcategory')) && gets('subcategory')!='all')
        {
            $this->db->where('mscId',gets('subcategory'));
        }

    }
    public function select_user($start, $limit,$lang,$type)
    {

        $this->db->select('*,SUBSTRING(mlText,1,150) as mlText,meId as memeId, (select count(mfId) from mediafiles join medias on meId=mfMediaId where mfMediaId=memeId) as fileCount');
        $this->db->from('mediacategory');
        $this->db->join('mediasubcategory','mcId=mscCategoryId');
        $this->db->join('medialangs','mscId=mlSubCategoryId');
        $this->db->join('medias','meId=mlMediaId');
        $this->db->join('mediafiles','meId=mfMediaId','left');
        $this->db->join('(select * from `mediauser` order by field(muUserId,'.$_SESSION['usId'].') desc) as muser ','meId=muser.muMediaId','left outer');
        $this->db->where('mcType',$type);        
        $this->db->where('mcLang',$lang);                
        // $this->db->group_start();
        // $this->db->where("(case when muUserId is NULL or muUserId != '$_SESSION[usId]' then '' else '$_SESSION[usId]' end)=",$_SESSION['usId'] , false);
        // $this->db->where('muUserId',$_SESSION['usId']);
        // $this->db->or_where('muUserId !=',$_SESSION['usId']);
        // $this->db->or_where('muUserId IS NULL');
        // $this->db->group_end();

        $this->db->order_by('meId desc');
        $this->db->group_by('meId');
        $this->conditionsuser();
        // echo $this->db->get_compiled_select();
        if($medias=$this->db->limit($limit,$start)->get())
        {
            $medias=$medias->result();
            // $ids=array();
            // foreach ($medias as $media) 
            // {
            //     $ids[]=$media->meId;
            // }
            // if(sizeof($ids)>0)
            // {
            //     $this->db->set('meSeen','IFNULL(meSeen, 1) + 1',false);
            //     $this->db->where_in('meId',$ids)->update('medias');
            // }
            return $medias;
        }
        else
            return false;
    }

    public function select_join_num_user($lang,$type)
    {

        $this->db->select('meId');
        $this->db->distinct('meId');
        $this->db->from('mediacategory');
        $this->db->join('mediasubcategory','mcId=mscCategoryId');
        $this->db->join('medialangs','mscId=mlSubCategoryId');
        $this->db->join('medias','meId=mlMediaId');
        $this->db->join('mediafiles','meId=mfMediaId','left');
        $this->db->join('mediauser','meId=muMediaId','left');
        $this->db->where('mcType',$type);   
        $this->db->where('mcLang',$lang);     
        $this->db->group_start();
        $this->db->where('muUserId',$_SESSION['usId']);
        $this->db->or_where('muUserId IS NULL');
        $this->db->group_end();

        // $this->db->order_by('meId desc');
        // $this->db->group_by('meId');
        $this->db->where('mcType',$type);
        $this->conditionsuser();
        // echo $this->db->get_compiled_select();
        if($mediasNum=$this->db->count_all_results())
        {
            return $mediasNum;   
        }
        else
            return false;
    }





    public function select_one($meId)
    {
         if($medias=$this->db->where('meId',$meId)->get('medias',1,0))
            return $medias->result();
        else
            return false;
    }

    public function set_value($params)
    {
        $this->meType=(isset($params['type']) && (isset($params['type'])))?$params['type']:$this->meType;
        // $this->mfSeen=(isset($array['seen']) && (isset($array['seen'])))?$array['seen']:$this->mfSeen;
        // $this->mfRate=(isset($array['rate']) && (isset($array['rate'])))?$array['rate']:$this->mfRate;
        // $this->mfDownloaded=(isset($array['downloaded']) && (isset($array['downloaded'])))?$array['downloaded']:$this->mfDownloaded;
        // $this->meDoration=(isset($params['duration']) && (isset($params['duration'])))?$params['duration']:$this->meDoration;
        // $this->mePass=(isset($params['pass']) && (isset($params['pass'])))?$params['pass']:$this->mePass;
        // $this->meLang=(isset($params['lang']) && (isset($params['lang'])))?$params['lang']:$this->meLang;
        // $this->meBirthday=(isset($params['birthday']) && (isset($params['birthday'])))?$params['birthday']:$this->meBirthday;
        // $this->meHash=(isset($params['hash']) && (isset($params['hash'])))?$params['hash']:$this->meHash;
    }

    public function insert()
    {
        $this->meSeen=1;
        $this->meRate=1;
        $this->meDownloaded=1;
        
        if($this->db->insert('medias', $this))
            return $this->db->insert_id();
        else
            return false;
    }

    public function like($meId)
    {
        // echo $this->db->get_compiled_select();
        if($this->db->where('muMediaId',$meId)->where('muUserId',$_SESSION['usId'])->count_all_results('mediauser')==0)
        {
            $this->db->set('meRate','IFNULL(meRate, 1) + 1',false);
            // $this->db->set('meSeen','IFNULL(meSeen, 1) + 1',false);
            $this->db->where('meId',$meId);
            if($this->db->update('medias'))
            {
                if($this->db->set('muUserId',$_SESSION['usId'])->set('muMediaId',$meId)->insert('mediauser'))
                    return $this->db->insert_id();
            }
            return $this->db->insert_id();
        }
        else
            return false;
    }
    public function seen($meId)
    {   
        $this->db->set('meSeen','IFNULL(meSeen, 1) + 1',false);
        $this->db->where('meId',$meId);
        if($this->db->update('medias'))
        {
            return true;
        }
        else
            return false;
    }
    public function download($meId)
    {   
        $this->db->set('meDownloaded','IFNULL(meDownloaded, 1) + 1',false);
        $this->db->where('meId',$meId);
        if($this->db->update('medias'))
        {
            return true;
        }
        else
            return false;
    }
    public function delete($meId)
    {
        $this->load->model('mediafiles');
        $media=$this->medias->select_one($meId);
        $type=$media[0]->meType;
        if( $this->mediafiles->delete_medias($meId,$type) 
            && $this->db->where('mlMediaId',$meId)->delete('medialangs')
            && $this->db->where('meId',$meId)->delete('medias'))
            return true;
        else
            return false;
    }
} /* end of class medias */

?>