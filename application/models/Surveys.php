<?php

error_reporting(E_ALL);

/**
 * untitledModel - class.surveys.php
 *
 * $Id$
 *
 * This file is part of untitledModel.
 *
 * Automatically generated on 04.05.2016, 19:45:53 with ArgoUML PHP module 
 * (last revised $Date: 2010-01-12 20:14:42 +0100 (Tue, 12 Jan 2010) $)
 *
 * @author firstname and lastname of author, <author@example.org>
 */
/* suer defined includes */
// section 127-0-1-1--4c9faab0:154441ae362:-8000:0000000000000BEA-includes begin
// section 127-0-1-1--4c9faab0:154441ae362:-8000:0000000000000BEA-includes end

/* user defined constants */
// section 127-0-1-1--4c9faab0:154441ae362:-8000:0000000000000BEA-constants begin
// section 127-0-1-1--4c9faab0:154441ae362:-8000:0000000000000BEA-constants end

/**
 * Short description of class surveys
 *
 * @access public
 * @author firstname and lastname of author, <author@example.org>
 */
class surveys extends CI_Model
{
    // --- ASSOCIATIONS ---

    public function __construct()
    {
        parent::__construct();
    }


    // --- ATTRIBUTES ---

    /**
     * Short description of attribute suId
     *
     * @access public
     * @var Integer
     */
    public $suId = null;

    /**
     * Short description of attribute suQuestion
     *
     * @access public
     * @var String
     */
    public $suQuestion = null;

    /**
     * Short description of attribute suAnswer1
     *
     * @access public
     * @var string
     */
    public $suAnswer1 = '';

    /**
     * Short description of attribute suAnswer2
     *
     * @access public
     * @var String
     */
    public $suAnswer2 = null;

    /**
     * Short description of attribute suAnswer3
     *
     * @access public
     * @var String
     */
    public $suAnswer3 = null;

    /**
     * Short description of attribute suAnswer4
     *
     * @access public
     * @var String
     */
    public $suAnswer4 = null;

    /**
     * Short description of attribute suAnswer5
     *
     * @access public
     * @var String
     */
    public $suAnswer5 = null;

    // --- OPERATIONS ---
    /**
     * Short description of method select_join
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $start
     * @param  $limit
     * @return array
     */
    public function select_user($start, $limit)
    {
        $this->db->from('surveys');
        $this->db->join('surveyuseranswer','suaSurveyId=suId','left');
        // $this->db->where('suaUserId NOT IN','(select suaUserId from surveyuseranswer where suaSurveyId=suId)',false);
        $this->db->where('suaUserId',$_SESSION['usId']);
        $this->db->or_where('suaUserId IS NULL');
        $this->db->order_by('suId desc');
        $this->db->limit($limit,$start);
        // echo $this->db->get_compiled_select();
        if($surveys=$this->db->get())
            return $surveys->result();
        else
            return false;
    }

    /**
     * Short description of method select
     *
     * @access public
     * @author Hassan Shojaei
     * @return int
     */
    public function select_num()
    {
        $this->db->from('surveys');
        $this->db->join('surveyuseranswer','suaSurveyId=suId','left');
        // $this->db->where('suaUserId NOT IN','(select suaUserId from surveyuseranswer where suaSurveyId=suId)',false);
        $this->db->where('suaUserId',$_SESSION['usId']);
        $this->db->or_where('suaUserId IS NULL');
        // echo $this->db->count_all_results();
        if($surveysNum=$this->db->count_all_results())
            return $surveysNum;   
        else
            return false;
    }
    public function select_one($suId)
    {
         if($food=$this->db->where('suId',$suId)->get('surveys',1,0))
            return $food->result();
        else
            return false;
    }
    public function set_value($params)
    {
        $this->suQuestion=(isset($params['question']) && (isset($params['question'])))?$params['question']:$this->suQuestion;
        $this->suAnswer1=(isset($params['answer1']) && (isset($params['answer1'])))?$params['answer1']:$this->suAnswer1;
        $this->suAnswer2=(isset($params['answer2']) && (isset($params['answer2'])))?$params['answer2']:$this->suAnswer2;
        $this->suAnswer3=(isset($params['answer3']) && (isset($params['answer3'])))?$params['answer3']:$this->suAnswer3;
        $this->suAnswer4=(isset($params['answer4']) && (isset($params['answer4'])))?$params['answer4']:$this->suAnswer4;
        $this->suAnswer5=(isset($params['answer5']) && (isset($params['answer5'])))?$params['answer5']:$this->suAnswer5;
    }
    public function insert()
    {
        if($this->db->insert('surveys', $this))
            return $this->db->insert_id();
        else
            return false;
    }

    public function answer($params)
    {
        $answer=$params['answer'];
        $suId=$params['surveyId'];
        $this->db->set('suResult'.$answer,'suResult'.$answer.'+1',false);
        $this->db->where('suId',$suId);
        if($this->db->update('surveys'))
            return true;
        else
            return false;
    }
} /* end of class surveys */

?>