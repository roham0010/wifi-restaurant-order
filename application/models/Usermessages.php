<?php

/**
 * Short description of class userMessage
 *
 * @access public
 * @author firstname and lastname of author, <author@example.org>
 */
class Usermessages extends CI_Model
{
    // --- ASSOCIATIONS ---


    // --- ATTRIBUTES ---

    /**
     * Short description of attribute umId
     *
     * @access public
     * @var Integer
     */
    public $umId = null;

    /**
     * Short description of attribute umUserId
     *
     * @access public
     * @var Integer
     */
    public $umUserId = null;

    /**
     * Short description of attribute umSubject
     *
     * @access public
     * @var String
     */
    public $umSubject = null;

    /**
     * Short description of attribute umText
     *
     * @access public
     * @var Text
     */
    public $umText = null;

    // --- OPERATIONS ---

    /**
     * Short description of method select_join
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $start
     * @param  $limit
     * @return array
     */
    public function select($start, $limit)
    {
        $this->db->from('usermessages');
        $this->db->where('umUserId',$_SESSION['usId']);
        $this->db->order_by('umId desc');
        $this->db->limit($limit,$start);
        // echo $this->db->get_compiled_select();
        if($usermessages=$this->db->get())
        {
            $usermessages=$usermessages->result();
            $ids=array();
            foreach ($usermessages as $usermessage) 
            {
                $ids[]=$usermessage->umId;
            }

            if(sizeof($ids)>0)
                $this->db->set('umSeen',1)->where_in('umId',$ids)->where('umSeen','0')->update('usermessages');
            return $usermessages;
        }
        else
            return false;
    }
    /**
     * Short description of method select
     *
     * @access public
     * @author Hassan Shojaei
     * @return int
     */
    public function select_num()
    {
        $this->db->from('usermessages');
        $this->db->where('umUserId',$_SESSION['usId']);
        // echo $this->db->count_all_results();
        if($usermessagesNum=$this->db->count_all_results())
            return $usermessagesNum;   
        else
            return false;
    }
    public function set_value($params)
    {
        $this->umId=(isset($params['id'])) && ($params['id'])?$params['id']:'';
        $this->umUserId=(isset($params['userId'])) && ($params['userId'])?$params['userId']:$this->umUserId;
        $this->umSubject=(isset($params['subject'])) && ($params['subject'])?$params['subject']:$this->umSubject;
        $this->umText=(isset($params['text'])) && ($params['text'])?$params['text']:$this->umText;
    }
    /**
     * Short description of method insert
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $foId
     * @return boolean
     */
    public function insert()
    {
        if($this->db->insert('Usermessages', $this))
            return $this->db->insert_id();
        else
            return false;
    }
} /* end of class userMessage */

?>