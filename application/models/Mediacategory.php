<?php
/**
 * untitledModel - class.mediacategorys.php
 *
 * $Id$
 *
 * This file is part of untitledModel.
 *
 * Automatically generated on 04.05.2016, 19:45:53 with ArgoUML PHP module 
 * (last revised $Date: 2010-01-12 20:14:42 +0100 (Tue, 12 Jan 2010) $)
 *
 * @author Hassan Shojaei
 */

/**
 * Short description of class mediacategorys
 *
 * @access public
 * @author Hassan Shojaei
 */
class Mediacategory extends CI_Model 
{
    // --- ASSOCIATIONS ---


    // --- ATTRIBUTES ---

    /**
     * Short description of attribute mcId
     *
     * @access public
     * @var Integer
     */
    public $mcId = null;

    /**
     * Short description of attribute mcName
     *
     * @access public
     * @var String
     */
    public $mcName = null;

    /**
     * Short description of attribute mcName
     *
     * @access public
     * @var String
     */
    public $mcLang = null;

    // --- OPERATIONS ---

    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Short description of method select
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $start
     * @param  $limit
     * @return array
     */
    public function select($start, $limit)
    {
        if($mediacategorys=$this->db->get('mediacategory',$limit,$start))
            return $mediacategorys->result();
        else
            return false;
    }

    /**
     * Short description of method select_all
     *
     * @access public
     * @author Hassan Shojaei
     * @return array
     */
    public function select_all($type='')
    {
        global $mediaTypes,$langsShow;
        if((gets('lang')) && gets('lang')!='all')
            $this->db->where('mcLang',$langsShow[gets('lang')]);

        if((gets('mediatype')) && gets('mediatype')!='all')
            $this->db->where('mctype',$mediaTypes[gets('mediatype')]);
        
        if(!empty($type))
            $this->db->where('mctype',$type);
        
        // echo $this->db->get_compiled_select('media');exit;
        if($mediacategorys=$this->db->order_by('mcId asc')->get('mediacategory'))
        {
            return $mediacategorys->result();
        }
        else
            return false;
    }
    /**
     * Short description of method select_all
     *
     * @access public
     * @author Hassan Shojaei
     * @return array
     */
    public function select_all_user($lang,$type)
    {
        global $mediaTypes,$langsShow;
        $this->db->where('mcLang',$lang);

        if(!empty($type))
            $this->db->where('mcType',$type);
        
        // echo $this->db->get_compiled_select('media');exit;
        if($mediacategorys=$this->db->order_by('mcId asc')->get('mediacategory'))
        {
            return $mediacategorys->result();
        }
        else
            return false;
    }

    /**
     * Short description of method selectone
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $mcId
     * @return array
     */
    public function select_one($mcId)
    {
         if($mediacategory=$this->db->where('mcId',$mcId)->get('mediacategory',1,0))
            return $mediacategory->result_array();
        else
            return false;
    }
    /**
     * Short description of method set_value
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $mcId
     * @return boolean
     */
    public function set_value($params)
    {
        $this->mcId=(isset($params['catid']) && (isset($params['catid'])))?$params['catid']:'';
        $this->mcName=(isset($params['name']) && (isset($params['name'])))?$params['name']:$this->mcName;
        $this->mcLang=(isset($params['lang']) && (isset($params['lang'])))?$params['lang']:$this->mcLang;
        $this->mcType=(isset($params['type']) && (isset($params['type'])))?$params['type']:$this->mcType;
    }

    /**
     * Short description of method update
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $mcId
     * @return boolean
     */
    public function update()
    {
        if($this->db->set('mcName',$this->mcName)->set('mcLang',$this->mcLang)->where('mcId',$this->mcId)->update('mediacategory'))
            return true;
        else
            return false;
    }

    /**
     * Short description of method update
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $mcId
     * @return boolean
     */
    public function active($mcId)
    {
        if($this->db->set('mcActive','NOT mcActive',false)->where('mcId',$mcId)->update('mediacategory'))
            return true;
        else
            return false;
    }

    /**
     * Short description of method insert
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $mcId
     * @return boolean
     */
    public function insert()
    {
        if($this->db->insert('mediacategory', $this))
            return $this->db->insert_id();
        else
            return false;
    }

    /**
     * Short description of method delete
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $mcId
     * @return boolean
     */
    public function delete($mcId)
    {
        if($this->db->where('mcId',$mcId)->delete('mediacategory'))
            return true;
        else
            return false;
    }


} /* end of class mediacategorys */

?>