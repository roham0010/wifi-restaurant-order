<?php
class Posts extends CI_Model
{

    // --- ATTRIBUTES ---

    /**
     * Short description of attribute mpoId
     *
     * @access public
     * @var Integer
     */
    public $poId = null;

    /**
     * Short description of attribute mpopostId
     *
     * @access public
     * @var Integer
     */
    public $poCategoryId = null;

    /**
     * Short description of attribute poSubject
     *
     * @access public
     * @var string
     */
    public $poSubject = '';

    /**
     * Short description of attribute poText
     *
     * @access public
     * @var String
     */
    public $poText = null;

    /**
     * Short description of attribute poSubject
     *
     * @access public
     * @var string
     */
    public $poDate = '';

    /**
     * Short description of attribute poSubject
     *
     * @access public
     * @var string
     */
    public $poSeen = '';

    /**
     * Short description of attribute poSubject
     *
     * @access public
     * @var string
     */
    public $poRate = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function conditions()
    {
        global $postTypes;
        if((gets('search')))
        {
            $search=gets('search');
            $this->db->group_start();
            $this->db->or_like('poSubject',$search);
            $this->db->or_like('pcName',$search);
            // $this->db->or_like('poName',$search);
            $this->db->group_end();
        }
        if((gets('lang')))
        {
            $lang=gets('lang');
            if($lang!='alllang')
                $this->db->where('pclang',$lang);
        }

        if((gets('category')))
        {
            $category=gets('category');
            if($category!='allcategory')
                $this->db->where('pcId',$category);
        }

    }
    public function select($start, $limit)
    {
        $this->db->from('postcategory');
        $this->db->join('posts','poCategoryId=pcId');
        $this->db->order_by('poId DESC');
        $this->db->group_by('poId');
        
        $this->posts->conditions();
        // echo $this->db->get_compiled_select();
        if($posts=$this->db->limit($limit,$start)->get())
            return $posts->result();
        else
            return false;
    }
    public function select_user($start, $limit)
    {
        global $langsShow;
        $this->db->select('*,SUBSTRING(poText,1,150) as poText,poId as mepoId');
        $this->db->from('postcategory');
        $this->db->join('posts','pcId=poCategoryId');
        $this->db->join('(select * from `postuser` order by field(puUserId,'.$_SESSION['usId'].') desc) as puser ','poId=puser.puPostId','left outer');
        $this->db->where('pcLang',$langsShow[$_SESSION['usLang']]);                

        $this->db->order_by('poId desc');
        $this->db->group_by('poId');
        $this->conditions();
        // echo $this->db->get_compiled_select();
        if($posts=$this->db->limit($limit,$start)->get())
        {
            $posts=$posts->result();
            return $posts;
        }
        else
            return false;
    }

    public function select_join_num()
    {

        $this->db->from('postcategory');
        $this->db->distinct('poId');
        $this->db->join('posts','poCategoryId=pcId');
        $this->db->order_by('poId DESC');        
        $this->posts->conditions();
        // echo $this->db->get_compiled_select();
        if($postsNum=$this->db->count_all_results())
        {
            return $postsNum;   
        }
        else
            return false;
    }
    public function select_one($poId)
    {
        $this->db->join('postcategory','poCategoryId=pcId');
        $this->db->where('poId',$poId);
        $this->db->join('(select * from `postuser` order by field(puUserId,'.$_SESSION['usId'].') desc) as puser ','poId=puser.puPostId','left outer');
        
        // echo $this->db->get_compiled_select();
        if($post=$this->db->get('posts',1,0))
        {
            $post=$post->result();
            $this->db->set('poSeen','poSeen + 1',false)->where('poId',$post[0]->poId)->update('posts');
            return $post;
        }
        else
            return false;
    }

    public function like($poId)
    {
        // echo $this->db->get_compiled_select();
        if($this->db->where('puPostId',$poId)->where('puUserId',$_SESSION['usId'])->count_all_results('postuser')==0)
        {
            $this->db->set('poRate','IFNULL(poRate, 1) + 1',false);
            $this->db->where('poId',$poId);
            if($this->db->update('posts'))
            {
                if($this->db->set('puUserId',$_SESSION['usId'])->set('puPostId',$poId)->insert('postuser'))
                    return $this->db->insert_id();
            }
            return $this->db->insert_id();
        }
        else
            return false;
    }
    public function set_value($params)
    {
        $this->poId=(isset($params['poId']) && (isset($params['poId'])))?$params['poId']:$this->poId;
        $this->poSubject=(isset($params['subject']) && (isset($params['subject'])))?$params['subject']:$this->poSubject;
        $this->poText=(isset($params['text']) && (isset($params['text'])))?$params['text']:$this->poText;
        $this->poCategoryId=(isset($params['categoryId']) && (isset($params['categoryId'])))?$params['categoryId']:$this->poCategoryId;
    }
    public function insert()
    {
        if($this->db->insert('posts', $this))
            return $this->db->insert_id();
        else
            return false;
    }
        /**
     * Short description of pothod update
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $pcId
     * @return boolean
     */
    public function update()
    {
        if($this->db->where('poId',gets('edit'))->update('posts',$this))
            return true;
        else
            return false;
    }
    function get_posts($number = 10, $start = 0)
    {
        // $this->db->select();
        $this->db->from('posts');
        $this->db->where('poActive',1);
        // $this->db->order_by('date_added','desc');
        $this->db->limit($number, $start);
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_post_count()
    {
        $this->db->select()->from('posts')->where('poActive',1);
        $query = $this->db->get();
        return $query->num_rows;
    }
    function get_post($post_id)
    {
        $this->db->select();
        $this->db->from('posts');
        $this->db->where(array('poActive'=>1,'post_id'=>$post_id));
        // $this->db->order_by('date_added','desc');
        $query = $this->db->get();
        return $query->first_row('array');
    }
    // function insert_post($data)
    // {
    //     $this->db->insert('posts',$data);
    //     return $this->db->insert_id();
    // }
    
    function update_post($post_id, $data)
    {
        $this->db->where('post_id',$post_id);
        $this->db->update('posts',$data);
    }
    
    public function delete($poId)
    {
        if($this->db->where('poId',$poId)->delete('posts'))
            return true;
        else
            return false;
    }
}
