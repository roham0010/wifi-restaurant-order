<?php
class User extends CI_Model {

    public $usId;
    public $usName;
    public $usMac;
    public $usPhone;
    public $usPass;
    public $usLang;
    private $usIsAdmin;
    public $usBirthday;
    public $usAnniversary;
    public $usHash;
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }
    public function check_mac($mac)
    {
        if($this->db->where('usMac',$mac)->count_all_results('tusers')>0)
            return true;
        else
            return false;
    }
    public function login_mac($mac)
    {
        if($user=$this->db->where('usMac',$mac)->get('tusers',1,0))
            return $user->result();
        else
            return false;
    }
    public function set_value($params)
    {
        $this->usName=(isset($params['name']) && (isset($params['name'])))?$params['name']:$this->usName;
        $this->usPhone=(isset($params['phone']) && (isset($params['phone'])))?$params['phone']:$this->usPhone;
        $this->usPass=(isset($params['pass']) && (isset($params['pass'])))?$params['pass']:$this->usPass;
        $this->usLang=(isset($params['lang']) && (isset($params['lang'])))?$params['lang']:$this->usLang;
        $this->usBirthday=(isset($params['birthday']) && (isset($params['birthday'])))?$params['birthday']:$this->usBirthday;
        $this->usHash=(isset($params['hash']) && (isset($params['hash'])))?$params['hash']:$this->usHash;
    }
    public function get_last_ten_entries()
    {
        $query = $this->db->get('tusers', 10);
        return $query->result();
    }

    public function insert()
    {
        $this->db->insert('tusers', $this);
    }
    public function new_mac($mac,$myLang)
    {
        $this->usMac = $mac;
        $this->usLang = $myLang;
        if($this->db->insert('tusers', $this))
            return true;
        else
            return false;
    }
    public function edit_lang($myLang)
    {
        $this->usLang = $myLang;
        if($this->db->set('usLang',$myLang)->where('usMac',$_SESSION['usMac'])->update('tusers'))
            return true;
        else
            return false;
    }
    public function first_come()
    {
        if(isset($_SESSION['usLang']) && !empty($_SESSION['usLang']))
        {
//            echo 'is langaaaaaaaaaaaaaaaaaaa';
            return;
            
        }
        
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $macAddr=false;
        $arp=`arp -a $ipAddress`;
        $lines=explode("\n", $arp);
        $cols=preg_split('/\s+/', trim($lines[0]));
        $macAddr=$cols[3];
        $_SESSION['usMac']=$macAddr;
        if('picklang'!==params(1))
        {
            if(!$this->user->check_mac($macAddr))
            {
                echo "1";
                $_SESSION['usMac']=$macAddr;
                redirect('picklang');

            }
            else
            {
                echo "2";
                $user=$this->user->login_mac($macAddr);
                $user=$user[0];
                $this->user=$user;
                $user =  (array) $user;
                print_r($user);
                $this->session->set_userdata($user);
                if(empty($user['usLang']))
                    redirect('picklang');                
            }
        }
    }
    public function update()
    {
            $this->usName    = $this->input->post('name'); // please read the below note
            $this->usPhone    = $this->input->post('phone'); // please read the below note
            $this->usPass    = $this->input->post('pass'); // please read the below note
            $this->usLang    = $this->input->post('lang'); // please read the below note
            $this->usEmail    = $this->input->post('email'); // please read the below note
            $this->usBirthday    = $this->input->post('birthday'); // please read the below note
            $this->usHash    = $this->input->post('hash'); // please read the below note
            $this->db->update('tusers', $this, array('id' => $this->input->post('id')));
    }

}