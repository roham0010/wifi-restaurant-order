<?php

/**
 * Short description of class orderItems
 *
 * @access public
 * @author firstname and lastname of author, <author@example.org>
 */
class Orderitems extends CI_Model
{
    // --- ASSOCIATIONS ---


    // --- ATTRIBUTES ---

    /**
     * Short description of attribute otId
     *
     * @access public
     * @var Integer
     */
    public $otId = null;

    /**
     * Short description of attribute otUserOrderId
     *
     * @access public
     * @var Integer
     */
    public $otUserOrderId = null;

    /**
     * Short description of attribute otFoodId
     *
     * @access public
     * @var Integer
     */
    public $otFoodId = null;

    /**
     * Short description of attribute otStuff
     *
     * @access public
     * @var Integer
     */
    public $otCount = null;

    // --- OPERATIONS ---
    /**
     * Short description of method select_join
     *
     * @access public
     * @author Hassan Shojaei
     * @param  $start
     * @param  $limit
     * @return array
     */
    public function select_items($uoId)
    {
        $this->db->from('orderitems');
        $this->db->join('foods','foId=otFoodId');
        // $this->db->join('orderitems','uoId=otUserOrderId');
        
        $this->db->where('otUserOrderId',$uoId);
        $this->db->where('foActive',1);
        // $this->conditions();
        // echo $this->db->limit($limit,$start)->get_compiled_select();
        if($orderitems=$this->db->get())
            return $orderitems->result();
        else
            return false;
    }

    public function set_value($array)
    {
        $this->otUserOrderId=(isset($array['userOrderId']) && (isset($array['userOrderId'])))?$array['userOrderId']:$this->otUserOrderId;
        $this->otFoodId=(isset($array['foodId']) && (isset($array['foodId'])))?$array['foodId']:$this->otFoodId;
        $this->otCount=(isset($array['count']) && (isset($array['count'])))?$array['count']:$this->otCount;
    }

    public function insert($userOrderId)
    {
        foreach ((array)@$_SESSION['basket'] as $key => $basket) 
        {
            $array['userOrderId']=$userOrderId;
            $array['foodId']=$key;
            $array['count']=$basket['count'];
            $this->set_value($array);
            $this->db->insert('orderitems',$this);
        }
    }
} /* end of class orderItems */

?>