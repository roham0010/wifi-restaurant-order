<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manager extends CI_Controller {
    public $pgConf;
    public function __construct() {
        parent::__construct();
        /*Pagination Config*/
        $this->pgConf['page_query_string'] = TRUE;
        $this->pgConf['query_string_segment'] = 'page';
        $this->pgConf['use_page_numbers'] = TRUE;
        $this->pgConf['per_page'] = (isset($_COOKIE['stuffAtPage']) && $_COOKIE['stuffAtPage']!='')?$_COOKIE['stuffAtPage']:10;
        $this->pgConf['page'] = 1;
        $this->pgConf['full_tag_open'] = '<div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                        <ul class="pagination">';
        $this->pgConf['full_tag_close'] = '</ul>
                                    </div>';
        $this->pgConf['num_tag_open'] = '<li class="paginate_button ">';
        $this->pgConf['num_tag_close'] = '</li>';
        $this->pgConf['cur_tag_open'] = '<li class="paginate_button active"><a href="#">';
        $this->pgConf['cur_tag_close'] = '</a></li>';
        $this->pgConf['prev_tag_open'] = '<li class="paginate_button previous" id="example1_previous">';
        $this->pgConf['prev_tag_close'] = '</li>';
        $this->pgConf['next_tag_open'] = '<li class="paginate_button next" id="example1_previous">';
        $this->pgConf['next_tag_close'] = '</li>';
        $this->pgConf['last_link'] = false;
        $this->pgConf['first_link'] = false;
        /*END Pagination Config-----*/

        $this->load->library(array('form_validation','parser','pagination'));
        $this->load->helper(array('form','language'));
        $this->lang->load('persian','persian');
        $this->config->set_item('language', 'persian');
        $this->load->database();

        $this->load->model('users');
        $this->users->first_come();
        if (!$this->session->userdata('usIsAdmin'))
        {
            redirect();
        }
        $this->load->library('parser');
        $this->form_validation->set_error_delimiters ('<divspan class="text-red"> ', ' </span>');


    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $data['msg_hello']=$this->lang->line('hello');
        $this->parser->parse('manager/header',$data);
        $this->parser->parse('manager/home',$data);
        $this->parser->parse('manager/footer',$data);
    }
    public function orders()
    {
        if(posts('action') && posts('action')=='selectorders' && $this->input->is_ajax_request())
        {
            $this->load->library('jdf');
            $limit=posts('limit');
            $this->load->model('userorders');
            $userorders=$this->userorders->select_join(0,15);
            foreach ($userorders as $userorder)
            {
                if($userorder->uoState==0)
                    $state="<i class='glyphicon glyphicon-log-in'></i>";
                elseif($userorder->uoState==1)
                    $state="<i class='glyphicon glyphicon-print'></i>";
                elseif($userorder->uoState==2)
                    $state="<i class='glyphicon glyphicon-fire'></i>";
                elseif($userorder->uoState==3)
                    $state="<i class='glyphicon glyphicon-cutlery'></i>";
                echo "
                <tr role='row'>
                  <td>$userorder->uoId</td>
                  <td  class='text-center'>$userorder->uoDesk</td>
                  <td  class='text-center'>$userorder->uoChair</td>
                  <td><button type='button' class='btn btn-default btn-xs' data-toggle='tooltip' data-placement='bottom' title='$userorder->usName'>$userorder->usId</button></td>
                  <td>".lang_date('m/d-H:i',strtotime($userorder->uoDate))."</td>
                  <td class='text-center'>$state</td>
                  <td class='text-center'>$userorder->uoPrice</td>
                  <td class='text-center'><a data-toggle='modal' data-target='#deletemodal'><i class='glyphicon glyphicon-print state' order-id='$userorder->uoId' val='1'>پرینت</i></a></td>
                  <td class='text-center'><a data-toggle='modal' data-target='#deletemodal'><i class='glyphicon glyphicon-fire state' order-id='$userorder->uoId' val='2'>سرو</i></a></td>
                  <td class='text-center'><a data-toggle='modal' data-target='#deletemodal'><i class='glyphicon glyphicon-cutlery state' order-id='$userorder->uoId' val='3'>تحویل</i></a></td>
                </tr>
                ";
                  // <td class='text-center'><a data-toggle='modal' data-target='#deletemodal'><i class='glyphicon glyphicon-ok' order-id='$userorder->uoId' val='4'>تحویل</i></a></td>
            }
        }
    }
    public function orderstate()
    {
        if(posts('action') && posts('action')=='orderstate' && $this->input->is_ajax_request())
        {
            $this->load->library('jdf');
            $orderId=posts('orderId');
            $val=posts('val');
            $this->load->model('userorders');
            if($val < 4 && $this->userorders->state($orderId,$val))
            {
                if($val==1)
                {
                    $userorder=$this->userorders->select_one($orderId);
                    $userorder=$userorder[0];
                    echo "
                    <table>
                        <tbody>
                            <tr><td>کد سفارش</td><td>$userorder->uoId</td></tr>
                        </tbody>
                    </table>
                    ";
                }
            }
            else
                echo '2';
        }
    }

    /*Foods things*/
    public function foodcategory()
    {
        global $managerError;
        $data=array('messageUpdate'=>@$_SESSION['messageUpdate'],'messageInsert'=>@$_SESSION['messageInsert'],'messageDelete'=>@$_SESSION['messageDelete'],);
        $this->load->model('foodcategory');
        if($this->form_validation->run()==false)
        {
            ;
        }
        else
        {
            if((posts('insert')))
            {
                $this->foodcategory->set_value(posts());
                if($res=$this->foodcategory->insert())
                {
                    // redirect(current_url());
                    flashdata('messageInsert', message('success','درج با موفقیت انجام شد'));
                    redirect(current_url());
                }
                else
                {
                    $data['messageInsert']=message('success',$managerError['unexpected']);
                }
            }
            elseif((posts('update')))
            {
                $this->foodcategory->set_value(posts());
                if($this->foodcategory->update())
                {
                    flashdata('message', message('success','ویرایش با موفقیت انجام شد'));
                    redirect(current_url());
                }
                else
                {
                    $data['messageDelete']=message('success',$managerError['unexpected']);
                }
            }
        }
        if(gets('delete'))
        {
            if($this->foodcategory->delete(gets('delete')))
            {
                flashdata('messageDelete', message('success','حذف با موفقیت انجام شد'));
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $data['messageDelete']=message('success',$managerError['unexpected']);
            }
        }

        if(gets('active'))
        {
            if($this->foodcategory->active(gets('active')))
            {
                flashdata('messageDelete', message('success','وظعیت با موفقیت تغییر یافت'));
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $data['messageDelete']=message('success',$managerError['unexpected']);
            }
        }

        if(gets('edit'))
        {
            $foodcategory=$this->foodcategory->select_one(gets('edit'));
            $foodcategory=$foodcategory[0];
            $data['fcName']=$foodcategory['fcName'];
            $data['fcLang']=$foodcategory['fcLang'];
            $data['fcId']=$foodcategory['fcId'];
        }

        $foodCategorys=$this->foodcategory->select_all();
        // $foodCategorys= $foodCategorys;
        $data['foodCategorys'] = $foodCategorys;
        $this->parser->parse('manager/header',$data);
        $this->load->view('deleteModal');
        $this->parser->parse('manager/foodcategory',$data);
        $this->parser->parse('manager/footer',$data);
    }
    public function postcategory()
    {
        global $managerError;
        $data=array('messageUpdate'=>@$_SESSION['messageUpdate'],'messageInsert'=>@$_SESSION['messageInsert'],'messageDelete'=>@$_SESSION['messageDelete'],);
        $this->load->model('postcategory');
        if($this->form_validation->run()==false)
        {
            ;
        }
        else
        {
            if((posts('insert')))
            {
                $this->postcategory->set_value(posts());
                if($res=$this->postcategory->insert())
                {
                    // redirect(current_url());
                    flashdata('messageInsert', message('success','درج با موفقیت انجام شد'));
                    redirect(current_url());
                }
                else
                {
                    $data['messageInsert']=message('success',$managerError['unexpected']);
                }
            }
            elseif((posts('update')))
            {
                $this->postcategory->set_value(posts());
                if($this->postcategory->update())
                {
                    flashdata('message', message('success','ویرایش با موفقیت انجام شد'));
                    redirect(current_url());
                }
                else
                {
                    $data['messageDelete']=message('success',$managerError['unexpected']);
                }
            }
        }
        if(gets('delete'))
        {
            if($this->postcategory->delete(gets('delete')))
            {
                flashdata('messageDelete', message('success','حذف با موفقیت انجام شد'));
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $data['messageDelete']=message('success',$managerError['unexpected']);
            }
        }

        if(gets('active'))
        {
            if($this->postcategory->active(gets('active')))
            {
                flashdata('messageDelete', message('success','وظعیت با موفقیت تغییر یافت'));
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $data['messageDelete']=message('success',$managerError['unexpected']);
            }
        }

        if(gets('edit'))
        {
            $postcategory=$this->postcategory->select_one(gets('edit'));
            $postcategory=$postcategory[0];
            $data['pcName']=$postcategory['pcName'];
            $data['pcLang']=$postcategory['pcLang'];
            $data['pcId']=$postcategory['pcId'];
        }

        $postcategorys=$this->postcategory->select_all();
        // $postcategorys= $postcategorys;
        $data['postcategorys'] = $postcategorys;
        $this->parser->parse('manager/header',$data);
        $this->load->view('deleteModal');
        $this->parser->parse('manager/postcategory',$data);
        $this->parser->parse('manager/footer',$data);
    }
    public function post()
    {
        global $managerError,$langsShow;
        $data=array('message'=>@$_SESSION['message'],);

        $lang=(gets('lang'))?gets('lang'):'all';
        $data[$lang]='active';
        $this->load->library('ckeditor');
        $this->load->library('ckfinder');
        $this->ckeditor->basePath = asset_url().'ckeditor/';
        $this->ckfinder->SetupCKEditor($this->ckeditor,'assets');

        $this->load->model('posts');
        $this->load->model('postcategory');

        if((posts('insert')))
        {
        // print_r(posts());exit;
            if($this->form_validation->run('post')==false);
            else
            {
                /*upload new post*/
                if(!gets('edit'))
                {
                    // print_r(posts());
                    // exit;
                    $this->posts->set_value(posts());
                    if($res=$this->posts->insert())
                    {
                        flashdata('message', message('success','درج با موفقیت انجام شد'));
                        redirect($_SERVER['HTTP_REFERER']);
                    }
                    else
                    {
                        $data['messageInsert']=message('success',$managerError['unexpected']);
                    }
                }
                /*edit post and  of post*/
                else
                {

                }
            }
        }
        if((gets('edit')))
        {
            $post=$this->posts->select_one(gets('edit'));
            $post=$post;

            $data['postjoin']=$post;
            if((posts('update')))
            {

                if($this->form_validation->run('post')==false);
                else
                {
                    // print_r(posts());
                    // exit;
                    $this->posts->set_value(posts(),gets('edit'));
                    if($res1=$this->posts->update())
                    {
                        // redirect(current_url());
                        flashdata('message', message('success','ویرایش با موفقیت انجام شد'));
                        redirect($_SERVER['HTTP_REFERER']);
                    }
                    else
                    {
                        $data['message']=message('success',$managerError['unexpected']);
                    }
                }
            }
            !empty($post[0]->pcId)?$this->db->where('pcLang',$post[0]->pcLang):'';
        }
        $postCategorys=$this->postcategory->select_all();
        $data['postCategorys'] = $postCategorys;
        // print_r($post);
        // exit;

        $this->parser->parse('manager/header',$data);
        $this->parser->parse('manager/post',$data);
        $this->parser->parse('manager/footer',$data);
    }
    public function posts()
    {
        global $managerError;
        $data=array('message'=>@$_SESSION['message']);
        $lang=(gets('lang'))?gets('lang'):'alllang';
        $data['persian']=$lang=='ایرانی'?'active':'';
        $data['English']=$lang=='English'?'active':'';
        $data['arabic']=$lang=='العربیه'?'active':'';
        $data['alllang']=$lang=='alllang'?'active':'';

        $this->load->model('posts');
        if(gets('delete'))
        {
            if($this->posts->delete(gets('delete')))
            {
                flashdata('message', message('success','حذف با موفقیت انجام شد'));
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $data['message']=message('success',$managerError['unexpected']);
            }
        }
        $start=(gets('page'))?gets('page'):1;
        $start=($start-1)*$this->pgConf['per_page'];
        $posts=$this->posts->select($start,$this->pgConf['per_page']);
        $data['posts']=$posts;
        // print_r($posts);
        // exit;
        $this->pgConf['base_url'] = manager_url('posts').'?lang='.gets('lang').'&mediatype='.gets('mediatype').'&search='.gets('search');
        $countAll=$this->posts->select_join_num();
        $this->pgConf['total_rows'] = $countAll;
        // echo $countAll;exit;

        $this->pagination->initialize($this->pgConf);

        $data['pagination']=$this->pagination->create_links();
        $data['start']=$start;
        $data['limit']=$this->pgConf['per_page']>$countAll?$countAll:$this->pgConf['per_page'];
        $data['countAll']=$countAll;
        $this->parser->parse('manager/header',$data);
        $this->parser->parse('deleteModal',$data);
        $this->parser->parse('manager/posts',$data);
        $this->parser->parse('manager/footer',$data);
    }
    public function foods()
    {
        global $managerError;
        $data=array('message'=>@$_SESSION['message']);
        $lang=(gets('lang'))?gets('lang'):'all';
        $data['persian']=$lang=='ایرانی'?'active':'';
        $data['English']=$lang=='English'?'active':'';
        $data['arabic']=$lang=='العربیه'?'active':'';
        $data['all']=$lang=='all'?'active':'';
        $this->load->model('foods');

        $start=(gets('page'))?gets('page'):1;
        $start=($start-1)*$this->pgConf['per_page'];
        $foods=$this->foods->select_join($start,$this->pgConf['per_page']);
        $data['foods']=$foods;

        if(gets('delete'))
        {
            if($this->foods->delete(gets('delete')))
            {
                flashdata('messageDelete', message('success','حذف با موفقیت انجام شد'));
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $data['messageDelete']=message('success',$managerError['unexpected']);
            }
        }
        if(gets('active'))
        {
            if($this->foods->active(gets('active')))
            {
                flashdata('messageDelete', message('success','وظعیت با موفقیت تغییر یافت'));
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $data['messageDelete']=message('success',$managerError['unexpected']);
            }
        }
        // echo $_SERVER['REQUEST_URI'];
        // exit;
        $this->pgConf['base_url'] = manager_url('foods').'?lang='.gets('lang').'&search='.gets('search');
        $countAll=$this->foods->select_join_num();
        $this->pgConf['total_rows'] = $countAll;


        $this->pagination->initialize($this->pgConf);

        $data['pagination']=$this->pagination->create_links();
        $data['start']=$start;
        $data['limit']=$this->pgConf['per_page']>$countAll?$countAll:$this->pgConf['per_page'];
        $data['countAll']=$countAll;
        $this->parser->parse('manager/header',$data);
        $this->load->view('deleteModal');
        $this->parser->parse('manager/foods',$data);
        $this->parser->parse('manager/footer',$data);
    }
    public function food()
    {
        global $managerError;
        $data=array('message'=>@$_SESSION['message']);
        $this->load->model('foods');
        $this->load->model('foodcategory');
        if((posts('insert')))
        {
            if($this->form_validation->run()==false)
            {
            }
            else
            {
                $this->foods->set_value(posts());
                    // print_r($this->foods);
                if($res=$this->foods->insert())
                {
                    // print_r($_FILES);
                    if(($_FILES['file']['name']))
                    {
                        $file=$_FILES['file'];
                        $suffix= end((explode(".", $file['name'])));
                        $fileName=$res.'.'.'jpg';
                        $config = array(
                                    'upload_path' => FCPATH.'upload/foodimages',
                                    'allowed_types' => 'jpg|gif|png',
                                    'max_size' => '2097152000000',
                                    'overwrite' => true,
                                    'file_name' => $fileName,
                                    // 'multi' => 'all'
                                );
                        $this->load->library('upload', $config);
                        if($this->upload->do_upload("file"))
                        {
                            flashdata('message', message('success','درج با موفقیت انجام شد'));
                            redirect(current_url());
                        }
                        else
                        {
                            if(!gets('edit'))
                                $this->foods->delete($res);
                            $data['message'] = $this->upload->display_errors();
                        }

                    }
                    else
                        $this->foods->delete($res);
                }
                else
                {
                    print_r($this->db->error());
                    $data['message']=message('success',$managerError['unexpected']);
                }
            }
        }
        elseif((posts('update')))
        {
            if($this->form_validation->run()==false)
            {

            }
            else
            {
                $this->foods->set_value(posts());
                    print_r($this->foods);
                if($res=$this->foods->update())
                {
                    if(!empty($_FILES['file']['name']))
                    {
                        $file=$_FILES['file'];
                        $suffix= end((explode(".", $file['name'])));
                        $fileName=gets('edit').'.'.'jpg';
                        $config = array(
                                    'upload_path' => FCPATH.'upload/foodimages',
                                    'allowed_types' => 'jpg|gif|png',
                                    'max_size' => '2097152000000',
                                    'overwrite' => true,
                                    'file_name' => $fileName,
                                    // 'multi' => 'all'
                                );
                        $this->load->library('upload', $config);
                        if($this->upload->do_upload("file"))
                        {
                            flashdata('message', message('success','ویرایش با موفقیت انجام شد'));
                            // redirect(current_url());
                        }
                        else
                        {
                            if(!gets('edit'))
                                $this->foods->delete($res);
                            $data['message'] = $this->upload->display_errors();
                            redirect($_SERVER['HTTP_REFERER']);
                        }

                    }
                    flashdata('message', message('success','ویرایش با موفقیت انجام شد'));
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    print_r($this->db->error());
                    $data['message']=message('success',$managerError['unexpected']);
                }
            }
        }

        if(gets('edit'))
        {
            $foods=$this->foods->select_one(gets('edit'));
            $foods=$foods[0];
            $data=array_merge($data,(array)$foods);
        }
        $foodCategorys=$this->foodcategory->select_all();
        $foodCategorys=$array = json_decode(json_encode($foodCategorys), true);;
        $data['foodCategorys'] = $foodCategorys;

        $this->parser->parse('manager/header',$data);
        $this->parser->parse('manager/food',$data);
        $this->parser->parse('manager/footer',$data);
	}
    /*END Foods*/
	public function customers()
	{
        global $managerError;
        $data=array('message'=>@$_SESSION['message']);
        $lang=(gets('lang'))?gets('lang'):'all';
        $data['persian']=$lang=='ایرانی'?'active':'';
        $data['English']=$lang=='English'?'active':'';
        $data['arabic']=$lang=='العربیه'?'active':'';
        $data['all']=$lang=='all'?'active':'';

        $this->load->model('users');
        if(gets('delete'))
        {
            if($this->users->delete(gets('delete')))
            {
                flashdata('message', message('success','حذف با موفقیت انجام شد'));
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $data['message']=message('success',$managerError['unexpected']);
            }
        }
        if(gets('importance'))
        {
            if($this->users->importance(gets('importance')))
            {
                flashdata('message', message('success','اهمیت با موفقیت تغییر یافت'));
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $data['message']=message('success',$managerError['unexpected']);
            }
        }
        if(posts('sendMessage'))
        {
            if($this->form_validation->run('sendMessage')==false)
            {
                ;
            }
            else
            {
                $this->load->model('usermessage');
                $this->usermessage->set_value(posts());
                if($this->usermessage->insert())
                {
                    flashdata('message', message('success','پیام با موفقیت ارسال شد'));
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $data['message']=message('success',$managerError['unexpected']);
                }
            }
        }
        $start=(gets('page'))?gets('page'):1;
        $start=($start-1)*$this->pgConf['per_page'];
        $users=$this->users->select($start,$this->pgConf['per_page']);
        $data['users']=$users;

        $this->pgConf['base_url'] = manager_url('customers').'?search='.gets('search');
        $countAll=$this->users->select_join_num();
        $this->pgConf['total_rows'] = $countAll;


        $this->pagination->initialize($this->pgConf);

        $data['pagination']=$this->pagination->create_links();
        $data['start']=$start;
        $data['limit']=$this->pgConf['per_page']>$countAll?$countAll:$this->pgConf['per_page'];
        $data['countAll']=$countAll;
        $this->parser->parse('manager/header',$data);
        $this->load->view('deleteModal');
        $this->parser->parse('manager/customers',$data);
        $this->parser->parse('manager/footer',$data);
	}
	public function customer()
	{
        $data=array();
        $this->load->model('users');
        if((gets('edit')))
        {
            $users=$this->users->select_one(gets('edit'));
            $users=$users[0];
            $data=(object)array_merge($data,(array)$users);
        }
        if((posts('update')))
        {
            if($this->form_validation->run('customer')==false)
            {
                ;
            }
            else
            {
                $this->users->set_value(posts());
                    print_r($this->users);
                if($res=$this->users->update())
                {
                    flashdata('message', message('success','ویرایش با موفقیت انجام شد'));
                    redirect(current_url());
                }
                else
                {
                    print_r($this->db->error());
                    $data['message']=message('success',$managerError['unexpected']);
                }
            }
        }
        $this->parser->parse('manager/header',$data);
        $this->parser->parse('manager/customer',$data);
        $this->parser->parse('manager/footer',$data);
	}

    public function surveys()
    {
        global $managerError;
        $data=array('message'=>@$_SESSION['message']);
        $this->load->model('surveys');

        $start=(gets('page'))?gets('page'):1;
        $start=($start-1)*$this->pgConf['per_page'];
        $surveys=$this->surveys->select_user($start,$this->pgConf['per_page']);
        $data['surveys']=$surveys;

        $this->pgConf['base_url'] = manager_url('surveys');
        $countAll=$this->surveys->select_num();
        $this->pgConf['total_rows'] = $countAll;


        $this->pagination->initialize($this->pgConf);

        $data['pagination']=$this->pagination->create_links();
        $data['start']=$start;
        $data['limit']=$this->pgConf['per_page']>$countAll?$countAll:$this->pgConf['per_page'];
        $data['countAll']=$countAll;

        $this->parser->parse('manager/header',$data);
        $this->parser->parse('manager/surveys',$data);
        $this->parser->parse('manager/footer',$data);
    }
    public function survey()
    {
        global $managerError;
        $data=array('message'=>@$_SESSION['message']);
        $this->load->model('surveys');
        if(posts('insert'))
        {
            if($this->form_validation->run()==false)
            {
                ;
            }
            else
            {
                $this->surveys->set_value(posts());
                if($this->surveys->insert())
                {
                    flashdata('message', message('success','نظر سنجی با موفقیت ثبت شد'));
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $data['message']=message('success',$managerError['unexpected']);
                }
            }
        }

        $this->parser->parse('manager/header',$data);
        $this->parser->parse('manager/survey',$data);
        $this->parser->parse('manager/footer',$data);
    }
    public function surveyresult()
    {
        global $managerError;
        $data=array('message'=>@$_SESSION['message']);
        $this->load->model('surveys');

        $surveys=$this->surveys->select_one(gets('survey'));
        $surveys=$surveys[0];
        $data=(object)array_merge($data,(array)$surveys);

        $this->parser->parse('manager/header',$data);
        $this->parser->parse('manager/surveyresult',$data);
        $this->parser->parse('manager/footer',$data);
    }

    public function offers()
    {
        global $managerError;
        $data=array('message'=>@$_SESSION['message']);
        $this->load->model('offers');
        if(posts('sendMessage'))
        {
            if($this->form_validation->run('sendMessage')==false)
            {
                ;
            }
            else
            {
                $this->load->model('usermessage');
                $this->usermessage->set_value(posts());
                if($this->usermessage->insert())
                {
                    flashdata('message', message('success','پیام با موفقیت ارسال شد'));
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $data['message']=message('success',$managerError['unexpected']);
                }
            }
        }
        $start=(gets('page'))?gets('page'):1;
        $start=($start-1)*$this->pgConf['per_page'];
        $offers=$this->offers->select($start,$this->pgConf['per_page']);
        $data['offers']=$offers;
        $this->pgConf['base_url'] = manager_url('offers');
        $countAll=$this->offers->select_num();
        $this->pgConf['total_rows'] = $countAll;
        $this->pagination->initialize($this->pgConf);
        $data['pagination']=$this->pagination->create_links();
        $data['start']=$start;
        $data['limit']=$this->pgConf['per_page']>$countAll?$countAll:$this->pgConf['per_page'];
        $data['countAll']=$countAll;

        $this->parser->parse('manager/header',$data);
        $this->parser->parse('manager/offers',$data);
        $this->parser->parse('manager/footer',$data);
    }


    public function mediacategory()
    {
        global $managerError;
        $data=array('messageUpdate'=>@$_SESSION['messageUpdate'],'messageInsert'=>@$_SESSION['messageInsert'],'messageDelete'=>@$_SESSION['messageDelete'],);

        $mediatype=(gets('mediatype'))?gets('mediatype'):'all';
        $data[$mediatype]='active';
        $this->load->model('mediacategory');
        if($this->form_validation->run()==false)
        {
            ;
        }
        else
        {
            if((posts('insert')))
            {
                $this->mediacategory->set_value(posts());
                if($res=$this->mediacategory->insert())
                {
                    // redirect(current_url());
                    flashdata('messageInsert', message('success','درج با موفقیت انجام شد'));
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $data['messageInsert']=message('success',$managerError['unexpected']);
                }
            }
            elseif((posts('update')))
            {
                $this->mediacategory->set_value(posts());
                if($this->mediacategory->update())
                {
                    flashdata('messageDelete', message('success','ویرایش با موفقیت انجام شد'));
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $data['messageDelete']=message('success',$managerError['unexpected']);
                }
            }
        }
        if(gets('delete'))
        {
            if($this->mediacategory->delete(gets('delete')))
            {
                flashdata('messageDelete', message('success','حذف با موفقیت انجام شد'));
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $data['messageDelete']=message('success',$managerError['unexpected']);
            }
        }

        if(gets('active'))
        {
            if($this->mediacategory->active(gets('active')))
            {
                flashdata('messageDelete', message('success','وظعیت با موفقیت تغییر یافت'));
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $data['messageDelete']=message('success',$managerError['unexpected']);
            }
        }

        if(gets('edit'))
        {
            $mediacategory=$this->mediacategory->select_one(gets('edit'));
            $mediacategory=$mediacategory[0];
            $data['mcName']=$mediacategory['mcName'];
            $data['mcLang']=$mediacategory['mcLang'];
            $data['mcId']=$mediacategory['mcId'];
            $data['mcType']=$mediacategory['mcType'];
        }

        $mediaCategorys=$this->mediacategory->select_all();
        $mediaCategorys=$mediaCategorys;
        $data['mediaCategorys'] = $mediaCategorys;

        $this->parser->parse('manager/header',$data);
        $this->load->view('deleteModal');
        $this->parser->parse('manager/mediacategory',$data);
        $this->parser->parse('manager/footer',$data);
    }
    public function mediasubcategory()
    {
        global $managerError;
        $data=array('messageUpdate'=>@$_SESSION['messageUpdate'],'messageInsert'=>@$_SESSION['messageInsert'],'messageDelete'=>@$_SESSION['messageDelete'],);
        $mediatype=(gets('mediatype'))?gets('mediatype'):'all';
        $data[$mediatype]='active';
        $this->load->model('mediasubcategory');
        $this->load->model('mediacategory');
        if($this->form_validation->run()==false)
        {
            ;
        }
        else
        {
            if((posts('insert')))
            {
                $this->mediasubcategory->set_value(posts());
                if($res=$this->mediasubcategory->insert())
                {
                    // redirect(current_url());
                    flashdata('messageInsert', message('success','درج با موفقیت انجام شد'));
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $data['messageInsert']=message('success',$managerError['unexpected']);
                }
            }
            elseif((posts('update')))
            {
                $this->mediasubcategory->set_value(posts());
                // print_r($this->mediasubcategory);
                // exit;
                if($this->mediasubcategory->update())
                {
                    flashdata('messageUpdate', message('success','ویرایش با موفقیت انجام شد'));
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $data['messageUpdate']=message('success',$managerError['unexpected']);
                }
            }
        }
        if(gets('delete'))
        {
            if($this->mediasubcategory->delete(gets('delete')))
            {
                flashdata('messageDelete', message('success','حذف با موفقیت انجام شد'));
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $data['messageDelete']=message('success',$managerError['unexpected']);
            }
        }

        if(gets('active'))
        {
            if($this->mediasubcategory->active(gets('active')))
            {
                flashdata('messageDelete', message('success','وظعیت با موفقیت تغییر یافت'));
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $data['messageDelete']=message('success',$managerError['unexpected']);
            }
        }

        if(gets('edit'))
        {
            $mediasubcategory=$this->mediasubcategory->select_one(gets('edit'));
            $mediasubcategory=$mediasubcategory[0];
            $data['mscName']=$mediasubcategory['mscName'];
            $data['mscCategoryId']=$mediasubcategory['mscCategoryId'];
            $data['mscId']=$mediasubcategory['mscId'];
        }

        $mediasubcategorys=$this->mediasubcategory->select_all();
        $mediasubcategorys=$mediasubcategorys;
        $data['mediasubcategorys'] = $mediasubcategorys;

        $mediaCategorys=$this->mediacategory->select_all();
        $mediaCategorys=$mediaCategorys;
        $data['mediaCategorys'] = $mediaCategorys;

        $this->parser->parse('manager/header',$data);
        $this->load->view('deleteModal');
        $this->parser->parse('manager/mediasubcategory',$data);
        $this->parser->parse('manager/footer',$data);
    }
    public function media()
    {
        global $managerError,$langsShow;
        $data=array('message'=>@$_SESSION['message'],);
        $mediatype=(gets('mediatype'))?gets('mediatype'):'all';
        $data[$mediatype]='active';
        $lang=(gets('lang'))?gets('lang'):'all';
        $data[$lang]='active';
        $this->load->library('ckeditor');
        $this->load->library('ckfinder');
        $this->ckeditor->basePath = asset_url().'ckeditor/';
        $this->ckfinder->SetupCKEditor($this->ckeditor,'assets');

        $this->load->model('medias');
        $this->load->model('mediacategory');
        $this->load->model('mediafiles');
        $this->load->model('medialangs');

        if(posts('action') && posts('action')=='selectSubCategory' && $this->input->is_ajax_request())
        {
            if($this->form_validation->run('selectSubCategory')==false)
            {
                ;
            }
            else
            {
                $this->load->model('mediasubcategory');
                $mediasubcategorys=$this->mediasubcategory->select_all();
                // print_r($mediasubcategorys);
                $options="<option value=''>زیر دسته را انتخاب کنید</options>";
                $flag=false;
                foreach ($mediasubcategorys as $mediasubcategory)
                {
                    $flag=true;
                    $options.="<option value='$mediasubcategory->mscId'>$mediasubcategory->mscName</options>";
                }
                $flag==false?$options="<option value=''>زیر دسته ندارد</options>":'';

                echo $options;
            }
                exit;
        }
        if((posts('insert')) || (posts('insert1')))
        {
        // print_r(posts());exit;
            if($this->form_validation->run('media')==false);
            else
            {
                /*upload new media*/
                if(!gets('edit'))
                {
                    // print_r(posts());
                    // exit;
                    $this->medias->set_value(posts());
                    if($res=$this->medias->insert())
                    {
                        $this->medialangs->set_value(posts(),$res);
                        if($res1=$this->medialangs->insert())
                        // redirect(current_url());
                        flashdata('message', message('success','درج با موفقیت انجام شد'));
                        if((posts('insert')))
                            redirect(manager_url("mediafiles?upload=$res"));
                        else
                            redirect($_SERVER['HTTP_REFERER']);
                    }
                    else
                    {
                        $data['messageInsert']=message('success',$managerError['unexpected']);
                    }
                }
                /*edit media and lang of media*/
                else
                {

                }
            }
        }
        if((gets('edit')))
        {
            $mediajoin=$this->medias->select_one_lang(gets('edit'));
            $mediajoin=$mediajoin;
            $data['mediajoin']=$mediajoin;

            $media=$this->medias->select_one(gets('edit'));
            $media=$media;

            $this->load->model('mediasubcategory');
            $mcId=(posts('categoryId'))?posts('categoryId'):@$mediajoin[0]->mcId;
            $mediasubcategorys=$this->mediasubcategory->select_one_edit($mcId);
            $data['mediaSubCategorys'] = $mediasubcategorys;

            $medialang=$this->medialangs->select_langs(gets('edit'));
            $medialang=$medialang;
            $data['medialang']=$medialang;
            if((posts('update')))
            {

                if($this->form_validation->run('mediaupdate')==false);
                else
                {
                    // print_r(posts());
                    // exit;
                    $this->medialangs->set_value(posts(),gets('edit'));
                    if($res1=$this->medialangs->update())
                    {
                        // redirect(current_url());
                        flashdata('message', message('success','ویرایش با موفقیت انجام شد'));
                        redirect($_SERVER['HTTP_REFERER']);
                    }
                    else
                    {
                        $data['message']=message('success',$managerError['unexpected']);
                    }
                }
            }
            $data['media']=$media;
            !empty($media[0]->meId)?$this->db->where('mcType',$media[0]->meType):'';
        }
        $mediaCategorys=$this->mediacategory->select_all();
        $data['mediaCategorys'] = $mediaCategorys;
        // print_r($data);

        $this->parser->parse('manager/header',$data);
        $this->parser->parse('manager/media',$data);
        $this->parser->parse('manager/footer',$data);
    }
    public function mediafiles()
    {
        global $managerError,$langsShow;
        $data=array('message'=>@$_SESSION['message'],);
        if((gets('upload')))
        {
            $this->load->model('medias');
            $this->load->model('medialangs');
            $this->load->model('mediafiles');
            $media=$this->medias->select_one(@gets('upload'));
            $data['media'] = $media;
            if(count($media)>0)
            {
                $media=$media[0];
                if(gets('delete'))
                {
                    if($this->mediafiles->delete(gets('delete'),$medias->meType))
                    {
                        flashdata('message', message('success','حذف با موفقیت انجام شد'));
                        redirect($_SERVER['HTTP_REFERER']);
                    }
                    else
                    {
                        $data['message']=message('success',$managerError['unexpected']);
                    }
                }

                $mediafiles=$this->mediafiles->select_files(gets('upload'));
                $data['mediafiles'] = $mediafiles;

                $medialangs=$this->medialangs->select_langs(@gets('upload'));
                $data['medialangs'] = $medialangs;


                if((posts('upload')))
                {
                    $file=$_FILES['file'];
                    $fileDetails['suffix']= end((explode(".", $file['name'])));
                    $fileDetails['size']=bcdiv(bcdiv($file['size'],1024,2),1024,2);
                    $fileDetails['name']=$fileName=time().'.'.$fileDetails['suffix'];
                    $fileDetails['realName']=$file['name'];
                    $this->mediafiles->set_value(posts(),$fileDetails);
                    if($mfId=$this->mediafiles->insert())
                    {
                        // $fileName=$mfId;
                        switch($media->meType)
                        {
                            case '1':
                                $config = array(
                                    'upload_path' => FCPATH.'upload/images',
                                    'allowed_types' => 'jpg|gif|png',
                                    'max_size' => '2097152000000',
                                    'file_name' => $fileName,
                                    // 'multi' => 'all'
                                );
                            break;
                            case '2':
                                $config = array(
                                    'upload_path' => FCPATH.'upload/videos',
                                    'allowed_types' => 'mpeg|mpg|mpe|qt|mov|avi|movie|mp4|mkv',
                                    'max_size' => '2097152000000',
                                    'file_name' => $fileName,
                                    // 'multi' => 'all'
                                );
                            break;
                            case '3':
                                $config = array(
                                    'upload_path' => FCPATH.'upload/games',
                                    'allowed_types' => 'apk',
                                    'max_size' => '2097152000000',
                                    'file_name' => $fileName,
                                    // 'multi' => 'all'
                                );
                            break;
                            case '4':
                                $config = array(
                                    'upload_path' => FCPATH.'upload/texts',
                                    'allowed_types' => 'pdf|txt|doc',
                                    'max_size' => '2097152000000',
                                    'file_name' => $fileName,
                                    // 'multi' => 'all'
                                );
                            break;
                            case '5':
                                $config = array(
                                    'upload_path' => FCPATH.'upload/musics',
                                    'allowed_types' => 'mp3|ogg|mpeg',
                                    'max_size' => '2097152000000',
                                    'file_name' => $fileName,
                                    // 'multi' => 'all'
                                );
                            break;
                            default:
                                $config = array(
                                    'upload_path' => FCPATH.'upload/',
                                    'allowed_types' => '',
                                    'max_size' => '2097152000000',
                                    'file_name' => $fileName,
                                    // 'multi' => 'all'
                                );
                            break;
                        }
                        $this->load->library('upload', $config);
                        if($this->upload->do_upload("file"))
                        {
                            flashdata('message', message('success','آپلود با موفقیت انجام شد.'));
                            redirect($_SERVER['HTTP_REFERER']);
                        }
                        else
                        {
                            $this->mediafiles->delete($mfId,$media->meType);
                            $data['message'] = $this->upload->display_errors();
                        }
                            // echo $this->upload->display_errors();
                    }
                    // //Perform upload.

                }
            }
            else
            {
                $this->medias->delete(@gets('upload'));
                $data['message']='مدیایی با این مشخصات موجود نیست.';
            }
        }
        else
            $data['message']='مدیایی انتخاب نشده است.';
        $this->parser->parse('manager/header',$data);
        $this->parser->parse('deleteModal',$data);
        $this->parser->parse('manager/mediafiles',$data);
        $this->parser->parse('manager/footer',$data);
    }
    public function medias()
    {
        global $managerError;
        $data=array('message'=>@$_SESSION['message']);
        $lang=(gets('lang'))?gets('lang'):'alllang';
        $data['persian']=$lang=='ایرانی'?'active':'';
        $data['English']=$lang=='English'?'active':'';
        $data['arabic']=$lang=='العربیه'?'active':'';
        $data['alllang']=$lang=='alllang'?'active':'';
        $mediatype=(gets('mediatype'))?gets('mediatype'):'all';
        $data[$mediatype]='active';

        $this->load->model('medias');
        if(gets('delete'))
        {
            if($this->medias->delete(gets('delete')))
            {
                flashdata('message', message('success','حذف با موفقیت انجام شد'));
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $data['message']=message('success',$managerError['unexpected']);
            }
        }
        $start=(gets('page'))?gets('page'):1;
        $start=($start-1)*$this->pgConf['per_page'];
        $medias=$this->medias->select($start,$this->pgConf['per_page']);
        $data['medias']=$medias;
        // print_r($medias);
        // exit;
        $this->pgConf['base_url'] = manager_url('medias').'?lang='.gets('lang').'&mediatype='.gets('mediatype').'&search='.gets('search');
        $countAll=$this->medias->select_join_num();
        $this->pgConf['total_rows'] = $countAll;
        // echo $countAll;exit;

        $this->pagination->initialize($this->pgConf);

        $data['pagination']=$this->pagination->create_links();
        $data['start']=$start;
        $data['limit']=$this->pgConf['per_page']>$countAll?$countAll:$this->pgConf['per_page'];
        $data['countAll']=$countAll;
        $this->parser->parse('manager/header',$data);
        $this->parser->parse('deleteModal',$data);
        $this->parser->parse('manager/medias',$data);
        $this->parser->parse('manager/footer',$data);
    }
    public function sendChat()
    {

        if($this->input->is_ajax_request())
        {
            if($this->form_validation->run()==false);
            else
            {
                $this->load->model('chats');
                $this->chats->set_value(posts());
                if($id=$this->chats->insert())
                    echo json_encode(['status' => 'ok', 'callback' => 'chat_added', 'result' => $id]);
                else
                    echo json_encode(['status' => 'false', 'callback' => 'chat_added']);
            }
                exit;
        }

    }
    public function getChats()
    {
        if(posts('action') && posts('action')=='getChats' && $this->input->is_ajax_request())
        {
            $this->load->model('chats');
            $this->load->model('users');
            $chats['chats']=$this->chats->select();
            $chats['onlineusers']=$this->users->select_online();
            echo json_encode($chats);
        }
    }
    public function chatShowMore()
    {
        if(posts('action') && posts('action')=='chatShowMore' && $this->input->is_ajax_request())
        {
            $this->load->model('chats');
            $chats['chats']=$this->chats->select();
            echo json_encode($chats);
        }
    }
    public function userreport()
    {
        $data['msg_hello']=$this->lang->line('hello');
        $this->parser->parse('manager/header',$data);
        $this->parser->parse('manager/userreport',$data);
        $this->parser->parse('manager/footer',$data);
    }


}
?>
