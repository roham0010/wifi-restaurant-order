<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    public $pgConf;
    public function __construct(){
        parent::__construct();
        // print_r($_SESSION);
        // exit;
        /*Pagination Config*/
        $this->pgConf['page_query_string'] = TRUE;
        $this->pgConf['query_string_segment'] = 'page';
        $this->pgConf['use_page_numbers'] = TRUE;
        $this->pgConf['per_page'] = (isset($_COOKIE['stuffAtPage']) && $_COOKIE['stuffAtPage']!='')?$_COOKIE['stuffAtPage']:10;
        $this->pgConf['page'] = 1;
        $this->pgConf['full_tag_open'] = '<ul class="uk-pagination">';
        $this->pgConf['full_tag_close'] = '</ul>';
        $this->pgConf['num_tag_open'] = '<li class=""><span>';
        $this->pgConf['num_tag_close'] = '</span></li>';
        $this->pgConf['cur_tag_open'] = '<li class="uk-active"><a href="#"><span>';
        $this->pgConf['cur_tag_close'] = '</span></a></li>';
        $this->pgConf['prev_tag_open'] = '<li class="uk-pagination-left" id="example1_previous">';
        $this->pgConf['prev_tag_close'] = '</li>';
        $this->pgConf['next_tag_open'] = '<li class="uk-pagination-right" id="example1_previous">';
        $this->pgConf['next_tag_close'] = '</li>';
        $this->pgConf['last_link'] = false;
        $this->pgConf['first_link'] = false;
        /*END Pagination Config-----*/

        $this->load->database();
        $this->load->library(array('form_validation','session','parser','pagination'));
        $this->load->helper(array('form','language','url'));

        $this->load->model('users');
        $this->users->first_come();
        $lang=(isset($_SESSION['usLang']) && !empty($_SESSION['usLang']))?$_SESSION['usLang']:'persian';

        $this->lang->load(@$lang,@$lang);
        $this->config->set_item('language', @$lang);

        $this->form_validation->set_error_delimiters ('<span class="uk-text-danger"> ', ' </span>');



    }
    public function basket()
    {
        global $defines;
        // unset($_SESSION['basket']);
        if((isset($_POSTS['foodId'])) && $this->form_validation->run('foodbasket')==false);
        else
        {
            if(posts('action') && posts('action')=='addfood' && $this->input->is_ajax_request())
            {
                $this->load->model('foods');
                $food=$this->foods->select_one(posts('foodId'));
                if(sizeof($food)>0)
                    foreach ($food as $food1)
                    {
                        if(isset($_SESSION['basket'][$food1->foId]))
                        {
                            $_SESSION['basket'][$food1->foId]['count']++;
                        }
                        else
                        {
                            $food1->foType==1?$discounted=$food1->foPrice-(($food1->foPrice*$food1->foDiscount)/100):$discounted=$food1->foPrice;
                            $_SESSION['basket'][$food1->foId]['count']=1;
                            $_SESSION['basket'][$food1->foId]['name']=$food1->foName;
                            $_SESSION['basket'][$food1->foId]['price']=$discounted;
                        }
                    }
            }
            elseif(posts('action') && posts('action')=='reducefood' && $this->input->is_ajax_request())
            {
                if(isset($_SESSION['basket'][posts('foodId')]))
                {
                    if($_SESSION['basket'][posts('foodId')]['count']>1)
                        $_SESSION['basket'][posts('foodId')]['count']--;
                    else
                        unset($_SESSION['basket'][posts('foodId')]);
                }
            }
            elseif(posts('action') && posts('action')=='deletefood' && $this->input->is_ajax_request())
            {
                if(isset($_SESSION['basket'][posts('foodId')]))
                {
                    unset($_SESSION['basket'][posts('foodId')]);
                }
            }
        }

        if((isset($_POSTS['orderId'])) && $this->form_validation->run('orderedbasket')==false)
        {
            print_r($this->form_validation->error_array());
                echo posts('orderId');
            ;
        }
        else
        {
            if(posts('action') && posts('action')=='addorder' && $this->input->is_ajax_request())
            {
                $this->load->model('orderitems');
                $items=$this->orderitems->select_items(posts('orderId'));
                if(sizeof($items)>0)
                    foreach ($items as $food1)
                    {
                        if(isset($_SESSION['basket'][$food1->foId]))
                        {
                            $_SESSION['basket'][$food1->foId]['count']+=$food1->otCount;
                        }
                        else
                        {
                            $food1->foType==1?$discounted=$food1->foPrice-(($food1->foPrice*$food1->foDiscount)/100):$discounted=$food1->foPrice;
                            $_SESSION['basket'][$food1->foId]['count']=$food1->otCount;
                            $_SESSION['basket'][$food1->foId]['name']=$food1->foName;
                            $_SESSION['basket'][$food1->foId]['price']=$discounted;
                        }
                    }
            }
        }
        if($this->input->is_ajax_request())
        {
            $foodOrdered='';
            $sumPrice=0;
            $flag=false;
            if($_SERVER['HTTP_REFERER']==base_url('mybasket'))
                $nameColor='text-danger';
            else
                $nameColor='link';

            foreach ((array)@$_SESSION['basket'] as $key => $basket)
            {
                $flag=true;
                $sumPrice+=$basket['price']*$basket['count'];
                $foodOrdered.="<div class=' uk-padding-small-bottom uk-margin-small-top' style='border-bottom: 1px solid rgba(51, 51, 51, 0.6);'>
                              <i class='uk-clearfix'>
                                <span class='uk-float-left uk-$nameColor'>$basket[name] </span>
                                <span class='uk-h3 uk-float-right' style=''>
                                  <i class='uk-link uk-icon-plus-circle uk-margin-small-right add-basket' food-id='$key'></i>
                                  <i  class='uk-link uk-icon-minus-circle reduce-basket' food-id='$key'></i>
                                </span>
                              </i>

                              <i class='uk-clearfix'>
                                <i class='uk-float-left uk-margin-right uk-margin-small-top uk-h4 uk-link uk-icon-trash delete-basket' food-id='$key'></i>
                                <span class='uk-float-left'>".$basket['price']." تومان</span>
                                <span class='uk-float-right'> ".lang('menu_order_list_count').": $basket[count]</span>
                              </i>
                            </div>
                            ";

            }
            $tax=$sumPrice*$defines['tax'];
            $allPrice=$sumPrice-$tax;

            $foodOrdered.="
                <div class='uk-margin-large-top'>
                  <i class='uk-clearfix'>
                    <i class='uk-clearfix'>
                      <span class='uk-float-left uk-text-success'>".lang('menu_order_list_sum')."</span>
                      <span class='uk-float-right'>".number_format($sumPrice)." تومان</span>
                    </i>
                    <i class='uk-clearfix'>
                      <span class='uk-float-left uk-text-success'>".lang('menu_order_list_tax')."</span>
                      <span class='uk-float-right'>".number_format($tax)." تومان</span>
                    </i>
                    <i class='uk-clearfix'>
                      <span class='uk-float-left uk-text-success'>".lang('menu_order_list_all')."</span>
                      <span class='uk-float-right'>".number_format($allPrice)." تومان</span>
                    </i>
                  </i>
                </div>
            ";
            $flag==false?$foodOrdered="سفارش نداده اید":'';

            echo $foodOrdered;
        }
    }
    public function mybasket()
    {
        global $managerError;
        $data=array('message'=>@$_SESSION['message']);

        if($this->form_validation->run('order')==false)
        {
            ;
        }
        else
        {
            if(isset($_SESSION['basket']) && sizeof($_SESSION['basket'])>0)
            {
                $this->users->order();
                if ($this->db->trans_status() === FALSE)
                {
                    $data['message']=messageuser('success',$managerError['unexpected']);
                    $this->db->trans_rollback();
                }
                else
                {

                    $this->db->trans_commit();
                    flashdata('message', messageuser('success','سفارش شما با موفقیت ثبت شد'));
                    unset($_SESSION['basket']);
                    redirect(current_url());
                    // unset session basket
                    // show what is in progress of ordered foods
                    // redirect(current_url());
                }
                //done!
            }
            else
                $data['message']=messageuser('danger','شما غذایی سفارش نداده اید.');
        }
        $this->load->library('jdf');
        $data['nowDate']=lang_date('H:i:s Y/m/d');
        $this->parser->parse('header',$data);
        $this->parser->parse('user/mybasket',$data);
        $this->parser->parse('footer',$data);
    }
    public function index()
    {
        $this->load->library('jdf');
        $data['nowDate']=lang_date('H:i:s Y/m/d');
        $this->parser->parse('header',$data);
        $this->parser->parse('home/home',$data);
        $this->parser->parse('footer',$data);
    }
    public function orderedlists()
    {
        $data=array('message'=>@$_SESSION['message']);
        $this->load->library('jdf');
        $this->load->model('userorders');
        $this->load->model('orderitems');

        $start=(gets('page'))?gets('page'):1;
        $start=($start-1)*$this->pgConf['per_page'];

        $userorders=$this->userorders->select_join($start,$this->pgConf['per_page'],$_SESSION['usId']);
        $orderitems='';
        foreach ($userorders as $userorder)
        {
            $items=$this->orderitems->select_items($userorder->uoId);
            foreach ($items as $item)
            {
                $orderitems[$userorder->uoId][]=$item;
            }
        }
        $data['userorders']=$userorders;
        $data['orderitems']=$orderitems;

        $this->pgConf['base_url'] = base_url('orderedlists');
        $countAll=$this->userorders->select_join_num($_SESSION['usId']);
        $this->pgConf['total_rows'] = $countAll;
        $this->pagination->initialize($this->pgConf);
        $data['pagination']=$this->pagination->create_links();
        $data['start']=$start;
        $data['limit']=$this->pgConf['per_page']>$countAll?$countAll:$this->pgConf['per_page'];
        $data['countAll']=$countAll;

        // echo $data['pagination'];
        // print_r($userorders);
        // print_r($orderitems);
        // exit;
        $data['nowDate']=lang_date('H:i:s Y/m/d');
        $this->parser->parse('header',$data);
        $this->parser->parse('user/orderedlists',$data);
        $this->parser->parse('footer',$data);
    }
    public function offer()
    {
        $data=array('message'=>@$_SESSION['message']);

        $this->load->model('offers');
        if($this->form_validation->run('offer')==false)
        {
        }
        else
        {
            $this->offers->set_value(posts());
            if($this->offers->insert())
            {
                flashdata('message', messageuser('success','نظر شما با موفقیت درج شد.'));
                redirect(current_url());
            }
            else
            {
                $data['message']=messageuser('danger',$managerError['unexpected']);
            }
        }
        $this->parser->parse('header',$data);
        $this->parser->parse('user/offer',$data);
        $this->parser->parse('footer',$data);
    }
    public function survey()
    {
        $data=array('message'=>@$_SESSION['message']);
        $this->load->model('surveys');

        $start=(gets('page'))?gets('page'):1;
        $start=($start-1)*1;

        if($this->form_validation->run('survey')==false)
        {
        }
        else
        {
            // $this->surveys->set_value(posts());
            $this->load->model('surveyuseranswer');
            if($this->surveys->answer(posts()) && $this->surveyuseranswer->insert(posts()))
            {
                flashdata('message', messageuser('success','نظر شما با موفقیت ثبت شد.'));
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $data['message']=messageuser('danger',$managerError['unexpected']);
            }
        }
        $this->pgConf['full_tag_open'] = '<ul class="uk-pagination uk-text-center">';
        $this->pgConf['full_tag_close'] = '</ul>';
        $this->pgConf['cur_tag_open'] = '<li class="uk-active"><span>';
        $this->pgConf['cur_tag_close'] = '</span></li>';
        $this->pgConf['num_links'] = 0;
        $this->pgConf['per_page'] = 1;
        $this->pgConf['base_url'] = base_url('survey');
        $countAll=$this->surveys->select_num();
        $this->pgConf['total_rows'] = $countAll;
        $this->pagination->initialize($this->pgConf);
        $data['pagination']=$this->pagination->create_links();
        // $data['start']=$start;
        // $data['limit']=$countAll;
        // $data['countAll']=$countAll;

        $surveys=$this->surveys->select_user($start,$this->pgConf['per_page']);
        $data['surveys']=$surveys;

        // echo $data['pagination'].$countAll;
        // print_r($surveys);
        // exit;
        $this->parser->parse('header',$data);
        $this->parser->parse('user/survey',$data);
        $this->parser->parse('footer',$data);
    }
    public function messages()
    {
        $data=array('message'=>@$_SESSION['message']);
        $this->load->library('jdf');
        $this->load->model('usermessages');

        $start=(gets('page'))?gets('page'):1;
        $start=($start-1)*$this->pgConf['per_page'];

        $usermessages=$this->usermessages->select($start,$this->pgConf['per_page']);
        $data['usermessages']=$usermessages;

        $this->pgConf['base_url'] = base_url('messages');
        $countAll=$this->usermessages->select_num();
        $this->pgConf['total_rows'] = $countAll;
        $this->pagination->initialize($this->pgConf);
        $data['pagination']=$this->pagination->create_links();
        $data['start']=$start;
        $data['limit']=$this->pgConf['per_page']>$countAll?$countAll:$this->pgConf['per_page'];
        $data['countAll']=$countAll;

        // echo $data['pagination'];
        // print_r($usermessages);
        // print_r($orderitems);
        // exit;
        $data['nowDate']=lang_date('H:i:s Y/m/d');
        $this->parser->parse('header',$data);
        $this->parser->parse('user/messages',$data);
        $this->parser->parse('footer',$data);
    }
    public function picturegallery()
    {
        global $managerError,$langsShow,$defines;

        $lang=(gets('lang'))?gets('lang'):$langsShow[$_SESSION['usLang']];
        $data['persian']=$lang=='ایرانی'?'uk-active':'';
        $data['english']=$lang=='English'?'uk-active':'';
        $data['arabic']=$lang=='العربیه'?'uk-active':'';
        // echo $lang;
        $this->load->model('medias');
        $this->load->model('mediacategory');
        $this->load->model('mediasubcategory');

        $start=(gets('page'))?gets('page'):1;
        $start=($start-1)*$this->pgConf['per_page'];

        $medias=$this->medias->select_user($start,$this->pgConf['per_page'],$lang,1);
        $data['medias']=$medias;

        $mediacategorys=$this->mediacategory->select_all_user($lang,1);
        $data['mediacategorys']=$mediacategorys;

        $mediasubcategorys=$this->mediasubcategory->select_all_user($lang,1);
        $data['mediasubcategorys']=$mediasubcategorys;
        $data['mn'.gets('category')]='uk-active';
        $this->pgConf['base_url'] = base_url('picturegallery').'?category='.gets('category').'&subcategory='.gets('subcategory').'&search='.gets('search');
        $countAll=$this->medias->select_join_num_user($lang,1);
        $this->pgConf['total_rows'] = $countAll;

        // print_r($mediasubcategorys);
        // print_r($countAll);
        // exit;

        $this->pagination->initialize($this->pgConf);

        $data['pagination']=$this->pagination->create_links();
        $data['start']=$start;
        $data['limit']=$this->pgConf['per_page']>$countAll?$countAll:$this->pgConf['per_page'];
        $data['countAll']=$countAll;

        $data['title']=lang('picturestitle');
        $data['description']=lang('picturesdescription');
        $data['image']='image.jpg';

        $this->parser->parse('header',$data);
        $this->parser->parse('user/galleryheader',$data);
        $this->parser->parse('user/picturegallery',$data);
        $this->parser->parse('user/galleryfooter',$data);
        $this->parser->parse('footer',$data);
    }
    public function videogallery()
    {
        global $managerError,$langsShow,$defines;

        $lang=(gets('lang'))?gets('lang'):$langsShow[$_SESSION['usLang']];
        $data['persian']=$lang=='ایرانی'?'uk-active':'';
        $data['english']=$lang=='English'?'uk-active':'';
        $data['arabic']=$lang=='العربیه'?'uk-active':'';
        // echo $lang;
        $this->load->model('medias');
        $this->load->model('mediacategory');
        $this->load->model('mediasubcategory');

        $start=(gets('page'))?gets('page'):1;
        $start=($start-1)*$this->pgConf['per_page'];

        $medias=$this->medias->select_user($start,$this->pgConf['per_page'],$lang,2);
        $data['medias']=$medias;

        $mediacategorys=$this->mediacategory->select_all_user($lang,2);
        $data['mediacategorys']=$mediacategorys;

        $mediasubcategorys=$this->mediasubcategory->select_all_user($lang,2);
        $data['mediasubcategorys']=$mediasubcategorys;
        $data['mn'.gets('category')]='uk-active';
        $this->pgConf['base_url'] = base_url('picturegallery').'?category='.gets('category').'&subcategory='.gets('subcategory').'&search='.gets('search');
        $countAll=$this->medias->select_join_num_user($lang,2);
        $this->pgConf['total_rows'] = $countAll;
        // $this->medias->like(35);
        // exit;
        // print_r($mediasubcategorys);
        // print_r($countAll);
        // exit;

        $this->pagination->initialize($this->pgConf);

        $data['pagination']=$this->pagination->create_links();
        $data['start']=$start;
        $data['limit']=$this->pgConf['per_page']>$countAll?$countAll:$this->pgConf['per_page'];
        $data['countAll']=$countAll;

        $data['title']=lang('videostitle');
        $data['description']=lang('videosdescription');
        $data['image']='image.jpg';

        $this->parser->parse('header',$data);
        $this->parser->parse('user/galleryheader',$data);
        $this->parser->parse('user/videogallery',$data);
        $this->parser->parse('user/galleryfooter',$data);
        $this->parser->parse('footer',$data);
    }
    public function musics()
    {
        global $managerError,$langsShow,$defines;

        $lang=(gets('lang'))?gets('lang'):$langsShow[$_SESSION['usLang']];
        $data['persian']=$lang=='ایرانی'?'uk-active':'';
        $data['english']=$lang=='English'?'uk-active':'';
        $data['arabic']=$lang=='العربیه'?'uk-active':'';
        // echo $lang;
        $this->load->model('medias');
        $this->load->model('mediacategory');
        $this->load->model('mediasubcategory');

        $start=(gets('page'))?gets('page'):1;
        $start=($start-1)*$this->pgConf['per_page'];

        $medias=$this->medias->select_user($start,$this->pgConf['per_page'],$lang,5);
        $data['medias']=$medias;

        $mediacategorys=$this->mediacategory->select_all_user($lang,5);
        $data['mediacategorys']=$mediacategorys;

        $mediasubcategorys=$this->mediasubcategory->select_all_user($lang,5);
        $data['mediasubcategorys']=$mediasubcategorys;
        $data['mn'.gets('category')]='uk-active';
        $this->pgConf['base_url'] = base_url('picturegallery').'?category='.gets('category').'&subcategory='.gets('subcategory').'&search='.gets('search');
        $countAll=$this->medias->select_join_num_user($lang,5);
        $this->pgConf['total_rows'] = $countAll;

        // print_r($mediasubcategorys);
        // print_r($countAll);
        // exit;

        $this->pagination->initialize($this->pgConf);

        $data['pagination']=$this->pagination->create_links();
        $data['start']=$start;
        $data['limit']=$this->pgConf['per_page']>$countAll?$countAll:$this->pgConf['per_page'];
        $data['countAll']=$countAll;

        $data['title']=lang('musicstitle');
        $data['description']=lang('musicsdescription');
        $data['image']='image.jpg';

        $this->parser->parse('header',$data);
        $this->parser->parse('user/galleryheader',$data);
        $this->parser->parse('user/musics',$data);
        $this->parser->parse('user/galleryfooter',$data);
        $this->parser->parse('footer',$data);
    }
    public function games()
    {
        global $managerError,$langsShow,$defines;

        $lang=(gets('lang'))?gets('lang'):$langsShow[$_SESSION['usLang']];
        $data['persian']=$lang=='ایرانی'?'uk-active':'';
        $data['english']=$lang=='English'?'uk-active':'';
        $data['arabic']=$lang=='العربیه'?'uk-active':'';
        // echo $lang;
        $this->load->model('medias');
        $this->load->model('mediacategory');
        $this->load->model('mediasubcategory');

        $start=(gets('page'))?gets('page'):1;
        $start=($start-1)*$this->pgConf['per_page'];

        $medias=$this->medias->select_user($start,$this->pgConf['per_page'],$lang,3);
        $data['medias']=$medias;

        $mediacategorys=$this->mediacategory->select_all_user($lang,3);
        $data['mediacategorys']=$mediacategorys;

        $mediasubcategorys=$this->mediasubcategory->select_all_user($lang,3);
        $data['mediasubcategorys']=$mediasubcategorys;
        $data['mn'.gets('category')]='uk-active';
        $this->pgConf['base_url'] = base_url('games').'?category='.gets('category').'&subcategory='.gets('subcategory').'&search='.gets('search');
        $countAll=$this->medias->select_join_num_user($lang,3);
        $this->pgConf['total_rows'] = $countAll;
        // print_r($medias);
        // print_r($mediasubcategorys);
        // print_r($countAll);
        // exit;

        $this->pagination->initialize($this->pgConf);

        $data['pagination']=$this->pagination->create_links();
        $data['start']=$start;
        $data['limit']=$this->pgConf['per_page']>$countAll?$countAll:$this->pgConf['per_page'];
        $data['countAll']=$countAll;

        $data['title']=lang('gamestitle');
        $data['description']=lang('gamesdescription');
        $data['image']='image.jpg';

        $this->parser->parse('header',$data);
        $this->parser->parse('user/galleryheader',$data);
        $this->parser->parse('user/games',$data);
        $this->parser->parse('user/galleryfooter',$data);
        $this->parser->parse('footer',$data);
    }
    public function texts()
    {
        global $managerError,$langsShow,$defines;

        $lang=(gets('lang'))?gets('lang'):$langsShow[$_SESSION['usLang']];
        $data['persian']=$lang=='ایرانی'?'uk-active':'';
        $data['english']=$lang=='English'?'uk-active':'';
        $data['arabic']=$lang=='العربیه'?'uk-active':'';
        // echo $lang;
        $this->load->model('medias');
        $this->load->model('mediacategory');
        $this->load->model('mediasubcategory');

        $start=(gets('page'))?gets('page'):1;
        $start=($start-1)*$this->pgConf['per_page'];

        $medias=$this->medias->select_user($start,$this->pgConf['per_page'],$lang,4);
        $data['medias']=$medias;

        $mediacategorys=$this->mediacategory->select_all_user($lang,4);
        $data['mediacategorys']=$mediacategorys;

        $mediasubcategorys=$this->mediasubcategory->select_all_user($lang,4);
        $data['mediasubcategorys']=$mediasubcategorys;
        $data['mn'.gets('category')]='uk-active';
        $this->pgConf['base_url'] = base_url('texts').'?category='.gets('category').'&subcategory='.gets('subcategory').'&search='.gets('search');
        $countAll=$this->medias->select_join_num_user($lang,4);
        $this->pgConf['total_rows'] = $countAll;
        // print_r($medias);
        // print_r($mediasubcategorys);
        // print_r($countAll);
        // exit;

        $this->pagination->initialize($this->pgConf);

        $data['pagination']=$this->pagination->create_links();
        $data['start']=$start;
        $data['limit']=$this->pgConf['per_page']>$countAll?$countAll:$this->pgConf['per_page'];
        $data['countAll']=$countAll;

        $data['title']=lang('textstitle');
        $data['description']=lang('textsdescription');
        $data['image']='image.jpg';

        $this->parser->parse('header',$data);
        $this->parser->parse('user/galleryheader',$data);
        $this->parser->parse('user/texts',$data);
        $this->parser->parse('user/galleryfooter',$data);
        $this->parser->parse('footer',$data);
    }
    public function select_media(&$data)
    {
        global $managerError,$langsShow,$defines;

        $data=array('messageDownload'=>@$_SESSION['messageDownload']);
        $mlId=end((explode('-', params(2))));

        $this->load->model('medialangs');
        $this->load->model('mediafiles');
        // $this->load->model('mediasubcategory');

        $medialang=$this->medialangs->select_lang($mlId);
        // $medialang=$medialang[0];
        $data['medialang']=$medialang;
        $meId=$medialang[0]->mlMediaId;

        $mediafiles=$this->mediafiles->select_files(@$meId);
        $data['mediafiles']=$mediafiles;

    }
    public function text()
    {
        $data='';
        $this->select_media($data);
        $this->parser->parse('header',$data);
        // $this->parser->parse('user/galleryheader',$data);
        $this->parser->parse('user/text',$data);
        $this->parser->parse('user/galleryfooter',$data);
        $this->parser->parse('footer',$data);
    }
    public function game()
    {
        $data='';
        $this->select_media($data);
        $this->parser->parse('header',$data);
        // $this->parser->parse('user/galleryheader',$data);
        $this->parser->parse('user/game',$data);
        $this->parser->parse('user/galleryfooter',$data);
        $this->parser->parse('footer',$data);
    }
    public function video()
    {
        $data='';
        $this->select_media($data);
        $this->parser->parse('header',$data);
        // $this->parser->parse('user/galleryheader',$data);
        $this->parser->parse('user/video',$data);
        $this->parser->parse('user/galleryfooter',$data);
        $this->parser->parse('footer',$data);
    }
    public function picture()
    {
        $data='';
        $this->select_media($data);
        $this->parser->parse('header',$data);
        // $this->parser->parse('user/galleryheader',$data);
        $this->parser->parse('user/picture',$data);
        $this->parser->parse('user/galleryfooter',$data);
        $this->parser->parse('footer',$data);
    }
    public function music()
    {
        $data='';
        $this->select_media($data);
        $this->parser->parse('header',$data);
        // $this->parser->parse('user/galleryheader',$data);
        $this->parser->parse('user/music',$data);
        $this->parser->parse('user/galleryfooter',$data);
        $this->parser->parse('footer',$data);
    }
    public function download()
    {

        if((gets('fileid')))
        {
            $this->load->model('mediafiles');
            $mfId=gets('fileid');
            $mediafile=$this->mediafiles->select_file($mfId);
            $mediafile=$mediafile[0];
            switch ($mediafile->meType)
            {

            // $mediaTypes=array('picture'=>'1','video'=>'2','game'=>'3','text'=>'4','music'=>'5','all'=>'');
                case '1':
                    $filename=FCPATH."upload/images/".$mediafile->mfName;
                    $filename1=base_url()."upload/images/".$mediafile->mfName;
                    break;
                case '2':
                    $filename=FCPATH."upload/videos/".$mediafile->mfName;
                    $filename1=base_url()."upload/videos/".$mediafile->mfName;
                    break;
                case '3':
                    $filename=FCPATH."upload/games/".$mediafile->mfName;
                    $filename1=base_url()."upload/games/".$mediafile->mfName;
                    break;
                case '4':
                    $filename=FCPATH."upload/texts/".$mediafile->mfName;
                    $filename1=base_url()."upload/texts/".$mediafile->mfName;
                    break;
                case '5':
                    $filename=FCPATH."upload/musics/".$mediafile->mfName;
                    $filename1=base_url()."upload/musics/".$mediafile->mfName;
                    break;

            }
            // exit;
            if (file_exists($filename))
            {
            echo $filename;
                $this->load->model('medias');
                $this->medias->download($mediafile->meId);
                $this->load->helper('download');

                force_download($mediafile->mfRealName,file_get_contents($filename1));
            }
            else
                flashdata('messageDownload', messageuser('success','خطایی در دانلود فایل رخ داده است.'));
        }
        else
            flashdata('messageDownload', messageuser('success','خطایی در دانلود فایل رخ داده است.'));
        // exit;
        redirect($_SERVER['HTTP_REFERER']);
    }
    public function medialike()
    {
        global $defines;
        if($this->form_validation->run('medialike')==false);
        else
        {
            if(posts('action') && posts('action')=='favorite' && $this->input->is_ajax_request())
            {
                $this->load->model('medias');
                if($this->medias->like(posts('mediaId')))
                    echo '1';
                else
                    echo '2';
            }
        }
    }
    public function mediaseen()
    {
        global $defines;
        if($this->form_validation->run('medialike')==false);
        else
        {
            if(posts('action') && posts('action')=='seen' && $this->input->is_ajax_request())
            {
                $this->load->model('medias');
                if($this->medias->seen(posts('mediaId')))
                    echo '1';
                else
                    echo '2';
            }
        }
    }
    public function mediadownload()
    {
        global $defines;
        if($this->form_validation->run('medialike')==false);
        else
        {
            if(posts('action') && posts('action')=='download' && $this->input->is_ajax_request())
            {
                $this->load->model('medias');
                if($this->medias->download(posts('mediaId')))
                    echo '1';
                else
                    echo '2';
            }
        }
    }
    public function menu()
    {

        $foodtype=(gets('foodtype'))?gets('foodtype'):'ایرانی';
        $data['persian']=$foodtype=='ایرانی'?'uk-active':'';
        $data['english']=$foodtype=='English'?'uk-active':'';
        $data['arabic']=$foodtype=='العربیه'?'uk-active':'';
        $this->load->model('foods');

        // $foodCats=$this->foods->select_cats();
        // $data['foodCats']=$foodCats;

        $foods=$this->foods->select_all();
        $data['foods']=$foods;

        $foodDeserts=$this->foods->select_all_deserts();
        $data['foodDeserts']=$foodDeserts;

        $this->parser->parse('header',$data);
        $this->parser->parse('user/menu',$data);
        $this->parser->parse('footer',$data);
	}
    public function picklang()
    {
        if(null!==gets('mylang'))
        {
            global $langs;
            if(in_array(gets('mylang'),$langs))
            {
                $this->load->model('users');
                if(!$this->users->check_mac($_SESSION['usMac']))
                {
                    if($this->users->new_mac($_SESSION['usMac'],gets('mylang')))
                    {
                        $_SESSION['usLang']=gets('mylang');
                        $user=$this->users->login_mac($_SESSION['usMac']);
                        $user=$user[0];
                        $this->user=$user;
                        $user =  (array) $user;
                        $this->session->set_userdata($user);
                        // echo 'fdfd';
                        // exit;
                        redirect();
                    }
                    else
                        echo lang('picklang_error_new_mac');
                }
                else
                {
                    if($this->users->edit_lang(gets('mylang')))
                    {

                        // echo '1111';
                        $_SESSION['usLang']=gets('mylang');
                        redirect();
                    }
                        echo lang('picklang_error_edit_lang');
                }
            }
        }
        $this->load->view('header');
        $this->load->view('home/picklang');
        $this->load->view('footer');
    }

}
