<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Acts extends CI_Controller {

    var $data=array();
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','language','url'));
        $this->load->library(array('form_validation'));

        $this->load->database();
        $this->load->model('users');
//        echo 'fdsafds';
        $this->users->first_come();
//        echo $_SESSION['usLang'];
        $this->lang->load($_SESSION['usLang'],$_SESSION['usLang']);
        $this->config->set_item('language', $_SESSION['usLang']);

    }
	public function register()
	{
        $this->form_validation->set_error_delimiters('<span class="uk-h6 uk-text-warning " style="font-weight:normal;">', '</span>');
        $this->form_validation->set_rules('name','lang:register_name_label','required');
        $this->form_validation->set_rules('phone','lang:register_phone_label',array('required','regex_match[/^09(1[0-9]|3[1-9]|2[1-9])-?[0-9]{3}-?[0-9]{4}$/]','is_unique[tusers.usPhone]'));
        $this->form_validation->set_rules('pass','lang:register_pass_label','required');
        $this->form_validation->set_rules('repass','lang:register_repass_label','required|matches[pass]');

        $this->load->model('users');

//        if($this->user->check_mac(11))
//            echo 'true';
//        else
//            echo 'false';
        if($this->form_validation->run()==false)
        {
            $this->load->view('header',$this->data);
            $this->load->view('user/register');
            $this->load->view('footer',$this->data);
        }
        else
        {


            $this->users->set_value($this->input->post());
            $this->users->insert();
            echo 'fdsafsd';
            $this->load->view('header',$this->data);
            $this->load->view('user/register');
            $this->load->view('footer',$this->data);
        }

//    }
	}

}
