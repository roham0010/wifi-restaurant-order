<?php

class Blog extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('posts');

        $this->load->database();
          /*Pagination Config*/
        $this->pgConf['page_query_string'] = TRUE;
        $this->pgConf['query_string_segment'] = 'page';
        $this->pgConf['use_page_numbers'] = TRUE;
        $this->pgConf['per_page'] = (isset($_COOKIE['stuffAtPage']) && $_COOKIE['stuffAtPage']!='')?$_COOKIE['stuffAtPage']:10;
        $this->pgConf['page'] = 1;
        $this->pgConf['full_tag_open'] = '<ul class="uk-pagination">';
        $this->pgConf['full_tag_close'] = '</ul>';
        $this->pgConf['num_tag_open'] = '<li class=""><span>';
        $this->pgConf['num_tag_close'] = '</span></li>';
        $this->pgConf['cur_tag_open'] = '<li class="uk-active"><a href="#"><span>';
        $this->pgConf['cur_tag_close'] = '</span></a></li>';
        $this->pgConf['prev_tag_open'] = '<li class="uk-pagination-left" id="example1_previous">';
        $this->pgConf['prev_tag_close'] = '</li>';
        $this->pgConf['next_tag_open'] = '<li class="uk-pagination-right" id="example1_previous">';
        $this->pgConf['next_tag_close'] = '</li>';
        $this->pgConf['last_link'] = false;
        $this->pgConf['first_link'] = false;
        /*END Pagination Config-----*/

        $this->load->database();
        $this->load->library(array('form_validation','session','parser','pagination'));
        $this->load->helper(array('form','language','url'));

        $this->load->model('users');
        $this->users->first_come();
        $lang=(isset($_SESSION['usLang']) && !empty($_SESSION['usLang']))?$_SESSION['usLang']:'persian';

        $this->lang->load(@$lang,@$lang);
        $this->config->set_item('language', @$lang);

        $this->form_validation->set_error_delimiters ('<span class="uk-text-danger"> ', ' </span>');


    }
    function index()//index page
    {
        global $managerError,$langsShow,$defines;

        $this->load->model('posts');
        $this->load->model('postcategory');
        $this->load->library('jdf');

        $start=(gets('page'))?gets('page'):1;
        $start=($start-1)*$this->pgConf['per_page'];

        $posts=$this->posts->select_user($start,$this->pgConf['per_page']);
        $data['posts']=$posts;

        $postcategorys=$this->postcategory->select_all();
        $data['postcategorys']=$postcategorys;

        $data['mn'.gets('category')]='uk-active';

        $this->pgConf['base_url'] = base_url('blog').'?category='.gets('category').'&search='.gets('search');
        $countAll=$this->posts->select_join_num();
        $this->pgConf['total_rows'] = $countAll;

        // print_r($postsubcategorys);
        // print_r($countAll);
        // exit;

        $this->pagination->initialize($this->pgConf);

        $data['pagination']=$this->pagination->create_links();
        $data['start']=$start;
        $data['limit']=$this->pgConf['per_page']>$countAll?$countAll:$this->pgConf['per_page'];
        $data['countAll']=$countAll;


        $this->parser->parse('header',$data);
        $this->parser->parse('blog/posts',$data);
        $this->parser->parse('footer',$data);
    }
    function post()//index page
    {
        global $managerError,$langsShow,$defines;
        $data=array('message'=>@$_SESSION['message']);
        $poId=end((explode('-', params(3))));

        $this->load->model('posts');
        $this->load->model('postcategory');
        $this->load->model('comments');
        $this->load->library('jdf');
        if($this->form_validation->run('comment')==false);
        else
        {
            $this->comments->set_value(posts(),$poId);
            if($this->comments->insert())
            {
                flashdata('message', messageuser('success','نظر شما با موفقیت درج شد.'));
                redirect(current_url());
            }
            else
            {
                $data['message']=messageuser('danger',$managerError['unexpected']);
            }
        }

        $posts=$this->posts->select_one($poId);
        $data['posts']=$posts;

        $comments=$this->comments->select($poId);
        $data['comments']=$comments;


        $postcategorys=$this->postcategory->select_all();
        $data['postcategorys']=$postcategorys;

        $data['mn'.gets('category')]='uk-active';

        $this->parser->parse('header',$data);
        $this->parser->parse('blog/post',$data);
        $this->parser->parse('footer',$data);
    }
    public function postlike()
    {
        global $defines;
        if($this->form_validation->run('postlike')==false);
        else
        {
            if(posts('action') && posts('action')=='favorite' && $this->input->is_ajax_request())
            {
                $this->load->model('posts');
                if($this->posts->like(posts('postId')))
                    echo '1';
                else
                    echo '2';
            }
        }
    }
    function new_post()//Creating new post page
    {
        if(!$this->check_permissions('author'))//when the blog is not an andmin and author
        {
            redirect(base_url().'users/login');
        }
        if($this->input->post())
        {
            $data = array(
                'post_title' => $this->input->post('post_title'),
                'post' => $this->input->post('post'),
                'active' => 1,
            );
            $this->posts->insert_post($data);
            redirect(base_url().'blog/');
        }
        else{

            $class_name = array(
            'home_class'=>'current',
            'login_class' =>'',
            'register_class' => '',
            'upload_class'=>'',
            'contact_class'=>'');
            $this->load->view('header',$class_name);
            $this->load->view('v_new_post');
            $this->load->view('footer');
        }
    }
    function add_comment($postID)
    {
        if(!$this->input->post())
        {
            redirect(base_url().'blog/post'.$postID);
        }

        $user_type = $this->session->userdata('user_type');
        if(!$user_type)
        {
            redirect(base_url().'users/login');
        }

        $this->load->model('m_comment');
        $data = array(
            'post_id' => $postID,
            'user_id' => $this->session->userdata('user_id'),
            'comment' => $this->input->post('comment'),
        );
        $this->m_comment->add_comment($data);
        redirect(base_url().'blog/post/'.$postID);
    }
    function editpost($post_id)//Edit post page
    {
        if(!$this->check_permissions('author'))//when the user is not an andmin and author
        {
            redirect(base_url().'users/login');
        }
        $data['success'] = 0;

        if($this->input->post())
        {
            $data = array(
                'post_title' => $this->input->post('post_title'),
                'post' => $this->input->post('post'),
                'active' => 1
            );
            $this->posts->update_post($post_id, $data);
            $data['success'] = 1;
        }
        $data['post'] = $this->posts->get_post($post_id);

        $class_name = array(
            'home_class'=>'current',
            'login_class' =>'',
            'register_class' => '',
            'upload_class'=>'',
            'contact_class'=>'');
        $this->load->view('header',$class_name);
        $this->load->view('v_edit_post',$data);
        $this->load->view('footer');
    }
    function deletepost($post_id)//delete post page
    {
        if(!$this->check_permissions('author'))//when the user is not an andmin and author
        {
            redirect(base_url().'users/login');
        }
        $this->posts->delete_post($post_id);
        redirect(base_url().'blog/');
    }

    function check_permissions($required)//checking current user's permission
    {
        $user_type = $this->session->userdata('user_type');//curren user
        if($required == 'user')//requirment is user
        {
            if($user_type){return TRUE;}//all user have permission
        }
        elseif($required == 'author')//when requirement is author
        {
            if($user_type == 'author' || $user_type == 'admin')//author and admin have the permission
            {
                return TRUE;
            }
        }
        elseif($required == 'admin')//when required is admin
        {
            if($user_type == 'admin'){return TRUE;}//only admin have the permission
        }
    }
}
