<?php
$lang['hello']='سلام';
$lang['register_title']='ثبت نام';
$lang['register_button1']='ثبت نام';
$lang['register_name_label']='نام';
$lang['register_phone_label']='شماره همراه';
$lang['register_pass_label']='رمزعبور';
$lang['register_repass_label']='تکرار رمزعبور';
$lang['register_birthday_label']='تاریخ تولد';
$lang['register_change_pass']='تغییر رمز عبور';
$lang['register_link_not_now']='فعلا ثبت نام نمیکنم';

$lang['picklang_error_new_mac']='خطای رخ داده لطفا دوباره تلاش کنید';
$lang['picklang_error_edit_lang']='خطای رخ داده لطفا دوباره تلاش کنید';
