<?php
global $defines;
$lang['hello']='سلام';
$lang['register_title']='ثبت نام';
$lang['register_button1']='ثبت نام';
$lang['register_name_label']='نام';
$lang['register_phone_label']='شماره همراه';
$lang['register_pass_label']='رمزعبور';
$lang['register_repass_label']='تکرار رمزعبور';
$lang['register_birthday_label']='تاریخ تولد';
$lang['register_change_pass']='تغییر رمز عبور';
$lang['register_link_not_now']='فعلا ثبت نام نمیکنم';

$lang['picklang_error_new_mac']='خطای رخ داده لطفا دوباره تلاش کنید';
$lang['picklang_error_edit_lang']='خطای رخ داده لطفا دوباره تلاش کنید';

$lang['menu_slider_title']="منوی $defines[COMPANYNAME]";
$lang['menu_slider_text']="لذت یک غذای خوب با $defines[COMPANYNAME]";

$lang['menu_nav_menu']="منو";
$lang['menu_nav_home']="خانه";
$lang['menu_nav_orderedlists']="سفارشات من";
$lang['menu_nav_offer']="پیشنهادات";
$lang['menu_nav_survey']="نظر سنجی";
$lang['menu_nav_messages']="پیام های مدیر";
$lang['menu_nav_media']="سرگرمی و مدیا";
$lang['menu_nav_musics']="موسیقی ها";
$lang['menu_nav_pictures']="عکس ها";
$lang['menu_nav_videos']="فیلم ها";
$lang['menu_nav_games']="بازی ها";
$lang['menu_nav_texts']="متن ها";
$lang['menu_nav_blog']="بلاگ";
$lang['menu_nav_contactus']="ارتباط با ما";

$lang['menu_order_list_title']="سفارشات";
$lang['menu_order_list_count']="تعداد";
$lang['menu_order_list_sum']="مجموع";
$lang['menu_order_list_tax']="مالیات";
$lang['menu_order_list_all']="پرداخت";
$lang['menu_order_set_desk']="شماره میز";
$lang['menu_order_set_chair']="شماره صندلی";
$lang['menu_order_final_button']="ثب سفارش";
$lang['menu_order_final_reorder']="اضافه کردن غذا";

$lang['menu_ordered_title']="سفارش‍ات قبلی";
$lang['menu_ordered_count']="تعداد";
$lang['menu_ordered_orders']="سفارشات";

$lang['offer_title']="انتقادات پیشنهادات";
$lang['offer_label_name']="به نام";
$lang['offer_label_text']="متن";
$lang['offer_label_subject']="موضوع";
$lang['offer_label_userid']="به نام من";
$lang['offer_label_nouserid']="ناشناس";
$lang['offer_label_button']="ارسال";

$lang['survey_title']="نظر سنجی";
$lang['survey_empty']="نظر سنجی وجود ندارد.";
$lang['survey_youranswer']="جواب شما";
$lang['survey_count']="تعداد";
$lang['survey_button']="ارسال";

$lang['messages_title']="پیام های مدیر";
$lang['messages_badge_noseen']="خوانده نشده";
$lang['messages_badge_seen']="خوانده شده";
$lang['messages_message_subject']="موضوع";
$lang['messages_message_text']="متن";
$lang['messages_empty_subject']="پیامی موجپد نیست";
$lang['messages_empty_text']="تا کنون از طرف مدیر پیامی برای شما ارسال نشده است";
$lang['messages_badge_empty_text']="تا کنون از طرف مدیر پیامی برای شما ارسال نشده است";

$lang['picturestitle']="عکسها";
$lang['picturesdescription']="مجموعه ای از بهترین ها برای بترین ها";

$lang['musicstitle']="موسیقی";
$lang['musicsdescription']="مجموعه ای از بهترین ها برای بترین ها";

$lang['videostitle']="ویدئو";
$lang['videosdescription']="مجموعه ای از بهترین ها برای بترین ها";

$lang['gamestitle']="بازی";
$lang['gamesdescription']="مجموعه ای از بهترین ها برای بترین ها";

$lang['textstitle']="کتاب و متن";
$lang['textsdescription']="مجموعه ای از بهترین ها برای بترین ها";
$lang['texts_type']="کتاب:";

$lang['gallerys_button_search']="جستجو";
$lang['gallerys_sub_all']="همه";
$lang['gallerys_collection']="مجموعه";
$lang['gallerys_empty']="مدیایی موجود نیست.";
$lang['gallerys_download']="دانلود";

$lang['posts_date1']="توسط مدیر";
$lang['posts_comment_title']="ارسال کامنت";

$lang['more']="ادامه مطلب";
$lang['subject']="موضوع";
$lang['text']="متن";
$lang['time_at']="ساعت";
