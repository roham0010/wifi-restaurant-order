
  <!-- bottom-e -->
  <div id="tm-bottom-e" class="tm-block-bottom-e uk-block uk-block-default tm-inner-shadow" data-uk-parallax="{bg: '-200'}" style="background-image: url('<?=asset_url()?>images/background/bg-1.jpg');">
    <div class="uk-container uk-container-center">
      <section class="tm-bottom-e uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>

        <div class="uk-width-1-1">
          <div class="uk-panel uk-text-center">

            <img src="<?=asset_url()?>images/demo/default/logo/logo-bottom.svg" width="300" height="120" alt="demo">
          </div>
        </div>
      </section>
    </div>
  </div>
  <!-- offcanvas menu -->
  <div id="offcanvas" class="uk-offcanvas">
    <div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
      <ul class="uk-nav uk-nav-parent-icon uk-nav-offcanvas" data-uk-nav>

        <li><a href="<?=base_url('menu')?>"> <i class="uk-icon-cutlery"></i> <?=lang('menu_nav_menu')?></a></li>
        <li><a href="<?=base_url('home')?>"><?=lang('menu_nav_home')?></a></li>
        <li><a href="<?=base_url('orderedlists')?>"><?=lang('menu_nav_orderedlists')?></a></li>
        <li><a href="<?=base_url('blog')?>"><?=lang('menu_nav_blog')?></a></li>
        
        <li class="uk-parent"><a href="#"><?=lang('menu_nav_media')?></a>
          <ul class="uk-nav-sub">
            <li><a href="<?=base_url('musics')?>"><?=lang('menu_nav_musics')?></a></li>
            <li><a href="<?=base_url('picturegallery')?>"><?=lang('menu_nav_pictures')?></a></li>
            <li><a href="<?=base_url('videogallery')?>"><?=lang('menu_nav_videos')?></a></li>
            <li><a href="<?=base_url('games')?>"><?=lang('menu_nav_games')?></a></li>
            <li><a href="<?=base_url('texts')?>"><?=lang('menu_nav_texts')?></a></li>
          </ul>
        </li>

        <li class="uk-parent"><a href="#"><?=lang('menu_nav_contactus')?></a>
          <ul class="uk-nav-sub">
            <li><a href="<?=base_url('messages')?>"><?=lang('menu_nav_messages')?></a></li>                                        
            <li><a href="<?=base_url('offer')?>"><?=lang('menu_nav_offer')?></a></li>
            <li><a href="<?=base_url('survey')?>"><?=lang('menu_nav_survey')?></a></li>
          </ul>
        </li>
        <li><a href="<?=base_url('manager')?>">مدیریت</a></li>
        <li><a href="<?=base_url()?>picklang">انتخاب زبان</a></li>
    </ul>
    </div>
  </div>

  <!-- js -->
  <!-- uikit -->
  <script src="<?=asset_url()?>vendor/uikit/js/uikit-<?=langDirection()?>.js" type="text/javascript"></script>
  <script src="<?=asset_url()?>vendor/uikit/js/components/accordion.min.js" type="text/javascript"></script>
  <script src="<?=asset_url()?>vendor/uikit/js/components/autocomplete.min.js" type="text/javascript"></script>
  <script src="<?=asset_url()?>vendor/uikit/js/components/datepicker.min.js" type="text/javascript"></script>
  <script src="<?=asset_url()?>vendor/uikit/js/components/grid.min.js" type="text/javascript"></script>
  <script src="<?=asset_url()?>vendor/uikit/js/components/lightbox.min.js" type="text/javascript"></script>
  <script src="<?=asset_url()?>vendor/uikit/js/components/parallax.min.js" type="text/javascript"></script>
  <script src="<?=asset_url()?>vendor/uikit/js/components/slider.min.js" type="text/javascript"></script>
  <script src="<?=asset_url()?>vendor/uikit/js/components/slideset.min.js" type="text/javascript"></script>
  <script src="<?=asset_url()?>vendor/uikit/js/components/slideshow.min.js" type="text/javascript"></script>
  <script src="<?=asset_url()?>vendor/uikit/js/components/sticky.min.js" type="text/javascript"></script>
  <script src="<?=asset_url()?>vendor/uikit/js/components/timepicker.min.js" type="text/javascript"></script>
  <script src="<?=asset_url()?>vendor/uikit/js/components/tooltip.min.js" type="text/javascript"></script>

  <!-- theme -->
  <script src="<?=asset_url()?>js/theme.js" type="text/javascript"></script>
</body>
</html>