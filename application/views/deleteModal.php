<?php
function deleteModal($id)
{
  $queryString=$_SERVER["QUERY_STRING"]!=""?$_SERVER["REQUEST_URI"]."&delete=".$id:$_SERVER["REQUEST_URI"]."?delete=".$id;
  return "javascript: return deleteModal('$queryString')";
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="deletemodal" aria-labelledby="gridSystemModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">حذف</h4>
  </div>
  <div class="modal-body">
    <p>آیا مایل به حذف هستید&hellip;</p>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
    <a type="button" class="btn btn-primary" id="deleteBtn" href="#">حذف</a>
  </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
// bsee_url='<?=base_url()?>';
// manager_url='<?=base_url().'manager/'?>';
function deleteModal(attr)
{
    $('#deleteBtn').attr('href',attr);
}

</script>