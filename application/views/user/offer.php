

  <!-- main content -->
  <div id="tm-main" class="tm-block-main uk-block uk-block-default">
    <div class="uk-container uk-container-center">
      <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
        <div class="tm-main uk-width-medium-1-1">
          <main id="tm-content" class="tm-content">
            <article class="uk-article tm-article">
              <div class="tm-article-wrapper">
                <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                  <div class="tm-article">
                      <div class="uk-text-center">
                            <h1 class="uk-h2 uk-module-title-alt"><?=lang('offer_title')?></h1>
                      </div>

                  <br>
                      
                    <div class="tm-sidebar uk-width-medium-1-3 uk-hidden-small uk-row-first"></div>
                    <div class="uk-width-medium-1-3 uk-form uk-form-stacked uk-align-center">
                        <fieldset>
                            {message}
                            <?php
                            echo form_open();
                            ?>
                            <div class="uk-form-row">
                                <div class="uk-form-inline margin-bottom">
                                    <div class="margin-bottom"><?=form_label(lang('offer_label_name').': '.form_error('userId'),'userId',array('class'=>"uk-form-label"))?></div>
                                    <div class="radio uk-width-1-2 uk-align-right margin-right">
                                        <label>
                                            <?php
                                            echo form_radio(array('name'=>'userId','id'=>'userId1','class'=>''),'0',set_radio('userId','0'));
                                            echo lang('offer_label_nouserid');
                                            ?>
                                        </label>
                                    </div>
                                    <div class="radio uk-width-1-2 margin-right">
                                        <label>
                                            <?php
                                            echo form_radio(array('name'=>'userId','id'=>'userId','class'=>''),$_SESSION['usId'],set_radio('userId',$_SESSION['usId'],true));
                                            echo lang('offer_label_userid');
                                            ?>
                                            
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <?php
                                echo form_label(lang('offer_label_subject').': '.form_error('subject'),'subject',array('class'=>"uk-form-label"));
                                echo form_input(array('name'=>'subject','id'=>'','class'=>'uk-width-1-1',''=>''),set_value('subject'));
                                ?>
                            </div>
                            <div class="uk-form-row">
                                <?php
                                echo form_label(lang('offer_label_text').': '.form_error('text'),'text',array('class'=>"uk-form-label"));
                                echo form_textarea(array('name'=>'text','rows'=>'4','class'=>'uk-width-1-1',''=>''),set_value('text'));
                                ?>
                            </div>
                            <div class="uk-form-row uk-width-1-1">
                                <?php
                                echo form_submit('register',lang('offer_label_button'),array('class'=>'uk-width-medium-1-1 uk-align-center uk-botton  uk-text-center uk-button-primary uk-button-large'));
                                ?>
                            </div>
                            
                        </fieldset>
                    </div>

                      
                    
                  </div>

                  
                </div>
              </div>
            </article>
          </main>
        </div>
      </div>
    </div>
  </div>

