<div id="tm-header-a" class="tm-block-header-a uk-block uk-block-default tm-block-fullwidth tm-grid-collapse ">
<div class="uk-container uk-container-center">
  <section class="tm-header-a uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}">
    <div class="uk-width-1-1">
      <div class="uk-panel uk-text-center uk-contrast tm-overlay-secondary tm-header-height">
        <div class="tm-background-cover uk-cover-background uk-flex uk-flex-center uk-flex-middle" style="min-height: 250px;background-position: 50% 0px; background-image:url(images/background/{image})" data-uk-parallax="{bg: '-200'}">
          <div class="uk-position-relative uk-container">
            <div data-uk-parallax="{opacity: '1,0', y: '-50'}">
              <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:300}">
                <h1 class="uk-module-title-alt uk-margin-top">{title}</h1>
              </div>
              <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:800}">
                <h5 class="uk-sub-title-small">{description}</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
</div>