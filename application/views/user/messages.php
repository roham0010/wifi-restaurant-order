<!-- main content -->
<div id="tm-main" class="tm-block-main uk-block uk-block-default">
<div class="uk-container uk-container-center">
  <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
    <div class="tm-main uk-width-medium-1-1">
      <main id="tm-content" class="tm-content">
        <div id="system-message-container"></div>
          <article class="uk-article tm-article">
            <div class="tm-article-wrapper">
              <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                <div class="tm-article">
                  <div class="tm-slideset-gaucho" data-uk-slideset="{default: 1, animation: 'fade'}">
                    <div class="uk-slidenav-position uk-margin uk-width-medium-1-1">
                      <div class="uk-grid uk-grid-width-medium-1-2 uk-grid-width-large-1-2" data-uk-grid-margin>
                        <?php
                        foreach ($usermessages as $usermessage) 
                        {
                          $badge=empty($usermessage->umSeen)
                          ?"<div class='uk-button uk-button-mini uk-button-danger'>".lang('messages_badge_noseen')."</div>"
                          :"<div class='uk-button uk-button-mini uk-button-success'>".lang('messages_badge_seen')."</div>";
                          echo "
                          <div class=''>
                            <div class='uk-width-1-1'>
                              <div class='uk-width-1-1 uk-row-first'>
                                <span class='tm-block uk-align-right uk-margin-remove uk-padding-remove'>$badge</span>
                                <div class='uk-alert uk-alert-danger' data-uk-alert=''>
                                  <h3 class=' uk-h5 uk-title'><strong>".lang('messages_message_subject').":</strong> $usermessage->umSubject</h3>
                                  <div class='tm-menu-desc uk-text-small'><strong>".lang('messages_message_text').":</strong> $usermessage->umText</div>
                                </div>
                              </div>
                            </div>
                          </div>
                          ";
                        }
                        if(empty($usermessages))
                          echo "<div class=''>
                            <div class='uk-width-1-1'>
                              <div class='uk-width-1-1 uk-row-first'>
                                <span class='tm-block uk-align-right uk-margin-remove uk-padding-remove'></span>
                                <div class='uk-alert uk-alert-danger' data-uk-alert=''>
                                  <h3 class=' uk-h5 uk-title'>".lang('messages_empty_subject')."</h3>
                                  <div class='tm-menu-desc uk-text-small'>".lang('messages_empty_text').".</div>
                                </div>
                              </div>
                            </div>
                          </div>";
                        ?>
                        <!-- <div class="">
                          <div class="uk-width-1-1">
                            <div class="uk-width-1-1 uk-row-first">
                              <span class="tm-block uk-align-right uk-margin-remove uk-padding-remove"><div class="uk-button uk-button-mini uk-button-danger">دیده نشده</div></span>
                              <div class="uk-alert uk-alert-danger" data-uk-alert="">
                                <h3 class=" uk-h5 uk-title">پیام مدیر</h3>
                                <div class="tm-menu-desc uk-text-small">
                                  اثر آقای فلان و با برگردان آقای فلان فلان زیر نویس شده به دست فلانی فلانیان برای مردم خوب فلان فلان...
                                  اثر آقای فلان و با بفلان زیر نویس شده به دست فلانی فلانیان برای مردم خوب فلان فلان...
                                  اثر آقای فلان و با برگردان آقای فلان فلان زیر نویس شده به دست فلانی فلانیان برای مردم خوب فلان فلان...
                                  اثر آقای فلان و با برگردان آقای فلان فلان زیر نویس شده به دست فلانی فلانیان برای مردم خوب فلان فلان...
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="">
                          <div class="uk-width-1-1">
                            <div class="uk-width-1-1 uk-row-first">
                              <span class="tm-block uk-align-right uk-margin-remove uk-padding-remove"><div class="uk-button uk-button-mini uk-button-danger">دیده نشده</div></span>
                              <div class="uk-alert uk-alert-danger" data-uk-alert="">
                                <h3 class=" uk-h5 uk-title">پیام مدیر</h3>
                                <div class="tm-menu-desc uk-text-small">
                                  اثر آقای فلان و با برگردان آقای فلان فلان زیر نویس شده به دست فلانی فلانیان برای مردم خوب فلان فلان...
                                  اثر آقای فلان و با برگردان آقای فلان فلان زیر نویس شده به دست فلانی فلانیان برای مردم خوب فلان فلان...
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="">
                          <div class="uk-width-1-1">
                            <div class="uk-width-1-1 uk-row-first">
                              <span class="tm-block uk-align-right uk-margin-remove uk-padding-remove"><div class="uk-button uk-button-mini uk-button-danger">دیده نشده</div></span>
                              <div class="uk-alert uk-alert-danger" data-uk-alert="">
                                <h3 class=" uk-h5 uk-title">پیام مدیر</h3>
                                <div class="tm-menu-desc uk-text-small">
                                  اثر آقای فلان و با برگردان آقایه به دست فلانی فلانیان برای مردم خوب فلان فلان...
                                  اثر آقای انیان برای مردم خوب فلان فلان...
                                  اثر آقای فلان و با برگردان آقای فلان فلان لانیان برای مردم خوب فلان فلان...
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="">
                          <div class="uk-width-1-1">
                            <div class="uk-width-1-1 uk-row-first">
                              <span class="tm-block uk-align-right uk-margin-remove uk-padding-remove"><div class="uk-button uk-button-mini uk-button-success">دیده شده</div></span>
                              <div class="uk-alert" data-uk-alert="">
                                <h3 class=" uk-h5 uk-title">شما ۲۵ امتیاز تا به حال دریافت نمیدوه اید</h3>
                                <div class="tm-menu-desc uk-text-small">
                                اثر آقای فلان و با برگردان آقای فلان فلان زیر نویس شده به دست فلانی فلانیان برای مردم خوب فلان فلان...
                                </div>
                              </div>
                            </div>
                          </div>
                        </div> -->

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </div>
      </main>
    </div>
  </div>
</div>