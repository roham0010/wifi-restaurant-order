<?php global $langsShow;
$newest=array('ایرانی'=>'جدیدترین ها','English'=>'new','العربیه'=>'جدید',);
$newLang=@gets('lang')?gets('lang'):'ایرانی';
?>

<div id="tm-main" class="tm-block-main uk-block uk-block-default">
    <div class="uk-container uk-container-center">
      <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
        <div class="tm-main uk-width-medium-1-1">
          <main id="tm-content" class="tm-content">
            <div id="system-message-container"></div>
            <article class="uk-article tm-article">
              <div class="tm-article-wrapper">

                <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">


                  <div class="tm-article">

                    <div class="tm-slideset-gaucho" data-uk-slideset="{default: 1, animation: 'fade'}">
                      <ul class="uk-subnav uk-flex-center">
                      <li class="{persian}"><a href="<?=base_url()."texts?lang=".$langsShow['persian']?>"><?=$langsShow['persian']?></a></li>
                      <li class="{english}"><a href="<?=base_url()."texts?lang=".$langsShow['english']?>"><?=$langsShow['english']?></a></li>
                      <li class="{arabic}"><a href="<?=base_url()."texts?lang=".$langsShow['arabic']?>"><?=$langsShow['arabic']?></a></li>
                    </ul>
                    <ul class="uk-subnav uk-flex-center">

                      <li class="{mn}"><a href="<?=base_url("texts?lang=".@gets('lang'))?>"><?=$newest[$newLang]?></a></li>
                      <?php
                      foreach ($mediacategorys as $mediacategory) 
                      {
                      
                        echo "<li class='{mn$mediacategory->mcId}'><a href=".base_url("texts?lang=".@gets('lang')."&category=$mediacategory->mcId").">$mediacategory->mcName</a></li>";
                      }
                      ?>
                    </ul>
                    <div class="tm-middle uk-grid">
                    <!-- slideset -->
                    <aside class="tm-sidebar-b uk-width-medium-1-5 uk-grid-margin uk-row-first">
                      <div class="uk-width-medium-1-1 uk-form uk-form-stacked uk-text-center">
                        <fieldset>
                          <form action="http://127.0.0.1/restaurant/texts" method="get" accept-charset="utf-8">
                            <div class="uk-form-row">
                                <input type="text" name="search" class="uk-form-small uk-width-3-5" value="<?=@gets('search')?>">
                                <button type="submit" class="uk-button uk-button-success uk-button-small uk-width-1-3"><?=lang('gallerys_button_search')?></button>
                            </div>                              
                          </form>
                        </fieldset>
                      </div>
                      <ul class="uk-list">
                        <?php
                        if((gets('subcategory')))
                          echo "<li>".lang('gallerys_sub_all')."</li>";
                        else
                          echo "<li><a href=".base_url("texts?lang=".@gets('lang')."&category=".@gets('category')).">".lang('gallerys_sub_all')."</a></li>";
                        foreach ($mediasubcategorys as $mediasubcategory) 
                        {
                          echo "<li class='{mn$mediasubcategory->mscId}'>";
                          echo gets('subcategory')==$mediasubcategory->mscId?"<abbr>$mediasubcategory->mscName</abbr>":"<a href=".base_url("texts?lang=".@gets('lang')."&category=".@gets('category')."&subcategory=$mediasubcategory->mscId").">$mediasubcategory->mscName</a>";
                          echo "</li>";
                        }
                        ?>
                      </ul>
                    </aside>


                      <div class="uk-slidenav-position uk-margin uk-width-medium-4-5">
                        <div class="uk-grid uk-grid-width-medium-1-2 uk-grid-width-large-1-2" data-uk-grid-margin>

                            <?php
                            foreach ($medias as $media) 
                            {
                              $o=$media->muUserId==$_SESSION['usId']?'':"-o";
                              $favorit=$media->muUserId==$_SESSION['usId']?'':" favorit";
                              $bunch='';
                              if($media->fileCount>1)
                                $bunch='<i class="uk-icon-list-ol uk-align-left uk-margin-right"></i>';
                              $download='';
                              $type='';
                              if(file_exists(FCPATH."upload/texts/".$media->mfName) && ($media->mfName))
                              {
                                
                                if($media->fileCount == 1)
                                  $download= "<span class='tm-block uk-align-right uk-margin-remove uk-padding-bottom-remove   uk-padding-remove '>  
                                              [<i class='uk-icon-download uk-text-primary'> $media->meDownloaded</i>]<a target='_blank' href='".base_url("download?fileid=$media->mfId")."' class='uk-button uk-button-primary uk-button-mini uk-margin-small-left'>".lang('gallerys_download')."</a>
                                            </span>";
                                $type=lang('texts_type');
                                
                              }
                              $subject=preg_replace('/ /', '-', $media->mlSubject);
                                echo "
                                <div class='border-bottom uk-margin-bottom'>
                                  <div class='uk-grid' data-uk-grid-margin>
                                    <div class='uk-width-1-1'>
                                      <div class='uk-margin-small-left'>
                                        <div class='uk-width-1-1'>
                                          <span class='tm-block uk-align-right uk-margin-remove uk-padding-remove uk-icon-small'><i class='uk-icon-heart$o uk-text-danger $favorit' media-id='$media->meId'></i></span>
                                          <h3 class='uk-h5 uk-margin-small-bottom'>$bunch<a href='".base_url("text/$subject-$media->mlId")."'>$type $media->mlSubject </a></h3>
                                        </div>
                                        <div class='tm-menu-desc uk-text-small'>
                                        $media->mlText
                                          $download 
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class='uk-grid uk-grid-medium uk-margin-small-bottom'>
                                    <div class='uk-width-1-2'>
                                      <div class='uk-align-left uk-margin-small-top uk-padding-right'>
                                        <i class='uk-icon-eye uk-margin-left'> $media->meSeen </i>
                                        <i class='uk-icon-download uk-margin-left'> $media->meDownloaded </i>
                                      </div>
                                    </div>
                                    <div class='uk-width-1-2'>
                                      <div class='uk-align-right uk-margin-top uk-padding-right'>
                                          <i class='uk-icon-heart$o uk-text-danger $favorit' media-id='$media->meId'> $media->meRate </i> 
                                      </div>
                                    </div>
                                  </div>
                                  </div>

                                ";
                              
                                // <div class='border-bottom uk-margin-bottom'>
                                //   <div class='uk-grid' data-uk-grid-margin>
                                //     <div class='uk-width-1-1'>
                                //       <div class='uk-margin-small-left'>
                                //         <div class='uk-width-1-1'>
                                //           <span style='z-index:1000;' media-id='$media->meId' class='$favorit tm-block uk-align-right uk-margin-remove uk-padding-remove uk-icon-small'><i class='uk-icon-heart$o uk-text-danger' media-id='$media->meId'></i></span>
                                //           <h3 class='uk-h5 uk-margin-small-bottom'><a href='#'>$media->mlSubject </a></h3>
                                //         </div>

                                //       </div>
                                //     </div>
                                //   </div>
                                  
                                //   <div class='uk-width-1-1 uk-margin-top'>
                                //     <span class='tm-block uk-align-right uk-padding-remove uk-width-2-10'>  
                                //       <span class='uk-button uk-button-primary uk-button-small uk-width-1-1 download' media-id='$media->meId'>".lang('gallerys_download')."</span>
                                //     </span>
                                //     <span class='tm-block uk-align-left uk-margin-remove uk-padding-remove'>  
                                //       <i class='uk-icon-download uk-margin-left'> $media->meDownloaded </i>
                                //     </span>
                                //   </div>
                                
                                //   <div class='uk-grid uk-grid-medium uk-margin-small-bottom uk-margin-top'>
                                //     <div class='uk-width-1-2'>
                                //       <div class='uk-align-left uk-margin-small-top uk-padding-right'>
                                //         <i class='uk-icon-eye uk-margin-left'> $media->meSeen </i>
                                //       </div>
                                //     </div>
                                //     <div class='uk-width-1-2'>
                                //       <div class='uk-align-right uk-margin-small-top uk-padding-right'>
                                //           <i class='uk-icon-heart$o uk-text-danger $favorit' media-id='$media->meId'> $media->meRate </i> 
                                //       </div>
                                //     </div>
                                //   </div>
                                //   </div>
                            }
                            if(empty($medias))
                              echo lang('gallerys_empty');
                            ?>
                          </div>
                          <p>
                            {pagination}
                          </p>
                        </div>


                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </article>
          </main>
        </div>
      </div>
    </div>
