<div id="tm-main" class="tm-block-main uk-block uk-block-default">
    <div class="uk-container uk-container-center">
      <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
        <div class="tm-main uk-width-medium-1-1">
          <main id="tm-content" class="tm-content">
            <div id="system-message-container"></div>
            <article class="uk-article tm-article">
              <div class="tm-article-wrapper">

                <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">


                  <div class="tm-article">

                    <div class="tm-slideset-gaucho" data-uk-slideset="{default: 1, animation: 'fade'}">
                    <!-- data filters -->
                      <div class="uk-text-center uk-margin-bottom">
                        <h1 class="uk-h2 uk-module-title-alt"><?=lang('survey_title')?></h1>
                      </div>


                      <div class="uk-slidenav-position uk-margin uk-width-medium-5-5">
                        <div class="uk-width-medium-1-1 uk-text-align-center">
                            <div class="uk-margin-bottom uk-width-medium-2-5 uk-align-center">
                            
                            <?=form_open()?>
                              <div class="uk-grid" data-uk-grid-margin>
                                <ul class="uk-margin-top uk-form">
                                {message}
                                  <?php 

                                  foreach ($surveys as $survey) 
                                  {
                                    ?>
                                    <h2 class="uk-title uk-h4"><?=$survey->suQuestion?></h2>
                                    <?php
                                    echo form_error('answer');
                                    echo ($survey->suaAnswer)?lang('survey_youranswer').': '.$survey->{"suAnswer".$survey->suaAnswer}:'';
                                    for ($i=1; $i < 6; $i++) 
                                    { 
                                      echo form_hidden('surveyId',$survey->suId);
                                      if((trim($survey->{"suAnswer".$i})))
                                      {
                                        if(empty($survey->suaUserId))
                                        {
                                          ?>
                                          <div class="radio uk-width-1-1 margin-right">
                                            <label>
                                                <?php
                                                echo form_radio(array('name'=>'answer','id'=>'answer','class'=>''),$i,set_radio('answer',$i,true));
                                                echo $survey->{"suAnswer".$i};
                                                ?>
                                                
                                            </label>
                                          </div>
                                          <?php
                                        }
                                        else
                                        {
                                          ?>
                                          <div class="radio uk-width-1-1 margin-right">
                                            <label>
                                                <?php
                                                echo $survey->{"suAnswer".$i}.' {'.lang('survey_count').': '.$survey->{"suResult".$i}.'}';
                                                ?>
                                                
                                            </label>
                                          </div>
                                          <?php
                                        }
                                          
                                      }
                                    }
                                  }
                                  if(empty($surveys))
                                    echo lang('survey_empty');
                                  ?>
                                </ul>
                              </div>
                              <div class="uk-text-center">
                                <?php if(($surveys) && empty($survey->suaAnswer)){ ?><button type="submit" class="uk-button uk-align-center uk-button-primary uk-width-medium-1-3" ><?=lang('survey_button')?></button><?php } ?>
                              
                                <!-- {pagination} -->
                              </div>
                                <?=form_close()?>
                            </div>
                                {pagination}
                          </div>
                        </div>


                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </article>
          </main>
        </div>
      </div>
    </div>
  