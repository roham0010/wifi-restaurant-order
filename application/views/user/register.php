

  <!-- main content -->
  <div id="tm-main" class="tm-block-main uk-block uk-block-default">
    <div class="uk-container uk-container-center">
      <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
        <div class="tm-main uk-width-medium-1-1">
          <main id="tm-content" class="tm-content">
            <article class="uk-article tm-article">
              <div class="tm-article-wrapper">
                <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                  <div class="tm-article">
                      <div class="uk-text-center">
                            <h1 class="uk-h2 uk-module-title-alt">ثبت نام</h1>
                      </div>

                  <br>
                      
                    <div class="tm-sidebar uk-width-medium-1-3 uk-hidden-small uk-row-first"></div>
                    <div class="uk-width-medium-1-3 uk-form uk-form-stacked uk-align-center">
                        <fieldset>
                            <legend>فیلدها را پر کنید</legend>
                            <?php
                            $attr=array('class'=>'dd','id'=>'fdsf');
                            echo form_open('','class="my_class"');
                            ?>
                            <div class="uk-form-row">
                                <?php
                                echo form_label(lang('register_name_label').': '.form_error('name'),'name',array('class'=>"uk-form-label"));
                                echo "<div class='uk-form-controls'>".form_input(array('name'=>'name','id'=>'','class'=>'uk-width-1-1',''=>''),set_value('name'))."</div>";
//                                echo form_error('name');
                                ?>
                            </div>
                            <div class="uk-form-row">
                                <?php
                                echo form_label(lang('register_phone_label').': '.form_error('phone'),'phone',array('class'=>"uk-form-label"));
                                echo "<div class='uk-form-controls'>".form_input(array('name'=>'phone','id'=>'','class'=>'uk-width-1-1',''=>''),set_value('phone'))."</div>";
//                                echo form_error('phone');
                                ?>
                            </div>
                            <div class="uk-form-row">
                                <?php
                                echo form_label(lang('register_pass_label').': '.form_error('pass'),'pass',array('class'=>"uk-form-label"));
                                echo "<div class='uk-form-controls'>".form_password(array('name'=>'pass','id'=>'','class'=>'uk-width-1-1',''=>''),set_value('pass'))."</div>";
//                                echo "<div class='uk-width-medium-1-2 uk-text-small'><em class=''>دقت کنید رمز عبورتان را به جهت زمانی که گوشی را تعویض کردید باید به خاطر بسپارید</em></div>";
//                                echo form_error('pass');
                                ?>
                            </div>
                            <div class="uk-form-row">
                                <?php
                                echo form_label(lang('register_repass_label').': '.form_error('repass'),'repass',array('class'=>"uk-form-label"));
                                echo "<div class='uk-form-controls'>".form_password(array('name'=>'repass','id'=>'','class'=>'uk-width-1-1',''=>''),set_value('repass'))."</div>";
//                                echo form_error('repass');
                                ?>
                            </div>
                            <div class="uk-form-row">
                                <?php
                                echo form_label(lang('register_birthday_label').': '.form_error('year'),'year',array('class'=>"uk-form-label"));
                                echo "<div class='uk-form-controls'>"
                                    .form_input(array('name'=>'day','class'=>'uk-form-small uk-width-1-5','placeholder'=>'روز'),set_value('day'))
                                    .form_input(array('name'=>'mount','class'=>'uk-form-small uk-width-1-5','placeholder'=>'ماه'),set_value('mount'))
                                    .form_input(array('name'=>'year','class'=>'uk-form-small uk-width-1-4','placeholder'=>'سال'),set_value('year'))."</div>";
//                                echo form_error('year');
                                ?>
                            </div>
                            <?php
                            echo "<div class='uk-margin-top uk-width-1-1'>".form_submit('register',lang('register_button1'),array('class'=>'uk-width-3-3 uk-align-center uk-botton uk-button-primary uk-button-large'))."</div>";
                            if(@$_SESSION['is_login'])echo "<div class='uk-margin-top'>".anchor('changePass',lang('register_change_pass'),array('class'=>''))."</div>";
                            ?>
                        </fieldset>
                    </div>

                      
                    
                  </div>

                  
                </div>
              </div>
            </article>
          </main>
        </div>
      </div>
    </div>
  </div>

