  <!-- main content -->
  <div id="tm-main" class="tm-block-main uk-block uk-block-default">
    <div class="uk-container uk-container-center">
      <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
        <div class="tm-main uk-width-medium-1-1">
          <main id="tm-content" class="tm-content">
            <div id="system-message-container"></div>
            <article class="uk-article tm-article">
              <div class="tm-article-wrapper">

                <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">


                  <div class="tm-article">

                    <div class="tm-slideset-gaucho" data-uk-slideset="{default: 1, animation: 'fade'}">

                   
                      <!-- slideset -->
                      <div class="uk-slidenav-position uk-margin">
                        <ul class="uk-grid uk-slideset uk-grid-match uk-flex-center">
                          <li data-uk-filter="appetizers">
                            <div class="uk-panel uk-text-left">
                              <div class="uk-grid uk-grid-width-medium-1-2 uk-grid-width-large-1-2" data-uk-grid-margin>
                                <div class="uk-panel uk-panel-space uk-align-center">

                                  <div class="uk-text-center uk-margin-bottom">
                                    <h1 class="uk-h2 uk-module-title-alt"><?=lang('menu_order_list_title')?></h1>
                                    
                                      <div class="uk-form-row uk-width-1-1">
                                          <?php
                                          echo form_error('desk');
                                          ?>
                                      </div>
                                      <div class="uk-form-row uk-width-1-1">
                                          <?php
                                          echo form_error('chair');
                                          ?>
                                      </div>
                                  </div>
                                  <div class="uk-text-center uk-text-success">{message}</div>
                                  <?php
                                  echo form_open('','class="uk-form"');
                                  ?>
                                    <i class="uk-width-1-1   uk-clearfix " >
                                    <span class="uk-align-right uk-text-right"><?=$nowDate?></span>
                                    <span class="uk-align-left uk-text-left uk-form-row uk-margin-remove uk-width-1-2"><?=form_error('name')?><input name="name" class="uk-form-small " placeholder="نام لیست مثال: خانوادگی"></span>
                                    </i>
                                    <span id="order-list"></span>
                                    <div class="uk-margin-top">
                                    </div>
                                    <?php 
                                    if(@$_SESSION['basket']['final']===true) 
                                    {
                                      echo "سفارش ثبت شده است";
                                    }
                                    else
                                    { 
                                      ?>
                                      <div class="uk-form-row uk-width-1-2 uk-float-right">
                                          <?php
                                          echo form_label(lang('menu_order_set_chair').': ','chair',array('class'=>"uk-form-label"));
                                          echo form_input(array('name'=>'chair','id'=>'','class'=>'uk-width-3-4 uk-text-center',''=>''),set_value('chair'));
                                          ?>
                                      </div>
                                      <div class="uk-form-row uk-width-1-2">
                                          <?php
                                          echo form_label(lang('menu_order_set_desk').': ','desk',array('class'=>"uk-form-label"));
                                          echo form_input(array('name'=>'desk','id'=>'','class'=>'uk-width-3-4 uk-text-center',''=>''),set_value('desk'));
                                          ?>
                                      </div>
                                      
                                        <button type="submit" class="uk-button uk-button-success uk-margin-top uk-margin-bottom uk-align-center uk-width-2-3"><?=lang('menu_order_final_button')?></button>
                                        <a type="submit" href="<?=base_url("menu")?>" class="uk-button uk-button-primary uk-margin-top uk-margin-bottom uk-align-center uk-width-2-3"><?=lang('menu_order_final_reorder')?></a>
                                      
                                      <?php 
                                      echo form_close();
                                    } 
                                    ?>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </li>
                        </ul>

                        <!-- slideset navigation -->



                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </article>
          </main>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
  URL='<?=base_url()?>';
  $(function(){                            
    $(document).on('click','.add-basket',function(){
      foodId=$(this).attr('food-id');
      $.ajax({
          url: URL + 'basket',
          type: 'post',
          data: {action: 'addfood',foodId},
          success: function (data) {
              $('#order-list').html(data);
              
              // ajaxEnd();
          },error:function(){
              ajaxEnd();
              myPrompt('changeName');
          }
      });
    });
    $(document).on('click','.reduce-basket',function(){
      foodId=$(this).attr('food-id');
      $.ajax({
          url: URL + 'basket',
          type: 'post',
          data: {action: 'reducefood',foodId},
          success: function (data) {
              $('#order-list').html(data);
              
              // ajaxEnd();
          },error:function(){
              ajaxEnd();
              myPrompt('changeName');
          }
      });

    });
    $(document).on('click','.delete-basket',function(){
      foodId=$(this).attr('food-id');
      $.ajax({
          url: URL + 'basket',
          type: 'post',
          data: {action: 'deletefood',foodId},
          success: function (data) {
              $('#order-list').html(data);
              
              // ajaxEnd();
          },error:function(){
              ajaxEnd();
              myPrompt('changeName');
          }
      });

    });
    $.ajax({
        url: URL + 'basket',
        type: 'post',
        data: {},
        // dataType: 'json',
        success: function (data) {
            // alert();
            $('#order-list').html(data);
            
            // ajaxEnd();
        },error:function(e){
            // ajaxEnd();
            console.log(e);
            myPrompt('changeName');
        }
      });
  });
</script>