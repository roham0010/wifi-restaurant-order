<div id="tm-main" class="tm-block-main uk-block uk-block-default">
    <div class="uk-container uk-container-center">
      <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
        <div class="tm-main uk-width-medium-1-1">
          <main id="tm-content" class="tm-content">
            <div id="system-message-container"></div>
            <article class="uk-article tm-article">
              <div class="tm-article-wrapper">

                <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">


                  <div class="tm-article">

                    <div class="tm-slideset-gaucho" data-uk-slideset="{default: 1, animation: 'fade'}">
                    <!-- data filters -->
                      <div class="uk-text-center uk-margin-bottom">
                        <h1 class="uk-h2 uk-module-title-alt"><?=lang('musicstitle')?></h1>
                      </div>


                      <div class="uk-slidenav-position uk-margin uk-width-medium-5-5">
                        <div class="uk-width-medium-1-1 uk-text-align-center">
                            <div class="border-bottom uk-margin-bottom uk-width-medium-4-5 uk-align-center">
                            <?php
                            $medialang=$medialang[0];
                            $o=$medialang->muUserId==$_SESSION['usId']?'':"-o";
                            $favorit=$medialang->muUserId==$_SESSION['usId']?'':" favorit";
                            ?>
                              <div class="uk-grid" data-uk-grid-margin>
                                <div class="">
                                  <h1 class="uk-h4 uk-panel-title"><?=$medialang->mlSubject?></h1>
                                  {messageDownload}
                                </div>
                                <div class="uk-width-1-1">
                                  <div class="uk-margin-small-left">
                                    <div class="tm-menu-desc">
                                      <span class="tm-block uk-align-right uk-margin-remove uk-padding-remove uk-icon-small"><i class="uk-icon-heart<?=$o?> uk-text-danger <?=$favorit?>" media-id='<?=$medialang->meId?>'></i></span>
                                      <div class="">
                                        <?="<strong>".lang('text').': '."</strong>".$medialang->mlText?>
                                        <span class="tm-block uk-align-right uk-margin-remove uk-padding-bottom-remove   uk-padding-top ">  
                                          <!-- <a href="#" class="uk-button uk-button-primary uk-button-mini uk-margin-small-left"> دانلود</a> -->
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="uk-width-1-1 uk-margin-remove uk-row-first">
                                  <div class="uk-width-1-2 uk-float-left">
                                    <div class='uk-align-left uk-margin-small-top uk-padding-right'>
                                      <i class='uk-icon-eye uk-margin-left'> <?=$medialang->meSeen?> </i>
                                      <i class="uk-icon-download"> <?=$medialang->meDownloaded?> </i>
                                    </div>
                                  </div>
                                  <div class="uk-width-1-2 uk-float-right">
                                    <div class='uk-align-right uk-margin-top uk-padding-right'>
                                      <i class='uk-icon-heart<?=$o?> uk-text-danger <?=$favorit?>' media-id='<?=$medialang->meId?>'> <?=$medialang->meRate?> </i> 
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class=" uk-margin-bottom uk-width-medium-4-5 uk-align-center">
                              <?php 
                              foreach ($mediafiles as $mediafile) 
                              {
                                echo "
                                <div class='uk-grid'>
                                  <div class='uk-width-medium-2-6 uk-text-success' style='word-wrap: break-word;'>
                                    $mediafile->mfRealName
                                  </div>
                                  <div class='uk-width-medium-1-2 uk-text-center'>
	                                  <audio class='uk-width-1-1 seen1' onplay=".'"javascript: return seen('."'".$medialang->meId."'".');"'." src='".base_url()."upload/musics/".$mediafile->mfName."'  controls media-id='$medialang->meId' preload='none'>
	                                    Your browser does not support the audio element.
	                                    </audio>
                                  </div>
                                  <div class='uk-width-medium-1-6 uk-text-right uk-margin-small-top'>
                                    <a target='_blank' href='".base_url("download?fileid=$mediafile->mfId")."' class='uk-button uk-button-primary uk-button-mini uk-margin-small-left download' media-id='$medialang->meId'>".lang('gallerys_download')."</a>
                                  </div>
                                </div>
                                ";  
                              }
                              ?>

                            </div>
                          </div>
                        </div>


                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </article>
          </main>
        </div>
      </div>
    </div>
  