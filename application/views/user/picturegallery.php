<?php global $langsShow;
$newest=array('ایرانی'=>'جدیدترین ها','English'=>'new','العربیه'=>'جدید',);
$newLang=@gets('lang')?gets('lang'):'ایرانی';
?>

<!-- main content -->
<div id="tm-main" class="tm-block-main uk-block uk-block-default">
<div class="uk-container uk-container-center">
  <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
    <div class="tm-main uk-width-medium-1-1">
      <main id="tm-content" class="tm-content">
        <div id="system-message-container"></div>
          <article class="uk-article tm-article">
            <div class="tm-article-wrapper">
              <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                <div class="tm-article">
                  <div class="tm-slideset-gaucho" data-uk-slideset="{default: 1, animation: 'fade'}">
                    <ul class="uk-subnav uk-flex-center">
                      <li class="{persian}"><a href="<?=base_url()."picturegallery?lang=".$langsShow['persian']?>"><?=$langsShow['persian']?></a></li>
                      <li class="{english}"><a href="<?=base_url()."picturegallery?lang=".$langsShow['english']?>"><?=$langsShow['english']?></a></li>
                      <li class="{arabic}"><a href="<?=base_url()."picturegallery?lang=".$langsShow['arabic']?>"><?=$langsShow['arabic']?></a></li>
                    </ul>
                    <ul class="uk-subnav uk-flex-center">

                      <li class="{mn}"><a href="<?=base_url("picturegallery?lang=".@gets('lang'))?>"><?=$newest[$newLang]?></a></li>
                      <?php
                      foreach ($mediacategorys as $mediacategory) 
                      {
                      
                        echo "<li class='{mn$mediacategory->mcId}'><a href=".base_url("picturegallery?lang=".@gets('lang')."&category=$mediacategory->mcId").">$mediacategory->mcName</a></li>";
                      }
                      ?>
                    </ul>
                    <div class="tm-middle uk-grid">
                      <aside class="tm-sidebar-b uk-width-medium-1-5 uk-grid-margin uk-row-first">
                        <div class="uk-width-medium-1-1 uk-form uk-form-stacked uk-text-center">
                          <fieldset>
                            <form action="http://127.0.0.1/restaurant/picturegallery" method="get" accept-charset="utf-8">
                              <div class="uk-form-row">
                                  <input type="text" name="search" class="uk-form-small uk-width-3-5" value="<?=@gets('search')?>">
                                  <button type="submit" class="uk-button uk-button-success uk-button-small uk-width-1-3"><?=lang('gallerys_button_search')?></button>
                              </div>                              
                            </form>
                          </fieldset>
                        </div>
                        <ul class="uk-list">
                          <?php
                          if((gets('subcategory')))
                            echo "<li>".lang('gallerys_sub_all')."</li>";
                          else
                            echo "<li><a href=".base_url("picturegallery?lang=".@gets('lang')."&category=".@gets('category')).">".lang('gallerys_sub_all')."</a></li>";
                          foreach ($mediasubcategorys as $mediasubcategory) 
                          {
                          
                            echo "<li class='{mn$mediasubcategory->mscId}'>";
                            echo gets('subcategory')==$mediasubcategory->mscId?"<abbr>$mediasubcategory->mscName</abbr>":"<a href=".base_url("picturegallery?lang=".@gets('lang')."&category=".@gets('category')."&subcategory=$mediasubcategory->mscId").">$mediasubcategory->mscName</a>";
                            echo "</li>";
                          }
                          ?>
                        </ul>
                      </aside>
                      <div class="uk-slidenav-position uk-margin uk-width-medium-4-5">
                        <div class="uk-grid uk-grid-width-medium-1-2 uk-grid-width-large-1-3" data-uk-grid-margin>
                          <?php
                          foreach ($medias as $media) 
                          {
                            $o=$media->muUserId==$_SESSION['usId']?'':"-o";
                            $favorit=$media->muUserId==$_SESSION['usId']?'':" favorit";
                            $bunch='';

                            if($media->fileCount>1)
                              $bunch='<span class="uk-badge uk-badge uk-align-right uk-margin-right">'.lang('gallerys_collection').'</span>';
                            if($media->fileCount == 1)
                                  $download= "";
                            $subject=preg_replace('/ /', '-', $media->mlSubject);
                            // echo $media->fileCount;
                            if(file_exists(FCPATH."upload/images/".$media->mfName) && ($media->mfName))
                              echo "
                              <figure>
                                <div class='uk-thumbnail'>
                                  <div style='z-index:1000;' media-id='$media->meId' class='$favorit uk-link uk-panel-overlay uk-align-left uk-margin-small-top uk-width-2-6 uk-padding-right uk-position-absolute uk-padding-small-top uk-text-muted'>
                                    <i class='uk-icon-heart$o uk-margin-left uk-icon-medium uk-text-danger' media-id='$media->meId'></i>
                                    $bunch  
                                  </div>
                                  <a href='".base_url()."upload/images/".$media->mfName."' class='seen'  media-id='$media->meId' data-uk-lightbox="."{group:'gallery1'}"." title='$media->mlSubject'>
                                    <img src='".base_url()."upload/images/".$media->mfName."' alt='sample_1' width='800' height='800'>
                                  </a>
                                  <div class='uk-thumbnail-caption uk-margin-remove'><a href='".base_url("picture/$subject-$media->mlId")."'>$media->mlSubject</a></div>
                                  <div class='uk-align-right uk-margin-small-top uk-padding-right'>
                                      <i class='uk-icon-heart$o uk-text-danger $favorit' media-id='$media->meId'> $media->meRate </i> 
                                  </div>
                                  <div class='uk-align-left uk-margin-small-top uk-padding-right'>
                                    <i class='uk-icon-eye uk-margin-left'> $media->meSeen </i>
                                  </div>
                                </div>
                              </figure>
                              ";
                          }
                          if(empty($medias))
                            echo lang('gallerys_empty');
                          ?>
                        </div>
                        <p>
                          {pagination}
                        </p>
                      </div>
                    </div>
                   


                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
      </main>
    </div>
  </div>
</div>
<script type="text/javascript">
URL='<?=base_url()?>';
$(function(){
  $(document).on('click','.favorit',function(){
    mediaId=$(this).attr('media-id');
    mthis=$(this);
    $.ajax({
        url: URL + 'medialike',
        type: 'post',
        data: {action: 'favorite',mediaId},
        success: function (data) {
          // alert(mthis.attr('media-id'))
          if(data=='1')
          {
            // $("i[media-id='"+mediaId+"']").
            $("i[media-id='"+mediaId+"']").removeClass('uk-icon-heart-o');
            $("i[media-id='"+mediaId+"']").addClass('uk-icon-heart');
            
          }

            // ajaxEnd();
        },error:function(){
            ajaxEnd();
            myPrompt('changeName');
        }
    });
  });
  $(document).on('click','.seen',function(){
    mediaId=$(this).attr('media-id');
    mthis=$(this);
    $.ajax({
        url: URL + 'mediaseen',
        type: 'post',
        data: {action: 'seen',mediaId},
        success: function (data) {
        },error:function(){
        }
    });
  });
  
});
</script>