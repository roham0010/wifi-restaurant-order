<div id="tm-main" class="tm-block-main uk-block uk-block-default">
    <div class="uk-container uk-container-center">
      <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
        <div class="tm-main uk-width-medium-1-1">
          <main id="tm-content" class="tm-content">
            <div id="system-message-container"></div>
            <article class="uk-article tm-article">
              <div class="tm-article-wrapper">

                <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">


                  <div class="tm-article">

                    <div class="tm-slideset-gaucho" data-uk-slideset="{default: 1, animation: 'fade'}">

                      <div class="uk-text-center uk-margin-bottom">
                        <h1 class="uk-h3 uk-module-title-alt"><?=lang('menu_ordered_title')?></h1>
                      </div>
                      <!-- slideset -->
                      <div class="uk-slidenav-position uk-margin">
                        <ul class="uk-grid uk-slideset uk-grid-match uk-flex-center">

                          <li data-uk-filter="appetizers">
                            <div class="uk-panel uk-text-left">

                              <div class="uk-grid uk-grid-width-medium-1-2 uk-grid-width-large-1-2" data-uk-grid-margin>

                              <?php 

                              foreach ($userorders as $userorder) 
                              {
                                $desc='';$count=0;$price=0;
                                $datein=lang_date('H:i:s Y/m/d',strtotime($userorder->ulIn));
                                foreach ($orderitems[$userorder->uoId] as $orderitem) 
                                {
                                  $desc.=$orderitem->foName."{".$orderitem->otCount."}, ";
                                  $count+=$orderitem->otCount;
                                  $orderitem->foType==1?
                                  $price+=$orderitem->foPrice*$orderitem->otCount-(($orderitem->foPrice*$orderitem->otCount*$orderitem->foDiscount)/100):
                                  $price+=$orderitem->foPrice*$orderitem->otCount;
                                  // $price+=$orderitem->foPrice*$orderitem->otCount;
                                }
                                $status=(time()<=strtotime($userorder->uoDate)+3600)?"<span class='uk-badge uk-badge-info uk-float-right'>در حال سرو</span>":'';
                                $name=($userorder->uoName)?$userorder->uoName:"لیست شماره $userorder->uoId";
                                echo "
                                <div class='tm-menu-item tm-menu-compound uk-margin-bottom'>
                                  <div class='uk-grid' data-uk-grid-margin>
                                    <div class='uk-width-5-5'>
                                      <div class='uk-margin-small-left'>
                                        <div class='uk-width-1-1'>
                                          <h3 class='tm-menu-name uk-margin-small-bottom'>$name</h3>
                                          <span class='tm-menu-dots'></span>
                                          <span class='tm-menu-price uk-float-right'>
                                            <i class='uk-link uk-icon-cart-plus uk-icon-small add-order' order-id='$userorder->uoId'></i>
                                          </span>
                                        </div>
                                      </div>
                                      <div class='discount'>
                                        <div class='uk-float-left uk-text-success uk-width-1-2'>".lang('menu_ordered_count').": $count</div>
                                        <div class='tm-menu-price uk-width-1-2' style='height: 30px;'>  
                                          <div class='uk-clearfix uk-align-right uk-text-success'>$price تومان</div>
                                        </div>
                                      </div>
                                      <div class='uk-width-1-1'>
                                        <span class='tm-menu-desc uk-text-small uk-text-small'>
                                          <strong>".lang('menu_ordered_orders').": </strong> $desc
                                          <p class='uk-text-small'>{ $datein } $status</p>
                                        </span>
                                      </div>
                                      
                                    </div>
                                    <div class='uk-width-1-1 uk-clearfix uk-margin-remove'>
                                      
                                    </div>
                                  </div>
                                </div>";
                              }
                              ?>
                                
                                <!-- <div class="tm-menu-item tm-menu-compound">
                                  <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-4-5">
                                      <div class="uk-margin-small-left">
                                        <div class="uk-width-1-1">
                                          <h3 class="tm-menu-name uk-margin-small-bottom">منو محمد رضا!</h3>
                                          <span class="tm-menu-dots"></span>
                                          <span class="tm-menu-price uk-float-right">
                                            <i class="uk-link uk-icon-cart-plus uk-icon-small"></i>
                                          </span>
                                        </div>
                                        <div class="discount" style="height: 30px;">
                                          <div class="uk-float-left uk-text-success uk-width-1-3">تعداد: ۲</div>
                                          <span class="tm-menu-price uk-float-left">  
                                            <div class="uk-clearfix uk-text-success">۲۳۰۰۰ تومان</div>
                                          </span>
                                        </div>
                                        <div class="uk-width-1-1">
                                          <span class="tm-menu-desc uk-text-small uk-text-small">
                                            <strong>سفارش:</strong> کباب بره، ماست، تخم مرغم واسه محمد رضا :P !
                                            <span class="uk-text-small">{۱۴۰۱/۱۲/۳۰}</span>
                                          
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="tm-menu-item tm-menu-compound">
                                  <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-4-5">
                                      <div class="uk-margin-small-left">
                                        <div class="uk-width-1-1">
                                          <h3 class="tm-menu-name uk-margin-small-bottom">تهناییه تهنایی</h3>
                                          <span class="tm-menu-dots"></span>
                                          <span class="tm-menu-price uk-float-right">
                                            <i class="uk-link uk-icon-cart-plus uk-icon-small"></i>
                                          </span>
                                        </div>
                                        <div class="discount" style="height: 30px;">
                                          <div class="uk-float-left uk-text-success uk-width-1-3">تعداد: ۱</div>
                                          <span class="tm-menu-price uk-float-left">  
                                            <div class="uk-clearfix uk-text-success">۲۵۰۰۰۰ تومان</div>
                                          </span>
                                        </div>
                                        <div class="uk-width-1-1">
                                          <span class="tm-menu-desc uk-text-small uk-text-small">
                                           <strong>سفارش:</strong>  شیش لیک، کباب غاز
                                           <span class="uk-text-small">{۱۳۹۵/۰۱/۲۵}</span>
                                          
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> -->


                            </div>
                          </li>


                        </ul>
                          {pagination}

                        <!-- slideset navigation -->
                        
                          </div>
                        </div>

                        <button id="order-button" class="uk-button uk-button-mini uk-width-1-5" style="position:fixed;right:30px;bottom: 10px;z-index: 100;" data-uk-offcanvas="{target:'#offcanvas-1'}"><?=lang('menu_order_list_title')?></button>
                        <div id="offcanvas-1" class="uk-offcanvas">
                          <div class="uk-offcanvas-bar"  style="width: 90%; max-width: 300px;">

                            <ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav="">
                              <div class="uk-panel">
                                <h3 class="uk-panel-title"><?=lang('menu_order_list_title')?></h3> 
                                  <span id="order-list"></span>
                                  <div class="uk-margin-top">
                                  </div>
                                  <a href="<?=base_url('mybasket')?>" class="uk-button uk-button-success uk-margin-top uk-margin-bottom uk-align-center uk-width-2-3">بررسی نهایی</a>
                              </div>
                            </ul>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </article>
          </main>
        </div>
      </div>
  <script type="text/javascript">
    URL='<?=base_url()?>';
    $(function(){
      function successBasket(data)
      {
        $('#order-list').html(data);
        if(data.length>50)
          $('#order-button').addClass('uk-button-success');
        else
          $('#order-button').removeClass('uk-button-success');
      }
      $(document).on('click','.add-order',function(){
        orderId=$(this).attr('order-id');
        $.ajax({
            url: URL + 'basket',
            type: 'post',
            data: {action: 'addorder',orderId},
            success: function (data) {
                successBasket(data);
                
                // ajaxEnd();
            },error:function(){
                ajaxEnd();
                myPrompt('changeName');
            }
        });
      });
      $(document).on('click','.add-basket',function(){
        foodId=$(this).attr('food-id');
        $.ajax({
            url: URL + 'basket',
            type: 'post',
            data: {action: 'addfood',foodId},
            success: function (data) {
                successBasket(data);
                
                // ajaxEnd();
            },error:function(){
                ajaxEnd();
                myPrompt('changeName');
            }
        });
      });
      $(document).on('click','.reduce-basket',function(){
        foodId=$(this).attr('food-id');
        $.ajax({
            url: URL + 'basket',
            type: 'post',
            data: {action: 'reducefood',foodId},
            success: function (data) {
                successBasket(data);
                
                // ajaxEnd();
            },error:function(){
                ajaxEnd();
                myPrompt('changeName');
            }
        });

      });
      $(document).on('click','.delete-basket',function(){
        foodId=$(this).attr('food-id');
        $.ajax({
            url: URL + 'basket',
            type: 'post',
            data: {action: 'deletefood',foodId},
            success: function (data) {
                successBasket(data);
                
                // ajaxEnd();
            },error:function(){
                ajaxEnd();
                myPrompt('changeName');
            }
        });

      });
      $.ajax({
          url: URL + 'basket',
          type: 'post',
          data: {},
          // dataType: 'json',
          success: function (data) {
              // alert();
              successBasket(data);
              
              // ajaxEnd();
          },error:function(e){
              // ajaxEnd();
              console.log(e);
              myPrompt('changeName');
          }
        });
    });
  </script>