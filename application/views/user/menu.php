<?php
global $defines,$langsShow;

?>
  <!-- header-a -->
  <div id="tm-header-a" class="tm-block-header-a uk-block uk-block-default tm-block-fullwidth tm-grid-collapse ">
    <div class="uk-container uk-container-center">
      <section class="tm-header-a uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}">

        <div class="uk-width-1-1">
          <div class="uk-panel uk-text-center uk-contrast tm-overlay-secondary tm-header-height">
            <div class="tm-background-cover uk-cover-background uk-flex uk-flex-center uk-flex-middle" style="background-position: 50% 0px; background-image:url(images/background/bg-image-4.jpg)" data-uk-parallax="{bg: '-200'}">
              <div class="uk-position-relative uk-container">

                <div data-uk-parallax="{opacity: '1,0', y: '-50'}">

                  <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:300}">
                    <h1 class="uk-module-title-alt uk-margin-top"><?=lang('menu_slider_title')?></h1>
                  </div>

                  <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:800}">
                    <h5 class="uk-sub-title-small"><?=lang('menu_slider_text')?></h5>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>

  <!-- main content -->
  <div id="tm-main" class="tm-block-main uk-block uk-block-default">
    <div class="uk-container uk-container-center">
      <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
        <div class="tm-main uk-width-medium-1-1">
          <main id="tm-content" class="tm-content">
            <div id="system-message-container"></div>
            <article class="uk-article tm-article">
              <div class="tm-article-wrapper">

                <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">


                  <div class="tm-article">

                    <div class="tm-slideset-gaucho" data-uk-slideset="{default: 1, animation: 'fade'}">

                    <!-- data filters -->
                      <ul class="uk-subnav uk-flex-center">
                        <li class="{persian}"><a href="<?=base_url()."?foodtype=".$langsShow['persian']?>"><?=$langsShow['persian']?></a></li>
                        <li class="{english}"><a href="<?=base_url()."?foodtype=".$langsShow['english']?>"><?=$langsShow['english']?></a></li>
                        <li class="{arabic}"><a href="<?=base_url()."?foodtype=".$langsShow['arabic']?>"><?=$langsShow['arabic']?></a></li>
                      </ul>
                      <?php 
                      // print_r($foods); 
                      $menus='';
                      $contents='';
                      for($i=0;$i<sizeof($foods);$i++) 
                      {
                        $food=$foods[$i];
                        if($i==0 || $foods[$i-1]->fcName!=$foods[$i]->fcName)
                        {
                          $menus.="<li data-uk-filter='$food->fcName'><a href='#'>$food->fcName</a></li>";
                          $contents.="<li data-uk-filter='$food->fcName'>
                                        <div class='uk-panel uk-text-left'>
                                          <div class='uk-grid uk-grid-width-medium-1-2 uk-grid-width-large-1-2' data-uk-grid-margin>
                                            ";
                        }
                        $chefFood=($food->foChefFood)?"":"";
                        $discounted=$food->foPrice-(($food->foPrice*$food->foDiscount)/100);
                        $discount=($food->foDiscount)?"
                                        <div class='discount' style='height: 45px;'>
                                          <div class='uk-badge-note uk-badge'>$food->foDiscount% تخفیف</div>
                                          <span class='tm-menu-price uk-float-left'>  
                                            <del class='uk-float-right uk-text-small'> $food->foPrice تومان</del>
                                            <div class='uk-clearfix uk-text-success'>$discounted تومان</div>
                                          </span>
                                        </div>
                                        ":"
                                        <div class='uk-margin-top-remove' style='height: 30px;'>
                                          <span class='tm-menu-price uk-text-success'>$food->foPrice تومان</span>
                                        </div>
                                        ";
                      $contents.=             "<div class='tm-menu-item tm-menu-compound'>
                                                <div class='uk-grid' data-uk-grid-margin>
                                                  <div class='uk-width-1-5'>
                                                    <img class='uk-align-left uk-thumbnail uk-border-circle' src='".base_url()."upload/foodimages/".$food->foId.".jpg"."' width='72' height='72' alt='classic nachos'>
                                                  </div>
                                                  <div class='uk-width-4-5'>
                                                    <div class='uk-margin-small-left'>
                                                      <div class='uk-width-1-1'>
                                                        <h3 class='tm-menu-name uk-margin-small-bottom'>$food->foName</h3>
                                                        <span class='tm-menu-dots'></span>
                                                        <span class='tm-menu-price uk-float-right'>
                                                          <i food-id='$food->foId' class='uk-link uk-icon-cart-plus uk-icon-small uk-margin-small-right add-basket'></i>
                                                        </span>
                                                      </div>
                                                      $discount
                                                      <span class='tm-menu-desc uk-text-small'>$food->foDescription</span>
                                                      $chefFood
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>";
                        if($i==sizeof($foods))
                        {
                          $contents.="      </div>
                                          </div>
                                        
                                      </li>";
                        }
                      }
                      $deserts='';
                      foreach ($foodDeserts as $foodDesert) 
                      {
                        $deserts.="
                        <div class='tm-menu-item'>
                          <div class='uk-float-left'>
                            <i class='uk-link uk-icon-cart-plus uk-icon-small uk-margin-small-right add-basket' food-id='$foodDesert->foId'></i>
                          </div>
                          <h3 class='tm-menu-name uk-margin-top-remove'>$foodDesert->foName</h3>

                          <span class='tm-menu-dots'></span>
                          <span class='tm-menu-price'>$foodDesert->foPrice</span>
                        </div>
                        ";
                      }
                      $menus.='';
                      // echo $menus;
                      ?>
                      <ul class="uk-subnav uk-flex-center">
                        <li data-uk-filter=""><a href="#">همه</a></li>
                        <?=$menus?>
                        <li data-uk-filter="deserts"><a href="#">کنار غذا</a></li>
                      </ul>

                      <!-- slideset -->
                      <div class='uk-slidenav-position uk-margin'>
                        <ul class='uk-grid uk-slideset uk-grid-match uk-flex-center'>
                          <?=$contents?>
                          <li data-uk-filter="deserts">
                            <div class="uk-panel uk-text-left">

                              <div class="uk-grid uk-grid-width-medium-1-2 uk-grid-width-large-1-2" data-uk-grid-margin>

                                <div class="uk-panel uk-align-center uk-panel-space">
                                  <?=$deserts?>
                                </div>

                              </div>

                            </div>
                          </li>
                        </ul>

                        <!-- slideset navigation -->
                        <ul class="uk-slideset-nav uk-dotnav uk-flex-center uk-margin-bottom-remove">
                          <li data-uk-slideset-item="0"><a></a></li>
                        </ul>
                        <button id="order-button" class="uk-button uk-button-mini uk-width-1-5" style="position:fixed;right:30px;bottom: 10px;z-index: 100;" data-uk-offcanvas="{target:'#offcanvas-1'}"><?=lang('menu_order_list_title')?></button>
                        <div id="offcanvas-1" class="uk-offcanvas">
                          <div class="uk-offcanvas-bar"  style="width: 90%; max-width: 300px;">

                            <ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav="">
                              <div class="uk-panel">
                                <h3 class="uk-panel-title"><?=lang('menu_order_list_title')?></h3> 
                                  <span id="order-list"></span>
                                  <div class="uk-margin-top">
                                  </div>
                                  <a href="<?=base_url('mybasket')?>" class="uk-button uk-button-success uk-margin-top uk-margin-bottom uk-align-center uk-width-2-3">بررسی نهایی</a>
                              </div>
                            </ul>
                          </div>
                        </div>


                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </article>
          </main>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    URL='<?=base_url()?>';
    $(function(){
      function successBasket(data)
      {
        $('#order-list').html(data);
        if(data.length>50)
          $('#order-button').addClass('uk-button-success');
        else
          $('#order-button').removeClass('uk-button-success');
      }
      $(document).on('click','.add-basket',function(){
        foodId=$(this).attr('food-id');
        $.ajax({
            url: URL + 'basket',
            type: 'post',
            data: {action: 'addfood',foodId},
            success: function (data) {
                successBasket(data);
                
                // ajaxEnd();
            },error:function(){
                ajaxEnd();
                myPrompt('changeName');
            }
        });
      });
      $(document).on('click','.reduce-basket',function(){
        foodId=$(this).attr('food-id');
        $.ajax({
            url: URL + 'basket',
            type: 'post',
            data: {action: 'reducefood',foodId},
            success: function (data) {
                successBasket(data);
                
                // ajaxEnd();
            },error:function(){
                ajaxEnd();
                myPrompt('changeName');
            }
        });

      });
      $(document).on('click','.delete-basket',function(){
        foodId=$(this).attr('food-id');
        $.ajax({
            url: URL + 'basket',
            type: 'post',
            data: {action: 'deletefood',foodId},
            success: function (data) {
                successBasket(data);
                
                // ajaxEnd();
            },error:function(){
                ajaxEnd();
                myPrompt('changeName');
            }
        });

      });
      $.ajax({
          url: URL + 'basket',
          type: 'post',
          data: {},
          // dataType: 'json',
          success: function (data) {
              // alert();
              successBasket(data);
              
              // ajaxEnd();
          },error:function(e){
              // ajaxEnd();
              console.log(e);
              myPrompt('changeName');
          }
        });
    });
  </script>