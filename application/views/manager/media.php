<?php global $mediaTypes,$langsShow;?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            آپلود مدیا
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> مدیریت</a></li>
            <li class="active">آپلود مدیا</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box">
                    <?php   
                    $editUrl=(gets('edit'))?'&edit='.gets('edit'):'';
                    $mediajoin=@$mediajoin[0];
                    // print_r($mediajoin);
                    ?>
                    <div class="row margin-bottom">
                        <div class="col-sm-12 text-center">
                            <?=form_error('lang')?form_error('lang').'<br>':''?>
                                
                            <label class="btn btn-primary padding-small-right padding-small-left {persian}">
                                <a href="<?=manager_url('media?lang=persian'.$editUrl)?>" class="text-white"> ایرانی</a>
                            </label>
                            <label class="btn btn-primary padding-small-right padding-small-left {english}">
                                <a href="<?=manager_url('media?lang=english'.$editUrl)?>" class="text-white"> انگلیسی</a>
                            </label>
                            <label class="btn btn-primary padding-small-right padding-small-left {arabic}">
                                <a href="<?=manager_url('media?lang=arabic'.$editUrl)?>" class="text-white"> عربی</a>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <?php 
                            if(!gets('edit'))
                            { ?>
                                <?=form_error('type')?form_error('type').'<br>':''?>
                                <label class="btn btn-primary padding-small-right padding-small-left {picture}">
                                    <a href="<?=manager_url('media?mediatype=picture&lang='.@gets('lang').$editUrl)?>" class="text-white">عکس جدید</a>
                                </label>
                                <label class="btn btn-primary padding-small-right padding-small-left {picture}">
                                    <a href="<?=manager_url('media?mediatype=music&lang='.@gets('lang').$editUrl)?>" class="text-white">موسیقی جدید</a>
                                </label>
                                <label class="btn btn-primary padding-small-right padding-small-left {video}">
                                    <a href="<?=manager_url('media?mediatype=video&lang='.@gets('lang').$editUrl)?>" class="text-white">فیلم جدید</a>
                                </label>
                                <label class="btn btn-primary padding-small-right padding-small-left {game}">
                                    <a href="<?=manager_url('media?mediatype=game&lang='.@gets('lang').$editUrl)?>" class="text-white">بازی جدید</a>
                                </label>
                                <label class="btn btn-primary padding-small-right padding-small-left {text}">
                                    <a href="<?=manager_url('media?mediatype=text&lang='.@gets('lang').$editUrl)?>" class="text-white"> متن جدید</a>
                                </label>
                            <?php } ?>
                        </div>
                    </div>
                    <?php 
                    echo form_open_multipart(this_url(),'rool="form"');
                    ?>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="box-header with-border">
                                <h3 class="box-title">برای زبان <?=@$langsShow[@gets('lang')]?> {message}
                                </h3>
                                <p class="margin-top">
                                 <?php
                                    // print_r($medialang);
                                    foreach ((array)@$medialang as $medialang1) 
                                    {
                                        echo "موضوع:";
                                        if($medialang1->mlLang=='ایرانی')    
                                        {
                                            echo $medialang1->mlSubject;
                                            break;
                                        }
                                        elseif($medialang1->mlLang=='english')    
                                        {
                                            echo $medialang1->mlSubject;
                                            break;
                                        }
                                        elseif($medialang1->mlLang=='العربیه')    
                                        {
                                            echo $medialang1->mlSubject;
                                            break;
                                        }
                                    }
                                ?>
                                </p>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <?php
                                $options1[]='';
                                foreach ($mediaCategorys as $mediaCategory) 
                                {
                                    $options1[$mediaCategory->mcId]=$mediaCategory->mcName;
                                }
                                $js1 = array(
                                        'onChange' => "select_subcategory(this,'persian');"
                                );
                                $options2[]='';
                                if((gets('edit')))
                                {
                                    foreach ($mediaSubCategorys as $mediaSubCategory) 
                                    {
                                        $options2[$mediaSubCategory->mscId]=$mediaSubCategory->mscName;
                                    }                                    
                                }
                                ?>
                                <div class="form-group">
                                    <?php
                                    echo form_label('انتخاب دسته مدیا'.': '.form_error('categoryId'),'categoryId');
                                    echo form_dropdown(array('name'=>'categoryId','id'=>'categoryId','class'=>'form-control'), $options1, set_value('categoryId',@$mediajoin->mcId),$js1);
                                    ?>
                                </div>
                                <div class="form-group">
                                    <?php
                                    echo form_label('انتخاب زیر دسته مدیا'.': '.form_error('subCategoryId'),'subCategoryId');
                                    echo form_dropdown(array('name'=>'subCategoryId','id'=>'subCategoryId','class'=>'form-control'), $options2, set_value('subCategoryId',@$mediajoin->mscId));
                                    ?>
                                </div>
                                <div class="form-group">
                                    <?php
                                    // print_r($mediajoin);
                                    echo form_label('موضوع'.': '.form_error('subject'),'subject');
                                    echo form_input(array('name'=>'subject','id'=>'subject','class'=>'form-control'),set_value('subject',@$mediajoin->mlSubject));
                                    echo (gets('edit'))?form_hidden('type',@$mediajoin->meType):form_hidden('type',@$mediaTypes[gets('mediatype')]);
                                    echo ($mediajoin->mlLang)?form_hidden('lang',@$mediajoin->mlLang):form_hidden('lang',@$langsShow[gets('lang')]);
                                    echo ($mediajoin->mlId)?form_hidden('mlId',@$mediajoin->mlId):'';
                                    ?>
                                </div>
                                <div class="form-group">
                                    <?php
                                    echo form_label('توضیحات'.': '.form_error('text'),'text');
                                    echo $this->ckeditor->editor("text",set_value('text',@$mediajoin->mlText));
                                    ?>
                                </div>
                                
                            </div>
                        </div>
                            
                    </div>

                    
                        <div class="row">
                            <div class="col-md-12">
                                <?php 
                                echo (gets('edit'))?
                                form_submit('insert','ثبت این مدیای > آپلود فایل',array('class'=>'btn btn-primary col-md-4 col-xs-offset-4 margin-top text-center'))
                                .form_submit('insert1','ثبت این مدیای > مدیای جدید',array('class'=>'btn btn-primary col-md-4 col-xs-offset-4 margin-top text-center'))
                                :
                                form_submit('update',' ویرایش یا اضافه کردن زبان',array('class'=>'btn btn-primary col-md-4 col-xs-offset-4 margin-top text-center'));
                                ?>
                            </div><!-- /.box-body -->
                            <div class="col-md-6"></div>
                        </div><!-- /.box-body -->
                        
                    </div><!-- /.box-body -->
                <?php echo form_close();?>
                </div><!--/.col (right) -->
            </div>
        </div>   <!-- /.row -->
        <?php if((gets('edit'))): ?>
        <div class="row">
            <div class="box">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">نمایش فایل</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <video controls preload="false" class="" style="max-width: 100%;" width="" height="">
                                <source src="<?=asset_url()?>movies/1.mp4" type="video/mp4">
                                <source src="<?=asset_url()?>movies/1.mp4" type="video/ogg">
                            </video>
                        </div><!-- /.box-body -->
                    </div><!-- /.box-body -->
                </div><!-- /.box-body -->
            </div><!-- /.box-body -->
        </div><!-- /.box-body -->
        <?php endif;?>
    </section><!-- /.content -->
</div>
<script type="text/javascript">
    MGURL='<?=manager_url()?>';
    mediatype='<?=@$mediaTypes[@gets('mediatype')]?>';
    function select_subcategory(mthis,lang)
    {
        categoryId=mthis.value;
        $.ajax({
            url: MGURL + '/media',
            type: 'post',
            data: {action: 'selectSubCategory',categoryId},
            dataType: 'json',
            success: function (data) {
                // alert();
                $('#subCategoryId').html(data);
                
                // ajaxEnd();
            },error:function(){
                ajaxEnd();
                myPrompt('changeName');
            }
        });
    }
    $(function(){
        $(document).on('click','#new-file',function(){
            $('#files-input').append('<input type="file" name="file[]" class="margin-small-top">');
        });
        $(document).on('click','#upload',function(){

            if (typeof FormData !== 'undefined') 
            {

                // send the formData
                var formData = new FormData( $("#uploadForm")[0] );

                $.ajax({
                    url : baseUrl + 'uploadImage',  // Controller URL
                    type : 'POST',
                    data : formData,
                    async : false,
                    cache : false,
                    contentType : false,
                    processData : false,
                    success : function(data) {
                        alert();
                    }
                });

            } 
            else 
            {
               message("Your Browser Don't support FormData API! Use IE 10 or Above!");
            }
        });
    });

</script>
<script type="text/javascript" src="<?=asset_url()?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=asset_url()?>ckfinder/ckfinder.js"></script>