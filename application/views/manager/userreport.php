<div class="content-wrapper" style="min-height: 1036px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            گزارش مشتری
            <small>محمد رضا موسوی نژاد</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> مدیریت</a></li>
            <li><a href="#">گزارش مشتری</a></li>
            <li class="active">محمد رضا موسوی نژاد</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>۱۵۰</h3>
                  <p>تعداد سفارش غذا</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>۷۲۰،۰۰۰</h3>
                  <p>میزان خرید</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3>۴۴</h3>
                  <p>تعداد امتیاز</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3>۳۶۵</h3>
                  <p>تعداد بازدید</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
              </div>
            </div><!-- ./col -->
          </div>
          <!-- row -->
          <div class="row">
            <div class="col-md-12">
              <!-- The time line -->
              <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                  <span class="bg-red">
                    ۱۴ بهمن ۱۳۹۴
                  </span>
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                  <i class="fa fa-sign-in bg-blue"></i>
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i> 11:30</span>
                    <h3 class="timeline-header">ورود</h3>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="ion ion-pie-graph bg-aqua"></i>
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i> 11:39</span>
                    <h3 class="timeline-header no-border">سفارش غذا</h3>
                    <div class="timeline-body">
                    <div class="uk-panel">  
                      <table id="example2" class="table table-bordered table-hover">
                        <tbody>
                          <tr>
                            <td>قرمه سبزی</td>
                            <td>۵ پرس</td>
                            <td>۱۲۵۰۰۰ تومان</td>
                          </tr>
                          <tr>
                            <td>نوشابه</td>
                            <td>۳ عدد</td>
                            <td>۱۲۵۰۰ تومان</td>
                          </tr>
                          <tr>
                            <td>خورشت قیمه با ماهی</td>
                            <td>۱۵ پرس</td>
                            <td>۷۰۰۰۰۰ تومان</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </li>
                <li>
                  <i class="fa fa-sign-out bg-blue"></i>
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
                    <h3 class="timeline-header">خروج</h3>
                  </div>
                </li>
                <!-- timeline time label -->
                <li class="time-label">
                  <span class="bg-red">
                    ۱۳ فروردین ۱۴۰۰
                  </span>
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                  <i class="fa fa-sign-in bg-blue"></i>
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i> 11:30</span>
                    <h3 class="timeline-header">ورود</h3>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="ion ion-pie-graph bg-aqua"></i>
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i> 11:39</span>
                    <h3 class="timeline-header no-border">سفارش غذا</h3>
                    <div class="timeline-body">
                    <div class="uk-panel">  
                      <table id="example2" class="table table-bordered table-hover">
                        <tbody>
                          <tr>
                            <td>قرمه سبزی</td>
                            <td>۵ پرس</td>
                            <td>۱۲۵۰۰۰ تومان</td>
                          </tr>
                          <tr>
                            <td>نوشابه</td>
                            <td>۳ عدد</td>
                            <td>۱۲۵۰۰ تومان</td>
                          </tr>
                          <tr>
                            <td>خورشت قیمه با ماهی</td>
                            <td>۱۵ پرس</td>
                            <td>۷۰۰۰۰۰ تومان</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </li>
                <li>
                  <i class="fa fa-sign-out bg-blue"></i>
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
                    <h3 class="timeline-header">خروج</h3>
                  </div>
                </li>
              </ul>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>