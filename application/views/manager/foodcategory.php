<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            ایجاد غذای جدید
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> مدیریت</a></li>
            <li><a href="#">غذا</a></li>
            <li class="active">غذای جدید</li>
        </ol>
    </section>

        
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <!-- Horizontal Form -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">دسته های غذایی {messageDelete}</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <thead>

                                <tr role="row">
                                    <th>#</th>
                                    <th>نام</th>
                                    <th>نوع غذا</th>
                                    <th>وظعیت</th>
                                    <th>ویرایش</th>
                                    <th>حذف</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php global $langsShow; $i=1;?>
                            <?php foreach ($foodCategorys as $foodCategory):?>
                               
                                <tr role="row" class="">
                                    <td class="sorting_1"><?=$i++?></td>
                                    <td><?=$foodCategory->fcName?></td>
                                    <td><?=$foodCategory->fcLang?></td>
                                    <td class="text-center"><a href="<?=url_maker('active',$foodCategory->fcId)?>"><?=$foodCategory->fcActive==1?'<i class="fa fa-toggle-on"></i>':'<i class="fa fa-toggle-off"></i>'?></a></td>
                                    <td class="text-center"><a href="<?=url_maker('edit',$foodCategory->fcId)?>"><i class="fa fa-edit"></i></a></td>
                                    <td class="text-center"><a data-toggle="modal" data-target="#deletemodal" onclick="<?=deleteModal($foodCategory->fcId)?>"><i class="fa fa-trash"></i></a></td>
                                </tr>
                            <?php endforeach;?>
                                
                                

                                
                            </tbody>
                            <tfoot>
                                <tr role="row">
                                    <th class="sorting_asc" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 167px;">#</th>
                                    <th class="sorting" aria-label="Browser: activate to sort column ascending" style="width: 213px;">نام</th>
                                    <th class="sorting" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">نوع غذا</th>
                                    <th class="sorting" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">ویرایش</th>
                                    <th class="sorting" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">حذف</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                    
                </div><!-- /.box-body -->
            </div><!--/.col (right) -->
            <?php
            if((gets('edit')))
            {
                ?>
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">دسته جدید {messageInsert}</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <?php
                            $attr=array('class'=>'dd','id'=>'fdsf');
                            echo form_open('','rool="form"');
                            ?>
                            <div class="form-inline margin-bottom">
                                <div class="margin-bottom"><?=form_error('lang')?></div>
                                <div class="radio">
                                    <label>
                                        <?php
                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['persian'],set_radio('lang',$langsShow['persian']));
                                        ?>
                                        غذای <?=$langsShow['persian']?>
                                    </label>
                                </div>
                                <div class="radio col-md-offset-1">
                                    <label>
                                        <?php
                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['english'],set_radio('lang',$langsShow['english']));
                                        ?>
                                        غذای <?=$langsShow['english']?>
                                    </label>
                                </div>
                                <div class="radio col-md-offset-1">
                                    <label>
                                        <?php
                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['arabic'],set_radio('lang',$langsShow['arabic']));
                                        ?>
                                        غذای <?=$langsShow['arabic']?>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php
                                echo form_label('نام دسته'.': '.form_error('name'),'name');
                                echo form_input(array('name'=>'name','id'=>'name','class'=>'form-control'),set_value('name'));
                                ?>
                            </div>
                            <?php
                            echo form_submit('insert','ثبت دسته جدید',array('class'=>'btn btn-info pull-right'));
                            echo form_close();
                            ?>
                        </div>
                    </div><!-- /.box-body -->
                </div>
                <?php
            }
            else
            {
                ?>
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">ویرایش دسته  {messageUpdate}</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <?php
                            $attr=array('class'=>'dd','id'=>'fdsf');
                            echo form_open(manager_url('foodcategory?edit='.$fcId),'rool="form"');
                            ?>
                            <div class="form-inline margin-bottom">
                                <div class="margin-bottom"><?=form_error('lang')?></div>
                                <div class="radio">
                                    <label>
                                        <?php

                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['persian'],set_radio('lang',$langsShow['persian'],$fcLang==$langsShow['persian']?true:false));
                                        ?>
                                        غذای <?=$langsShow['persian']?>
                                    </label>
                                </div>
                                <div class="radio col-md-offset-1">
                                    <label>
                                        <?php
                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['english'],set_radio('lang',$langsShow['english'],$fcLang==$langsShow['english']?true:false));
                                        ?>
                                        غذای <?=$langsShow['english']?>
                                    </label>
                                </div>
                                <div class="radio col-md-offset-1">
                                    <label>
                                        <?php
                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['arabic'],set_radio('lang',$langsShow['arabic'],$fcLang==$langsShow['arabic']?true:false));
                                        ?>
                                        غذای <?=$langsShow['arabic']?>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php
                                echo form_label('نام دسته'.': '.form_error('name'),'name');
                                echo form_input(array('name'=>'name','id'=>'name','class'=>'form-control'),$fcName);
                                echo form_hidden('catid',$fcId);
                                ?>
                            </div>
                            <?php
                            echo form_submit('update','پیرایش دشته',array('class'=>'btn btn-info pull-right'));
                            echo anchor(manager_url('foodcategory'),'دسته جدید','class=btn btn-info pull-right');
                            ?>
                        </div>
                    </div><!-- /.box-body -->
                </div>
                <?php
            }
            ?>
            
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>
