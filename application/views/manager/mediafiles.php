<?php global $mediaTypes,$langsShow;?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            آپلود مدیا
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> مدیریت</a></li>
            <li class="active">آپلود مدیا</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                    <?php if(sizeof($media)>0){ ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-header with-border">
                                    <h3 class="box-title">
                                    موضوع: 
                                    <?php
                                    // print_r($medialangs);
                                    foreach ((array)@$medialangs as $medialang) 
                                    {
                                        if($medialang->mlLang=='ایرانی')    
                                        {
                                            echo $medialang->mlSubject;
                                            break;
                                        }
                                        elseif($medialang->mlLang=='english')    
                                        {
                                            echo $medialang->mlSubject;
                                            break;
                                        }
                                        elseif($medialang->mlLang=='العربیه')    
                                        {
                                            echo $medialang->mlSubject;
                                            break;
                                        }
                                    }
                                    ?>
                                    </h3>{message}
                                </div><!-- /.box-header -->

                                <?=form_open_multipart(this_url(),'rool="form"')?>
                                <div class="form-group" id="files-input">
                                    <?php
                                    echo form_label('فایل'.': '.@$messageFile,'file');
                                    echo form_upload('file',set_value('file'));
                                    echo form_hidden('mediaId',@gets('upload'));
                                    echo form_submit('upload','upload',array('class'=>'btn btn-primary'));
                                    ?>
                                </div>
                                <?=form_close()?>
                                
                            </div>
                        </div>

                        
                        <div class="row">
                                <?php
                                if(($mediafiles))
                                {

                                    foreach ($mediafiles as $mediafile) 
                                    {
                                        ?>
                                        <div class="col-md-6">
                                            <div class="box box-default">
                                                <div class="box-body">
                                                    <?php
                                                    switch ($media[0]->meType) 
                                                    {
                                                        case '1':
                                                            echo "
                                                            <div class='row'>
                                                                <img src='".base_url()."upload/images/".$mediafile->mfName."' style='max-width:100%;'>
                                                                
                                                            </div>
                                                            <div class='row margin-right margin-top margin-left'>
                                                                <div class='row'>
                                                                    <a data-toggle='modal' class='pull-left' data-target='#deletemodal' onclick=".'"'.deleteModal($mediafile->mfId).'"'."><i class='fa fa-trash'></i></a>
                                                                    <span class='pull-right'>تعداد دیده شده: $mediafile->mfSeen</span>
                                                                </div>
          
                                                                <div class='row'>
                                                                    تعداد دانلود شده: $mediafile->mfDownloaded
                                                                </div>
                                                                <div class='row'>
                                                                    تعداد لایک: $mediafile->mfRate
                                                                </div>
                                                                <div class='row'>
                                                                    سایز: $mediafile->mfSize MB
                                                                </div>
                                                            </div>
                                                            ";
                                                            break;
                                                        case '2':
                                                            echo "
                                                            <div class='row'>
                                                                <video controls preload='false' class='col-md-12' style='max-width:100%;'>
                                                                    <source src='".base_url()."upload/videos/".$mediafile->mfName."'>
                                                                </video>
                                                            </div>
                                                            <div class='row margin-right margin-top margin-left'>
                                                                <div class='row'>
                                                                    <a data-toggle='modal' class='pull-left' data-target='#deletemodal' onclick=".'"'.deleteModal($mediafile->mfId).'"'."><i class='fa fa-trash'></i></a>
                                                                    <span class='pull-right'>تعداد دیده شده: $mediafile->mfSeen</span>
                                                                </div>
          
                                                                <div class='row'>
                                                                    تعداد دانلود شده: $mediafile->mfDownloaded
                                                                </div>
                                                                <div class='row'>
                                                                    تعداد لایک: $mediafile->mfRate
                                                                </div>
                                                                <div class='row'>
                                                                    سایز: $mediafile->mfSize MB
                                                                </div>
                                                            </div>
                                                            ";
                                                            break;
                                                        case '5':
                                                            echo "
                                                            <div class='row'>
                                                                 <audio controls class='col-md-12' preload='none'>
                                                                    <source src='".base_url()."upload/musics/".$mediafile->mfName."' type='audio/ogg'>
                                                                    <source src='".base_url()."upload/musics/".$mediafile->mfName."' type='audio/mpeg'>
                                                                  Your browser does not support the audio element.
                                                                  </audio>
                                                            </div>
                                                            <div class='row margin-right margin-top margin-left'>
                                                                <div class='row'>
                                                                    <a data-toggle='modal' class='pull-left' data-target='#deletemodal' onclick=".'"'.deleteModal($mediafile->mfId).'"'."><i class='fa fa-trash'></i></a>
                                                                    <span class='pull-right'>تعداد دیده شده: $mediafile->mfSeen</span>
                                                                </div>
          
                                                                <div class='row'>
                                                                    تعداد دانلود شده: $mediafile->mfDownloaded
                                                                </div>
                                                                <div class='row'>
                                                                    تعداد لایک: $mediafile->mfRate
                                                                </div>
                                                                <div class='row'>
                                                                    سایز: $mediafile->mfSize MB
                                                                </div>
                                                            </div>
                                                            ";
                                                            break;
                                                        case '3':
                                                            echo "
                                                            <div class='row margin-right'>
                                                                $mediafile->mfRealName
                                                            </div>
                                                            <div class='row margin-right margin-top margin-left'>
                                                                <div class='row'>
                                                                    <a data-toggle='modal' class='pull-left' data-target='#deletemodal' onclick=".'"'.deleteModal($mediafile->mfId).'"'."><i class='fa fa-trash'></i></a> 
                                                                    <span class='pull-right'>تعداد دیده شده: $mediafile->mfSeen</span>
                                                                </div>
          
                                                                <div class='row'>
                                                                    تعداد دانلود شده: $mediafile->mfDownloaded
                                                                </div>
                                                                <div class='row'>
                                                                    تعداد لایک: $mediafile->mfRate
                                                                </div>
                                                                <div class='row'>
                                                                    سایز: $mediafile->mfSize MB
                                                                </div>
                                                            </div>
                                                            ";
                                                            break;
                                                        case '4':
                                                            echo "
                                                            <div class='row margin-right'>
                                                                $mediafile->mfRealName
                                                            </div>
                                                            <div class='row margin-right margin-top margin-left'>
                                                                <div class='row'>
                                                                    <a data-toggle='modal' class='pull-left' data-target='#deletemodal' onclick=".'"'.deleteModal($mediafile->mfId).'"'."><i class='fa fa-trash'></i></a> 
                                                                    <span class='pull-right'>تعداد دیده شده: $mediafile->mfSeen</span>
                                                                </div>
          
                                                                <div class='row'>
                                                                    تعداد دانلود شده: $mediafile->mfDownloaded
                                                                </div>
                                                                <div class='row'>
                                                                    تعداد لایک: $mediafile->mfRate
                                                                </div>
                                                                <div class='row'>
                                                                    سایز: $mediafile->mfSize MB
                                                                </div>
                                                            </div>
                                                            ";
                                                            break;
                                                        
                                                        default:
                                                            # code...
                                                            break;
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>      
                                        <?php
                                    }
                                }
                                else
                                {
                                    ?>
                                    <div class="col-md-12">
                                        <div class="">
                                            <div class="box-body">
                                                فایلی موجود نیست.
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?><!-- /.box-body -->
                        </div> 
                    <?php 
                    }
                    else
                        echo 'مدیایی با این مشخصات موجود نیست';
                    ?>
                    </div> 
                </div>
            </div>
        </div>   
        
    </section>
</div>
<script type="text/javascript">
    MGURL='<?=manager_url()?>';
    mediatype='<?=@$mediaTypes[@gets('mediatype')]?>';
    function select_subcategory(mthis,lang)
    {
        categoryId=mthis.value;
        $.ajax({
            url: MGURL + '/media',
            type: 'post',
            data: {action: 'selectSubCategory',categoryId},
            // dataType: 'json',
            success: function (data) {
                // alert();
                $('#subCategoryId').html(data);
                
                // ajaxEnd();
            },error:function(){
                ajaxEnd();
                myPrompt('changeName');
            }
        });
    }
    $(function(){
        $(document).on('click','#new-file',function(){
            $('#files-input').append('<input type="file" name="file[]" class="margin-small-top">');
        });
        $(document).on('click','#upload',function(){

            if (typeof FormData !== 'undefined') 
            {

                // send the formData
                var formData = new FormData( $("#uploadForm")[0] );

                $.ajax({
                    url : baseUrl + 'uploadImage',  // Controller URL
                    type : 'POST',
                    data : formData,
                    async : false,
                    cache : false,
                    contentType : false,
                    processData : false,
                    success : function(data) {
                        alert();
                    }
                });

            } 
            else 
            {
               message("Your Browser Don't support FormData API! Use IE 10 or Above!");
            }
        });
    });

</script>
<script type="text/javascript" src="<?=asset_url()?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=asset_url()?>ckfinder/ckfinder.js"></script>