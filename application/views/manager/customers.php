<div class="content-wrapper" style="min-height: 946px;">
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        مدیریت مشتریان
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> مدیریت</a></li>
        <li><a href="#">مشتریان</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->


            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">مشتریان {message}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                           <div class="col-sm-4">
                                <?=pgPerPage()?>
                            </div>

                            <div class="col-sm-4 text-center">
                                <label class="btn btn-primary padding-small-right padding-small-left {all}">
                                    <a href="<?=manager_url('customers?lang=all')?>" class="text-white"> همه</a>
                                </label>
                                <label class="btn btn-primary padding-small-right padding-small-left {persian}">
                                    <a href="<?=manager_url('customers?lang=ایرانی')?>" class="text-white"> ایرانی</a>
                                </label>
                                <label class="btn btn-primary padding-small-right padding-small-left {English}">
                                    <a href="<?=manager_url('customers?lang=English')?>" class="text-white"> انگلیسی</a>
                                </label>
                                <label class="btn btn-primary padding-small-right padding-small-left {arabic}">
                                    <a href="<?=manager_url('customers?lang=العربیه')?>" class="text-white"> عربی</a>
                                </label>
                            </div>
                            <div class="col-sm-4 text-center">
                                <form class="form-inline" role='form'>
                                    
                                    <input name="lang" type="hidden" value="<?=@gets('lang')?>"></input>
                                    <input name="search" type="input" class="form-control" value="<?=@gets('search')?>"></input>
                                    <button type="submit" class="form-control">جستجو</button>
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                    <thead>

                                        <tr role="row">
                                            <th>#</th>
                                            <th>نام</th>
                                            <th>کد</th>
                                            <th>شماره همراه</th>
                                            <th>تاریخ تولد</th>
                                            <th>سالگرد ازدواج</th>
                                            <th>زبان</th>
                                            <th>امتیاز</th>
                                            <th>ارسال پیام</th>
                                            <th>اهمیت</th>
                                            <th>گزارش</th>
                                            <th>ویرایش</th>
                                            <th>حذف</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1; 
                                        foreach ($users as $user):?>    
                                        <tr role="row" class="">
                                            <td><?=$i++?></td>
                                            <td><?=$user->usName?></td>
                                            <td><?=$user->usId?></td>
                                            <td><?=$user->usPhone?></td>
                                            <td><?=$user->usBirthday?></td>
                                            <td><?=$user->usAnniversary?></td>
                                            <td><?=$user->usLang?></td>
                                            <td><?=$user->usPoints?></td>
                                            <td class="text-center"><a class="" data-toggle="modal" data-target="#sendMessage" onclick="javascript: return sendMessage('<?=!empty($user->usName)?$user->usName:$user->usId?>','<?=$user->usId?>')"><i class="fa fa-comment-o"></i></a></td>
                                            <td class="text-center"><a href="<?=url_maker('importance',$user->usId)?>"><?=$user->usImportance==1?'<i class="fa fa-heart"></i>':'<i class="fa fa-heart-o"></i>'?></a></td>
                                            <td class="text-center"><a href="<?=base_url('manager/userreport?user='.$user->usId)?>"><i class="fa fa-line-chart"></i></a></td>
                                            <td class="text-center"><a href="<?=base_url('manager/customer?edit='.$user->usId)?>"><i class="fa fa-edit"></i></a></td>
                                            <td class="text-center"><a data-toggle="modal" data-target="#deletemodal" onclick="<?=deleteModal($user->usId)?>"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                    <tfoot>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 167px;">#</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 213px;">نام</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 195px;">شماره همراه</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 144px;">تاریخ تولد</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">سالگرد ازدواج</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">زبان</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">ماهیت</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">ویرایش</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">حذف</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3">
                                <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">نمایش {start} تا {limit} از {countAll}</div>
                            </div>
                            <div class="col-sm-9">
                                {pagination}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            
            <div class="modal fade" tabindex="-1" role="dialog" id="sendMessage" aria-labelledby="gridSystemModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel">ارسال پیام به <span id="username"></span></h4>
                  </div>
                  <div class="modal-body">
                    <?php
                    $attr=array('class'=>'dd','id'=>'fdsf');
                    echo form_open('','rool="form"');
                    ?>
                    <div class="row">
                      <div class="col-md-3">موضوع پیام</div>
                      <div class="col-md-7">
                        <div class="form-group">
                            <?php
                            echo form_label('موضوع'.': '.form_error('subject'),'subject');
                            echo form_input(array('name'=>'subject','id'=>'subject','class'=>'form-control'),set_value('subject'));
                            echo form_input(array('name'=>'userId','id'=>'userId','type'=>'hidden'),set_value('userId'));
                            ?>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3">متن پیام</div>
                      <div class="col-md-7">
                        <div class="form-group">
                            <?php
                            echo form_label('موضوع'.': '.form_error('text'),'text');
                            echo form_textarea(array('name'=>'text','id'=>'text','rows'=>'4','class'=>'form-control'),set_value('text'));
                            ?>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
                    
                    <?php
                    echo form_submit('sendMessage','ارسال',array('class'=>'btn btn-primary'));
                    ?></div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

        </div>
        <!-- /.row -->
</section>
<!-- /.content -->
</div>
<script type="text/javascript">
    function sendMessage(name,id)
    {
        $('#username').html(name);
        $('#userId').val(id);
    }
</script>