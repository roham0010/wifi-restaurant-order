<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            مدیریت مدیا
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>مدیریت</a></li>
            <li><a href="#">مدیا</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form -->


                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">مدیا {message}</h3>
                        <a href="<?=manager_url('media')?>" class="pull-left btn btn-link text-medium "><small><i class=" fa fa-file margin-small-left "></i>مدیای جدید</small></a>
                    </div>
                    <div class="row">

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-4 col-md-offset-4 margin-bottom text-center">
                                    <label class="btn btn-primary padding-small-right padding-small-left {alllang}">
                                        <a href="<?=manager_url('medias?lang=alllang')?>" class="text-white"> همه</a>
                                    </label>
                                    <label class="btn btn-primary padding-small-right padding-small-left {persian}">
                                        <a href="<?=manager_url('medias?lang=ایرانی')?>" class="text-white"> ایرانی</a>
                                    </label>
                                    <label class="btn btn-primary padding-small-right padding-small-left {English}">
                                        <a href="<?=manager_url('medias?lang=English')?>" class="text-white"> انگلیسی</a>
                                    </label>
                                    <label class="btn btn-primary padding-small-right padding-small-left {arabic}">
                                        <a href="<?=manager_url('medias?lang=العربیه')?>" class="text-white"> عربی</a>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <?=pgPerPage()?>
                                </div>
                                <div class="col-md-4 text-center">
                                    <?=form_error('type')?form_error('type').'<br>':''?>
                                    <label class="btn btn-primary padding-small-right padding-small-left {all}">
                                        <a href="<?=manager_url('medias?mediatype=all&lang='.@gets('lang'))?>" class="text-white">همه</a>
                                    </label>
                                    <label class="btn btn-primary padding-small-right padding-small-left {picture}">
                                        <a href="<?=manager_url('medias?mediatype=picture&lang='.@gets('lang'))?>" class="text-white">عکس ها</a>
                                    </label>
                                    <label class="btn btn-primary padding-small-right padding-small-left {music}">
                                        <a href="<?=manager_url('medias?mediatype=music&lang='.@gets('lang'))?>" class="text-white">موسیقی</a>
                                    </label>
                                    <label class="btn btn-primary padding-small-right padding-small-left {video}">
                                        <a href="<?=manager_url('medias?mediatype=video&lang='.@gets('lang'))?>" class="text-white">فیلم ها</a>
                                    </label>
                                    <label class="btn btn-primary padding-small-right padding-small-left {game}">
                                        <a href="<?=manager_url('medias?mediatype=game&lang='.@gets('lang'))?>" class="text-white">بازی ها</a>
                                    </label>
                                    <label class="btn btn-primary padding-small-right padding-small-left {text}">
                                        <a href="<?=manager_url('medias?mediatype=text&lang='.@gets('lang'))?>" class="text-white"> متن ها</a>
                                    </label>
                                </div>
                                <div class="col-sm-4 text-center">
                                    <form class="form-inline" role='form'>
                                        <input name="lang" type="hidden" value="<?=@gets('lang')?>"></input>
                                        <input name="mediatype" type="hidden" value="<?=@gets('mediatype')?>"></input>
                                        <input name="search" type="input" class="form-control" value="<?=@gets('search')?>"></input>
                                        <button type="submit" class="form-control">جستجو</button>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                        <thead>

                                            <tr role="row">
                                                <th>#</th>
                                                <th>نام</th>
                                                <th>دسته</th>
                                                <th>زیردسته</th>
                                                <th>نوع مدیا</th>
                                                <th>دیده شده</th>
                                                <th>موردعلاقه</th>
                                                <th>دانلود</th>
                                                <th>ویرایش زبانها</th>
                                                <th>ویرایش فایلها</th>
                                                <th>حذف</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=1; 
                                            global $mediaTypesShow;
                                            // $arrayTypesIcon=array(1=>'file-image-o',2=>'file-video-o',3=>'gamepad',4=>'file-pdf-o');
                                            $arrayTypesIcon=array(1 =>'file-image-o',2 =>'file-video-o',3 =>'gamepad',4 =>'file-pdf-o',5 =>'file-audio-o');
                                            foreach ($medias as $media):?>    
                                            <tr role="row" class="">
                                                <td><?=$i++?></td>
                                                <td><?=$media->mlSubject?></td>
                                                <td><?=$media->mcName?></td>
                                                <td><?=$media->mscName?></td>
                                                <td><i class="fa fa-<?=$arrayTypesIcon[$media->meType]?>"></i> <?=$mediaTypesShow[$media->meType]?></td>
                                                <td><?=$media->mfSeen?></td>
                                                <td><?=$media->mfRate?></td>
                                                <td><?=$media->mfDownloaded?></td>
                                                <td class="text-center"><a href="<?=base_url('manager/media?edit='.$media->meId)?>"><i class="fa fa-edit"></i></a></td>
                                                <td class="text-center"><a href="<?=base_url('manager/mediafiles?upload='.$media->meId)?>"><i class="fa fa-file-text"></i></a></td>
                                                <td class="text-center"><a data-toggle="modal" data-target="#deletemodal" onclick="<?=deleteModal($media->meId)?>"><i class="fa fa-trash"></i></a></td>
                                            </tr>
                                            <?php endforeach;?>                                            
                                        </tbody>
                                        <tfoot>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 167px;">#</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 213px;">نام</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 195px;">شماره همراه</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 144px;">تاریخ تولد</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">سالگرد ازدواج</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">زبان</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">ماهیت</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">ویرایش</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">حذف</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">نمایش {start} تا {limit} از {countAll}</div>
                                </div>
                                <div class="col-sm-7">
                                    {pagination}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>


            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
    </div>
