<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            مدیریت دسته های مدیا
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> مدیریت</a></li>
            <li><a href="#">دسته های مدیا</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="row col-md-12">
                <div class="box">
                    <?php if((gets('edit'))): ?>
                        <div class="col-md-6 col-md-offset-3 text-center margin-small-bottom margin-small-top">
                        <?=form_error('type')?form_error('type').'<br>':''?>
                            <label class="btn btn-primary padding-small-right padding-small-left {all}">
                                <a href="<?=manager_url('mediacategory?mediatype=all')?>" class="text-white">همه</a>
                            </label>
                             <label class="btn btn-primary padding-small-right padding-small-left {picture}">
                                <a href="<?=manager_url('mediacategory?mediatype=picture')?>" class="text-white">عکس جدید</a>
                            </label>
                            <label class="btn btn-primary padding-small-right padding-small-left {music}">
                                <a href="<?=manager_url('mediacategory?mediatype=music')?>" class="text-white">موسیقی جدید</a>
                            </label>
                            <label class="btn btn-primary padding-small-right padding-small-left {video}">
                                <a href="<?=manager_url('mediacategory?mediatype=video')?>" class="text-white">فیلم جدید</a>
                            </label>
                            <label class="btn btn-primary padding-small-right padding-small-left {game}">
                                <a href="<?=manager_url('mediacategory?mediatype=game')?>" class="text-white">بازی جدید</a>
                            </label>
                            <label class="btn btn-primary padding-small-right padding-small-left {text}">
                                <a href="<?=manager_url('mediacategory?mediatype=text')?>" class="text-white"> متن جدید</a>
                            </label>
                        </div>
                    <?php endif;?>
                    <div class="box-body">
                    
            <div class="col-md-6">
                <!-- Horizontal Form -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">دسته های مدیایی {messageDelete}</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <thead>

                                <tr role="row">
                                    <th>#</th>
                                    <th>نام</th>
                                    <th>نوع مدیا</th>
                                    <th>وظعیت</th>
                                    <th>ویرایش</th>
                                    <th>حذف</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php global $langsShow,$mediaTypes; $i=1; ?>
                            <?php foreach ($mediaCategorys as $mediacategory):?>
                               
                                <tr role="row" class="">
                                    <td class="sorting_1"><?=$i++?></td>
                                    <td><?=$mediacategory->mcName?></td>
                                    <td><?=$mediacategory->mcLang?></td>
                                    <td class="text-center"><a href="<?=url_maker('active',$mediacategory->mcId)?>"><?=$mediacategory->mcActive==1?'<i class="fa fa-toggle-on"></i>':'<i class="fa fa-toggle-off"></i>'?></a></td>
                                    <td class="text-center"><a href="<?=url_maker('edit',$mediacategory->mcId)?>"><i class="fa fa-edit"></i></a></td>
                                    <td class="text-center"><a data-toggle="modal" data-target="#deletemodal" onclick="<?=deleteModal($mediacategory->mcId)?>"><i class="fa fa-trash"></i></a></td>
                                </tr>
                            <?php endforeach;?>
                                
                                

                                
                            </tbody>
                            <tfoot>
                                <tr role="row">
                                    <th class="sorting_asc" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 167px;">#</th>
                                    <th class="sorting" aria-label="Browser: activate to sort column ascending" style="width: 213px;">نام</th>
                                    <th class="sorting" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">نوع مدیا</th>
                                    <th class="sorting" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">ویرایش</th>
                                    <th class="sorting" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">حذف</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                    
                </div><!-- /.box-body -->
            </div><!--/.col (right) -->
            <?php
            if((gets('edit')))
            {
                ?>
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">دسته جدید {messageInsert}</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <?php
                            $attr=array('class'=>'dd','id'=>'fdsf');
                            echo form_open(this_url(),'rool="form"');
                            ?>
                            <div class="form-inline margin-bottom">
                                <div class="margin-bottom"><?=form_error('lang')?></div>
                                <div class="radio">
                                    <label>
                                        <?php
                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['persian'],set_radio('lang',$langsShow['persian']));
                                        ?>
                                        مدیای <?=$langsShow['persian']?>
                                    </label>
                                </div>
                                <div class="radio col-md-offset-1">
                                    <label>
                                        <?php
                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['english'],set_radio('lang',$langsShow['english']));
                                        ?>
                                        مدیای <?=$langsShow['english']?>
                                    </label>
                                </div>
                                <div class="radio col-md-offset-1">
                                    <label>
                                        <?php
                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['arabic'],set_radio('lang',$langsShow['arabic']));
                                        ?>
                                        مدیای <?=$langsShow['arabic']?>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php
                                echo form_label('نام دسته'.': '.form_error('name'),'name');
                                echo form_input(array('name'=>'name','id'=>'name','class'=>'form-control'),set_value('name'));
                                echo form_hidden('type',@$mediaTypes[gets('mediatype')]);
                                ?>
                            </div>
                            <?php
                            echo form_submit('insert','ثبت دسته جدید',array('class'=>'btn btn-info pull-right'));
                            echo anchor(manager_url('mediasubcategory'),'زیر دسته ها',array("class"=>'btn  '));
                            echo form_close();
                            ?>
                        </div>
                    </div><!-- /.box-body -->
                </div>
                <?php
            }
            else
            {
                ?>
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">ویرایش دسته  {messageUpdate}</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <?php
                            $attr=array('class'=>'dd','id'=>'fdsf');
                            echo form_open(manager_url('mediacategory?edit='.$mcId),'rool="form"');
                            ?>
                            <div class="form-inline margin-bottom">
                                <div class="margin-bottom"><?=form_error('lang')?></div>
                                <div class="radio">
                                    <label>
                                        <?php

                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['persian'],set_radio('lang',$langsShow['persian'],$mcLang==$langsShow['persian']?true:false));
                                        ?>
                                        مدیای <?=$langsShow['persian']?>
                                    </label>
                                </div>
                                <div class="radio col-md-offset-1">
                                    <label>
                                        <?php
                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['english'],set_radio('lang',$langsShow['english'],$mcLang==$langsShow['english']?true:false));
                                        ?>
                                        مدیای <?=$langsShow['english']?>
                                    </label>
                                </div>
                                <div class="radio col-md-offset-1">
                                    <label>
                                        <?php
                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['arabic'],set_radio('lang',$langsShow['arabic'],$mcLang==$langsShow['arabic']?true:false));
                                        ?>
                                        مدیای <?=$langsShow['arabic']?>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php
                                echo form_label('نام دسته'.': '.form_error('name'),'name');
                                echo form_input(array('name'=>'name','id'=>'name','class'=>'form-control'),$mcName);
                                echo form_hidden('catid',$mcId);
                                echo form_hidden('type',$mcType);
                                ?>
                            </div>
                            <?php
                            echo form_submit('update','پیرایش دشته',array('class'=>'btn btn-info pull-right'));
                            echo form_close();
                            echo anchor(manager_url('mediacategory?mediatype='.@gets('all')),'دسته جدید','class=btn btn-info pull-right');
                            ?>
                        </div>
                    </div><!-- /.box-body -->
                </div>
                <?php
            }
            ?>
            
        </div>
        </div>
        </div>

        
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>