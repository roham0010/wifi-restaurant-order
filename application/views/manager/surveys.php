<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            مدیریت نظر سنجی ها
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> مدیریت</a></li>
            <li><a href="#">نظر سنجی ها</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form -->


                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">لیست نظر سنجی ها<small> (دفت کنید درلحظه یک نظر سنجی میتواند فعال باشد)</small> {message}</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-4">
                                    <?=pgPerPage()?>
                                </div>

                                <div class="col-sm-4 text-center">
                                </div>
                                <div class="col-sm-4 text-center">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                        <thead>
                                            <tr role="row">
                                                <th>#</th>
                                                <th>سوال</th>
                                                <th>تاریخ ایجاد</th>
                                                <th>تاریخ پایان</th>
                                                <th>گزینه منتخب</th>
                                                <th>مشاهده نتیجه</th>
                                                <!-- <th>راه اندازی دوباره</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=1; 
                                            foreach ($surveys as $survey):
                                                $results='';
                                                $results['suAnswer1']=$survey->suResult1;
                                                $results['suAnswer2']=$survey->suResult2;
                                                $results['suAnswer3']=$survey->suResult3;
                                                $results['suAnswer4']=$survey->suResult4;
                                                $results['suAnswer5']=$survey->suResult5;
                                                $surveyResult=array_keys($results, max($results));
                                                ?>
                                            <tr role="row" class="">
                                                <td><?=$i++?></td>
                                                <td><?=$survey->suQuestion?></td>
                                                <td><?=$survey->suStartDate?></td>
                                                <td><?=$survey->suEndDate?></td>
                                                <td><?=$survey->$surveyResult[0]?></td>
                                                <td class="text-center"><a href="<?=base_url('manager/surveyresult?survey='.$survey->suId)?>"<i class="fa fa-line-chart"></i></a></td>
                                                <!-- <td class="text-center"><a href=""><i class="fa fa-refresh"></i></a></td> -->
                                            </tr>
                                            <?php endforeach;?>

                                            
                                        </tbody>
                                        <tfoot>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 167px;">#</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 213px;">سوال</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 195px;">تاریخ ایجاد</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 144px;">تاریخ پایان</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">مشاهده نتیجه</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">راه اندازی دوباره</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">نمایش {start} تا {limit} از {countAll}</div>
                                </div>
                                <div class="col-sm-9">
                                    {pagination}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>


            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
    </div>
