<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            نتیجه نظرسنجی
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> مدیریت</a></li>
            <li><a href="#">نتیجه نظرسنجی</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box">
                    <div class="box-body">
                      <div class="row">
                        
                        <div class="col-md-8">  
                          <div class="box-header with-border">
                              <h3 class="box-title">{suQuestion}</h3>
                          </div><!-- /.box-header -->
                          <div class="box-body margin-large-top">
                            <canvas id="pieChart" style="height:250px"></canvas>
                          </div><!-- /.box-body -->
                        </div><!-- /.box-body -->

                        <div class="col-md-4">
                          <div class="box-header with-border">
                              <h3 class="box-title">جواب های نظرسنجی</h3>
                          </div><!-- /.box-header -->

                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="info-box">
                              <span class="info-box-icon" style="background-color: #f56954;"><i class="fa fa-pie-chart "></i></span>
                              <div class="info-box-content margin-remove margin-remove">
                                <span class="info-box-text margin-top padding-right"><strong>{suAnswer1}</strong></span>
                                <span class="info-box-text margin-top padding-right">جواب ۱</span>
                              </div><!-- /.info-box-content -->
                            </div><!-- /.info-box -->
                          </div>

                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="info-box">
                              <span class="info-box-icon" style="background-color: #00a65a;"><i class="fa fa-pie-chart"></i></span>
                              <div class="info-box-content margin-remove">
                                <span class="info-box-text margin-top padding-right"><strong>{suAnswer2}</strong></span>
                                <span class="info-box-text margin-top padding-right">جواب ۲</span>
                              </div><!-- /.info-box-content -->
                            </div><!-- /.info-box -->
                          </div>

                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="info-box">
                              <span class="info-box-icon" style="background-color: #f39c12;"><i class="fa fa-pie-chart"></i></span>
                              <div class="info-box-content margin-remove">
                                <span class="info-box-text margin-top padding-right"><strong>{suAnswer3}</strong></span>
                                <span class="info-box-text margin-top padding-right">جواب ۳</span>
                              </div><!-- /.info-box-content -->
                            </div><!-- /.info-box -->
                          </div>

                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="info-box">
                              <span class="info-box-icon" style="background-color: #00c0ef;"><i class="fa fa-pie-chart"></i></span>
                              <div class="info-box-content margin-remove">
                                <span class="info-box-text margin-top padding-right"><strong>{suAnswer4}</strong></span>
                                <span class="info-box-text margin-top padding-right">جواب ۴</span>
                              </div><!-- /.info-box-content -->
                            </div><!-- /.info-box -->
                          </div>

                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="info-box">
                              <span class="info-box-icon" style="background-color: #3c8dbc;"><i class="fa fa-pie-chart"></i></span>
                              <div class="info-box-content margin-remove">
                                <span class="info-box-text margin-top padding-right"><strong>{suAnswer5}</strong></span>
                                <span class="info-box-text margin-top padding-right">جواب ۵</span>
                              </div><!-- /.info-box-content -->
                            </div><!-- /.info-box -->
                          </div>

                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box-body -->
            </div><!--/.col (right) -->
            </div>
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>
    <!-- ChartJS 1.0.1 -->
    <script src="<?=asset_url()?>admin/plugins/chartjs/Chart.min.js"></script>
    <script type="text/javascript">
      //-------------
        //- PIE CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
        var pieChart = new Chart(pieChartCanvas);
        var PieData = [
          {
            value:{suResult1},
            color: "#f56954",
            highlight: "#f56954",
            label: "جواب ۱"
          },
          {
            value:{suResult2},
            color: "#00a65a",
            highlight: "#00a65a",
            label: "جواب ۲"
          },
          {
            value: {suResult3},
            color: "#f39c12",
            highlight: "#f39c12",
            label: "جواب ۳"
          },
          {
            value: {suResult4},
            color: "#00c0ef",
            highlight: "#00c0ef",
            label: "جواب ۴"
          },
          {
            value: {suResult5},
            color: "#3c8dbc",
            highlight: "#3c8dbc",
            label: "جواب ۵"
          }
        ];
        var pieOptions = {
          //Boolean - Whether we should show a stroke on each segment
          segmentShowStroke: true,
          //String - The colour of each segment stroke
          segmentStrokeColor: "#fff",
          //Number - The width of each segment stroke
          segmentStrokeWidth: 2,
          //Number - The percentage of the chart that we cut out of the middle
          percentageInnerCutout: 50, // This is 0 for Pie charts
          //Number - Amount of animation steps
          animationSteps: 100,
          //String - Animation easing effect
          animationEasing: "easeOutBounce",
          //Boolean - Whether we animate the rotation of the Doughnut
          animateRotate: true,
          //Boolean - Whether we animate scaling the Doughnut from the centre
          animateScale: false,
          //Boolean - whether to make the chart responsive to window resizing
          responsive: true,
          // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
          maintainAspectRatio: true,
          //String - A legend template
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        };
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        pieChart.Doughnut(PieData, pieOptions);
    </script>

              