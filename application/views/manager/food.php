<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            ایجاد غذای جدید
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> مدیریت</a></li>
            <li><a href="#">غذا</a></li>
            <li class="active">غذای جدید</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">مشخصات غذا را وارد کنید {message}</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <?php
                                // $attr=array('class'=>'dd','id'=>'fdsf');
                                echo form_open_multipart(manager_url('food?edit='.gets('edit')),'rool="form"'." action='".manager_url('food?edit='.gets('edit'))."'");
                                ?>
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                <!-- <div class="form-inline margin-bottom">
                                    <div class="margin-bottom"><?=form_error('lang')?></div>
                                    <div class="radio">
                                        <label>
                                            <?php

                                            echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),'persian',set_radio('lang','persian'));
                                            ?>
                                            غذای ایرانی
                                        </label>
                                    </div>
                                    <div class="radio col-md-offset-1">
                                        <label>
                                            <?php
                                            echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),'english',set_radio('lang','english'));
                                            ?>
                                            غذای انگلیسی
                                        </label>
                                    </div>
                                    <div class="radio col-md-offset-1">
                                        <label>
                                            <?php
                                            echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),'arabic',set_radio('lang','arabic'));
                                            ?>
                                            غذای عربی
                                        </label>
                                    </div>
                                </div> -->
                                <div class="form-group">
                                    <?php
                                    echo form_label('نام غذا'.': '.form_error('name'),'name');
                                    echo form_input(array('name'=>'name','id'=>'name','class'=>'form-control'),set_value('name',@$foName));
                                    echo isset($foId)?form_hidden('id',@$foId):'';
                                    ?>
                                </div>
                                <div class="form-group">
                                    <?php
                                    echo form_label('قیمت'.': '.form_error('price'),'price');
                                    echo form_input(array('name'=>'price','id'=>'price','class'=>'form-control'),set_value('price',@$foPrice));
                                    ?>
                                </div>
                                <div class="form-group">
                                    <?php
                                    echo form_label('درصد تخفیف'.': '.form_error('discount'),'discount');
                                    echo form_input(array('name'=>'discount','id'=>'discount','class'=>'form-control'),set_value('discount',@$foDiscount));
                                    ?>
                                </div>
                                <div class="form-group">
                                    <?php
                                    echo form_label('گروه غذایی'.': '.form_error('categoryId'),'categoryId');
                                    global $langsShow;
                                    foreach ($foodCategorys as $foodCategory) 
                                    {
                                        $options[$foodCategory['fcId']]=$foodCategory['fcName'].' - '.$foodCategory['fcLang'];
                                    }
                                    $shirts_on_sale = array('1');
                                    echo "<br>".form_dropdown(array('name'=>'categoryId','id'=>'categoryId','class'=>'form-control col-md-8'), $options, set_value('categoryId',@$foCategoryId));
                                    echo anchor(manager_url('foodcategory'),'اضافه کردن گروه جدید',array("class"=>'btn  '));
                                    ?>
                                </div>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <?php
                                            echo form_checkbox(array('name'=>'special','id'=>'special','class'=>''),'1',set_checkbox('special','1',(isset($foSpecial) && $foSpecial==1)?true:''));
                                            ?>
                                            مخصوص
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <?php
                                            echo form_checkbox(array('name'=>'chefFood','id'=>'chefFood','class'=>''),'1',set_checkbox('chefFood','1',(isset($foChefFood) && $foChefFood==1)?true:''));
                                            ?>
                                            غذای سرآشپز
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?php
                                    echo form_label('توضیحات'.': '.form_error('description'),'description');
                                    echo form_textarea(array('name'=>'description','id'=>'description','rows'=>'4','class'=>'form-control'),set_value('description',@$foDescription));
                                    ?>
                                </div>
                                <div class="form-inline margin-bottom">
                                    <div class="margin-bottom"><?=form_error('type')?></div>
                                    <div class="radio margin-right">
                                        <label>
                                            <?php
                                            echo form_radio(array('name'=>'type','id'=>'type','class'=>''),'1',set_radio('type','1',@$foType==1?true:false));
                                            ?>
                                            غذای اصلی
                                        </label>
                                    </div>
                                    <div class="radio margin-right">
                                        <label>
                                            <?php
                                            echo form_radio(array('name'=>'type','id'=>'type1','class'=>''),'0',set_radio('type','0',@$foType==0?true:false));
                                            ?>
                                            مخلفات کنار غذا
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group" id="files-input">
                                    <?php
                                    echo form_label('تصویر غذا'.': '.@$messageFile,'file');
                                    echo form_upload('file',set_value('file'));
                                    ?>
                                </div>
                                <?php
                                echo ''==gets('edit')?
                                form_submit('insert','ثبت غذای جدید',array('class'=>'btn btn-primary pull-right col-xs-6 col-xs-offset-3 float-center'))
                                :form_submit('update','ویرایش غذا',array('class'=>'btn btn-primary pull-right col-xs-6 col-xs-offset-3 float-center'));;
                                echo form_close();
                                ?>
                            </div><!-- /.box-body -->
                        </div><!-- /.box-body -->
                    </div><!-- /.box-body -->
                </div><!--/.col (right) -->
            </div><!--/.col (right) -->
        </div><!--/.col (right) -->
                           <!-- /.row -->
    </section><!-- /.content -->
</div>