<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            ویرایش مشتری
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> مدیریت</a></li>
            <li><a href="#">ویرایش مشتری</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">ویرایش مشخصات مشتری</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                        <?php
                        $attr=array('class'=>'dd','id'=>'fdsf');
                        echo form_open('','rool="form"');
                        ?>
                        <div class="form-group">
                            <?php
                            echo form_label('نام'.': '.form_error('name'),'name');
                            echo form_input(array('name'=>'name','id'=>'name','class'=>'form-control'),set_value('name',@$usName));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php
                            echo form_label('شماره همراه'.': '.form_error('phone'),'phone');
                            echo form_input(array('name'=>'phone','id'=>'phone','class'=>'form-control'),set_value('phone',@$usPhone));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php
                            echo form_label('تاریخ تولد'.': '.form_error('birthday'),'birthday');
                            echo form_input(array('name'=>'birthday','id'=>'birthday','class'=>'form-control'),set_value('birthday',@$usBirthday));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php
                            echo form_label('سالگرد ازدواج'.': '.form_error('anniversarey'),'anniversarey');
                            echo form_input(array('name'=>'anniversarey','id'=>'anniversarey','class'=>'form-control'),set_value('anniversarey',@$usAnniversarey));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php
                            echo form_label('زبان'.': '.form_error('lang'),'lang');
                            echo form_input(array('name'=>'lang','id'=>'lang','class'=>'form-control'),set_value('lang',@$usLang));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php
                            echo form_label('امتیاز'.': '.form_error('points'),'points');
                            echo form_input(array('name'=>'points','id'=>'points','class'=>'form-control'),set_value('points',@$usPoints));
                            ?>
                        </div>
                        <div class="form-inline margin-bottom">
                            <div class="margin-bottom"><?=form_error('type')?></div>
                            <div class="radio margin-right">
                                <label>
                                    <?php
                                    echo form_radio(array('name'=>'importance','id'=>'importance','class'=>''),'1',set_radio('importance','1',@$foImportance==1?true:''));
                                    ?>
                                    مشتری مهم(بالقوه)
                                </label>
                            </div>
                            <div class="radio margin-right">
                                <label>
                                    <?php
                                    echo form_radio(array('name'=>'importance','id'=>'importance1','class'=>''),'0',set_radio('importance','0',@$foImportance==0?true:''));
                                    ?>
                                    مشتری ساده
                                </label>
                            </div>
                        </div>
                        <?php
                        echo ''==gets('edit')?
                        form_submit('insert','ثبت مشتری جدید',array('class'=>'btn btn-primary pull-right col-xs-6 col-xs-offset-3 float-center')):
                        form_submit('update','ویرایش مشتری',array('class'=>'btn btn-primary pull-right col-xs-6 col-xs-offset-3 float-center'));
                                echo form_close();
                        // echo form_submit('insert','ثبت ویرایش',array('class'=>'btn btn-info pull-right'));

                        ?>
                            </div><!-- /.box-body -->
                        <div class="col-md-6"></div>
                    </div><!-- /.box-body -->
                </div><!-- /.box-body -->
            </div><!--/.col (right) -->
            </div>
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>