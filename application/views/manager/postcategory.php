<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            ایجاد بلاگ جدید
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> مدیریت</a></li>
            <li><a href="#">بلاگ</a></li>
            <li class="active">بلاگ جدید</li>
        </ol>
    </section>

        
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <!-- Horizontal Form -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">دسته های برای بلاگی {messageDelete}</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <thead>

                                <tr role="row">
                                    <th>#</th>
                                    <th>نام</th>
                                    <th>نوع بلاگ</th>
                                    <th>وظعیت</th>
                                    <th>ویرایش</th>
                                    <th>حذف</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php global $langsShow; $i=1;?>
                            <?php foreach ($postcategorys as $postcategory):?>
                               
                                <tr role="row" class="">
                                    <td class="sorting_1"><?=$i++?></td>
                                    <td><?=$postcategory->pcName?></td>
                                    <td><?=$postcategory->pcLang?></td>
                                    <td class="text-center"><a href="<?=url_maker('active',$postcategory->pcId)?>"><?=$postcategory->pcActive==1?'<i class="fa fa-toggle-on"></i>':'<i class="fa fa-toggle-off"></i>'?></a></td>
                                    <td class="text-center"><a href="<?=url_maker('edit',$postcategory->pcId)?>"><i class="fa fa-edit"></i></a></td>
                                    <td class="text-center"><a data-toggle="modal" data-target="#deletemodal" onclick="<?=deleteModal($postcategory->pcId)?>"><i class="fa fa-trash"></i></a></td>
                                </tr>
                            <?php endforeach;?>
                                
                                

                                
                            </tbody>
                            <tfoot>
                                <tr role="row">
                                    <th class="sorting_asc" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 167px;">#</th>
                                    <th class="sorting" aria-label="Browser: activate to sort column ascending" style="width: 213px;">نام</th>
                                    <th class="sorting" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">نوع بلاگ</th>
                                    <th class="sorting" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">ویرایش</th>
                                    <th class="sorting" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">حذف</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                    
                </div><!-- /.box-body -->
            </div><!--/.col (right) -->
            <?php
            if((gets('edit')))
            {
                ?>
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">دسته جدید {messageInsert}</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <?php
                            $attr=array('class'=>'dd','id'=>'fdsf');
                            echo form_open('','rool="form"');
                            ?>
                            <div class="form-inline margin-bottom">
                                <div class="margin-bottom"><?=form_error('lang')?></div>
                                <div class="radio">
                                    <label>
                                        <?php
                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['persian'],set_radio('lang',$langsShow['persian']));
                                        ?>
                                        برای بلاگ <?=$langsShow['persian']?>
                                    </label>
                                </div>
                                <div class="radio col-md-offset-1">
                                    <label>
                                        <?php
                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['english'],set_radio('lang',$langsShow['english']));
                                        ?>
                                        برای بلاگ <?=$langsShow['english']?>
                                    </label>
                                </div>
                                <div class="radio col-md-offset-1">
                                    <label>
                                        <?php
                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['arabic'],set_radio('lang',$langsShow['arabic']));
                                        ?>
                                        برای بلاگ <?=$langsShow['arabic']?>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php
                                echo form_label('نام دسته'.': '.form_error('name'),'name');
                                echo form_input(array('name'=>'name','id'=>'name','class'=>'form-control'),set_value('name'));
                                ?>
                            </div>
                            <?php
                            echo form_submit('insert','ثبت دسته جدید',array('class'=>'btn btn-info pull-right'));
                            echo form_close();
                            ?>
                        </div>
                    </div><!-- /.box-body -->
                </div>
                <?php
            }
            else
            {
                ?>
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">ویرایش دسته  {messageUpdate}</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <?php
                            $attr=array('class'=>'dd','id'=>'fdsf');
                            echo form_open(manager_url('postcategory?edit='.$pcId),'rool="form"');
                            ?>
                            <div class="form-inline margin-bottom">
                                <div class="margin-bottom"><?=form_error('lang')?></div>
                                <div class="radio">
                                    <label>
                                        <?php

                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['persian'],set_radio('lang',$langsShow['persian'],$pcLang==$langsShow['persian']?true:false));
                                        ?>
                                        برای بلاگ <?=$langsShow['persian']?>
                                    </label>
                                </div>
                                <div class="radio col-md-offset-1">
                                    <label>
                                        <?php
                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['english'],set_radio('lang',$langsShow['english'],$pcLang==$langsShow['english']?true:false));
                                        ?>
                                        برای بلاگ <?=$langsShow['english']?>
                                    </label>
                                </div>
                                <div class="radio col-md-offset-1">
                                    <label>
                                        <?php
                                        echo form_radio(array('name'=>'lang','id'=>'lang','class'=>''),$langsShow['arabic'],set_radio('lang',$langsShow['arabic'],$pcLang==$langsShow['arabic']?true:false));
                                        ?>
                                        برای بلاگ <?=$langsShow['arabic']?>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php
                                echo form_label('نام دسته'.': '.form_error('name'),'name');
                                echo form_input(array('name'=>'name','id'=>'name','class'=>'form-control'),$pcName);
                                echo form_hidden('catid',$pcId);
                                ?>
                            </div>
                            <?php
                            echo form_submit('update','پیرایش دشته',array('class'=>'btn btn-info pull-right'));
                            echo anchor(manager_url('postcategory'),'دسته جدید','class=btn btn-info pull-right');
                            ?>
                        </div>
                    </div><!-- /.box-body -->
                </div>
                <?php
            }
            ?>
            
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>
