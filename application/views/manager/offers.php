<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            مدیریت انتقادات و پیشنهادات
        </h1>
        {message}
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> مدیریت</a></li>
            <li><a href="#">انتقادات و پیشنهادات</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <?php foreach ($offers as $offer):?>
                    <div class="box">
                        <div class="box-header with-border">
                            <h5>توسط: <?=($offer->ofUserId)?"<a href='".base_url('manager/userreport?user='.$offer->ofUserId)."' class='btn btn-info'><i class='fa fa-line-chart'></i> نمایش گذارش این کاربر با کد: ".$offer->ofUserId."</a>":'کاربر ناشناس'?>
                            <?php if(($offer->ofUserId)):?><div class="pull-left"><a class="" data-toggle="modal" data-target="#sendMessage" onclick="javascript: return sendMessage('<?=($offer->ofUserId)?$offer->ofUserId:$offer->ofUserId?>','<?=$offer->ofUserId?>')"><i class="fa fa-comment-o"></i> ارسال پیام به این کاربر</a></div><?php endif;?>
                            </h5>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?=$offer->ofSubject?></h3>
                            </div><!-- /.box-header -->
                            <div>
                                <?=$offer->ofText?>
                            </div>
                              
                        </div><!-- /.box-body -->
                        
                    </div><!-- /.box-body -->
                <?php endforeach;?>

                <div class="box">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">نمایش {start} تا {limit} از {countAll}</div>
                        </div>
                        <div class="col-sm-6">
                            {pagination}
                        </div>
                        <div class="col-sm-3 form-inline">
                            <?=pgPerPage()?>
                        </div>

                    </div>
                </div>
                <div class="modal fade" tabindex="-1" role="dialog" id="sendMessage" aria-labelledby="gridSystemModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel">ارسال پیام به <span id="username"></span></h4>
                      </div>
                      <div class="modal-body">
                        <?php
                        $attr=array('class'=>'dd','id'=>'fdsf');
                        echo form_open('','rool="form"');
                        ?>
                        <div class="row">
                          <div class="col-md-3">موضوع پیام</div>
                          <div class="col-md-7">
                            <div class="form-group">
                                <?php
                                echo form_label('موضوع'.': '.form_error('subject'),'subject');
                                echo form_input(array('name'=>'subject','id'=>'subject','class'=>'form-control'),set_value('subject'));
                                echo form_input(array('name'=>'userId','id'=>'userId','type'=>'hidden'),set_value('userId'));
                                ?>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-3">متن پیام</div>
                          <div class="col-md-7">
                            <div class="form-group">
                                <?php
                                echo form_label('موضوع'.': '.form_error('text'),'text');
                                echo form_textarea(array('name'=>'text','id'=>'text','rows'=>'4','class'=>'form-control'),set_value('text'));
                                ?>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
                        
                        <?php
                        echo form_submit('sendMessage','ارسال',array('class'=>'btn btn-primary'));
                        ?></div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div><!--/.col (right) -->

        </div>
    </section><!-- /.content -->
</div><script type="text/javascript">
    function sendMessage(name,id)
    {
        $('#username').html(name);
        $('#userId').val(id);
    }
</script>