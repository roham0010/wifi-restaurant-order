<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            مدیریت غذاها
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> مدیریت</a></li>
            <li><a href="#">غذاها</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form -->


                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">غذاها {message}</h3>
                        <a href="<?=manager_url('food')?>" class="pull-left btn btn-link text-medium "><small><i class=" fa fa-cutlery margin-small-left "></i>غذای جدید</small></a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-4">
                                    <?=pgPerPage()?>
                                </div>

                                <div class="col-sm-4 text-center">
                                    <label class="btn btn-primary padding-small-right padding-small-left {all}">
                                        <a href="<?=manager_url('foods?lang=all')?>" class="text-white"> همه</a>
                                    </label>
                                    <label class="btn btn-primary padding-small-right padding-small-left {persian}">
                                        <a href="<?=manager_url('foods?lang=ایرانی')?>" class="text-white"> ایرانی</a>
                                    </label>
                                    <label class="btn btn-primary padding-small-right padding-small-left {English}">
                                        <a href="<?=manager_url('foods?lang=English')?>" class="text-white"> انگلیسی</a>
                                    </label>
                                    <label class="btn btn-primary padding-small-right padding-small-left {arabic}">
                                        <a href="<?=manager_url('foods?lang=العربیه')?>" class="text-white"> عربی</a>
                                    </label>
                                </div>
                                <div class="col-sm-4 text-center">
                                    <form class="form-inline" role='form'>
                                        
                                        <input name="lang" type="hidden" value="<?=@gets('lang')?>"></input>
                                        <input name="search" type="input" class="form-control" value="<?=@gets('search')?>"></input>
                                        <button type="submit" class="form-control">جستجو</button>
                                    </form>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                        <thead>

                                            <tr role="row">
                                                <th>#</th>
                                                <th>نام</th>
                                                <th>قیمت</th>
                                                <th>گروه</th>
                                                <th>نوع</th>
                                                <th>تخفیف</th>
                                                <th>مخصوص</th>
                                                <th>غذای سرآشپز</th>
                                                <th>توضیحات</th>
                                                <th>گزارش</th>
                                                <th>وظعیت</th>
                                                <th>ویرایش</th>
                                                <th>حذف</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=1; 
                                            foreach ($foods as $food):?>
                                                <tr role="row" class="">
                                                    <td><?=$i++?></td>
                                                    <td><?=$food->foName?></td>
                                                    <td><?=$food->foPrice?></td>
                                                    <td><?=$food->fcName?></td>
                                                    <td><?=$food->fcLang?></td>
                                                    <td><?=$food->foDiscount?></td>
                                                    <td  class="text-center"><?=$food->foSpecial==1?'<i class="glyphicon glyphicon-ok"></i>':''?></td>
                                                    <td  class="text-center"><?=$food->foChefFood==1?'<i class="glyphicon glyphicon-fire"></i>':''?></td>
                                                    <td><button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="right" title="<?=$food->foDescription?>">متن</button></td>
                                                    <td><a>نمایش</a></td>
                                                    <td class="text-center"><a href="<?=url_maker('active',$food->foId)?>"><?=$food->foActive==1?'<i class="fa fa-toggle-on"></i>':'<i class="fa fa-toggle-off"></i>'?></a></td>
                                                    <td class="text-center"><a href="<?=base_url('manager/food?edit='.$food->foId)?>"><i class="fa fa-edit"></i></a></td>
                                                    <td class="text-center"><a data-toggle="modal" data-target="#deletemodal" onclick="<?=deleteModal($food->foId)?>"><i class="fa fa-trash"></i></a></td>
                                                </tr>
                                            <?php endforeach;?>
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 167px;">#</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 213px;">نام</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 195px;">شماره همراه</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 144px;">تاریخ تولد</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">سالگرد ازدواج</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">زبان</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">ماهیت</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">ویرایش</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 106px;">حذف</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">نمایش {start} تا {limit} از {countAll}</div>
                                </div>
                                <div class="col-sm-9">
                                    {pagination}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>


            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
    </div>
