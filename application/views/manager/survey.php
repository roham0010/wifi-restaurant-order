<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            ایجاد نظر سنجی جدید
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> مدیریت</a></li>
            <li><a href="#">نظرسنجی جدید</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">ویرایش مشخصات مشتری {message}</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                        <?php
                        $attr=array('class'=>'dd','id'=>'fdsf');
                        echo form_open('','rool="form"');
                        ?>
                        <div class="form-group">
                            <?php
                            echo form_label('سوال نظر سنجی'.': '.form_error('name'),'name');
                            echo form_input(array('name'=>'question','id'=>'name','class'=>'form-control'),set_value('question'));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php
                            echo form_label('جواب شماره1'.': '.form_error('answer1'),'answer1');
                            echo form_input(array('name'=>'answer1','id'=>'answer1','class'=>'form-control'),set_value('answer1'));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php
                            echo form_label('جواب شماره2'.': '.form_error('answer2'),'answer2');
                            echo form_input(array('name'=>'answer2','id'=>'answer2','class'=>'form-control'),set_value('answer2'));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php
                            echo form_label('جواب شماره3'.': '.form_error('answer3'),'answer3');
                            echo form_input(array('name'=>'answer3','id'=>'answer3','class'=>'form-control'),set_value('answer3'));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php
                            echo form_label('جواب شماره4'.': '.form_error('answer4'),'answer4');
                            echo form_input(array('name'=>'answer4','id'=>'answer4','class'=>'form-control'),set_value('answer4'));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php
                            echo form_label('جواب شماره5'.': '.form_error('answer5'),'answer5');
                            echo form_input(array('name'=>'answer5','id'=>'answer5','class'=>'form-control'),set_value('answer5'));
                            ?>
                        </div>
                        <?php
                        echo form_submit('insert','ثبت نظر سنجی',array('class'=>'btn btn-info pull-right'));

                        ?>
                            </div><!-- /.box-body -->
                        <div class="col-md-6"></div>
                    </div><!-- /.box-body -->
                </div><!-- /.box-body -->
            </div><!--/.col (right) -->
            </div>
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>