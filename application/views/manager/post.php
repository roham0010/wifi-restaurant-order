<?php global $postTypes,$langsShow;?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            آپلود پست
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> مدیریت</a></li>
            <li class="active">آپلود پست</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box">
                    <?php   
                    $editUrl=(gets('edit'))?'&edit='.gets('edit'):'';
                    $postjoin=@$postjoin[0];
                    // print_r($postjoin);
                    ?>
                    <div class="row margin-bottom">
                        <div class="col-sm-12 text-center">
                            <?=form_error('lang')?form_error('lang').'<br>':''?>
                                
                            <label class="btn btn-primary padding-small-right padding-small-left {persian}">
                                <a href="<?=manager_url('post?lang=persian'.$editUrl)?>" class="text-white"> ایرانی</a>
                            </label>
                            <label class="btn btn-primary padding-small-right padding-small-left {english}">
                                <a href="<?=manager_url('post?lang=english'.$editUrl)?>" class="text-white"> انگلیسی</a>
                            </label>
                            <label class="btn btn-primary padding-small-right padding-small-left {arabic}">
                                <a href="<?=manager_url('post?lang=arabic'.$editUrl)?>" class="text-white"> عربی</a>
                            </label>
                        </div>
                    </div>
                    <?php 
                    echo form_open_multipart(this_url(),'rool="form"');
                    ?>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="box-header with-border">
                                <h3 class="box-title">برای زبان <?=@$langsShow[@gets('lang')]?> {message}
                                </h3>
                                <p class="margin-top">
                                 <?php
                                    // print_r($postlang);
                                    foreach ((array)@$posts as $post) 
                                    {
                                        echo "موضوع:";
                                        if($post->pcLang=='ایرانی')    
                                        {
                                            echo $post->poSubject;
                                            break;
                                        }
                                        elseif($post->pcLang=='english')    
                                        {
                                            echo $post->poSubject;
                                            break;
                                        }
                                        elseif($post->pcLang=='العربیه')    
                                        {
                                            echo $post->poSubject;
                                            break;
                                        }
                                    }
                                ?>
                                </p>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <?php
                                $options1[]='';

                                foreach ($postCategorys as $postCategory) 
                                {
                                    $options1[$postCategory->pcId]=$postCategory->pcName;
                                }
                                ?>
                                <div class="form-group">
                                    <?php
                                    echo form_label('انتخاب دسته پست'.': '.form_error('categoryId'),'categoryId');
                                    echo form_dropdown(array('name'=>'categoryId','id'=>'categoryId','class'=>'form-control'), $options1, set_value('categoryId',@$postjoin->pcId));
                                    ?>
                                </div>
                                <div class="form-group">
                                    <?php
                                    // print_r($postjoin);
                                    echo form_label('موضوع'.': '.form_error('subject'),'subject');
                                    echo form_input(array('name'=>'subject','id'=>'subject','class'=>'form-control'),set_value('subject',@$postjoin->poSubject));
                                    echo ($postjoin->poId)?form_hidden('poId',@$postjoin->poId):'';
                                    ?>
                                </div>
                                <div class="form-group">
                                    <?php
                                    echo form_label('توضیحات'.': '.form_error('text'),'text');
                                    echo $this->ckeditor->editor("text",set_value('text',@$postjoin->poText));
                                    ?>
                                </div>
                                
                            </div>
                        </div>
                            
                    </div>

                    
                        <div class="row">
                            <div class="col-md-12">
                                <?php 
                                echo (gets('edit'))?
                                form_submit('insert','ثبت پستی',array('class'=>'btn btn-primary col-md-4 col-xs-offset-4 margin-top text-center'))
                                :
                                form_submit('update',' ویرایش پست',array('class'=>'btn btn-primary col-md-4 col-xs-offset-4 margin-top text-center'));
                                ?>
                            </div><!-- /.box-body -->
                            <div class="col-md-6"></div>
                        </div><!-- /.box-body -->
                        
                    </div><!-- /.box-body -->
                <?php echo form_close();?>
                </div><!--/.col (right) -->
            </div>
        </div>   <!-- /.row -->
       
    </section><!-- /.content -->
</div>
<script type="text/javascript" src="<?=asset_url()?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=asset_url()?>ckfinder/ckfinder.js"></script>