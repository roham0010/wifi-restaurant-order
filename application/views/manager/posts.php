<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            مدیریت پستها
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>مدیریت</a></li>
            <li><a href="#">پستها</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form -->


                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">پستها {message}</h3>
                        <a href="<?=manager_url('post')?>" class="pull-left btn btn-link text-medium "><small><i class=" fa fa-file margin-small-left "></i>پست جدید</small></a>
                    </div>
                    <div class="row">

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-4 col-md-offset-4 margin-bottom text-center">
                                    <label class="btn btn-primary padding-small-right padding-small-left {alllang}">
                                        <a href="<?=manager_url('posts?lang=alllang')?>" class="text-white"> همه</a>
                                    </label>
                                    <label class="btn btn-primary padding-small-right padding-small-left {persian}">
                                        <a href="<?=manager_url('posts?lang=ایرانی')?>" class="text-white"> ایرانی</a>
                                    </label>
                                    <label class="btn btn-primary padding-small-right padding-small-left {English}">
                                        <a href="<?=manager_url('posts?lang=English')?>" class="text-white"> انگلیسی</a>
                                    </label>
                                    <label class="btn btn-primary padding-small-right padding-small-left {arabic}">
                                        <a href="<?=manager_url('posts?lang=العربیه')?>" class="text-white"> عربی</a>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <?=pgPerPage()?>
                                </div>
                                <div class="col-md-4 text-center">
                                    
                                </div>
                                <div class="col-sm-4 text-center">
                                    <form class="form-inline" role='form'>
                                        <input name="lang" type="hidden" value="<?=@gets('lang')?>"></input>
                                        <input name="search" type="input" class="form-control" value="<?=@gets('search')?>"></input>
                                        <button type="submit" class="form-control">جستجو</button>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                        <thead>

                                            <tr role="row">
                                                <th>#</th>
                                                <th>نام</th>
                                                <th>دسته</th>
                                                <th>ویرایش زبانها</th>
                                                <th>حذف</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=1; 
                                            global $postTypesShow;
                                            // $arrayTypesIcon=array(1=>'file-image-o',2=>'file-video-o',3=>'gamepad',4=>'file-pdf-o');
                                            $arrayTypesIcon=array(1 =>'file-image-o',2 =>'file-video-o',3 =>'gamepad',4 =>'file-pdf-o',5 =>'file-audio-o');
                                            foreach ($posts as $post):?>    
                                            <tr role="row" class="">
                                                <td><?=$i++?></td>
                                                <td><?=$post->poSubject?></td>
                                                <td><?=$post->pcName?></td>
                                                <td class="text-center"><a href="<?=base_url('manager/post?edit='.$post->poId)?>"><i class="fa fa-edit"></i></a></td>
                                                <td class="text-center"><a data-toggle="modal" data-target="#deletemodal" onclick="<?=deleteModal($post->poId)?>"><i class="fa fa-trash"></i></a></td>
                                            </tr>
                                            <?php endforeach;?>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">نمایش {start} تا {limit} از {countAll}</div>
                                </div>
                                <div class="col-sm-7">
                                    {pagination}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>


            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
    </div>
