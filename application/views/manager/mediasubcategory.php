<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            مدیریت دسته های مدیا
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> مدیریت</a></li>
            <li><a href="#">دسته های مدیا</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">مدیریت زیر دسته ها</h3>
                    </div>
                    <div class="row">

                        <div class="form-group col-md-3">

                            <?php
                            echo form_open('',array('method' => 'get', ));
                            echo form_label('انتخاب زیر دسته مدیا'.': '.form_error('categoryId'),'categoryId');
                            global $langsShow;
                            $options[]='';
                            foreach ($mediaCategorys as $mediaCategory) 
                            {
                                $options[$mediaCategory->mcId]=$mediaCategory->mcName;
                            }

                            $shirts_on_sale = array('1');
                            // echo form_hidden('lang',@gets('lang'));
                            echo form_hidden('mediatype',@gets('mediatype'));
                            echo form_dropdown(array('name'=>'categoryId','id'=>'categoryId','class'=>'form-control'), @$options, @gets('categoryId'));
                            echo form_submit('','انتخاب دسته',array('class'=>'btn btn-info'));
                            echo form_close();
                            ?>
                        </div>
                        <?php if((gets('edit'))): ?>
                        <div class="col-md-6 text-center margin-small-bottom margin-small-top">
                            <label class="btn btn-primary padding-small-right padding-small-left {all}">
                                <a href="<?=manager_url('mediasubcategory?mediatype=all')?>" class="text-white">همه</a>
                            </label>
                             <label class="btn btn-primary padding-small-right padding-small-left {picture}">
                                <a href="<?=manager_url('mediasubcategory?mediatype=picture')?>" class="text-white">عکس جدید</a>
                            </label>
                             <label class="btn btn-primary padding-small-right padding-small-left {picture}">
                                <a href="<?=manager_url('mediasubcategory?mediatype=music')?>" class="text-white">موسیقی جدید</a>
                            </label>
                            <label class="btn btn-primary padding-small-right padding-small-left {video}">
                                <a href="<?=manager_url('mediasubcategory?mediatype=video')?>" class="text-white">فیلم جدید</a>
                            </label>
                            <label class="btn btn-primary padding-small-right padding-small-left {game}">
                                <a href="<?=manager_url('mediasubcategory?mediatype=game')?>" class="text-white">بازی جدید</a>
                            </label>
                            <label class="btn btn-primary padding-small-right padding-small-left {text}">
                                <a href="<?=manager_url('mediasubcategory?mediatype=text')?>" class="text-white"> متن جدید</a>
                            </label>
                        </div>
                    <?php endif;?>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">زیر دسته ها</h3>
                                    </div>
                                    <div class="box-body">
                        
                                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            <thead>

                                                <tr role="row">
                                                    <th>#</th>
                                                    <?=(gets('categoryId'))?'<th>دسته اصلی</th>':''?>
                                                    <th>زیر دسته</th>
                                                    <th>ویرایش</th>
                                                    <th>حذف</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php global $langsShow; $i=1; ?>
                                            <?php foreach ($mediasubcategorys as $mediasubcategory):?>
                                               
                                                <tr role="row" class="">
                                                    <td class="sorting_1"><?=$i++?></td>
                                                    <?=(gets('categoryId'))?"<td>$mediasubcategory->mcName</td>":''?>
                                                    <td><?=$mediasubcategory->mscName?></td>
                                                    <td class="text-center"><a href="<?=url_maker('edit',$mediasubcategory->mscId)?>"><i class="fa fa-edit"></i></a></td>
                                                    <td class="text-center"><a data-toggle="modal" data-target="#deletemodal" onclick="<?=deleteModal($mediasubcategory->mscId)?>"><i class="fa fa-trash"></i></a></td>
                                                </tr>
                                            <?php endforeach;?>
                                                
                                                

                                                
                                            </tbody>
                                        </table>
                                    </div><!-- /.box-body -->
                                </div><!-- /.box-body -->
                            </div><!--/.col (right) -->
                            <?php
                            if((gets('edit')))
                            {
                                ?>
                                <div class="col-md-6">
                                    <div class="box">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">دسته جدید <small><b>برای:</b> <?=@$options[gets('categoryId')]?> </small>{messageInsert}</h3>
                                        </div><!-- /.box-header -->
                                        <div class="box-body">
                                            <?php
                                            $attr=array('class'=>'dd','id'=>'fdsf');
                                            echo form_open('','rool="form"');
                                            ?>
                                            
                                            <div class="form-group">
                                                <?php
                                                echo form_label('نام دسته'.': '.form_error('name'),'name');
                                                echo form_input(array('name'=>'name','id'=>'name','class'=>'form-control'),set_value('name'));
                                                echo form_hidden('categoryId',@gets('categoryId'));
                                                ?>
                                            </div>
                                            <?php
                                            echo form_submit('insert','ثبت دسته جدید',array('class'=>'btn btn-info pull-right'));
                                            echo form_close();
                                            ?>
                                        </div>
                                    </div><!-- /.box-body -->
                                </div>
                                <?php
                            }
                            else
                            {
                                ?>
                                <div class="col-md-6">
                                    <div class="box">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">ویرایش دسته  {messageUpdate}</h3>
                                        </div><!-- /.box-header -->
                                        <div class="box-body">
                                            <?php
                                            $attr=array('class'=>'dd','id'=>'fdsf');
                                            echo form_open(manager_url('mediasubcategory?edit='.$mscId),'rool="form"');
                                            ?>
                                            
                                            <div class="form-group">
                                                <?php
                                                echo form_label('نام دسته'.': '.form_error('name'),'name');
                                                echo form_input(array('name'=>'name','id'=>'name','class'=>'form-control'),$mscName);
                                                echo form_hidden('categoryId',$mscCategoryId);
                                                echo form_hidden('id',$mscId);
                                                ?>
                                            </div>
                                            <?php
                                            echo form_submit('update','ویرایش دسته',array('class'=>'btn btn-info pull-right'));
                                            echo form_close();
                                            echo anchor(manager_url('mediasubcategory'),'دسته جدید','class=btn btn-info pull-right');
                                            ?>
                                        </div>
                                    </div><!-- /.box-body -->
                                </div>
                                <?php
                            }
                            ?>
            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->
</div>