<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            پیشخوان
            <small>پنل مدیریت</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">پیشخوان</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>۱۵۰</h3>
                  <p>سفارش جدید</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="#" class="small-box-footer">اطلاعات بیشتر <i class="fa fa-arrow-circle-left"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>۵۳<sup style="font-size: 20px">%</sup></h3>
                  <p>افزایش آمار</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">اطلاعات بیشتر <i class="fa fa-arrow-circle-left"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3>۴۴</h3>
                  <p>کاربر ثبت نام کرده</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="#" class="small-box-footer">اطلاعات بیشتر <i class="fa fa-arrow-circle-left"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3>۶۵</h3>
                  <p>بازدید کننده یکتا</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">اطلاعات بیشتر <i class="fa fa-arrow-circle-left"></i></a>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-8 connectedSortable">
              <!-- Custom tabs (Charts with tabs)-->
              <div class="nav-tabs-custom">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right">
                  <li class="pull-right header"><i class="fa fa-inbox"></i> سفارشات</li>
                </ul>
                <div class="tab-content no-padding">
                  <!-- Morris chart - Sales -->
                  <div class=" tab-pane active" id="" style="position: relative;cursor: auto;">
                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                      <thead>
                        <tr role="row">
                          <th>کد</th>
                          <th>میز</th>
                          <th>صندلی</th>
                          <th>مشتری</th>
                          <th>زمان</th>
                          <th>وضعیت</th>
                          <th>مبلغ</th>
                          <th>وضعیت</th>
                          <!-- <th>وضعیت</th> -->
                          <th>وضعیت</th>
                          <th>وضعیت</th>
                        </tr>
                      </thead>
                      <tbody id="orders">
                        <tr role="row">
                          <td>25</td>
                          <td  class="text-center">3</td>
                          <td  class="text-center">6</td>
                          <td><button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="bottom" title="حسن شجاعی">387</button></td>
                          <td>12/05 05:12</td>
                          <td class="text-center"><i class="glyphicon glyphicon-fire"></i><i class="glyphicon glyphicon-ok"></i></td>
                          <td class="text-center">125000</td>
                          <td class="text-center"><a data-toggle="modal" data-target="#deletemodal" onclick=""><i class="fa fa-print"></i></a></td>
                        </tr>
                          
                      </tbody>
                  </table>
                  </div>
                </div>
              </div>
              <script type="text/javascript">
                limit=10;
                MGRURL='<?=manager_url()?>';
                orders();
                function orders()
                {
                  // alert();
                  $.ajax({
                      url: MGRURL + '/orders',
                      type: 'post',
                      data: {action: 'selectorders'},
                      success: function (data) {
                          $('#orders').html(data);
                      },error:function(){
                          myPrompt('changeName');
                      }
                  });
                }
                function print1()
                {
                  var mywindow = window.open('', 'my div', 'height=400,width=600');
                  mywindow.document.write(
                    "<html><body>"+
                    $('#print').html()
                    +"</body></html>"
                    );

                  mywindow.document.close(); // necessary for IE >= 10
                  mywindow.focus(); // necessary for IE >= 10

                  mywindow.print();
                  mywindow.close();
                }
                $(function(){
                  setInterval('orders()',5000);
                  $(document).on('click','.state',function(){
                    orderId=$(this).attr('order-id');
                    val=$(this).attr('val');
                    $.ajax({
                        url: MGRURL + '/orderstate',
                        type: 'post',
                        data: {action: 'orderstate',orderId,val},
                        success: function (data) {
                            $('#print').html(data);
                            if(val==1)
                            {
                              print1();
                            }
                            
                            // ajaxEnd();
                        },error:function(){
                            ajaxEnd();
                            myPrompt('changeName');
                        }
                    });

                });
                  $(document).on('click','.chat-box-close',function(){
                    $('.chat-box').addClass('hide');
                  });
                  $(document).on('click','.chat-box-open',function(){
                    // $('.chat-box').addClass('show');
                    $('.chat-box').removeClass('hide');
                  });
                });
              </script>
              <!-- Chat box -->

              <div class="box box-success col-md-2 col-xs-4 chat-box-open" style="position: fixed;margin:0px;bottom:0px;z-index: 10;height:30px;">
                <i class="fa fa-wechat"> گفتگو</i>
              </div>
              <div class="box box-success col-md-9 col-xs-11 chat-box" style="position: fixed;bottom:0px;z-index: 10;margin:0;padding:0;">
                <div class="box-header" style="border-bottom: 1px solid #f3f3f3;">
                  
                  <h3 class="box-title col-md-4"><i class="fa fa-comments-o"></i>گفتگو</h3>
                  <div class="col-md-4">
                    <h5 class="text-center chat-name">محمد علی کلی</h5>
                  </div>
                  <div class="box-tools pull-left" data-toggle="tooltip" title="Status">
                    <div class="btn-group" data-toggle="btn-toggle" >
                      <button type="button" class="btn btn-default btn-sm active"><i class="fa fa-square text-green"></i></button>
                      <button type="button" class="btn btn-default btn-sm"><i class="fa fa-square text-red"></i></button>
                    </div>
                    <div class="btn-group" data-toggle="btn-toggle" >
                      <button type="button" class="btn btn-default btn-sm chat-box-close"><i class="fa fa-close text-red"></i></button>
                    </div>
                  </div>
                </div>
                <div class="box-body" id="chat-box">
                  <!-- <div class="nav-tabs-custom col-md-3 chat-users">
                    <ul class="nav nav-tabs pull-right">
                      <li class="active"><a href="#received" data-toggle="tab">دریافتی</a></li>
                      <li><a href="#online" data-toggle="tab">آنلاین</a></li>
                    </ul>
                    <div class="tab-content no-padding">
                      <div class="tab-pane " id="online" >
                        <div class="row chat-user"><span class="text-success">محمد علی کلی</span></div>
                        <div class="row chat-user"><span class="text-success">محمد علی کلی</span></div>
                        <div class="row chat-user"><span class="text-success">محمد علی کلی</span></div>
                        
                      </div>
                      <div class="tab-pane active" id="received">
                        <div class="row chat-user"><span class="text-success">محمد علی کلی</span></div>
                        <div class="row chat-user"><span class="text-success">محمد علی کلی</span></div>
                        <div class="row chat-user"><span class="text-success">محمد علی کلی</span></div>
                        
                        <div class="row chat-user"><span class="text-success">محمد علی کلی</span></div>
                        <div class="row chat-user"><span class="text-success">محمد علی کلی</span></div>
                        <div class="row chat-user"><span class="text-success">محمد علی کلی</span></div>
                        <div class="row chat-user"><span class="text-success">محمد علی کلی</span></div>
                        <div class="row chat-user"><span class="text-success">محمد علی کلی</span></div>
                        <div class="row chat-user"><span class="text-success">محمد علی کلی</span></div>
                        
                        <div class="row chat-user"><span class="text-success">محمد علی کلی</span></div>
                        <div class="row chat-user"><span class="text-success">محمد علی کلی</span></div>
                        <div class="row chat-user"><span class="text-success">محمد علی کلی</span></div>
                        
                      </div>
                    </div>
                  </div> -->
                <div class="col-md-3 chat-users">
                    
                </div>
                  <div class="col-md-9 chat-body">
                  
                  </div>
                </div><!-- /.chat -->
                <div class="box-footer">
                  <form action="<?=manager_url('sendChat')?>" class="ajax-form">
                    <div class="input-group">
                      <input class="form-control" name="text" placeholder="متن...">
                      <input type="hidden" name="userId" id="chatUserId">
                      <div class="input-group-btn">
                        <button class="btn btn-success"><i class="fa fa-plus"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div><!-- /.box (chat box) -->
              <script type="text/javascript">
                userId='<?=$_SESSION["usId"]?>';
                userName='<?=($_SESSION["usName"])?$_SESSION["usName"]:$_SESSION["usId"]?>';
                  setInterval('getChats()',2000);
                  limits=[];
                function getChats()
                {
                  $.ajax({
                      url: MGRURL + '/getChats',
                      type: 'post',
                      dataType:'json',
                      data: {action: 'getChats'},
                      success: function (data) {
                        users=data['onlineusers'];
                        data=data['chats'];
                          for(i=0 ; i < data.length ; i++)
                          {
                            if($('.user-'+data[i]['chSendBy']).length == 0)
                            {
                              $('.chat-users').append('<div class="row chat-user flash user-'+data[i]['chSendBy']+'" user-id="'+data[i]['chSendBy']+'"><span class="text-success">'+data[i]['chSendByName']+'</span></div>');
                            }
                            if($('.user-content-'+data[i]['chSendBy']).length == 0)
                            {
                              $('.chat-body').append('<div class="chat-user-content user-content-'+data[i]['chSendBy']+' hide"><span class="col-md-12 text-center load-more" user-id="'+data[i]['chSendBy']+'">بیشتر</span><div class="user-chats"></div></div>') 
                            }
                            
                            if($('.chat-'+data[i]['chId']).length == 0)
                            {

                              /*when a user sended a new message*/
                              limits[data[i]['chSendBy']]=data[i]['chId'];
                              if($('.user-content-'+data[i]['chSendBy']).hasClass('hide'))
                                $('.user-'+data[i]['chSendBy']).addClass('flash');

                              dir='direction:ltr;';
                              if(data[i]['chSendBy']!=userId)
                              {
                                id=data[i]['chSendBy'];
                              }
                              else
                              {
                                dir='';
                                id=data[i]['chReceiveBy'];
                              }
                              $('.user-content-'+id).children('.user-chats').append('<div class="item chat-'+data[i]['chId']+'" style="'+dir+'"><span class="text-primary"> '+data[i]['chSendByName']+' </span><p class="message">'+
                                  '</a>'+data[i]['chText']+'</p></div>'
                                  );
                            }
                          
                          }
                          for(i=0 ; i < users.length ; i++)
                          {
                            if($('.user-'+users[i]['ulUserId']).length == 0)
                            {
                              username=users[i]['usName']!=undefined?users[i]['usName']:users[i]['usId'];
                              $('.chat-users').append('<div class="row chat-user user-'+users[i]['ulUserId']+'" user-id="'+users[i]['ulUserId']+'"><span class="text-success">'+username+'</span></div>');
                            }
                          
                          }
                          
                          // ajaxEnd();
                      },error:function(){
                          myPrompt('changeName');
                      }
                    });
                }
                $(function() {
                  $(document).on('click','.chat-user',function(){
                    userId1=$(this).attr('user-id');
                    $('#chatUserId').val(userId1);
                    $('.chat-user-content').addClass('hide');
                    $('.user-content-'+userId1).removeClass('hide');
                    $(this).removeClass('flash');
                    $('.chat-name').html($(this).children('span').html());
                  });

                  $(document).on('click','.load-more',function(){
                    mthis=$(this);
                    userId1=$(this).attr('user-id');
                    $.ajax({
                      url: MGRURL + '/chatShowMore',
                      type: 'post',
                      dataType:'json',
                      data: {action: 'chatShowMore','userId':userId1 ,'limit':limits[userId1]},
                      success: function (data) {
                        data=data['chats'];
                          for(i=0 ; i < data.length ; i++)
                          {
                            if($('.chat-'+data[i]['chId']).length == 0)
                            {
                              /*when a user sended a new message*/
                              dir='direction:ltr;';
                              if(data[i]['chSendBy']!=userId)
                              {
                                id=data[i]['chSendBy'];
                              }
                              else
                              {
                                dir='';
                                id=data[i]['chReceiveBy'];
                              }
                              $('.user-content-'+id).children('.user-chats').html('<div class="item chat-'+data[i]['chId']+'" style="'+dir+'"><span class="text-primary"> '+data[i]['chSendByName']+' </span><p class="message">'+
                                  '</a>'+data[i]['chText']+'</p></div>'+$('.user-content-'+id).children('.user-chats').html()
                                  );
                              
                            }
                              // if(i==0)
                                limits[id]=data[i]['chId'];
                          
                          }  
                          if(data.length==0)
                          {
                            mthis.remove();
                          }
                          // ajaxEnd();
                      },error:function(){
                          myPrompt('changeName');
                      }
                    });
                  });

                });
                function chat_added(data)
                {
                  alert(userId1);
                  $('.user-content-'+userId1).children('.user-chats').append('<div class="item chat-'+data.result+'"><span class="text-primary"> '+userName+' </span><p class="message">'+
                                  '</a>'+$('input[name="text"]').val()+'</p></div>'
                                  );
                  $('input[name="text"]').val('');
                }
              </script>
              <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">To Do List</h3>
                  <div class="box-tools pull-left">
                    <ul class="pagination pagination-sm inline">
                      <li><a href="#">&laquo;</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">&raquo;</a></li>
                    </ul>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list">
                    <li>
                      <!-- drag handle -->
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text">Design a nice theme</span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Make the theme responsive</span>
                      <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Check your messages and notifications</span>
                      <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Let theme shine like a star</span>
                      <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
                      <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
                  </ul>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix no-border">
                  <button class="btn btn-default pull-left"><i class="fa fa-plus"></i> Add item</button>
                </div>
              </div><!-- /.box -->

              <!-- quick email widget -->
              <div class="box box-info">
                <div class="box-header">
                  <i class="fa fa-envelope"></i>
                  <h3 class="box-title">Quick Email</h3>
                  <!-- tools box -->
                  <div class="pull-left box-tools">
                    <button class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                  </div><!-- /. tools -->
                </div>
                <div class="box-body">
                  <form action="#" method="post">
                    <div class="form-group">
                      <input type="email" class="form-control" name="emailto" placeholder="Email to:">
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" name="subject" placeholder="Subject">
                    </div>
                    <div>
                      <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>
                  </form>
                </div>
                <div class="box-footer clearfix">
                  <button class="pull-left btn btn-default" id="sendEmail">Send <i class="fa fa-arrow-circle-left"></i></button>
                </div>
              </div>

            </section><!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-4 connectedSortable">

              <!-- Map box -->
              <div class="box box-solid bg-light-blue-gradient">
                <div class="box-header">
                  <!-- tools box -->
                  <div class="pull-left box-tools">
                    <button class="btn btn-primary btn-sm daterange pull-left" data-toggle="tooltip" title="Date range"><i class="fa fa-calendar"></i></button>
                    <button class="btn btn-primary btn-sm pull-left" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->

                  <i class="fa fa-map-marker"></i>
                  <h3 class="box-title">
                    Visitors
                  </h3>
                </div>
                <div class="box-body">
                  <div id="world-map" style="height: 250px; width: 100%;"></div>
                </div><!-- /.box-body-->
                <div class="box-footer no-border">
                  <div class="row">
                    <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                      <div id="sparkline-1"></div>
                      <div class="knob-label">Visitors</div>
                    </div><!-- ./col -->
                    <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                      <div id="sparkline-2"></div>
                      <div class="knob-label">Online</div>
                    </div><!-- ./col -->
                    <div class="col-xs-4 text-center">
                      <div id="sparkline-3"></div>
                      <div class="knob-label">Exists</div>
                    </div><!-- ./col -->
                  </div><!-- /.row -->
                </div>
              </div>
              <!-- /.box -->

              <!-- solid sales graph -->
              <div class="box box-solid bg-teal-gradient">
                <div class="box-header">
                  <i class="fa fa-th"></i>
                  <h3 class="box-title">Sales Graph</h3>
                  <div class="box-tools pull-left">
                    <button class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body border-radius-none">
                  <div class="chart" id="line-chart" style="height: 250px;"></div>
                </div><!-- /.box-body -->
                <div class="box-footer no-border">
                  <div class="row">
                    <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                      <input type="text" class="knob" data-readonly="true" value="20" data-width="60" data-height="60" data-fgColor="#39CCCC">
                      <div class="knob-label">Mail-Orders</div>
                    </div><!-- ./col -->
                    <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                      <input type="text" class="knob" data-readonly="true" value="50" data-width="60" data-height="60" data-fgColor="#39CCCC">
                      <div class="knob-label">Online</div>
                    </div><!-- ./col -->
                    <div class="col-xs-4 text-center">
                      <input type="text" class="knob" data-readonly="true" value="30" data-width="60" data-height="60" data-fgColor="#39CCCC">
                      <div class="knob-label">In-Store</div>
                    </div><!-- ./col -->
                  </div><!-- /.row -->
                </div><!-- /.box-footer -->
              </div><!-- /.box -->

              <!-- Calendar -->
              <div class="box box-solid bg-green-gradient">
                <div class="box-header">
                  <i class="fa fa-calendar"></i>
                  <h3 class="box-title">Calendar</h3>
                  <!-- tools box -->
                  <div class="pull-left box-tools">
                    <!-- button with a dropdown -->
                    <div class="btn-group">
                      <button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></button>
                      <ul class="dropdown-menu pull-left" role="menu">
                        <li><a href="#">Add new event</a></li>
                        <li><a href="#">Clear events</a></li>
                        <li class="divider"></li>
                        <li><a href="#">View calendar</a></li>
                      </ul>
                    </div>
                    <button class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <!--The calendar -->
                  <div id="calendar" style="width: 100%"></div>
                </div><!-- /.box-body -->
                <div class="box-footer text-black">
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- Progress bars -->
                      <div class="clearfix">
                        <span class="pull-right">Task #1</span>
                        <small class="pull-left">90%</small>
                      </div>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 90%;"></div>
                      </div>

                      <div class="clearfix">
                        <span class="pull-right">Task #2</span>
                        <small class="pull-left">70%</small>
                      </div>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 70%;"></div>
                      </div>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                      <div class="clearfix">
                        <span class="pull-right">Task #3</span>
                        <small class="pull-left">60%</small>
                      </div>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 60%;"></div>
                      </div>

                      <div class="clearfix">
                        <span class="pull-right">Task #4</span>
                        <small class="pull-left">40%</small>
                      </div>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%;"></div>
                      </div>
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </div>
              </div><!-- /.box -->

            </section><!-- right col -->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->