<html dir="<?=langDirection()?>">
<head>
    <!-- meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="arrowthemes, bakery, cafe, ristorante theme, coffee shop, cooking, dining, food, full screen, html template, menu, pizza, responsive, restaurant, template" />
    <meta name="description" content="Responsive Restaurant Template" />
    <meta name="author" content="arrowthemes">
    <title>
        <?=lang(params(1).params(2).'_title');?>
    </title>
    <!-- fav icon -->
    <link href="<?=asset_url()?>images/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?=asset_url()?>images/apple-touch-icon.png">

    <!-- css -->
    <link rel="stylesheet" href="<?=asset_url()?>css/theme-<?=langDirection()?>.css">
    <link rel="stylesheet" href="<?=asset_url()?>css/my-theme-<?=langDirection()?>.css">
    <link rel="stylesheet" href="<?=asset_url()?>css/my-theme-both.css">
    <link rel="stylesheet" href="<?=asset_url()?>css/custom.css">
    <script src="<?=asset_url()?>js/jquery/jquery.min.js" type="text/javascript"></script>
</head>

<body>

    <!-- preloader -->
    <div class="tm-preload">
        <div class="spinner"></div>
    </div>

    <!-- to top scroller -->
    <div class="uk-sticky-placeholder">
        <div data-uk-smooth-scroll data-uk-sticky="{top:-500}">
            <a class="tm-totop-scroller uk-animation-slide-bottom" href="#"></a>
        </div>
    </div>

    <!-- toolbar -->
    <div class="tm-toolbar uk-clearfix">

        <!-- toolbar left -->
        <div class="uk-float-left">
            <div>
                <ul class="uk-subnav uk-subnav-line">
                    <li><a href="http://goo.gl/LqPQNI" target="_blank">Buy Now</a></li>
                    <li><a href="#">Login</a></li>
                </ul>
            </div>

            <div class=" uk-hidden-small">
                <p>Open daily: 7:30 am to 11:00 pm</p>
            </div>
        </div>

        <!-- toolbar right -->
        <div class="uk-float-right">

            <div>
                <a href="https://www.dribbble.com/arrowthemes" class="uk-icon-button uk-icon-dribbble" target="_blank"></a>
                <a href="https://www.facebook.com/arrowthemes" class="uk-icon-button uk-icon-facebook" target="_blank"></a>
                <a href="https://www.twitter.com/arrowthemes" class="uk-icon-button uk-icon-twitter" target="_blank"></a>
            </div>
        </div>
    </div>

    <!-- main bar -->
    <div class="tm-navbar" data-uk-sticky>
        <nav class="tm-navbar-container uk-flex">

            <!-- small logo -->
            <div class="tm-nav-logo-small">
                <a class="tm-logo-small" href="index.html">
                    <img src="<?=asset_url()?>images/demo/default/logo/logo-small.svg" width="170" height="65" alt="demo">
                </a>
            </div>

            <!-- main menu -->
            <div class="tm-nav uk-hidden-small">
                <ul class="uk-navbar-nav uk-hidden-small">

                    <!-- home menu  -->
                    <li><a href="<?=base_url('menu')?>"><i class="uk-icon-cutlery"></i><?=lang('menu_nav_menu')?></a></li>
                    <li><a href="<?=base_url('home')?>"><?=lang('menu_nav_home')?></a></li>
                    <li><a href="<?=base_url('orderedlists')?>"><?=lang('menu_nav_orderedlists')?></a></li>
                    <li><a href="<?=base_url('blog')?>"><?=lang('menu_nav_blog')?></a></li>
                    <!-- menu menu  -->
                    <li class="uk-parent" data-uk-dropdown><a href=""><?=lang('menu_nav_contactus')?></a>
                        <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-1">
                            <div class="uk-grid uk-dropdown-grid">
                                <div class="uk-width-1-1">
                                    <ul class="uk-nav uk-nav-navbar">
                                        <li><a href="<?=base_url('messages')?>"><?=lang('menu_nav_messages')?></a></li>                                        
                                        <li><a href="<?=base_url('offer')?>"><?=lang('menu_nav_offer')?></a></li>
                                        <li><a href="<?=base_url('survey')?>"><?=lang('menu_nav_survey')?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="uk-parent" data-uk-dropdown><a href="menu.html"><?=lang('menu_nav_media')?></a>
                        <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-1">
                            <div class="uk-grid uk-dropdown-grid">
                                <div class="uk-width-1-1">
                                    <ul class="uk-nav uk-nav-navbar">
                                        <li><a href="<?=base_url('musics')?>"><?=lang('menu_nav_musics')?></a></li>
                                        <li><a href="<?=base_url('picturegallery')?>"><?=lang('menu_nav_pictures')?></a></li>
                                        <li><a href="<?=base_url('videogallery')?>"><?=lang('menu_nav_videos')?></a></li>
                                        <li><a href="<?=base_url('games')?>"><?=lang('menu_nav_games')?></a></li>
                                        <li><a href="<?=base_url('texts')?>"><?=lang('menu_nav_texts')?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>


                    <!-- Blog menu -->
                    <li><a href="<?=base_url('manager')?>">مدیریت</a></li>

                    <!-- Contact us menu -->
<!--                    <li><a href="contact-us.html">Contact Us</a></li>-->
                    <li><a href="<?=base_url()?>picklang">انتخاب زبان</a></li>
                </ul>
            </div>

            <!-- offcanvas nav icon -->
            <a href="#offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>

        </nav>
    </div>

    <!-- header-a -->
    <!--
  <div id="tm-header-a" class="tm-block-header-a uk-block uk-block-default tm-block-fullwidth tm-grid-collapse ">
    <div class="uk-container uk-container-center">
      <section class="tm-header-a uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}">

        <div class="uk-width-1-1">
          <div class="uk-panel uk-text-center uk-contrast tm-overlay-secondary tm-header-height">
            <div class="tm-background-cover uk-cover-background uk-flex uk-flex-center uk-flex-middle" style="background-position: 50% 0px; background-image:url(images/background/bg-image-11.jpg)" data-uk-parallax="{bg: '-200'}">
              <div class="uk-position-relative uk-container">

                <div data-uk-parallax="{opacity: '1,0', y: '-50'}">

                  <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:300}">
                    <h1 class="uk-module-title-alt uk-margin-top">Elements</h1>
                  </div>

                  <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:800}">
                    <h5 class="uk-sub-title-small">If you can dream it, you can do it</h5>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
-->
