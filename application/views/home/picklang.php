
  <!-- main content -->
  <div id="tm-main" class="tm-block-main uk-block uk-block-default">
    <div class="uk-container uk-container-center">
      <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
        <div class="tm-main uk-width-medium-1-1">
          <main id="tm-content" class="tm-content">
            <article class="uk-article tm-article">
              <div class="tm-article-wrapper">
                <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                  <div class="tm-article">
                      <div class="uk-text-center">
                            <h1 class="uk-h2 uk-module-title-alt">Select your language</h1>
                      </div>

                  <br>

                    <div class="uk-grid uk-width-medium-1-3 uk-align-center"  data-uk-grid-margin>
                      <!-- panel box -->
                        <div class="uk-width-medium-1-3 uk-width-1-3 uk-padding-remove uk-align-center" style="">
                          <!-- panel box -->
                            <h2 class="uk-h3 uk-text-center"><?=anchor(base_url('picklang?mylang=english'),'English')?></h2>
                            <p class="uk-text-center">
                                <img class="uk-border-circle" src="<?=asset_url()?>images/my/flags/english.png" alt="sample_1" width="40" height="100">
                            </p>
                        </div>
                        <div class="uk-width-medium-1-3 uk-width-1-3 uk-padding-remove uk-align-center" style="">
                            <h2 class="uk-h3 uk-text-center"><?=anchor(base_url('picklang?mylang=persian'),'فارسی')?></h2>
                            <p class="uk-text-center">
                                <img class="uk-border-circle" src="<?=asset_url()?>images/my/flags/persian.png" alt="sample_1" width="40" height="100">
                            </p>
                        </div>
                        <div class="uk-width-medium-1-3 uk-width-1-3 uk-padding-remove uk-align-center" style="">
                          <!-- panel box -->
                            <h2 class="uk-h3 uk-text-center"><?=anchor(base_url('picklang?mylang=arabic'),'العربیه')?></h2>
                            <p class="uk-text-center">
                                <img class="uk-border-circle" src="<?=asset_url()?>images/my/flags/arabic.png" alt="sample_1" width="40" height="">
                            </p>
                        </div>
                      </div>

                      
                    
                  </div>

                  
                </div>
              </div>
            </article>
          </main>
        </div>
      </div>
    </div>
  </div>

