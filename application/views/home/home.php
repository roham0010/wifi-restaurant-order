
  <!-- header a -->
  <div id="tm-header-a" class="tm-block-header-a uk-block uk-block-default tm-block-fullwidth tm-grid-collapse tm-gradient-header" >
    <div class="uk-container uk-container-center">
      <section class="tm-header-a uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}">
        <div class="uk-width-1-1">
          <div class="uk-panel">

            <!-- slideshow -->
            <div class="tm-slideshow-gaucho uk-slidenav-position" data-uk-slideshow="{autoplay:false, animation: 'scale', pauseOnHover: true, duration: 750, autoplayInterval: 15000, kenburns: true, kenburnsanimations: 'uk-animation-top-center'}">
              <ul class="uk-slideshow uk-overlay-active">

                <!-- slide 1 -->
                <li>
                  <img src="images/demo/default/slider/1.jpg" width="1200" height="500" alt="demo">
                  <div class="uk-overlay-panel uk-flex uk-flex-middle uk-overlay-slide-left">
                    <div>
                      <h3>The true taste of argentina</h3>
                      <div class="uk-margin">
                        <p>It is not the strongest of the species that survives, nor the most intelligent that survives. It is the one that is the most adaptable to change. All changes, even the most longed for, have their melancholy;</p>
                      </div>
                      <a class="uk-button-default uk-button-large uk-button" href="#" target="_self">Learn more</a>
                    </div>
                  </div>
                </li>

                <!-- slide 2 -->
                <li>
                  <img src="images/demo/default/slider/2.jpg" width="1200" height="500" alt="demo">
                  <div class="uk-overlay-panel uk-flex uk-flex-middle uk-overlay-slide-left">
                    <div>
                      <h3>Great food. Tastes good.</h3>
                      <div class="uk-margin">
                        <p>Everything should be as simple as it is, but not simpler; Believe that life is worth living and your belief will help create the fact. We help your ideas come to life. Remember, everything should be simple;</p>
                      </div>
                      <a class="uk-button-default uk-button-large uk-button" href="#" target="_self">Learn more</a>
                    </div>
                  </div>
                </li>

                <!-- slide 3 -->
                <li>
                  <img src="images/demo/default/slider/3.jpg" width="1200" height="500" alt="demo">
                  <div class="uk-overlay-panel uk-flex uk-flex-middle uk-overlay-slide-left">
                    <div>
                      <h3>Eat healthy. Stay healthy</h3>
                      <div class="uk-margin">
                        <p>Believe in yourself! Have faith in your abilities! Without a humble but reasonable confidence in your own powers you cannot be successful or happy. Accept no one's definition.</p>
                      </div>
                      <a class="uk-button-default uk-button-large uk-button" href="#" target="_self">Learn more</a>
                    </div>
                  </div>
                </li>

              </ul>

              <!-- slide navigation buttons -->
              <div class="uk-margin">
                <ul class="uk-dotnav uk-flex-right uk-hidden-touch">
                  <li data-uk-slideshow-item="0"><a href=""></a></li>
                  <li data-uk-slideshow-item="1"><a href=""></a></li>
                  <li data-uk-slideshow-item="2"><a href=""></a></li>
                </ul>
              </div>

              <!-- slide navigation arrows -->
              <div class="tm-slidenav uk-flex uk-flex-right uk-flex-middle">
                <a href="" class="uk-slidenav uk-slidenav-previous uk-hidden-touch" data-uk-slideshow-item="previous"></a>
                <a href="" class="uk-slidenav uk-slidenav-next uk-hidden-touch" data-uk-slideshow-item="next"></a>
              </div>

            </div>
          </div>
        </div>
      </section>
    </div>
  </div>

  <!-- logo -->
  <div id="tm-logo">
    <div class="uk-container uk-container-center">
      <a class="tm-logo uk-hidden-small uk-flex uk-flex-middle uk-flex-center" href="index.html">
        <img src="images/demo/default/logo/logo.svg" width="300" height="120" alt="demo">
      </a>
    </div>
  </div>

  <div class="tm-header-box">
    <div class="uk-container uk-container-center">
    
    <!-- header b -->
    <div id="tm-header-b" class="tm-block-header-b uk-block tm-padding-remove uk-block-default" >
      <section class="tm-header-b uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>              
        <div class="uk-width-1-1 uk-width-medium-1-3">
          <div class="uk-panel uk-panel-box uk-panel-box-secondary uk-text-center">

            <div class="uk-text-center">
            <h3 class="tm-quote">good food is like music to the stomach. Every bite makes you want to dance</h3>
            </div>
            
            <img class="tm-img-bottom" src="images/demo/default/content/dish-6.png" alt="demo" width="400" height="260"></div></div>

            <div class="uk-width-1-1 uk-width-medium-2-3">
              <div class="uk-panel uk-panel-box uk-text-center">

                  <h1 class="uk-module-title-alt">Great food. Tastes good.</h1>
                  <h3 class="uk-sub-title"> If you can dream it, you can do it.</h3>
                  <p>Learn from the past, set vivid, detailed goals for the future, and live in the only moment of time over which you have any control: now. Optimism is the faith that leads to achievement. Nothing can be done without hope and confidence. If you can dream it, you can do it. Do it now, not tomorrow. Always continue the climb. It is possible for you to do.</p>
                  <br>

                  <a class="uk-button-primary uk-button-large uk-button" href="#" target="_self">See the menu</a>
                  <a class="uk-button-secondary uk-margin-left uk-button-large uk-button" href="contact-us.html" target="_self">Make reservations</a>

                  <img class="tm-img-bottom-right" src="images/demo/default/overlay/overlay-1.png" alt="demo" width="250" height="185">
              </div>
          </div>
        </section>
      </div>
    </div>
  </div>

  <!-- top-a -->
  <div id="tm-top-a" class="tm-block-top-a uk-block uk-block-default">
    <div class="uk-container uk-container-center">
      <section class="tm-top-a uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>

        <div class="uk-width-1-1">
          <div class="uk-panel uk-text-center">

            <h2 class="uk-module-title-alt uk-margin-large-bottom">Our most loved dishes</h2>

            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-4">
                <div data-uk-scrollspy111="{cls:'uk-animation-slide-bottom', delay:100}">
                  <h3 class="tm-primary-color uk-margin-small">Beef Medallion</h3>
                  <ul class="tm-rating">
                    <li><i class="uk-icon-star"></i></li>
                    <li><i class="uk-icon-star"></i></li>
                    <li><i class="uk-icon-star"></i></li>
                    <li><i class="uk-icon-star"></i></li>
                    <li><i class="uk-icon-star"></i></li>
                  </ul>
                  <img src="images/demo/default/content/dish-1.png" alt="demo" width="180" height="180">
                  <p>Sumptuous delicious food baked in our restaurants with curated ingredients</p>
                  <p class="tm-price">$ 9.50</p>
                </div>
              </div>

              <div class="uk-width-medium-1-4">
                <div data-uk-scrollspy111="{cls:'uk-animation-slide-bottom', delay:300}">
                  <h3 class="tm-primary-color uk-margin-small">Penne arrabiata</h3>
                  <ul class="tm-rating">
                    <li><i class="uk-icon-star"></i></li>
                    <li><i class="uk-icon-star"></i></li>
                    <li><i class="uk-icon-star"></i></li>
                    <li><i class="uk-icon-star"></i></li>
                    <li><i class="uk-icon-star"></i></li>
                  </ul>
                  <img src="images/demo/default/content/dish-2.png" alt="demo" width="180" height="180">
                  <p>Sumptuous delicious food baked in our restaurants with curated ingredients</p>
                  <p class="tm-price">$ 5.50</p>
                </div>
              </div>

              <div class="uk-width-medium-1-4">
                <div data-uk-scrollspy111="{cls:'uk-animation-slide-bottom', delay:500}">
                  <h3 class="tm-primary-color uk-margin-small">Sweet &amp; spicy chicken</h3>
                  <ul class="tm-rating">
                    <li><i class="uk-icon-star"></i></li>
                    <li><i class="uk-icon-star"></i></li>
                    <li><i class="uk-icon-star"></i></li>
                    <li><i class="uk-icon-star"></i></li>
                    <li><i class="uk-icon-star"></i></li>
                  </ul>
                  <img src="images/demo/default/content/dish-3.png" alt="demo" width="180" height="180">
                  <p>Sumptuous delicious food baked in our restaurants with curated ingredients</p>
                  <p class="tm-price">$ 10.50</p>
                </div>
              </div>

              <div class="uk-width-medium-1-4">
                <div data-uk-scrollspy111="{cls:'uk-animation-slide-bottom', delay:700}">
                  <h3 class="tm-primary-color uk-margin-small">Thai chicken curry</h3>
                  <ul class="tm-rating">
                    <li><i class="uk-icon-star"></i></li>
                    <li><i class="uk-icon-star"></i></li>
                    <li><i class="uk-icon-star"></i></li>
                    <li><i class="uk-icon-star"></i></li>
                    <li><i class="uk-icon-star"></i></li>
                  </ul>
                  <img src="images/demo/default/content/dish-4.png" alt="demo" width="180" height="180">
                  <p>Sumptuous delicious food baked in our restaurants with curated ingredients</p>
                  <p class="tm-price">$ 8.50</p>
                </div>
              </div>

            </div>
          </div>
        </div>
      </section>
    </div>
  </div>

  <!-- top-b -->
  <div id="tm-top-b" class="tm-block-top-b uk-block uk-block-default tm-inner-shadow tm-overlay-secondary" data-uk-parallax="{bg: '-200'}" style="background-image: url('images/background/bg-1.jpg');">
    <div class="uk-container uk-container-center">
      <section class="tm-top-b uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>

        <div class="uk-width-1-1">
          <div class="uk-panel uk-text-center uk-contrast">

            <h2 class="uk-module-title-alt uk-margin-large-bottom">Today's Specials</h2>
            <div class="tm-slideset-gaucho uk-contrast" data-uk-slideset="{default: 1,small: 2,medium: 2,large: 3, xlarge: 3, animation: 'fade'}">

              <!-- data filters -->
              <ul class="uk-subnav uk-flex-center">
                <li data-uk-filter=""><a href="#">All</a></li>
                <li data-uk-filter="bread"><a href="#">bread</a></li>
                <li data-uk-filter="chicken"><a href="#">chicken</a></li>
                <li data-uk-filter="fish"><a href="#">fish</a></li>
                <li data-uk-filter="pasta"><a href="#">pasta</a></li>
                <li data-uk-filter="prawns"><a href="#">prawns</a></li>
                <li data-uk-filter="salads"><a href="#">salads</a></li>
              </ul>

              <!-- slide panels -->
              <div class="uk-slidenav-position uk-margin">
                <ul class="uk-grid uk-slideset uk-grid-match uk-flex-center">

                  <li data-uk-filter="pasta">
                    <div class="uk-panel uk-panel-box uk-panel-box-hover uk-overlay-hover uk-text-left">
                      <a data-uk-lightbox="{group:'slideset'}" class="uk-position-cover uk-position-z-index" href="images/demo/menu/special-1.jpg"></a>
                      <div class="uk-text-center uk-panel-teaser">
                        <div class="uk-overlay ">
                          <img src="images/demo/menu/special-1.jpg" class="uk-overlay-scale" alt="Butternut ravioli" width="400" height="300">
                          <div class="uk-overlay-panel uk-overlay-background uk-overlay-icon uk-overlay-fade"></div>
                        </div>
                      </div>
                      <h3 class="uk-panel-title">Butternut ravioli</h3> Sumptuous delicious food baked in our restaurants with curated spices
                    </div>
                  </li>

                  <li data-uk-filter="fish">
                    <div class="uk-panel uk-panel-box uk-panel-box-hover uk-overlay-hover uk-text-left">
                      <a data-uk-lightbox="{group:'slideset'}" class="uk-position-cover uk-position-z-index" href="images/demo/menu/special-2.jpg"></a>
                      <div class="uk-text-center uk-panel-teaser">
                        <div class="uk-overlay ">
                          <img src="images/demo/menu/special-2.jpg" class="uk-overlay-scale" alt="Grilled snapper" width="400" height="300">
                          <div class="uk-overlay-panel uk-overlay-background uk-overlay-icon uk-overlay-fade"></div>
                        </div>
                      </div>
                      <h3 class="uk-panel-title">Grilled snapper</h3> Sumptuous delicious food baked in our restaurants with curated spices
                    </div>
                  </li>

                  <li data-uk-filter="chicken">
                    <div class="uk-panel uk-panel-box uk-panel-box-hover uk-overlay-hover uk-text-left">
                      <a data-uk-lightbox="{group:'slideset'}" class="uk-position-cover uk-position-z-index" href="images/demo/menu/special-3.jpg"></a>
                      <div class="uk-text-center uk-panel-teaser">
                        <div class="uk-overlay ">
                          <img src="images/demo/menu/special-3.jpg" class="uk-overlay-scale" alt="Roasted chicken" width="400" height="300">
                          <div class="uk-overlay-panel uk-overlay-background uk-overlay-icon uk-overlay-fade"></div>
                        </div>
                      </div>
                      <h3 class="uk-panel-title">Roasted chicken</h3> Sumptuous delicious food baked in our restaurants with curated spices
                    </div>
                  </li>

                  <li data-uk-filter="prawns,fish">
                    <div class="uk-panel uk-panel-box uk-panel-box-hover uk-overlay-hover uk-text-left">
                      <a data-uk-lightbox="{group:'slideset'}" class="uk-position-cover uk-position-z-index" href="images/demo/menu/special-4.jpg"></a>
                      <div class="uk-text-center uk-panel-teaser">
                        <div class="uk-overlay ">
                          <img src="images/demo/menu/special-4.jpg" class="uk-overlay-scale" alt="Golden Prawns" width="400" height="300">
                          <div class="uk-overlay-panel uk-overlay-background uk-overlay-icon uk-overlay-fade"></div>
                        </div>
                      </div>
                      <h3 class="uk-panel-title">Golden Prawns</h3> Sumptuous delicious food baked in our restaurants with curated spices
                    </div>
                  </li>

                  <li data-uk-filter="salads">
                    <div class="uk-panel uk-panel-box uk-panel-box-hover uk-overlay-hover uk-text-left">
                      <a data-uk-lightbox="{group:'slideset'}" class="uk-position-cover uk-position-z-index" href="images/demo/menu/special-5.jpg"></a>
                      <div class="uk-text-center uk-panel-teaser">
                        <div class="uk-overlay ">
                          <img src="images/demo/menu/special-5.jpg" class="uk-overlay-scale" alt="French Salad" width="400" height="300">
                          <div class="uk-overlay-panel uk-overlay-background uk-overlay-icon uk-overlay-fade"></div>
                        </div>
                      </div>
                      <h3 class="uk-panel-title">French Salad</h3> Sumptuous delicious food baked in our restaurants with curated spices
                    </div>
                  </li>

                  <li data-uk-filter="bread">
                    <div class="uk-panel uk-panel-box uk-panel-box-hover uk-overlay-hover uk-text-left">
                      <a data-uk-lightbox="{group:'slideset'}" class="uk-position-cover uk-position-z-index" href="images/demo/menu/special-6.jpg"></a>
                      <div class="uk-text-center uk-panel-teaser">
                        <div class="uk-overlay ">
                          <img src="images/demo/menu/special-6.jpg" class="uk-overlay-scale" alt="Focaccia Bread" width="400" height="300">
                          <div class="uk-overlay-panel uk-overlay-background uk-overlay-icon uk-overlay-fade"></div>
                        </div>
                      </div>
                      <h3 class="uk-panel-title">Focaccia Bread</h3> Sumptuous delicious food baked in our restaurants with curated spices
                    </div>
                  </li>

                  <li data-uk-filter="pasta">
                    <div class="uk-panel uk-panel-box uk-panel-box-hover uk-overlay-hover uk-text-left">
                      <a data-uk-lightbox="{group:'slideset'}" class="uk-position-cover uk-position-z-index" href="images/demo/menu/special-7.jpg"></a>
                      <div class="uk-text-center uk-panel-teaser">
                        <div class="uk-overlay ">
                          <img src="images/demo/menu/special-7.jpg" class="uk-overlay-scale" alt="Spaghetti Bolognese" width="400" height="300">
                          <div class="uk-overlay-panel uk-overlay-background uk-overlay-icon uk-overlay-fade"></div>
                        </div>
                      </div>
                      <h3 class="uk-panel-title">Spaghetti Bolognese</h3> Sumptuous delicious food baked in our restaurants with curated spices
                    </div>
                  </li>


                </ul>
                <a href="" class="uk-slidenav uk-slidenav-previous uk-hidden-touch" data-uk-slideset-item="previous"></a>
                <a href="" class="uk-slidenav uk-slidenav-next uk-hidden-touch" data-uk-slideset-item="next"></a>
              </div>

              <!-- slideset navigation -->
              <ul class="uk-slideset-nav uk-dotnav uk-flex-center uk-margin-bottom-remove">
                <li data-uk-slideset-item="0"><a></a></li>
              </ul>

            </div>
          </div>
        </div>
      </section>
    </div>
  </div>

  <!-- top-c -->
  <div id="tm-top-c" class="tm-block-top-c uk-block uk-block-default">
    <div class="uk-container uk-container-center">
      <section class="tm-top-c uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>

        <div class="uk-width-1-1 uk-width-medium-1-3">
          <div class="uk-panel uk-text-center">

            <img class="tm-img-top-left" src="images/demo/default/overlay/overlay-2.png" alt="demo" width="250" height="125">

            <br>
            <h1 class="uk-module-title-alt uk-margin-large-top uk-margin-bottom-small">Our menu</h1>
            <h3 class="uk-sub-title">Goodness &amp; Taste</h3>

            <p class="tm-padding-small-sides">
              Sumptuous delicious food baked in open ovens with spices
            </p>

            <br>
            <a class="uk-button-default uk-button-large uk-button" href="#" target="_self">See the menu</a>

            <div data-uk-scrollspy111="{cls:'uk-animation-slide-bottom', delay:100}">
              <img class="tm-img-bottom-center" src="images/demo/default/content/dish-5.png" alt="demo" width="340" height="230">
            </div>
          </div>
        </div>

        <div class="uk-width-1-1 uk-width-medium-2-3">
          <div class="uk-panel">

            <div class="uk-grid uk-grid-width-medium-1-2 uk-grid-width-large-1-2" data-uk-grid-margin>

              <div class="uk-panel uk-panel-space">
                <div data-uk-scrollspy111="{cls:'uk-animation-slide-bottom', delay:500}">
                  <div class="tm-menu-item tm-menu-compound">
                    <h3 class="tm-menu-name">Chicken schnitzel</h3>

                    <span class="tm-menu-dots"></span>
                    <span class="tm-menu-price">$ 6.50</span>
                    <span class="tm-menu-desc">Chicken breast . spiced bread crumbs . chips or mashed potatoes . aioli sauce</span>
                  </div>
                  <div class="tm-menu-item tm-menu-compound">
                    <h3 class="tm-menu-name">Fish cakes</h3>

                    <span class="tm-menu-dots"></span>
                    <span class="tm-menu-price">$ 8.50</span>
                    <span class="tm-menu-desc">Caramelized onions . tomatoes . mashed potatoes. tilapia</span>
                  </div>
                  <div class="tm-menu-item tm-menu-compound">
                    <h3 class="tm-menu-name">Sweet &amp; spicy chicken</h3>


                    <span class="tm-menu-dots"></span>
                    <span class="tm-menu-price">$ 6.50</span>
                    <span class="tm-menu-desc">Chicken breast . spiced bread crumbs . chips or mashed potatoes . aioli sauce</span>
                  </div>
                  <div class="tm-menu-item tm-menu-compound">
                    <h3 class="tm-menu-name">Thai red curry</h3>

                    <span class="tm-menu-dots"></span>
                    <span class="tm-menu-price">$ 7.50</span>
                    <span class="tm-menu-desc">Chicken . spiced bread crumbs . chips or mashed potatoes . aioli sauce</span>
                  </div>
                </div>
              </div>

              <div class="uk-panel uk-panel-space">
                <div data-uk-scrollspy111="{cls:'uk-animation-slide-bottom', delay:700}">
                  <div class="tm-menu-item tm-menu-compound">
                    <h3 class="tm-menu-name">Chicken schnitzel</h3>

                    <span class="tm-menu-dots"></span>
                    <span class="tm-menu-price">$ 6.50</span>
                    <span class="tm-menu-desc">Chicken breast . spiced bread crumbs . chips or mashed potatoes . aioli sauce</span>
                  </div>
                  <div class="tm-menu-item tm-menu-compound">
                    <h3 class="tm-menu-name">Fish cakes</h3>

                    <span class="tm-menu-dots"></span>
                    <span class="tm-menu-price">$ 8.50</span>
                    <span class="tm-menu-desc">Caramelized onions . tomatoes . mashed potatoes. tilapia</span>
                  </div>
                  <div class="tm-menu-item tm-menu-compound">
                    <h3 class="tm-menu-name">Sweet &amp; spicy chicken</h3>


                    <span class="tm-menu-dots"></span>
                    <span class="tm-menu-price">$ 6.50</span>
                    <span class="tm-menu-desc">Chicken breast . spiced bread crumbs . chips or mashed potatoes . aioli sauce</span>
                  </div>
                  <div class="tm-menu-item tm-menu-compound">
                    <h3 class="tm-menu-name">Thai red curry</h3>

                    <span class="tm-menu-dots"></span>
                    <span class="tm-menu-price">$ 7.50</span>
                    <span class="tm-menu-desc">Chicken . spiced bread crumbs . chips or mashed potatoes . aioli sauce</span>
                  </div>
                </div>
              </div>

            </div>

            <img class="tm-img-bottom-right" src="images/demo/default/overlay/overlay-3.png" alt="demo" width="250" height="200">
          </div>
        </div>
      </section>
    </div>
  </div>
