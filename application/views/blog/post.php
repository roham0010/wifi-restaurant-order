<?php global $langsShow;
?>

<!-- main content -->
<div id="tm-main" class="tm-block-main uk-block uk-block-default">
<div class="uk-container uk-container-center">
  <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
    <div class="tm-main uk-width-medium-1-1">
      <main id="tm-content" class="tm-content">
        <div id="system-message-container"></div>
          <article class="uk-article tm-article">
            <div class="tm-article-wrapper">
              <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                <div class="tm-article">
                  <div class="tm-slideset-gaucho" data-uk-slideset="{default: 1, animation: 'fade'}">
                    <div class="tm-middle uk-grid">
                      <aside class="tm-sidebar-b uk-width-medium-1-5 uk-grid-margin uk-row-first">
                        <div class="uk-width-medium-1-1 uk-form uk-form-stacked uk-text-center">
                          <fieldset>
                            <form action="http://127.0.0.1/restaurant/blog" method="get" accept-charset="utf-8">
                              <div class="uk-form-row">
                                  <input type="hidden" name="category" value="<?=@gets('category')?>">
                                  <input type="text" name="search" class="uk-form-small uk-width-3-5" value="<?=@gets('search')?>">
                                  <button type="submit" class="uk-button uk-button-success uk-button-small uk-width-1-3"><?=lang('gallerys_button_search')?></button>
                              </div>                              
                            </form>
                          </fieldset>
                        </div>
                        <ul class="uk-list">
                          <?php
                          if((gets('category')))
                            echo "<li>".lang('gallerys_sub_all')."</li>";
                          else
                            echo "<li><a href=".base_url("blog").">".lang('gallerys_sub_all')."</a></li>";
                          
                          foreach ($postcategorys as $postcategory) 
                          {
                          
                            echo "<li class='{mn$postcategory->pcId}'>";
                            echo gets('category')==$postcategory->pcId?"<abbr>$postcategory->pcName</abbr>":"<a href=".base_url("blog?lang=".@gets('lang')."&category=".$postcategory->pcId).">$postcategory->pcName</a>";
                            echo "</li>";
                          }
                          ?>
                        </ul>
                      </aside>
                      <div class="uk-slidenav-position uk-margin uk-width-medium-4-5">
                        <div class="uk-grid uk-grid-width-medium-1-1 uk-grid-width-large-1-1" data-uk-grid-margin>
                          <?php
                          foreach ($posts as $post) 
                          {
                            $o=$post->puUserId==$_SESSION['usId']?'':"-o";
                            $favorit=$post->puUserId==$_SESSION['usId']?'':" favorit";
                            $date=lang_date('m F Y',strtotime($post->poDate));
                            $subject=preg_replace('/ /', '-', $post->poSubject);
                            // echo $post->fileCount;
                            echo "
                            <article class='uk-article tm-article' data-permalink='blog-item.html'>
                              <div class='tm-article-wrapper'>
                                <!-- title -->
                                <div class='uk-width-1-1'>
                                  <h2 class='uk-article-title'>
                                    <a href='blog-item.html' class='uk-h3' title='$post->poSubject'>$post->poSubject</a>
                                  </h2>
                                  <i class='uk-icon-heart$o uk-align-right uk-margin-left uk-icon-medium uk-link uk-text-danger $favorit' post-id='$post->poId'> <small class='uk-h5'>$post->poRate</small></i>
                                </div>
                                <!-- meta data -->
                                <p class='uk-article-meta'>  ".lang('posts_date1')."
                                  <time datetime='2016-01-15'>$date</time>. 
                                  <i class='uk-icon-eye uk-margin-left uk-align-right'> $post->poSeen </i>
                                </p>

                                <!-- article content -->
                                <div class='tm-article-content uk-margin-large-bottom uk-margin-top-remove'>
                                  <div class='tm-article'>
                                    <p>$post->poText</p>
                                  </div>
                                </div>
                              </div>
                            </article>
                            ";
                          }
                          if(empty($posts))
                            echo lang('gallerys_empty');
                          ?>
                        </div>
                        <?php
                        foreach ($comments as $comment) 
                        {

                          echo "
                          <article class='uk-comment border-bottom uk-margin-large-top'>
                            <header class='uk-comment-header'>
                              <!-- <img class='uk-comment-avatar' src='images/demo/uikit/uikit_avatar.svg' width='50' height='50' alt='> -->
                              <h4 class='uk-comment-title'>$comment->usName</h4>
                              <p class='uk-comment-meta'>".lang_date('m F Y '.lang('time_at').' H:i A',strtotime($comment->coDate))."</p>
                            </header>
                            <div class='uk-comment-body'>
                              <p>$comment->coText</p>
                            </div>
                          </article>  
                          ";
                        }
                        
                        ?>
                        <div class="uk-width-medium-1-1 uk-margin-large-top uk-form uk-form-stacked uk-align-center">

                          <fieldset>
                            <div class="uk-panel-title"><?=lang('posts_comment_title')?></div>
                              {message}
                              <?php
                              echo form_open();
                              ?>
                              <div class="uk-form-row">
                                  <div class="uk-form-inline margin-bottom">
                                      <div class="margin-bottom"><?=form_label(lang('offer_label_name').': '.form_error('userId'),'userId',array('class'=>"uk-form-label"))?></div>
                                      <div class="radio uk-width-1-2 uk-align-right margin-right">
                                          <label>
                                              <?php
                                              echo form_radio(array('name'=>'userId','id'=>'userId1','class'=>''),'0',set_radio('userId','0'));
                                              echo form_hidden('postId',@$post->poId);
                                              echo lang('offer_label_nouserid');
                                              ?>
                                          </label>
                                      </div>
                                      <div class="radio uk-width-1-2 margin-right">
                                          <label>
                                              <?php
                                              echo form_radio(array('name'=>'userId','id'=>'userId','class'=>''),$_SESSION['usId'],set_radio('userId',$_SESSION['usId'],true));
                                              echo lang('offer_label_userid');
                                              ?>
                                          </label>
                                      </div>
                                  </div>
                              </div>
                              <div class="uk-form-row">
                                  <?php
                                  echo form_label(lang('offer_label_text').': '.form_error('text'),'text',array('class'=>"uk-form-label"));
                                  echo form_textarea(array('name'=>'text','rows'=>'4','class'=>'uk-width-1-1',''=>''),set_value('text'));
                                  ?>
                              </div>
                              <div class="uk-form-row uk-width-1-1">
                                  <?php
                                  echo form_submit('register',lang('offer_label_button'),array('class'=>'uk-width-medium-1-2 uk-align-center uk-botton  uk-text-center uk-button-primary uk-button-large'));
                                  ?>
                              </div>
                              
                          </fieldset>
                      </div>
                      </div>
                    </div>
                   


                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
      </main>
    </div>
  </div>
</div>
<script type="text/javascript">
URL='<?=base_url()?>';
$(function(){
  $(document).on('click','.favorit',function(){
    postId=$(this).attr('post-id');
    mthis=$(this);
    $.ajax({
        url: URL + 'blog/postlike',
        type: 'post',
        data: {action: 'favorite',postId},
        success: function (data) {
          // alert(mthis.attr('post-id'))
          if(data=='1')
          {
            // $("i[post-id='"+postId+"']").
            $("i[post-id='"+postId+"']").removeClass('uk-icon-heart-o');
            $("i[post-id='"+postId+"']").addClass('uk-icon-heart');
            
          }

            // ajaxEnd();
        },error:function(){
            ajaxEnd();
            myPrompt('changeName');
        }
    });
  });
  $(document).on('click','.seen',function(){
    postId=$(this).attr('post-id');
    mthis=$(this);
    $.ajax({
        url: URL + 'postseen',
        type: 'post',
        data: {action: 'seen',postId},
        success: function (data) {
        },error:function(){
        }
    });
  });
  
});
</script>