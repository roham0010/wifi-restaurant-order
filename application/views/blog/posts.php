<?php global $langsShow;
?>

<!-- main content -->
<div id="tm-main" class="tm-block-main uk-block uk-block-default">
<div class="uk-container uk-container-center">
  <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
    <div class="tm-main uk-width-medium-1-1">
      <main id="tm-content" class="tm-content">
        <div id="system-message-container"></div>
          <article class="uk-article tm-article">
            <div class="tm-article-wrapper">
              <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                <div class="tm-article">
                  <div class="tm-slideset-gaucho" data-uk-slideset="{default: 1, animation: 'fade'}">
                    <div class="tm-middle uk-grid">
                      <aside class="tm-sidebar-b uk-width-medium-1-5 uk-grid-margin uk-row-first">
                        <div class="uk-width-medium-1-1 uk-form uk-form-stacked uk-text-center">
                          <fieldset>
                            <form action="http://127.0.0.1/restaurant/blog" method="get" accept-charset="utf-8">
                              <div class="uk-form-row">
                                  <input type="hidden" name="category" value="<?=@gets('category')?>">
                                  <input type="text" name="search" class="uk-form-small uk-width-3-5" value="<?=@gets('search')?>">
                                  <button type="submit" class="uk-button uk-button-success uk-button-small uk-width-1-3"><?=lang('gallerys_button_search')?></button>
                              </div>                              
                            </form>
                          </fieldset>
                        </div>
                        <ul class="uk-list">
                          <?php
                          if((gets('category')))
                            echo "<li>".lang('gallerys_sub_all')."</li>";
                          else
                            echo "<li><a href=".base_url("blog").">".lang('gallerys_sub_all')."</a></li>";
                          
                          foreach ($postcategorys as $postcategory) 
                          {
                          
                            echo "<li class='{mn$postcategory->pcId}'>";
                            echo gets('category')==$postcategory->pcId?"<abbr>$postcategory->pcName</abbr>":"<a href=".base_url("blog?lang=".@gets('lang')."&category=".$postcategory->pcId).">$postcategory->pcName</a>";
                            echo "</li>";
                          }
                          ?>
                        </ul>
                      </aside>
                      <div class="uk-slidenav-position uk-margin uk-width-medium-4-5">
                        <div class="uk-grid uk-grid-width-medium-1-1 uk-grid-width-large-1-1" data-uk-grid-margin>
                          <?php
                          foreach ($posts as $post) 
                          {
                            $o=$post->puUserId==$_SESSION['usId']?'':"-o";
                            $favorit=$post->puUserId==$_SESSION['usId']?'':" favorit";
                            $date=lang_date('m F Y',strtotime($post->poDate));
                            $subject=preg_replace('/ /', '-', $post->poSubject);
                            // echo $post->fileCount;
                            echo "
                            <article class='uk-article tm-article' data-permalink='blog-item.html'>
                              <div class='tm-article-wrapper'>

                                
                                <!-- title -->
                                <div class='uk-width-1-1'>
                                  <h2 class='uk-article-title'>
                                    <a href='blog-item.html' class='uk-h3' title='$post->poSubject'>$post->poSubject</a>
                                  </h2>
                                  <i class='uk-icon-heart$o uk-align-right uk-margin-left uk-icon-medium uk-link uk-text-danger $favorit' post-id='$post->poId'> <small class='uk-h5'>$post->poRate</small></i>
                                </div>
                                <!-- meta data -->
                                <p class='uk-article-meta'> ".lang('posts_date1')."
                                  <time datetime='$post->poDate'>$date</time>. 
                                  <i class='uk-icon-eye uk-margin-left uk-align-right'> $post->poSeen </i>
                                </p>

                                <!-- article content -->
                                <div class='tm-article-content uk-margin-large-bottom uk-margin-top-remove'>
                                  <div class='tm-article'>
                                    <p>$post->poText</p>
                                  </div>
                                  <p><a class='uk-button uk-button-primary uk-button-large uk-margin-top uk-float-right' href='".base_url("blog/post/$subject-$post->poId")."' title='$post->poSubject'>ادامه مطلب</a>
                                  </p>
                                </div>
                              </div>
                            </article>
                            ";
                          }
                          if(empty($posts))
                            echo lang('gallerys_empty');
                          ?>
                        </div>
                        <p>
                          {pagination}
                        </p>
                      </div>
                    </div>
                   


                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
      </main>
    </div>
  </div>
</div>
<script type="text/javascript">
URL='<?=base_url()?>';
$(function(){
  $(document).on('click','.favorit',function(){
    postId=$(this).attr('post-id');
    mthis=$(this);
    $.ajax({
        url: URL + 'blog/postlike',
        type: 'post',
        data: {action: 'favorite',postId},
        success: function (data) {
          // alert(mthis.attr('post-id'))
          if(data=='1')
          {
            // $("i[post-id='"+postId+"']").
            $("i[post-id='"+postId+"']").removeClass('uk-icon-heart-o');
            $("i[post-id='"+postId+"']").addClass('uk-icon-heart');
            
          }

            // ajaxEnd();
        },error:function(){
            ajaxEnd();
            myPrompt('changeName');
        }
    });
  });
  $(document).on('click','.seen',function(){
    postId=$(this).attr('post-id');
    mthis=$(this);
    $.ajax({
        url: URL + 'postseen',
        type: 'post',
        data: {action: 'seen',postId},
        success: function (data) {
        },error:function(){
        }
    });
  });
  
});
</script>