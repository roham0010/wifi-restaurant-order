

var submitting = false;
$(function(){
    $(document).on('submit', '.ajax-form', function (e) {
        e.preventDefault();
        if (submitting)
            return false;
        submitting = true;
        var thisForm = $(this);
        var btnSubmit = $(this).find("button[type='submit']");
        if (undefined != thisForm.attr('formLoading')) {
            var targetContainer = $(thisForm.attr("target"));
            var targetForm = targetContainer.find('form').first();
            var loadingTarget = thisForm.parent(".card");
            loadingTarget.addClass('loading-card');
            $(thisForm).hide();
        } else
            btnSubmit.removeClass('loading').removeClass('done').addClass('loading');
        var data = $(this).serialize();
        data += '&ajax=true&_token=' + $("meta[name='_token']").attr('content');
        // action=$(".ajax-btn:focus").attr('myaction');
        var action = '';
        action = $(".ajax-btn:focus").attr('myaction');
        action = action == undefined ? $(this).prop('action') : action;
        if (action)
            $.post(action,
                data,
                function (data) {
                    data.thisForm = thisForm;
                    data.targetForm = targetForm;
                    if (undefined != thisForm.attr('formLoading')) {
                        data.targetContainer = targetContainer;
                        data.targetForm = targetForm;
                        data.loadingTarget = loadingTarget;
                        data.btnSubmit = btnSubmit;
                    }
                    return ajax_callback(data);
                },
                'json').done(function () {
                    //alert("second success");
                }).fail(function () {
                    //alert("error");
                }).always(function () {
                    if (undefined != thisForm.attr('formLoading'))
                        loadingTarget.removeClass('loading-card');
                    else
                        btnSubmit.removeClass('loading').removeClass('done').addClass('done');
                    submitting = false;
                });
        return false;
    });
});

function ajax_callback(data) {
    console.log(data);
    if (undefined != data) {
        if (undefined != data.status && 'OK' == data.status) {
            if (undefined != data.thisForm.attr('formLoading')) {
                if (undefined != data.result.targetContainer){
                    data.targetContainer = $(data.result.targetContainer);
                    delete data.result.targetContainer;
                    data.targetForm = data.targetContainer.find('form').first();
                }
                data.loadingTarget.hide();
                data.targetContainer.show();
            }
        }
        if (undefined != data.callback && data.callback.length) {
            var theCallback = data.callback.split('-');
            delete data.callback;
            if (undefined != theCallback[0] && theCallback[0].length) {
                //var theFunc = theCallback[0];
                window[theCallback[0]](data);
            }
        }
        if (undefined != data._token && data._token.length) {

            var token = data._token.split('-');
            if (undefined != token[1] && token[1].length) {
                $("meta[name='_token']").attr('content', token[1]);

            }
        }
    }
    return false;
}