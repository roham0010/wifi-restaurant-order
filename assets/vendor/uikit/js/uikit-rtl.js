/* Copyright (C) arrowthemes, http://www.gnu.org/licenses/gpl.html GNU/GPL */

! function (t) {
    if ("function" == typeof define && define.amd && define("uikit", function () {
            var i = window.UIkit || t(window, window.jQuery, window.document);
            return i.load = function (t, e, n, o) {
                var s, a = t.split(","),
                    r = [],
                    l = (o.config && o.config.uikit && o.config.uikit.base ? o.config.uikit.base : "").replace(/\/+$/g, "");
                if (!l) throw new Error("Please define base path to UIkit in the requirejs config.");
                for (s = 0; s < a.length; s += 1) {
                    var c = a[s].replace(/\./g, "/");
                    r.push(l + "/components/" + c)
                }
                e(r, function () {
                    n(i)
                })
            }, i
        }), !window.jQuery) throw new Error("UIkit requires jQuery");
    window && window.jQuery && t(window, window.jQuery, window.document)
}(function (t, i, e) {
    "use strict";
    var n = {},
        o = t.UIkit ? Object.create(t.UIkit) : void 0;
    if (n.version = "2.24.3", n.noConflict = function () {
            return o && (t.UIkit = o, i.UIkit = o, i.fn.uk = o.fn), n
        }, n.prefix = function (t) {
            return t
        }, n.$ = i, n.$doc = n.$(document), n.$win = n.$(window), n.$html = n.$("html"), n.support = {}, n.support.transition = function () {
            var t = function () {
                var t, i = e.body || e.documentElement,
                    n = {
                        WebkitTransition: "webkitTransitionEnd",
                        MozTransition: "transitionend",
                        OTransition: "oTransitionEnd otransitionend",
                        transition: "transitionend"
                    };
                for (t in n)
                    if (void 0 !== i.style[t]) return n[t]
            }();
            return t && {
                end: t
            }
        }(), n.support.animation = function () {
            var t = function () {
                var t, i = e.body || e.documentElement,
                    n = {
                        WebkitAnimation: "webkitAnimationEnd",
                        MozAnimation: "animationend",
                        OAnimation: "oAnimationEnd oanimationend",
                        animation: "animationend"
                    };
                for (t in n)
                    if (void 0 !== i.style[t]) return n[t]
            }();
            return t && {
                end: t
            }
        }(), function () {
            Date.now = Date.now || function () {
                return (new Date).getTime()
            };
            for (var t = ["webkit", "moz"], i = 0; i < t.length && !window.requestAnimationFrame; ++i) {
                var e = t[i];
                window.requestAnimationFrame = window[e + "RequestAnimationFrame"], window.cancelAnimationFrame = window[e + "CancelAnimationFrame"] || window[e + "CancelRequestAnimationFrame"]
            }
            if (/iP(ad|hone|od).*OS 6/.test(window.navigator.userAgent) || !window.requestAnimationFrame || !window.cancelAnimationFrame) {
                var n = 0;
                window.requestAnimationFrame = function (t) {
                    var i = Date.now(),
                        e = Math.max(n + 16, i);
                    return setTimeout(function () {
                        t(n = e)
                    }, e - i)
                }, window.cancelAnimationFrame = clearTimeout
            }
        }(), n.support.touch = "ontouchstart" in document || t.DocumentTouch && document instanceof t.DocumentTouch || t.navigator.msPointerEnabled && t.navigator.msMaxTouchPoints > 0 || t.navigator.pointerEnabled && t.navigator.maxTouchPoints > 0 || !1, n.support.mutationobserver = t.MutationObserver || t.WebKitMutationObserver || null, n.Utils = {}, n.Utils.isFullscreen = function () {
            return document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement || document.fullscreenElement || !1
        }, n.Utils.str2json = function (t, i) {
            try {
                return i ? JSON.parse(t.replace(/([\$\w]+)\s*:/g, function (t, i) {
                    return '"' + i + '":'
                }).replace(/'([^']+)'/g, function (t, i) {
                    return '"' + i + '"'
                })) : new Function("", "var json = " + t + "; return JSON.parse(JSON.stringify(json));")()
            } catch (e) {
                return !1
            }
        }, n.Utils.debounce = function (t, i, e) {
            var n;
            return function () {
                var o = this,
                    s = arguments,
                    a = function () {
                        n = null, e || t.apply(o, s)
                    },
                    r = e && !n;
                clearTimeout(n), n = setTimeout(a, i), r && t.apply(o, s)
            }
        }, n.Utils.removeCssRules = function (t) {
            var i, e, n, o, s, a, r, l, c, u;
            t && setTimeout(function () {
                try {
                    for (u = document.styleSheets, o = 0, r = u.length; r > o; o++) {
                        for (n = u[o], e = [], n.cssRules = n.cssRules, i = s = 0, l = n.cssRules.length; l > s; i = ++s) n.cssRules[i].type === CSSRule.STYLE_RULE && t.test(n.cssRules[i].selectorText) && e.unshift(i);
                        for (a = 0, c = e.length; c > a; a++) n.deleteRule(e[a])
                    }
                } catch (h) {}
            }, 0)
        }, n.Utils.isInView = function (t, e) {
            var o = i(t);
            if (!o.is(":visible")) return !1;
            var s = n.$win.scrollLeft(),
                a = n.$win.scrollTop(),
                r = o.offset(),
                l = r.left,
                c = r.top;
            return e = i.extend({
                topoffset: 0,
                leftoffset: 0
            }, e), c + o.height() >= a && c - e.topoffset <= a + n.$win.height() && l + o.width() >= s && l - e.leftoffset <= s + n.$win.width() ? !0 : !1
        }, n.Utils.checkDisplay = function (t, e) {
            var o = n.$("[data-uk-margin], [data-uk-grid-match], [data-uk-grid-margin], [data-uk-check-display]", t || document);
            return t && !o.length && (o = i(t)), o.trigger("display.uk.check"), e && ("string" != typeof e && (e = '[class*="uk-animation-"]'), o.find(e).each(function () {
                var t = n.$(this),
                    i = t.attr("class"),
                    e = i.match(/uk\-animation\-(.+)/);
                t.removeClass(e[0]).width(), t.addClass(e[0])
            })), o
        }, n.Utils.options = function (t) {
            if ("string" != i.type(t)) return t; - 1 != t.indexOf(":") && "}" != t.trim().substr(-1) && (t = "{" + t + "}");
            var e = t ? t.indexOf("{") : -1,
                o = {};
            if (-1 != e) try {
                o = n.Utils.str2json(t.substr(e))
            } catch (s) {}
            return o
        }, n.Utils.animate = function (t, e) {
            var o = i.Deferred();
            return t = n.$(t), e = e, t.css("display", "none").addClass(e).one(n.support.animation.end, function () {
                t.removeClass(e), o.resolve()
            }).width(), t.css("display", ""), o.promise()
        }, n.Utils.uid = function (t) {
            return (t || "id") + (new Date).getTime() + "RAND" + Math.ceil(1e5 * Math.random())
        }, n.Utils.template = function (t, i) {
            for (var e, n, o, s, a = t.replace(/\n/g, "\\n").replace(/\{\{\{\s*(.+?)\s*\}\}\}/g, "{{!$1}}").split(/(\{\{\s*(.+?)\s*\}\})/g), r = 0, l = [], c = 0; r < a.length;) {
                if (e = a[r], e.match(/\{\{\s*(.+?)\s*\}\}/)) switch (r += 1, e = a[r], n = e[0], o = e.substring(e.match(/^(\^|\#|\!|\~|\:)/) ? 1 : 0), n) {
                    case "~":
                        l.push("for(var $i=0;$i<" + o + ".length;$i++) { var $item = " + o + "[$i];"), c++;
                        break;
                    case ":":
                        l.push("for(var $key in " + o + ") { var $val = " + o + "[$key];"), c++;
                        break;
                    case "#":
                        l.push("if(" + o + ") {"), c++;
                        break;
                    case "^":
                        l.push("if(!" + o + ") {"), c++;
                        break;
                    case "/":
                        l.push("}"), c--;
                        break;
                    case "!":
                        l.push("__ret.push(" + o + ");");
                        break;
                    default:
                        l.push("__ret.push(escape(" + o + "));")
                } else l.push("__ret.push('" + e.replace(/\'/g, "\\'") + "');");
                r += 1
            }
            return s = new Function("$data", ["var __ret = [];", "try {", "with($data){", c ? '__ret = ["Not all blocks are closed correctly."]' : l.join(""), "};", "}catch(e){__ret = [e.message];}", 'return __ret.join("").replace(/\\n\\n/g, "\\n");', "function escape(html) { return String(html).replace(/&/g, '&amp;').replace(/\"/g, '&quot;').replace(/</g, '&lt;').replace(/>/g, '&gt;');}"].join("\n")), i ? s(i) : s
        }, n.Utils.events = {}, n.Utils.events.click = n.support.touch ? "tap" : "click", t.UIkit = n, n.fn = function (t, e) {
            var o = arguments,
                s = t.match(/^([a-z\-]+)(?:\.([a-z]+))?/i),
                a = s[1],
                r = s[2];
            return n[a] ? this.each(function () {
                var t = i(this),
                    s = t.data(a);
                s || t.data(a, s = n[a](this, r ? void 0 : e)), r && s[r].apply(s, Array.prototype.slice.call(o, 1))
            }) : (i.error("UIkit component [" + a + "] does not exist."), this)
        }, i.UIkit = n, i.fn.uk = n.fn, n.langdirection = "rtl" == n.$html.attr("dir") ? "right" : "left", n.components = {}, n.component = function (t, e) {
            var o = function (e, s) {
                var a = this;
                return this.UIkit = n, this.element = e ? n.$(e) : null, this.options = i.extend(!0, {}, this.defaults, s), this.plugins = {}, this.element && this.element.data(t, this), this.init(), (this.options.plugins.length ? this.options.plugins : Object.keys(o.plugins)).forEach(function (t) {
                    o.plugins[t].init && (o.plugins[t].init(a), a.plugins[t] = !0)
                }), this.trigger("init.uk.component", [t, this]), this
            };
            return o.plugins = {}, i.extend(!0, o.prototype, {
                defaults: {
                    plugins: []
                },
                boot: function () {},
                init: function () {},
                on: function (t, i, e) {
                    return n.$(this.element || this).on(t, i, e)
                },
                one: function (t, i, e) {
                    return n.$(this.element || this).one(t, i, e)
                },
                off: function (t) {
                    return n.$(this.element || this).off(t)
                },
                trigger: function (t, i) {
                    return n.$(this.element || this).trigger(t, i)
                },
                find: function (t) {
                    return n.$(this.element ? this.element : []).find(t)
                },
                proxy: function (t, i) {
                    var e = this;
                    i.split(" ").forEach(function (i) {
                        e[i] || (e[i] = function () {
                            return t[i].apply(t, arguments)
                        })
                    })
                },
                mixin: function (t, i) {
                    var e = this;
                    i.split(" ").forEach(function (i) {
                        e[i] || (e[i] = t[i].bind(e))
                    })
                },
                option: function () {
                    return 1 == arguments.length ? this.options[arguments[0]] || void 0 : void(2 == arguments.length && (this.options[arguments[0]] = arguments[1]))
                }
            }, e), this.components[t] = o, this[t] = function () {
                var e, o;
                if (arguments.length) switch (arguments.length) {
                    case 1:
                        "string" == typeof arguments[0] || arguments[0].nodeType || arguments[0] instanceof jQuery ? e = i(arguments[0]) : o = arguments[0];
                        break;
                    case 2:
                        e = i(arguments[0]), o = arguments[1]
                }
                return e && e.data(t) ? e.data(t) : new n.components[t](e, o)
            }, n.domready && n.component.boot(t), o
        }, n.plugin = function (t, i, e) {
            this.components[t].plugins[i] = e
        }, n.component.boot = function (t) {
            n.components[t].prototype && n.components[t].prototype.boot && !n.components[t].booted && (n.components[t].prototype.boot.apply(n, []), n.components[t].booted = !0)
        }, n.component.bootComponents = function () {
            for (var t in n.components) n.component.boot(t)
        }, n.domObservers = [], n.domready = !1, n.ready = function (t) {
            n.domObservers.push(t), n.domready && t(document)
        }, n.on = function (t, i, e) {
            return t && t.indexOf("ready.uk.dom") > -1 && n.domready && i.apply(n.$doc), n.$doc.on(t, i, e)
        }, n.one = function (t, i, e) {
            return t && t.indexOf("ready.uk.dom") > -1 && n.domready ? (i.apply(n.$doc), n.$doc) : n.$doc.one(t, i, e)
        }, n.trigger = function (t, i) {
            return n.$doc.trigger(t, i)
        }, n.domObserve = function (t, i) {
            n.support.mutationobserver && (i = i || function () {}, n.$(t).each(function () {
                var t = this,
                    e = n.$(t);
                if (!e.data("observer")) try {
                    var o = new n.support.mutationobserver(n.Utils.debounce(function (n) {
                        i.apply(t, []), e.trigger("changed.uk.dom")
                    }, 50));
                    o.observe(t, {
                        childList: !0,
                        subtree: !0
                    }), e.data("observer", o)
                } catch (s) {}
            }))
        }, n.init = function (t) {
            t = t || document, n.domObservers.forEach(function (i) {
                i(t)
            })
        }, n.on("domready.uk.dom", function () {
            n.init(), n.domready && n.Utils.checkDisplay()
        }), document.addEventListener("DOMContentLoaded", function () {
            var t = function () {
                n.$body = n.$("body"), n.ready(function (t) {
                    n.domObserve("[data-uk-observe]")
                }), n.on("changed.uk.dom", function (t) {
                    n.init(t.target), n.Utils.checkDisplay(t.target)
                }), n.trigger("beforeready.uk.dom"), n.component.bootComponents(), requestAnimationFrame(function () {
                    var t, i = {
                            x: window.pageXOffset,
                            y: window.pageYOffset
                        },
                        e = function () {
                            (i.x != window.pageXOffset || i.y != window.pageYOffset) && (t = {
                                x: 0,
                                y: 0
                            }, window.pageXOffset != i.x && (t.x = window.pageXOffset > i.x ? 1 : -1), window.pageYOffset != i.y && (t.y = window.pageYOffset > i.y ? 1 : -1), i = {
                                dir: t,
                                x: window.pageXOffset,
                                y: window.pageYOffset
                            }, n.$doc.trigger("scrolling.uk.document", [i])), requestAnimationFrame(e)
                        };
                    return n.support.touch && n.$html.on("touchmove touchend MSPointerMove MSPointerUp pointermove pointerup", e), (i.x || i.y) && e(), e
                }()), n.trigger("domready.uk.dom"), n.support.touch && navigator.userAgent.match(/(iPad|iPhone|iPod)/g) && n.$win.on("load orientationchange resize", n.Utils.debounce(function () {
                    var t = function () {
                        return i(".uk-height-viewport").css("height", window.innerHeight), t
                    };
                    return t()
                }(), 100)), n.trigger("afterready.uk.dom"), n.domready = !0
            };
            return ("complete" == document.readyState || "interactive" == document.readyState) && setTimeout(t), t
        }()), n.$html.addClass(n.support.touch ? "uk-touch" : "uk-notouch"), n.support.touch) {
        var s, a = !1,
            r = "uk-hover",
            l = ".uk-overlay, .uk-overlay-hover, .uk-overlay-toggle, .uk-animation-hover, .uk-has-hover";
        n.$html.on("mouseenter touchstart MSPointerDown pointerdown", l, function () {
            a && i("." + r).removeClass(r), a = i(this).addClass(r)
        }).on("mouseleave touchend MSPointerUp pointerup", function (t) {
            s = i(t.target).parents(l), a && a.not(s).removeClass(r)
        })
    }
    return n
}),
function (t) {
    function i(t, i, e, n) {
        return Math.abs(t - i) >= Math.abs(e - n) ? t - i > 0 ? "Left" : "Right" : e - n > 0 ? "Up" : "Down"
    }

    function e() {
        c = null, h.last && (void 0 !== h.el && h.el.trigger("longTap"), h = {})
    }

    function n() {
        c && clearTimeout(c), c = null
    }

    function o() {
        a && clearTimeout(a), r && clearTimeout(r), l && clearTimeout(l), c && clearTimeout(c), a = r = l = c = null, h = {}
    }

    function s(t) {
        return t.pointerType == t.MSPOINTER_TYPE_TOUCH && t.isPrimary
    }
    if (!t.fn.swipeLeft) {
        var a, r, l, c, u, h = {},
            d = 750;
        t(function () {
            var p, f, m, g = 0,
                v = 0;
            "MSGesture" in window && (u = new MSGesture, u.target = document.body), t(document).on("MSGestureEnd gestureend", function (t) {
                var i = t.originalEvent.velocityX > 1 ? "Right" : t.originalEvent.velocityX < -1 ? "Left" : t.originalEvent.velocityY > 1 ? "Down" : t.originalEvent.velocityY < -1 ? "Up" : null;
                i && void 0 !== h.el && (h.el.trigger("swipe"), h.el.trigger("swipe" + i))
            }).on("touchstart MSPointerDown pointerdown", function (i) {
                ("MSPointerDown" != i.type || s(i.originalEvent)) && (m = "MSPointerDown" == i.type || "pointerdown" == i.type ? i : i.originalEvent.touches[0], p = Date.now(), f = p - (h.last || p), h.el = t("tagName" in m.target ? m.target : m.target.parentNode), a && clearTimeout(a), h.x1 = m.pageX, h.y1 = m.pageY, f > 0 && 250 >= f && (h.isDoubleTap = !0), h.last = p, c = setTimeout(e, d), !u || "MSPointerDown" != i.type && "pointerdown" != i.type && "touchstart" != i.type || u.addPointer(i.originalEvent.pointerId))
            }).on("touchmove MSPointerMove pointermove", function (t) {
                ("MSPointerMove" != t.type || s(t.originalEvent)) && (m = "MSPointerMove" == t.type || "pointermove" == t.type ? t : t.originalEvent.touches[0], n(), h.x2 = m.pageX, h.y2 = m.pageY, g += Math.abs(h.x1 - h.x2), v += Math.abs(h.y1 - h.y2))
            }).on("touchend MSPointerUp pointerup", function (e) {
                ("MSPointerUp" != e.type || s(e.originalEvent)) && (n(), h.x2 && Math.abs(h.x1 - h.x2) > 30 || h.y2 && Math.abs(h.y1 - h.y2) > 30 ? l = setTimeout(function () {
                    void 0 !== h.el && (h.el.trigger("swipe"), h.el.trigger("swipe" + i(h.x1, h.x2, h.y1, h.y2))), h = {}
                }, 0) : "last" in h && (isNaN(g) || 30 > g && 30 > v ? r = setTimeout(function () {
                    var i = t.Event("tap");
                    i.cancelTouch = o, void 0 !== h.el && h.el.trigger(i), h.isDoubleTap ? (void 0 !== h.el && h.el.trigger("doubleTap"), h = {}) : a = setTimeout(function () {
                        a = null, void 0 !== h.el && h.el.trigger("singleTap"), h = {}
                    }, 250)
                }, 0) : h = {}, g = v = 0))
            }).on("touchcancel MSPointerCancel", o), t(window).on("scroll", o)
        }), ["swipe", "swipeLeft", "swipeRight", "swipeUp", "swipeDown", "doubleTap", "tap", "singleTap", "longTap"].forEach(function (i) {
            t.fn[i] = function (e) {
                return t(this).on(i, e)
            }
        })
    }
}(jQuery),
function (t) {
    "use strict";
    var i = [];
    t.component("stackMargin", {
            defaults: {
                cls: "uk-margin-small-top",
                rowfirst: !1
            },
            boot: function () {
                t.ready(function (i) {
                    t.$("[data-uk-margin]", i).each(function () {
                        var i, e = t.$(this);
                        e.data("stackMargin") || (i = t.stackMargin(e, t.Utils.options(e.attr("data-uk-margin"))))
                    })
                })
            },
            init: function () {
                var e = this;
                this.columns = [], t.$win.on("resize orientationchange", function () {
                    var i = function () {
                        e.process()
                    };
                    return t.$(function () {
                        i(), t.$win.on("load", i)
                    }), t.Utils.debounce(i, 20)
                }()), t.$html.on("changed.uk.dom", function (t) {
                    e.process()
                }), this.on("display.uk.check", function (t) {
                    this.element.is(":visible") && this.process()
                }.bind(this)), i.push(this)
            },
            process: function () {
                var i = this;
                if (this.columns = this.element.children(), t.Utils.stackMargin(this.columns, this.options), !this.options.rowfirst) return this;
                var e = this.columns.removeClass(this.options.rowfirst).filter(":visible").first().position();
                return e && this.columns.each(function () {
                    t.$(this)[t.$(this).position().left == e.left ? "addClass" : "removeClass"](i.options.rowfirst)
                }), this
            },
            revert: function () {
                return this.columns.removeClass(this.options.cls), this
            }
        }),
        function () {
            var i = [],
                e = function (t) {
                    if (t.is(":visible")) {
                        var i = t.parent().width(),
                            e = t.data("width"),
                            n = i / e,
                            o = Math.floor(n * t.data("height"));
                        t.css({
                            height: e > i ? o : t.data("height")
                        })
                    }
                };
            t.component("responsiveElement", {
                defaults: {},
                boot: function () {
                    t.ready(function (i) {
                        t.$("iframe.uk-responsive-width, [data-uk-responsive]", i).each(function () {
                            var i, e = t.$(this);
                            e.data("responsiveIframe") || (i = t.responsiveElement(e, {}))
                        })
                    })
                },
                init: function () {
                    var t = this.element;
                    t.attr("width") && t.attr("height") && (t.data({
                        width: t.attr("width"),
                        height: t.attr("height")
                    }).on("display.uk.check", function () {
                        e(t)
                    }), e(t), i.push(t))
                }
            }), t.$win.on("resize load", t.Utils.debounce(function () {
                i.forEach(function (t) {
                    e(t)
                })
            }, 15))
        }(), t.Utils.stackMargin = function (i, e) {
            e = t.$.extend({
                cls: "uk-margin-small-top"
            }, e), e.cls = e.cls, i = t.$(i).removeClass(e.cls);
            var n = !1,
                o = i.filter(":visible:first"),
                s = o.length ? o.position().top + o.outerHeight() - 1 : !1;
            s !== !1 && 1 != i.length && i.each(function () {
                var i = t.$(this);
                i.is(":visible") && (n ? i.addClass(e.cls) : i.position().top >= s && (n = i.addClass(e.cls)))
            })
        }, t.Utils.matchHeights = function (i, e) {
            i = t.$(i).css("min-height", ""), e = t.$.extend({
                row: !0
            }, e);
            var n = function (i) {
                if (!(i.length < 2)) {
                    var e = 0;
                    i.each(function () {
                        e = Math.max(e, t.$(this).outerHeight())
                    }).each(function () {
                        var i = t.$(this),
                            n = e - ("border-box" == i.css("box-sizing") ? 0 : i.outerHeight() - i.height());
                        i.css("min-height", n + "px")
                    })
                }
            };
            e.row ? (i.first().width(), setTimeout(function () {
                var e = !1,
                    o = [];
                i.each(function () {
                    var i = t.$(this),
                        s = i.offset().top;
                    s != e && o.length && (n(t.$(o)), o = [], s = i.offset().top), o.push(i), e = s
                }), o.length && n(t.$(o))
            }, 0)) : n(i)
        },
        function (i) {
            t.Utils.inlineSvg = function (e, n) {
                t.$(e || 'img[src$=".svg"]', n || document).each(function () {
                    var e = t.$(this),
                        n = e.attr("src");
                    if (!i[n]) {
                        var o = t.$.Deferred();
                        t.$.get(n, {
                            nc: Math.random()
                        }, function (i) {
                            o.resolve(t.$(i).find("svg"))
                        }), i[n] = o.promise()
                    }
                    i[n].then(function (i) {
                        var n = t.$(i).clone();
                        e.attr("id") && n.attr("id", e.attr("id")), e.attr("class") && n.attr("class", e.attr("class")), e.attr("style") && n.attr("style", e.attr("style")), e.attr("width") && (n.attr("width", e.attr("width")), e.attr("height") || n.removeAttr("height")), e.attr("height") && (n.attr("height", e.attr("height")), e.attr("width") || n.removeAttr("width")), e.replaceWith(n)
                    })
                })
            }, t.ready(function (i) {
                t.Utils.inlineSvg("[data-uk-svg]", i)
            })
        }({})
}(UIkit),
function (t) {
    "use strict";

    function i(i, e) {
        e = t.$.extend({
            duration: 1e3,
            transition: "easeOutExpo",
            offset: 0,
            complete: function () {}
        }, e);
        var n = i.offset().top - e.offset,
            o = t.$doc.height(),
            s = window.innerHeight;
        n + s > o && (n = o - s), t.$("html,body").stop().animate({
            scrollTop: n
        }, e.duration, e.transition).promise().done(e.complete)
    }
    t.component("smoothScroll", {
        boot: function () {
            t.$html.on("click.smooth-scroll.uikit", "[data-uk-smooth-scroll]", function (i) {
                var e = t.$(this);
                if (!e.data("smoothScroll")) {
                    t.smoothScroll(e, t.Utils.options(e.attr("data-uk-smooth-scroll")));
                    e.trigger("click")
                }
                return !1
            })
        },
        init: function () {
            var e = this;
            this.on("click", function (n) {
                n.preventDefault(), i(t.$(this.hash).length ? t.$(this.hash) : t.$("body"), e.options)
            })
        }
    }), t.Utils.scrollToElement = i, t.$.easing.easeOutExpo || (t.$.easing.easeOutExpo = function (t, i, e, n, o) {
        return i == o ? e + n : n * (-Math.pow(2, -10 * i / o) + 1) + e
    })
}(UIkit),
function (t) {
    "use strict";
    var i = t.$win,
        e = t.$doc,
        n = [],
        o = function () {
            for (var t = 0; t < n.length; t++) window.requestAnimationFrame.apply(window, [n[t].check])
        };
    t.component("scrollspy", {
        defaults: {
            target: !1,
            cls: "uk-scrollspy-inview",
            initcls: "uk-scrollspy-init-inview",
            topoffset: 0,
            leftoffset: 0,
            repeat: !1,
            delay: 0
        },
        boot: function () {
            e.on("scrolling.uk.document", o), i.on("load resize orientationchange", t.Utils.debounce(o, 50)), t.ready(function (i) {
                t.$("[data-uk-scrollspy]", i).each(function () {
                    var i = t.$(this);
                    if (!i.data("scrollspy")) {
                        t.scrollspy(i, t.Utils.options(i.attr("data-uk-scrollspy")))
                    }
                })
            })
        },
        init: function () {
            var i, e = this,
                o = this.options.cls.split(/,/),
                s = function () {
                    var n = e.options.target ? e.element.find(e.options.target) : e.element,
                        s = 1 === n.length ? 1 : 0,
                        a = 0;
                    n.each(function (n) {
                        var r = t.$(this),
                            l = r.data("inviewstate"),
                            c = t.Utils.isInView(r, e.options),
                            u = r.data("ukScrollspyCls") || o[a].trim();
                        !c || l || r.data("scrollspy-idle") || (i || (r.addClass(e.options.initcls), e.offset = r.offset(), i = !0, r.trigger("init.uk.scrollspy")), r.data("scrollspy-idle", setTimeout(function () {
                            r.addClass("uk-scrollspy-inview").toggleClass(u).width(), r.trigger("inview.uk.scrollspy"), r.data("scrollspy-idle", !1), r.data("inviewstate", !0)
                        }, e.options.delay * s)), s++), !c && l && e.options.repeat && (r.data("scrollspy-idle") && clearTimeout(r.data("scrollspy-idle")), r.removeClass("uk-scrollspy-inview").toggleClass(u), r.data("inviewstate", !1), r.trigger("outview.uk.scrollspy")), a = o[a + 1] ? a + 1 : 0
                    })
                };
            s(), this.check = s, n.push(this)
        }
    });
    var s = [],
        a = function () {
            for (var t = 0; t < s.length; t++) window.requestAnimationFrame.apply(window, [s[t].check])
        };
    t.component("scrollspynav", {
        defaults: {
            cls: "uk-active",
            closest: !1,
            topoffset: 0,
            leftoffset: 0,
            smoothscroll: !1
        },
        boot: function () {
            e.on("scrolling.uk.document", a), i.on("resize orientationchange", t.Utils.debounce(a, 50)), t.ready(function (i) {
                t.$("[data-uk-scrollspy-nav]", i).each(function () {
                    var i = t.$(this);
                    if (!i.data("scrollspynav")) {
                        t.scrollspynav(i, t.Utils.options(i.attr("data-uk-scrollspy-nav")))
                    }
                })
            })
        },
        init: function () {
            var e, n = [],
                o = this.find("a[href^='#']").each(function () {
                    "#" !== this.getAttribute("href").trim() && n.push(this.getAttribute("href"))
                }),
                a = t.$(n.join(",")),
                r = this.options.cls,
                l = this.options.closest || this.options.closest,
                c = this,
                u = function () {
                    e = [];
                    for (var n = 0; n < a.length; n++) t.Utils.isInView(a.eq(n), c.options) && e.push(a.eq(n));
                    if (e.length) {
                        var s, u = i.scrollTop(),
                            h = function () {
                                for (var t = 0; t < e.length; t++)
                                    if (e[t].offset().top >= u) return e[t]
                            }();
                        if (!h) return;
                        c.options.closest ? (o.blur().closest(l).removeClass(r), s = o.filter("a[href='#" + h.attr("id") + "']").closest(l).addClass(r)) : s = o.removeClass(r).filter("a[href='#" + h.attr("id") + "']").addClass(r), c.element.trigger("inview.uk.scrollspynav", [h, s])
                    }
                };
            this.options.smoothscroll && t.smoothScroll && o.each(function () {
                t.smoothScroll(this, c.options.smoothscroll)
            }), u(), this.element.data("scrollspynav", this), this.check = u, s.push(this)
        }
    })
}(UIkit),
function (t) {
    "use strict";
    var i = [];
    t.component("toggle", {
        defaults: {
            target: !1,
            cls: "uk-hidden",
            animation: !1,
            duration: 200
        },
        boot: function () {
            t.ready(function (e) {
                t.$("[data-uk-toggle]", e).each(function () {
                    var i = t.$(this);
                    if (!i.data("toggle")) {
                        t.toggle(i, t.Utils.options(i.attr("data-uk-toggle")))
                    }
                }), setTimeout(function () {
                    i.forEach(function (t) {
                        t.getToggles()
                    })
                }, 0)
            })
        },
        init: function () {
            var t = this;
            this.aria = -1 !== this.options.cls.indexOf("uk-hidden"), this.getToggles(), this.on("click", function (i) {
                t.element.is('a[href="#"]') && i.preventDefault(), t.toggle()
            }), i.push(this)
        },
        toggle: function () {
            if (this.totoggle.length) {
                if (this.options.animation && t.support.animation) {
                    var i = this,
                        e = this.options.animation.split(",");
                    1 == e.length && (e[1] = e[0]), e[0] = e[0].trim(), e[1] = e[1].trim(), this.totoggle.css("animation-duration", this.options.duration + "ms"), this.totoggle.each(function () {
                        var n = t.$(this);
                        n.hasClass(i.options.cls) ? (n.toggleClass(i.options.cls), t.Utils.animate(n, e[0]).then(function () {
                            n.css("animation-duration", ""), t.Utils.checkDisplay(n)
                        })) : t.Utils.animate(this, e[1] + " uk-animation-reverse").then(function () {
                            n.toggleClass(i.options.cls).css("animation-duration", ""), t.Utils.checkDisplay(n)
                        })
                    })
                } else this.totoggle.toggleClass(this.options.cls), t.Utils.checkDisplay(this.totoggle);
                this.updateAria()
            }
        },
        getToggles: function () {
            this.totoggle = this.options.target ? t.$(this.options.target) : [], this.updateAria()
        },
        updateAria: function () {
            this.aria && this.totoggle.length && this.totoggle.each(function () {
                t.$(this).attr("aria-hidden", t.$(this).hasClass("uk-hidden"))
            })
        }
    })
}(UIkit),
function (t) {
    "use strict";
    t.component("alert", {
        defaults: {
            fade: !0,
            duration: 200,
            trigger: ".uk-alert-close"
        },
        boot: function () {
            t.$html.on("click.alert.uikit", "[data-uk-alert]", function (i) {
                var e = t.$(this);
                if (!e.data("alert")) {
                    var n = t.alert(e, t.Utils.options(e.attr("data-uk-alert")));
                    t.$(i.target).is(n.options.trigger) && (i.preventDefault(), n.close())
                }
            })
        },
        init: function () {
            var t = this;
            this.on("click", this.options.trigger, function (i) {
                i.preventDefault(), t.close()
            })
        },
        close: function () {
            var t = this.trigger("close.uk.alert"),
                i = function () {
                    this.trigger("closed.uk.alert").remove()
                }.bind(this);
            this.options.fade ? t.css("overflow", "hidden").css("max-height", t.height()).animate({
                height: 0,
                opacity: 0,
                "padding-top": 0,
                "padding-bottom": 0,
                "margin-top": 0,
                "margin-bottom": 0
            }, this.options.duration, i) : i()
        }
    })
}(UIkit),
function (t) {
    "use strict";
    t.component("buttonRadio", {
        defaults: {
            activeClass: "uk-active",
            target: ".uk-button"
        },
        boot: function () {
            t.$html.on("click.buttonradio.uikit", "[data-uk-button-radio]", function (i) {
                var e = t.$(this);
                if (!e.data("buttonRadio")) {
                    var n = t.buttonRadio(e, t.Utils.options(e.attr("data-uk-button-radio"))),
                        o = t.$(i.target);
                    o.is(n.options.target) && o.trigger("click")
                }
            })
        },
        init: function () {
            var i = this;
            this.find(i.options.target).attr("aria-checked", "false").filter("." + i.options.activeClass).attr("aria-checked", "true"), this.on("click", this.options.target, function (e) {
                var n = t.$(this);
                n.is('a[href="#"]') && e.preventDefault(), i.find(i.options.target).not(n).removeClass(i.options.activeClass).blur(), n.addClass(i.options.activeClass), i.find(i.options.target).not(n).attr("aria-checked", "false"), n.attr("aria-checked", "true"), i.trigger("change.uk.button", [n])
            })
        },
        getSelected: function () {
            return this.find("." + this.options.activeClass)
        }
    }), t.component("buttonCheckbox", {
        defaults: {
            activeClass: "uk-active",
            target: ".uk-button"
        },
        boot: function () {
            t.$html.on("click.buttoncheckbox.uikit", "[data-uk-button-checkbox]", function (i) {
                var e = t.$(this);
                if (!e.data("buttonCheckbox")) {
                    var n = t.buttonCheckbox(e, t.Utils.options(e.attr("data-uk-button-checkbox"))),
                        o = t.$(i.target);
                    o.is(n.options.target) && o.trigger("click")
                }
            })
        },
        init: function () {
            var i = this;
            this.find(i.options.target).attr("aria-checked", "false").filter("." + i.options.activeClass).attr("aria-checked", "true"), this.on("click", this.options.target, function (e) {
                var n = t.$(this);
                n.is('a[href="#"]') && e.preventDefault(), n.toggleClass(i.options.activeClass).blur(), n.attr("aria-checked", n.hasClass(i.options.activeClass)), i.trigger("change.uk.button", [n])
            })
        },
        getSelected: function () {
            return this.find("." + this.options.activeClass)
        }
    }), t.component("button", {
        defaults: {},
        boot: function () {
            t.$html.on("click.button.uikit", "[data-uk-button]", function (i) {
                var e = t.$(this);
                if (!e.data("button")) {
                    t.button(e, t.Utils.options(e.attr("data-uk-button")));
                    e.trigger("click")
                }
            })
        },
        init: function () {
            var t = this;
            this.element.attr("aria-pressed", this.element.hasClass("uk-active")), this.on("click", function (i) {
                t.element.is('a[href="#"]') && i.preventDefault(), t.toggle(), t.trigger("change.uk.button", [t.element.blur().hasClass("uk-active")])
            })
        },
        toggle: function () {
            this.element.toggleClass("uk-active"), this.element.attr("aria-pressed", this.element.hasClass("uk-active"))
        }
    })
}(UIkit),
function (t) {
    "use strict";

    function i(i, e, n, o) {
        if (i = t.$(i), e = t.$(e), n = n || window.innerWidth, o = o || i.offset(), e.length) {
            var s = e.outerWidth();
            if (i.css("min-width", s), "right" == t.langdirection) {
                var a = n - (e.offset().left + s),
                    r = n - (i.offset().left + i.outerWidth());
                i.css("margin-right", a - r)
            } else i.css("margin-left", e.offset().left - o.left)
        }
    }
    var e, n = !1,
        o = {
            x: {
                "bottom-left": "bottom-right",
                "bottom-right": "bottom-left",
                "bottom-center": "bottom-right",
                "top-left": "top-right",
                "top-right": "top-left",
                "top-center": "top-right",
                "left-top": "right",
                "left-bottom": "right-bottom",
                "left-center": "right-center",
                "right-top": "left",
                "right-bottom": "left-bottom",
                "right-center": "left-center"
            },
            y: {
                "bottom-left": "top-left",
                "bottom-right": "top-right",
                "bottom-center": "top-center",
                "top-left": "bottom-left",
                "top-right": "bottom-right",
                "top-center": "bottom-center",
                "left-top": "top-left",
                "left-bottom": "left-bottom",
                "left-center": "top-left",
                "right-top": "top-left",
                "right-bottom": "bottom-left",
                "right-center": "top-left"
            },
            xy: {}
        };
    t.component("dropdown", {
        defaults: {
            mode: "hover",
            pos: "bottom-left",
            offset: 0,
            remaintime: 800,
            justify: !1,
            boundary: t.$win,
            delay: 0,
            dropdownSelector: ".uk-dropdown,.uk-dropdown-blank",
            hoverDelayIdle: 250,
            preventflip: !1
        },
        remainIdle: !1,
        boot: function () {
            var i = t.support.touch ? "click" : "mouseenter";
            t.$html.on(i + ".dropdown.uikit", "[data-uk-dropdown]", function (e) {
                var n = t.$(this);
                if (!n.data("dropdown")) {
                    var o = t.dropdown(n, t.Utils.options(n.attr("data-uk-dropdown")));
                    ("click" == i || "mouseenter" == i && "hover" == o.options.mode) && o.element.trigger(i), o.element.find(o.options.dropdownSelector).length && e.preventDefault()
                }
            })
        },
        init: function () {
            var i = this;
            this.dropdown = this.find(this.options.dropdownSelector), this.offsetParent = this.dropdown.parents().filter(function () {
                return -1 !== t.$.inArray(t.$(this).css("position"), ["relative", "fixed", "absolute"])
            }).slice(0, 1), this.centered = this.dropdown.hasClass("uk-dropdown-center"), this.justified = this.options.justify ? t.$(this.options.justify) : !1, this.boundary = t.$(this.options.boundary), this.boundary.length || (this.boundary = t.$win), this.dropdown.hasClass("uk-dropdown-up") && (this.options.pos = "top-left"), this.dropdown.hasClass("uk-dropdown-flip") && (this.options.pos = this.options.pos.replace("left", "right")), this.dropdown.hasClass("uk-dropdown-center") && (this.options.pos = this.options.pos.replace(/(left|right)/, "center")), this.element.attr("aria-haspopup", "true"), this.element.attr("aria-expanded", this.element.hasClass("uk-open")), "click" == this.options.mode || t.support.touch ? this.on("click.uikit.dropdown", function (e) {
                var n = t.$(e.target);
                n.parents(i.options.dropdownSelector).length || ((n.is("a[href='#']") || n.parent().is("a[href='#']") || i.dropdown.length && !i.dropdown.is(":visible")) && e.preventDefault(), n.blur()), i.element.hasClass("uk-open") ? (!i.dropdown.find(e.target).length || n.is(".uk-dropdown-close") || n.parents(".uk-dropdown-close").length) && i.hide() : i.show()
            }) : this.on("mouseenter", function (t) {
                i.trigger("pointerenter.uk.dropdown", [i]), i.remainIdle && clearTimeout(i.remainIdle), e && clearTimeout(e), n && n == i || (e = n && n != i ? setTimeout(function () {
                    e = setTimeout(i.show.bind(i), i.options.delay)
                }, i.options.hoverDelayIdle) : setTimeout(i.show.bind(i), i.options.delay))
            }).on("mouseleave", function () {
                e && clearTimeout(e), i.remainIdle = setTimeout(function () {
                    n && n == i && i.hide()
                }, i.options.remaintime), i.trigger("pointerleave.uk.dropdown", [i])
            }).on("click", function (e) {
                var o = t.$(e.target);
                return i.remainIdle && clearTimeout(i.remainIdle), n && n == i ? void((!i.dropdown.find(e.target).length || o.is(".uk-dropdown-close") || o.parents(".uk-dropdown-close").length) && i.hide()) : ((o.is("a[href='#']") || o.parent().is("a[href='#']")) && e.preventDefault(), void i.show())
            })
        },
        show: function () {
            t.$html.off("click.outer.dropdown"), n && n != this && n.hide(!0), e && clearTimeout(e), this.trigger("beforeshow.uk.dropdown", [this]), this.checkDimensions(), this.element.addClass("uk-open"), this.element.attr("aria-expanded", "true"), this.trigger("show.uk.dropdown", [this]), t.Utils.checkDisplay(this.dropdown, !0), n = this, this.registerOuterClick()
        },
        hide: function (t) {
            this.trigger("beforehide.uk.dropdown", [this, t]), this.element.removeClass("uk-open"), this.remainIdle && clearTimeout(this.remainIdle), this.remainIdle = !1, this.element.attr("aria-expanded", "false"), this.trigger("hide.uk.dropdown", [this, t]), n == this && (n = !1)
        },
        registerOuterClick: function () {
            var i = this;
            t.$html.off("click.outer.dropdown"), setTimeout(function () {
                t.$html.on("click.outer.dropdown", function (o) {
                    e && clearTimeout(e);
                    t.$(o.target);
                    n != i || i.element.find(o.target).length || (i.hide(!0), t.$html.off("click.outer.dropdown"))
                })
            }, 10)
        },
        checkDimensions: function () {
            if (this.dropdown.length) {
                this.dropdown.removeClass("uk-dropdown-top uk-dropdown-bottom uk-dropdown-left uk-dropdown-right uk-dropdown-stack").css({
                    "top-left": "",
                    left: "",
                    "margin-left": "",
                    "margin-right": ""
                }), this.justified && this.justified.length && this.dropdown.css("min-width", "");
                var e, n = t.$.extend({}, this.offsetParent.offset(), {
                        width: this.offsetParent[0].offsetWidth,
                        height: this.offsetParent[0].offsetHeight
                    }),
                    s = this.options.offset,
                    a = this.dropdown,
                    r = (a.show().offset() || {
                        left: 0,
                        top: 0
                    }, a.outerWidth()),
                    l = a.outerHeight(),
                    c = this.boundary.width(),
                    u = (this.boundary[0] !== window && this.boundary.offset() ? this.boundary.offset() : {
                        top: 0,
                        left: 0
                    }, this.options.pos),
                    h = {
                        "bottom-left": {
                            top: 0 + n.height + s,
                            left: 0
                        },
                        "bottom-right": {
                            top: 0 + n.height + s,
                            left: 0 + n.width - r
                        },
                        "bottom-center": {
                            top: 0 + n.height + s,
                            left: 0 + n.width / 2 - r / 2
                        },
                        "top-left": {
                            top: 0 - l - s,
                            left: 0
                        },
                        "top-right": {
                            top: 0 - l - s,
                            left: 0 + n.width - r
                        },
                        "top-center": {
                            top: 0 - l - s,
                            left: 0 + n.width / 2 - r / 2
                        },
                        "left-top": {
                            top: 0,
                            left: 0 - r - s
                        },
                        "left-bottom": {
                            top: 0 + n.height - l,
                            left: 0 - r - s
                        },
                        "left-center": {
                            top: 0 + n.height / 2 - l / 2,
                            left: 0 - r - s
                        },
                        "right-top": {
                            top: 0,
                            left: 0 + n.width + s
                        },
                        "right-bottom": {
                            top: 0 + n.height - l,
                            left: 0 + n.width + s
                        },
                        "right-center": {
                            top: 0 + n.height / 2 - l / 2,
                            left: 0 + n.width + s
                        }
                    },
                    d = {};
                if (e = u.split("-"), d = h[u] ? h[u] : h["bottom-left"], this.justified && this.justified.length) i(a.css({
                    left: 0
                }), this.justified, c);
                else if (this.options.preventflip !== !0) {
                    var p;
                    switch (this.checkBoundary(n.left + d.left, n.top + d.top, r, l, c)) {
                        case "x":
                            "x" !== this.options.preventflip && (p = o.x[u] || "right-top");
                            break;
                        case "y":
                            "y" !== this.options.preventflip && (p = o.y[u] || "top-left");
                            break;
                        case "xy":
                            this.options.preventflip || (p = o.xy[u] || "right-bottom")
                    }
                    p && (e = p.split("-"), d = h[p] ? h[p] : h["bottom-left"], this.checkBoundary(n.left + d.left, n.top + d.top, r, l, c) && (e = u.split("-"), d = h[u] ? h[u] : h["bottom-left"]))
                }
                r > c && (a.addClass("uk-dropdown-stack"), this.trigger("stack.uk.dropdown", [this])), a.css(d).css("display", "").addClass("uk-dropdown-" + e[0])
            }
        },
        checkBoundary: function (i, e, n, o, s) {
            var a = "";
            return (0 > i || i - t.$win.scrollLeft() + n > s) && (a += "x"), (e - t.$win.scrollTop() < 0 || e - t.$win.scrollTop() + o > window.innerHeight) && (a += "y"), a
        }
    }), t.component("dropdownOverlay", {
        defaults: {
            justify: !1,
            cls: "",
            duration: 200
        },
        boot: function () {
            t.ready(function (i) {
                t.$("[data-uk-dropdown-overlay]", i).each(function () {
                    var i = t.$(this);
                    i.data("dropdownOverlay") || t.dropdownOverlay(i, t.Utils.options(i.attr("data-uk-dropdown-overlay")))
                })
            })
        },
        init: function () {
            var e = this;
            this.justified = this.options.justify ? t.$(this.options.justify) : !1, this.overlay = this.element.find("uk-dropdown-overlay"), this.overlay.length || (this.overlay = t.$('<div class="uk-dropdown-overlay"></div>').appendTo(this.element)), this.overlay.addClass(this.options.cls), this.on({
                "beforeshow.uk.dropdown": function (t, n) {
                    e.dropdown = n, e.justified && e.justified.length && i(e.overlay.css({
                        display: "block",
                        "margin-left": "",
                        "margin-right": ""
                    }), e.justified, e.justified.outerWidth())
                },
                "show.uk.dropdown": function (i, n) {
                    var o = e.dropdown.dropdown.outerHeight(!0);
                    e.dropdown.element.removeClass("uk-open"), e.overlay.stop().css("display", "block").animate({
                        height: o
                    }, e.options.duration, function () {
                        e.dropdown.dropdown.css("visibility", ""), e.dropdown.element.addClass("uk-open"), t.Utils.checkDisplay(e.dropdown.dropdown, !0)
                    }), e.pointerleave = !1
                },
                "hide.uk.dropdown": function () {
                    e.overlay.stop().animate({
                        height: 0
                    }, e.options.duration)
                },
                "pointerenter.uk.dropdown": function (t, i) {
                    clearTimeout(e.remainIdle)
                },
                "pointerleave.uk.dropdown": function (t, i) {
                    e.pointerleave = !0
                }
            }), this.overlay.on({
                mouseenter: function () {
                    e.remainIdle && (clearTimeout(e.dropdown.remainIdle), clearTimeout(e.remainIdle))
                },
                mouseleave: function () {
                    e.pointerleave && n && (e.remainIdle = setTimeout(function () {
                        n && n.hide()
                    }, n.options.remaintime))
                }
            })
        }
    })
}(UIkit),
function (t) {
    "use strict";
    var i = [];
    t.component("gridMatchHeight", {
        defaults: {
            target: !1,
            row: !0,
            ignorestacked: !1
        },
        boot: function () {
            t.ready(function (i) {
                t.$("[data-uk-grid-match]", i).each(function () {
                    var i, e = t.$(this);
                    e.data("gridMatchHeight") || (i = t.gridMatchHeight(e, t.Utils.options(e.attr("data-uk-grid-match"))))
                })
            })
        },
        init: function () {
            var e = this;
            this.columns = this.element.children(), this.elements = this.options.target ? this.find(this.options.target) : this.columns, this.columns.length && (t.$win.on("load resize orientationchange", function () {
                var i = function () {
                    e.match()
                };
                return t.$(function () {
                    i()
                }), t.Utils.debounce(i, 50)
            }()), t.$html.on("changed.uk.dom", function (t) {
                e.columns = e.element.children(), e.elements = e.options.target ? e.find(e.options.target) : e.columns, e.match()
            }), this.on("display.uk.check", function (t) {
                this.element.is(":visible") && this.match()
            }.bind(this)), i.push(this))
        },
        match: function () {
            var i = this.columns.filter(":visible:first");
            if (i.length) {
                var e = Math.ceil(100 * parseFloat(i.css("width")) / parseFloat(i.parent().css("width"))) >= 100;
                return e && !this.options.ignorestacked ? this.revert() : t.Utils.matchHeights(this.elements, this.options), this
            }
        },
        revert: function () {
            return this.elements.css("min-height", ""), this
        }
    }), t.component("gridMargin", {
        defaults: {
            cls: "uk-grid-margin",
            rowfirst: "uk-row-first"
        },
        boot: function () {
            t.ready(function (i) {
                t.$("[data-uk-grid-margin]", i).each(function () {
                    var i, e = t.$(this);
                    e.data("gridMargin") || (i = t.gridMargin(e, t.Utils.options(e.attr("data-uk-grid-margin"))))
                })
            })
        },
        init: function () {
            t.stackMargin(this.element, this.options)
        }
    })
}(UIkit),
function (t) {
    "use strict";

    function i(i, e) {
        return e ? ("object" == typeof i ? (i = i instanceof jQuery ? i : t.$(i), i.parent().length && (e.persist = i, e.persist.data("modalPersistParent", i.parent()))) : i = "string" == typeof i || "number" == typeof i ? t.$("<div></div>").html(i) : t.$("<div></div>").html("UIkit.modal Error: Unsupported data type: " + typeof i), i.appendTo(e.element.find(".uk-modal-dialog")), e) : void 0
    }
    var e, n = !1,
        o = 0,
        s = t.$html;
    t.component("modal", {
        defaults: {
            keyboard: !0,
            bgclose: !0,
            minScrollHeight: 150,
            center: !1,
            modal: !0
        },
        scrollable: !1,
        transition: !1,
        hasTransitioned: !0,
        init: function () {
            if (e || (e = t.$("body")), this.element.length) {
                var i = this;
                this.paddingdir = "padding-" + ("left" == t.langdirection ? "right" : "left"), this.dialog = this.find(".uk-modal-dialog"), this.active = !1, this.element.attr("aria-hidden", this.element.hasClass("uk-open")), this.on("click", ".uk-modal-close", function (t) {
                    t.preventDefault(), i.hide()
                }).on("click", function (e) {
                    var n = t.$(e.target);
                    n[0] == i.element[0] && i.options.bgclose && i.hide()
                })
            }
        },
        toggle: function () {
            return this[this.isActive() ? "hide" : "show"]()
        },
        show: function () {
            if (this.element.length) {
                var i = this;
                if (!this.isActive()) return this.options.modal && n && n.hide(!0), this.element.removeClass("uk-open").show(), this.resize(), this.options.modal && (n = this), this.active = !0, o++, t.support.transition ? (this.hasTransitioned = !1, this.element.one(t.support.transition.end, function () {
                    i.hasTransitioned = !0
                }).addClass("uk-open")) : this.element.addClass("uk-open"), s.addClass("uk-modal-page").height(), this.element.attr("aria-hidden", "false"), this.element.trigger("show.uk.modal"), t.Utils.checkDisplay(this.dialog, !0), this
            }
        },
        hide: function (i) {
            if (!i && t.support.transition && this.hasTransitioned) {
                var e = this;
                this.one(t.support.transition.end, function () {
                    e._hide()
                }).removeClass("uk-open")
            } else this._hide();
            return this
        },
        resize: function () {
            var t = e.width();
            if (this.scrollbarwidth = window.innerWidth - t, e.css(this.paddingdir, this.scrollbarwidth), this.element.css("overflow-y", this.scrollbarwidth ? "scroll" : "auto"), !this.updateScrollable() && this.options.center) {
                var i = this.dialog.outerHeight(),
                    n = parseInt(this.dialog.css("margin-top"), 10) + parseInt(this.dialog.css("margin-bottom"), 10);
                i + n < window.innerHeight ? this.dialog.css({
                    top: window.innerHeight / 2 - i / 2 - n
                }) : this.dialog.css({
                    top: ""
                })
            }
        },
        updateScrollable: function () {
            var t = this.dialog.find(".uk-overflow-container:visible:first");
            if (t.length) {
                t.css("height", 0);
                var i = Math.abs(parseInt(this.dialog.css("margin-top"), 10)),
                    e = this.dialog.outerHeight(),
                    n = window.innerHeight,
                    o = n - 2 * (20 > i ? 20 : i) - e;
                return t.css({
                    "max-height": o < this.options.minScrollHeight ? "" : o,
                    height: ""
                }), !0
            }
            return !1
        },
        _hide: function () {
            this.active = !1, o > 0 ? o-- : o = 0, this.element.hide().removeClass("uk-open"), this.element.attr("aria-hidden", "true"), o || (s.removeClass("uk-modal-page"), e.css(this.paddingdir, "")), n === this && (n = !1), this.trigger("hide.uk.modal")
        },
        isActive: function () {
            return this.active
        }
    }), t.component("modalTrigger", {
        boot: function () {
            t.$html.on("click.modal.uikit", "[data-uk-modal]", function (i) {
                var e = t.$(this);
                if (e.is("a") && i.preventDefault(), !e.data("modalTrigger")) {
                    var n = t.modalTrigger(e, t.Utils.options(e.attr("data-uk-modal")));
                    n.show()
                }
            }), t.$html.on("keydown.modal.uikit", function (t) {
                n && 27 === t.keyCode && n.options.keyboard && (t.preventDefault(), n.hide())
            }), t.$win.on("resize orientationchange", t.Utils.debounce(function () {
                n && n.resize()
            }, 150))
        },
        init: function () {
            var i = this;
            this.options = t.$.extend({
                target: i.element.is("a") ? i.element.attr("href") : !1
            }, this.options), this.modal = t.modal(this.options.target, this.options), this.on("click", function (t) {
                t.preventDefault(), i.show()
            }), this.proxy(this.modal, "show hide isActive")
        }
    }), t.modal.dialog = function (e, n) {
        var o = t.modal(t.$(t.modal.dialog.template).appendTo("body"), n);
        return o.on("hide.uk.modal", function () {
            o.persist && (o.persist.appendTo(o.persist.data("modalPersistParent")), o.persist = !1), o.element.remove()
        }), i(e, o), o
    }, t.modal.dialog.template = '<div class="uk-modal"><div class="uk-modal-dialog" style="min-height:0;"></div></div>', t.modal.alert = function (i, e) {
        e = t.$.extend(!0, {
            bgclose: !1,
            keyboard: !1,
            modal: !1,
            labels: t.modal.labels
        }, e);
        var n = t.modal.dialog(['<div class="uk-margin uk-modal-content">' + String(i) + "</div>", '<div class="uk-modal-footer uk-text-right"><button class="uk-button uk-button-primary uk-modal-close">' + e.labels.Ok + "</button></div>"].join(""), e);
        return n.on("show.uk.modal", function () {
            setTimeout(function () {
                n.element.find("button:first").focus()
            }, 50)
        }), n.show()
    }, t.modal.confirm = function (i, e, n) {
        var o = arguments.length > 1 && arguments[arguments.length - 1] ? arguments[arguments.length - 1] : {};
        e = t.$.isFunction(e) ? e : function () {}, n = t.$.isFunction(n) ? n : function () {}, o = t.$.extend(!0, {
            bgclose: !1,
            keyboard: !1,
            modal: !1,
            labels: t.modal.labels
        }, t.$.isFunction(o) ? {} : o);
        var s = t.modal.dialog(['<div class="uk-margin uk-modal-content">' + String(i) + "</div>", '<div class="uk-modal-footer uk-text-right"><button class="uk-button js-modal-confirm-cancel">' + o.labels.Cancel + '</button> <button class="uk-button uk-button-primary js-modal-confirm">' + o.labels.Ok + "</button></div>"].join(""), o);
        return s.element.find(".js-modal-confirm, .js-modal-confirm-cancel").on("click", function () {
            t.$(this).is(".js-modal-confirm") ? e() : n(), s.hide()
        }), s.on("show.uk.modal", function () {
            setTimeout(function () {
                s.element.find(".js-modal-confirm").focus()
            }, 50)
        }), s.show()
    }, t.modal.prompt = function (i, e, n, o) {
        n = t.$.isFunction(n) ? n : function (t) {}, o = t.$.extend(!0, {
            bgclose: !1,
            keyboard: !1,
            modal: !1,
            labels: t.modal.labels
        }, o);
        var s = t.modal.dialog([i ? '<div class="uk-modal-content uk-form">' + String(i) + "</div>" : "", '<div class="uk-margin-small-top uk-modal-content uk-form"><p><input type="text" class="uk-width-1-1"></p></div>', '<div class="uk-modal-footer uk-text-right"><button class="uk-button uk-modal-close">' + o.labels.Cancel + '</button> <button class="uk-button uk-button-primary js-modal-ok">' + o.labels.Ok + "</button></div>"].join(""), o),
            a = s.element.find("input[type='text']").val(e || "").on("keyup", function (t) {
                13 == t.keyCode && s.element.find(".js-modal-ok").trigger("click")
            });
        return s.element.find(".js-modal-ok").on("click", function () {
            n(a.val()) !== !1 && s.hide()
        }), s.on("show.uk.modal", function () {
            setTimeout(function () {
                a.focus()
            }, 50)
        }), s.show()
    }, t.modal.blockUI = function (i, e) {
        var n = t.modal.dialog(['<div class="uk-margin uk-modal-content">' + String(i || '<div class="uk-text-center">...</div>') + "</div>"].join(""), t.$.extend({
            bgclose: !1,
            keyboard: !1,
            modal: !1
        }, e));
        return n.content = n.element.find(".uk-modal-content:first"), n.show()
    }, t.modal.labels = {
        Ok: "Ok",
        Cancel: "Cancel"
    }
}(UIkit),
function (t) {
    "use strict";

    function i(i) {
        var e = t.$(i),
            n = "auto";
        if (e.is(":visible")) n = e.outerHeight();
        else {
            var o = {
                position: e.css("position"),
                visibility: e.css("visibility"),
                display: e.css("display")
            };
            n = e.css({
                position: "absolute",
                visibility: "hidden",
                display: "block"
            }).outerHeight(), e.css(o)
        }
        return n
    }
    t.component("nav", {
        defaults: {
            toggle: ">li.uk-parent > a[href='#']",
            lists: ">li.uk-parent > ul",
            multiple: !1
        },
        boot: function () {
            t.ready(function (i) {
                t.$("[data-uk-nav]", i).each(function () {
                    var i = t.$(this);
                    if (!i.data("nav")) {
                        t.nav(i, t.Utils.options(i.attr("data-uk-nav")))
                    }
                })
            })
        },
        init: function () {
            var i = this;
            this.on("click.uikit.nav", this.options.toggle, function (e) {
                e.preventDefault();
                var n = t.$(this);
                i.open(n.parent()[0] == i.element[0] ? n : n.parent("li"))
            }), this.find(this.options.lists).each(function () {
                var e = t.$(this),
                    n = e.parent(),
                    o = n.hasClass("uk-active");
                e.wrap('<div style="overflow:hidden;height:0;position:relative;"></div>'), n.data("list-container", e.parent()[o ? "removeClass" : "addClass"]("uk-hidden")), n.attr("aria-expanded", n.hasClass("uk-open")), o && i.open(n, !0)
            })
        },
        open: function (e, n) {
            var o = this,
                s = this.element,
                a = t.$(e),
                r = a.data("list-container");
            this.options.multiple || s.children(".uk-open").not(e).each(function () {
                var i = t.$(this);
                i.data("list-container") && i.data("list-container").stop().animate({
                    height: 0
                }, function () {
                    t.$(this).parent().removeClass("uk-open").end().addClass("uk-hidden")
                })
            }), a.toggleClass("uk-open"), a.attr("aria-expanded", a.hasClass("uk-open")), r && (a.hasClass("uk-open") && r.removeClass("uk-hidden"), n ? (r.stop().height(a.hasClass("uk-open") ? "auto" : 0), a.hasClass("uk-open") || r.addClass("uk-hidden"), this.trigger("display.uk.check")) : r.stop().animate({
                height: a.hasClass("uk-open") ? i(r.find("ul:first")) : 0
            }, function () {
                a.hasClass("uk-open") ? r.css("height", "") : r.addClass("uk-hidden"), o.trigger("display.uk.check")
            }))
        }
    })
}(UIkit),
function (t) {
    "use strict";
    var i = {
            x: window.scrollX,
            y: window.scrollY
        },
        e = (t.$win, t.$doc, t.$html),
        n = {
            show: function (n) {
                if (n = t.$(n), n.length) {
                    var o = t.$("body"),
                        s = n.find(".uk-offcanvas-bar:first"),
                        a = "right" == t.langdirection,
                        r = s.hasClass("uk-offcanvas-bar-flip") ? -1 : 1,
                        l = r * (a ? -1 : 1),
                        c = window.innerWidth - o.width();
                    i = {
                        x: window.pageXOffset,
                        y: window.pageYOffset
                    }
                    n.addClass("uk-active");
//                    o.css({width: window.innerWidth - c,height: window.innerHeight}).addClass("uk-offcanvas-page");
                   // o.addClass("uk-offcanvas-page");
//                    o.css(a ? "margin-right" : "margin-left", (a ? -1 : 1) * (s.outerWidth() * l)).width();
//                    
//                    e.css("margin-top", -1 * i.y);
                    e.css('overflow','hidden');
                    s.addClass("uk-offcanvas-bar-show");
                    this._initElement(n);
                    s.trigger("show.uk.offcanvas", [n, s]);
                    n.attr("aria-hidden", "false");
                }
            },
            hide: function (n) {
                var o = t.$("body"),
                    s = t.$(".uk-offcanvas.uk-active"),
                    a = "right" == t.langdirection,
                    r = s.find(".uk-offcanvas-bar:first"),
                    l = function () {
                        o.removeClass("uk-offcanvas-page").css({
                            width: "",
                            height: "",
                            "margin-left": "",
                            "margin-right": ""
                        }), s.removeClass("uk-active"), r.removeClass("uk-offcanvas-bar-show"), e.css("margin-top", ""), window.scrollTo(i.x, i.y), r.trigger("hide.uk.offcanvas", [s, r]), s.attr("aria-hidden", "true")
                    };
                e.css('overflow','auto');
                s.length && (t.support.transition && !n ? (o.one(t.support.transition.end, function () {
                    l()
                }).css(a ? "margin-right" : "margin-left", ""), setTimeout(function () {
                    r.removeClass("uk-offcanvas-bar-show")
                }, 0)) : l())
            },
            _initElement: function (i) {
                i.data("OffcanvasInit") || (i.on("click.uk.offcanvas swipeRight.uk.offcanvas swipeLeft.uk.offcanvas", function (i) {
                    var e = t.$(i.target);
                    if (!i.type.match(/swipe/) && !e.hasClass("uk-offcanvas-close")) {
                        if (e.hasClass("uk-offcanvas-bar")) return;
                        if (e.parents(".uk-offcanvas-bar:first").length) return
                    }
                    i.stopImmediatePropagation(), n.hide()
                }), i.on("click", "a[href*='#']", function (i) {
                    var e = t.$(this),
                        o = e.attr("href");
                    "#" != o && (t.$doc.one("hide.uk.offcanvas", function () {
                        var i;
                        try {
                            i = t.$(e[0].hash)
                        } catch (n) {
                            i = ""
                        }
                        i.length || (i = t.$('[name="' + e[0].hash.replace("#", "") + '"]')), i.length && t.Utils.scrollToElement ? t.Utils.scrollToElement(i, t.Utils.options(e.attr("data-uk-smooth-scroll") || "{}")) : window.location.href = o
                    }), n.hide())
                }), i.data("OffcanvasInit", !0))
            }
        };
    t.component("offcanvasTrigger", {
        boot: function () {
            e.on("click.offcanvas.uikit", "[data-uk-offcanvas]", function (i) {
                i.preventDefault();
                var e = t.$(this);
                if (!e.data("offcanvasTrigger")) {
                    t.offcanvasTrigger(e, t.Utils.options(e.attr("data-uk-offcanvas")));
                    e.trigger("click")
                }
            }), e.on("keydown.uk.offcanvas", function (t) {
                27 === t.keyCode && n.hide()
            })
        },
        init: function () {
            var i = this;
            this.options = t.$.extend({
                target: i.element.is("a") ? i.element.attr("href") : !1
            }, this.options), this.on("click", function (t) {
                t.preventDefault(), n.show(i.options.target)
            })
        }
    }), t.offcanvas = n
}(UIkit),
function (t) {
    "use strict";

    function i(i, e, n) {
        var o, s = t.$.Deferred(),
            a = i,
            r = i;
        return n[0] === e[0] ? (s.resolve(), s.promise()) : ("object" == typeof i && (a = i[0], r = i[1] || i[0]), t.$body.css("overflow-x", "hidden"), o = function () {
            e && e.hide().removeClass("uk-active " + r + " uk-animation-reverse"), n.addClass(a).one(t.support.animation.end, function () {
                n.removeClass("" + a).css({
                    opacity: "",
                    display: ""
                }), s.resolve(), t.$body.css("overflow-x", ""), e && e.css({
                    opacity: "",
                    display: ""
                })
            }.bind(this)).show()
        }, n.css("animation-duration", this.options.duration + "ms"), e && e.length ? (e.css("animation-duration", this.options.duration + "ms"), e.css("display", "none").addClass(r + " uk-animation-reverse").one(t.support.animation.end, function () {
            o()
        }.bind(this)).css("display", "")) : (n.addClass("uk-active"), o()), s.promise())
    }
    var e;
    t.component("switcher", {
        defaults: {
            connect: !1,
            toggle: ">*",
            active: 0,
            animation: !1,
            duration: 200,
            swiping: !0
        },
        animating: !1,
        boot: function () {
            t.ready(function (i) {
                t.$("[data-uk-switcher]", i).each(function () {
                    var i = t.$(this);
                    if (!i.data("switcher")) {
                        t.switcher(i, t.Utils.options(i.attr("data-uk-switcher")))
                    }
                })
            })
        },
        init: function () {
            var i = this;
            if (this.on("click.uikit.switcher", this.options.toggle, function (t) {
                    t.preventDefault(), i.show(this)
                }), this.options.connect) {
                this.connect = t.$(this.options.connect), this.connect.find(".uk-active").removeClass(".uk-active"), this.connect.length && (this.connect.children().attr("aria-hidden", "true"), this.connect.on("click", "[data-uk-switcher-item]", function (e) {
                    e.preventDefault();
                    var n = t.$(this).attr("data-uk-switcher-item");
                    if (i.index != n) switch (n) {
                        case "next":
                        case "previous":
                            i.show(i.index + ("next" == n ? 1 : -1));
                            break;
                        default:
                            i.show(parseInt(n, 10))
                    }
                }), this.options.swiping && this.connect.on("swipeRight swipeLeft", function (t) {
                    t.preventDefault(), window.getSelection().toString() || i.show(i.index + ("swipeLeft" == t.type ? 1 : -1))
                }));
                var e = this.find(this.options.toggle),
                    n = e.filter(".uk-active");
                if (n.length) this.show(n, !1);
                else {
                    if (this.options.active === !1) return;
                    n = e.eq(this.options.active), this.show(n.length ? n : e.eq(0), !1)
                }
                e.not(n).attr("aria-expanded", "false"), n.attr("aria-expanded", "true"), this.on("changed.uk.dom", function () {
                    i.connect = t.$(i.options.connect)
                })
            }
        },
        show: function (n, o) {
            if (!this.animating) {
                if (isNaN(n)) n = t.$(n);
                else {
                    var s = this.find(this.options.toggle);
                    n = 0 > n ? s.length - 1 : n, n = s.eq(s[n] ? n : 0)
                }
                var a = this,
                    s = this.find(this.options.toggle),
                    r = t.$(n),
                    l = e[this.options.animation] || function (t, n) {
                        if (!a.options.animation) return e.none.apply(a);
                        var o = a.options.animation.split(",");
                        return 1 == o.length && (o[1] = o[0]), o[0] = o[0].trim(), o[1] = o[1].trim(), i.apply(a, [o, t, n])
                    };
                o !== !1 && t.support.animation || (l = e.none), r.hasClass("uk-disabled") || (s.attr("aria-expanded", "false"), r.attr("aria-expanded", "true"), s.filter(".uk-active").removeClass("uk-active"), r.addClass("uk-active"), this.options.connect && this.connect.length && (this.index = this.find(this.options.toggle).index(r), -1 == this.index && (this.index = 0), this.connect.each(function () {
                    var i = t.$(this),
                        e = t.$(i.children()),
                        n = t.$(e.filter(".uk-active")),
                        o = t.$(e.eq(a.index));
                    a.animating = !0, l.apply(a, [n, o]).then(function () {
                        n.removeClass("uk-active"), o.addClass("uk-active"), n.attr("aria-hidden", "true"), o.attr("aria-hidden", "false"), t.Utils.checkDisplay(o, !0), a.animating = !1
                    })
                })), this.trigger("show.uk.switcher", [r]))
            }
        }
    }), e = {
        none: function () {
            var i = t.$.Deferred();
            return i.resolve(), i.promise()
        },
        fade: function (t, e) {
            return i.apply(this, ["uk-animation-fade", t, e])
        },
        "slide-bottom": function (t, e) {
            return i.apply(this, ["uk-animation-slide-bottom", t, e])
        },
        "slide-top": function (t, e) {
            return i.apply(this, ["uk-animation-slide-top", t, e])
        },
        "slide-vertical": function (t, e, n) {
            var o = ["uk-animation-slide-top", "uk-animation-slide-bottom"];
            return t && t.index() > e.index() && o.reverse(), i.apply(this, [o, t, e])
        },
        "slide-left": function (t, e) {
            return i.apply(this, ["uk-animation-slide-left", t, e])
        },
        "slide-right": function (t, e) {
            return i.apply(this, ["uk-animation-slide-right", t, e])
        },
        "slide-horizontal": function (t, e, n) {
            var o = ["uk-animation-slide-right", "uk-animation-slide-left"];
            return t && t.index() > e.index() && o.reverse(), i.apply(this, [o, t, e])
        },
        scale: function (t, e) {
            return i.apply(this, ["uk-animation-scale-up", t, e])
        }
    }, t.switcher.animations = e
}(UIkit),
function (t) {
    "use strict";
    t.component("tab", {
        defaults: {
            target: ">li:not(.uk-tab-responsive, .uk-disabled)",
            connect: !1,
            active: 0,
            animation: !1,
            duration: 200,
            swiping: !0
        },
        boot: function () {
            t.ready(function (i) {
                t.$("[data-uk-tab]", i).each(function () {
                    var i = t.$(this);
                    if (!i.data("tab")) {
                        t.tab(i, t.Utils.options(i.attr("data-uk-tab")))
                    }
                })
            })
        },
        init: function () {
            var i = this;
            this.current = !1, this.on("click.uikit.tab", this.options.target, function (e) {
                if (e.preventDefault(), !i.switcher || !i.switcher.animating) {
                    var n = i.find(i.options.target).not(this);
                    n.removeClass("uk-active").blur(), i.trigger("change.uk.tab", [t.$(this).addClass("uk-active"), i.current]), i.current = t.$(this), i.options.connect || (n.attr("aria-expanded", "false"), t.$(this).attr("aria-expanded", "true"))
                }
            }), this.options.connect && (this.connect = t.$(this.options.connect)), this.responsivetab = t.$('<li class="uk-tab-responsive uk-active"><a></a></li>').append('<div class="uk-dropdown uk-dropdown-small"><ul class="uk-nav uk-nav-dropdown"></ul><div>'), this.responsivetab.dropdown = this.responsivetab.find(".uk-dropdown"), this.responsivetab.lst = this.responsivetab.dropdown.find("ul"), this.responsivetab.caption = this.responsivetab.find("a:first"), this.element.hasClass("uk-tab-bottom") && this.responsivetab.dropdown.addClass("uk-dropdown-up"), this.responsivetab.lst.on("click.uikit.tab", "a", function (e) {
                e.preventDefault(), e.stopPropagation();
                var n = t.$(this);
                i.element.children("li:not(.uk-tab-responsive)").eq(n.data("index")).trigger("click")
            }), this.on("show.uk.switcher change.uk.tab", function (t, e) {
                i.responsivetab.caption.html(e.text())
            }), this.element.append(this.responsivetab), this.options.connect && (this.switcher = t.switcher(this.element, {
                toggle: ">li:not(.uk-tab-responsive)",
                connect: this.options.connect,
                active: this.options.active,
                animation: this.options.animation,
                duration: this.options.duration,
                swiping: this.options.swiping
            })), t.dropdown(this.responsivetab, {
                mode: "click"
            }), i.trigger("change.uk.tab", [this.element.find(this.options.target).not(".uk-tab-responsive").filter(".uk-active")]), this.check(), t.$win.on("resize orientationchange", t.Utils.debounce(function () {
                i.element.is(":visible") && i.check()
            }, 100)), this.on("display.uk.check", function () {
                i.element.is(":visible") && i.check()
            })
        },
        check: function () {
            var i = this.element.children("li:not(.uk-tab-responsive)").removeClass("uk-hidden");
            if (!i.length) return void this.responsivetab.addClass("uk-hidden");
            var e, n, o, s = i.eq(0).offset().top + Math.ceil(i.eq(0).height() / 2),
                a = !1;
            if (this.responsivetab.lst.empty(), i.each(function () {
                    t.$(this).offset().top > s && (a = !0)
                }), a)
                for (var r = 0; r < i.length; r++) e = t.$(i.eq(r)), n = e.find("a"), "none" == e.css("float") || e.attr("uk-dropdown") || (e.hasClass("uk-disabled") || (o = e[0].outerHTML.replace("<a ", '<a data-index="' + r + '" '), this.responsivetab.lst.append(o)), e.addClass("uk-hidden"));
            this.responsivetab[this.responsivetab.lst.children("li").length ? "removeClass" : "addClass"]("uk-hidden")
        }
    })
}(UIkit),
function (t) {
    "use strict";
    t.component("cover", {
        defaults: {
            automute: !0
        },
        boot: function () {
            t.ready(function (i) {
                t.$("[data-uk-cover]", i).each(function () {
                    var i = t.$(this);
                    if (!i.data("cover")) {
                        t.cover(i, t.Utils.options(i.attr("data-uk-cover")))
                    }
                })
            })
        },
        init: function () {
            if (this.parent = this.element.parent(), t.$win.on("load resize orientationchange", t.Utils.debounce(function () {
                    this.check()
                }.bind(this), 100)), this.on("display.uk.check", function (t) {
                    this.element.is(":visible") && this.check()
                }.bind(this)), this.check(), this.element.is("iframe") && this.options.automute) {
                var i = this.element.attr("src");
                this.element.attr("src", "").on("load", function () {
                    this.contentWindow.postMessage('{ "event": "command", "func": "mute", "method":"setVolume", "value":0}', "*")
                }).attr("src", [i, i.indexOf("?") > -1 ? "&" : "?", "enablejsapi=1&api=1"].join(""))
            }
        },
        check: function () {
            this.element.css({
                width: "",
                height: ""
            }), this.dimension = {
                w: this.element.width(),
                h: this.element.height()
            }, this.element.attr("width") && !isNaN(this.element.attr("width")) && (this.dimension.w = this.element.attr("width")), this.element.attr("height") && !isNaN(this.element.attr("height")) && (this.dimension.h = this.element.attr("height")), this.ratio = this.dimension.w / this.dimension.h;
            var t, i, e = this.parent.width(),
                n = this.parent.height();
            e / this.ratio < n ? (t = Math.ceil(n * this.ratio), i = n) : (t = e, i = Math.ceil(e / this.ratio)), this.element.css({
                width: t,
                height: i
            })
        }
    })
}(UIkit);
! function (t) {
    var e;
    window.UIkit && (e = t(UIkit)), "function" == typeof define && define.amd && define("uikit-autocomplete", ["uikit"], function () {
        return e || t(UIkit)
    })
}(function (t) {
    "use strict";
    var e;
    return t.component("autocomplete", {
        defaults: {
            minLength: 3,
            param: "search",
            method: "post",
            delay: 300,
            loadingClass: "uk-loading",
            flipDropdown: !1,
            skipClass: "uk-skip",
            hoverClass: "uk-active",
            source: null,
            renderer: null,
            template: '<ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results">{{~items}}<li data-value="{{$item.value}}"><a>{{$item.value}}</a></li>{{/items}}</ul>'
        },
        visible: !1,
        value: null,
        selected: null,
        boot: function () {
            t.$html.on("focus.autocomplete.uikit", "[data-uk-autocomplete]", function (e) {
                var i = t.$(this);
                i.data("autocomplete") || t.autocomplete(i, t.Utils.options(i.attr("data-uk-autocomplete")))
            }), t.$html.on("click.autocomplete.uikit", function (t) {
                e && t.target != e.input[0] && e.hide()
            })
        },
        init: function () {
            var e = this,
                i = !1,
                s = t.Utils.debounce(function (t) {
                    return i ? i = !1 : void e.handle()
                }, this.options.delay);
            this.dropdown = this.find(".uk-dropdown"), this.template = this.find('script[type="text/autocomplete"]').html(), this.template = t.Utils.template(this.template || this.options.template), this.input = this.find("input:first").attr("autocomplete", "off"), this.dropdown.length || (this.dropdown = t.$('<div class="uk-dropdown"></div>').appendTo(this.element)), this.options.flipDropdown && this.dropdown.addClass("uk-dropdown-flip"), this.dropdown.attr("aria-expanded", "false"), this.input.on({
                keydown: function (t) {
                    if (t && t.which && !t.shiftKey) switch (t.which) {
                        case 13:
                            i = !0, e.selected && (t.preventDefault(), e.select());
                            break;
                        case 38:
                            t.preventDefault(), e.pick("prev", !0);
                            break;
                        case 40:
                            t.preventDefault(), e.pick("next", !0);
                            break;
                        case 27:
                        case 9:
                            e.hide()
                    }
                },
                keyup: s
            }), this.dropdown.on("click", ".uk-autocomplete-results > *", function () {
                e.select()
            }), this.dropdown.on("mouseover", ".uk-autocomplete-results > *", function () {
                e.pick(t.$(this))
            }), this.triggercomplete = s
        },
        handle: function () {
            var t = this,
                e = this.value;
            return this.value = this.input.val(), this.value.length < this.options.minLength ? this.hide() : (this.value != e && t.request(), this)
        },
        pick: function (e, i) {
            var s = this,
                o = t.$(this.dropdown.find(".uk-autocomplete-results").children(":not(." + this.options.skipClass + ")")),
                n = !1;
            if ("string" == typeof e || e.hasClass(this.options.skipClass)) {
                if ("next" == e || "prev" == e) {
                    if (this.selected) {
                        var a = o.index(this.selected);
                        n = "next" == e ? o.eq(a + 1 < o.length ? a + 1 : 0) : o.eq(0 > a - 1 ? o.length - 1 : a - 1)
                    } else n = o["next" == e ? "first" : "last"]();
                    n = t.$(n)
                }
            } else n = e;
            if (n && n.length && (this.selected = n, o.removeClass(this.options.hoverClass), this.selected.addClass(this.options.hoverClass), i)) {
                var l = n.position().top,
                    h = s.dropdown.scrollTop(),
                    r = s.dropdown.height();
                (l > r || 0 > l) && s.dropdown.scrollTop(h + l)
            }
        },
        select: function () {
            if (this.selected) {
                var t = this.selected.data();
                this.trigger("selectitem.uk.autocomplete", [t, this]), t.value && this.input.val(t.value).trigger("change"), this.hide()
            }
        },
        show: function () {
            return this.visible ? void 0 : (this.visible = !0, this.element.addClass("uk-open"), e && e !== this && e.hide(), e = this, this.dropdown.attr("aria-expanded", "true"), this)
        },
        hide: function () {
            return this.visible ? (this.visible = !1, this.element.removeClass("uk-open"), e === this && (e = !1), this.dropdown.attr("aria-expanded", "false"), this) : void 0
        },
        request: function () {
            var e = this,
                i = function (t) {
                    t && e.render(t), e.element.removeClass(e.options.loadingClass)
                };
            if (this.element.addClass(this.options.loadingClass), this.options.source) {
                var s = this.options.source;
                switch (typeof this.options.source) {
                    case "function":
                        this.options.source.apply(this, [i]);
                        break;
                    case "object":
                        if (s.length) {
                            var o = [];
                            s.forEach(function (t) {
                                t.value && -1 != t.value.toLowerCase().indexOf(e.value.toLowerCase()) && o.push(t)
                            }), i(o)
                        }
                        break;
                    case "string":
                        var n = {};
                        n[this.options.param] = this.value, t.$.ajax({
                            url: this.options.source,
                            data: n,
                            type: this.options.method,
                            dataType: "json"
                        }).done(function (t) {
                            i(t || [])
                        });
                        break;
                    default:
                        i(null)
                }
            } else this.element.removeClass(e.options.loadingClass)
        },
        render: function (t) {
            return this.dropdown.empty(), this.selected = !1, this.options.renderer ? this.options.renderer.apply(this, [t]) : t && t.length && (this.dropdown.append(this.template({
                items: t
            })), this.show(), this.trigger("show.uk.autocomplete")), this
        }
    }), t.autocomplete
});
! function (e) {
    var s;
    window.UIkit && (s = e(UIkit)), "function" == typeof define && define.amd && define("uikit-search", ["uikit"], function () {
        return s || e(UIkit)
    })
}(function (e) {
    "use strict";
    e.component("search", {
        defaults: {
            msgResultsHeader: "Search Results",
            msgMoreResults: "More Results",
            msgNoResults: "No results found",
            template: '<ul class="uk-nav uk-nav-search uk-autocomplete-results">                                      {{#msgResultsHeader}}<li class="uk-nav-header uk-skip">{{msgResultsHeader}}</li>{{/msgResultsHeader}}                                      {{#items && items.length}}                                          {{~items}}                                          <li data-url="{{!$item.url}}">                                              <a href="{{!$item.url}}">                                                  {{{$item.title}}}                                                  {{#$item.text}}<div>{{{$item.text}}}</div>{{/$item.text}}                                              </a>                                          </li>                                          {{/items}}                                          {{#msgMoreResults}}                                              <li class="uk-nav-divider uk-skip"></li>                                              <li class="uk-search-moreresults" data-moreresults="true"><a href="#" onclick="jQuery(this).closest(\'form\').submit();">{{msgMoreResults}}</a></li>                                          {{/msgMoreResults}}                                      {{/end}}                                      {{^items.length}}                                        {{#msgNoResults}}<li class="uk-skip"><a>{{msgNoResults}}</a></li>{{/msgNoResults}}                                      {{/end}}                                  </ul>',
            renderer: function (e) {
                var s = this.options;
                this.dropdown.append(this.template({
                    items: e.results || [],
                    msgResultsHeader: s.msgResultsHeader,
                    msgMoreResults: s.msgMoreResults,
                    msgNoResults: s.msgNoResults
                })), this.show()
            }
        },
        boot: function () {
            e.$html.on("focus.search.uikit", "[data-uk-search]", function (s) {
                var t = e.$(this);
                t.data("search") || e.search(t, e.Utils.options(t.attr("data-uk-search")))
            })
        },
        init: function () {
            var s = this;
            this.autocomplete = e.autocomplete(this.element, this.options), this.autocomplete.dropdown.addClass("uk-dropdown-search"), this.autocomplete.input.on("keyup", function () {
                s.element[s.autocomplete.input.val() ? "addClass" : "removeClass"]("uk-active")
            }).closest("form").on("reset", function () {
                s.value = "", s.element.removeClass("uk-active")
            }), this.on("selectitem.uk.autocomplete", function (e, t) {
                t.url ? location.href = t.url : t.moreresults && s.autocomplete.input.closest("form").submit()
            }), this.element.data("search", this)
        }
    })
});
! function (t) {
    var i;
    window.UIkit && (i = t(UIkit)), "function" == typeof define && define.amd && define("uikit-sticky", ["uikit"], function () {
        return i || t(UIkit)
    })
}(function (t) {
    "use strict";

    function i(i) {
        var o = arguments.length ? arguments : n;
        if (o.length && !(e.scrollTop() < 0))
            for (var a, r, p, h, c = e.scrollTop(), l = s.height(), d = e.height(), u = l - d, m = c > u ? u - c : 0, f = 0; f < o.length; f++)
                if (h = o[f], h.element.is(":visible") && !h.animate) {
                    if (h.check()) {
                        if (h.top < 0 ? a = 0 : (p = h.element.outerHeight(), a = l - p - h.top - h.options.bottom - c - m, a = 0 > a ? a + h.top : h.top), h.boundary && h.boundary.length) {
                            var g = h.boundary.offset().top;
                            r = h.boundtoparent ? l - (g + h.boundary.outerHeight()) + parseInt(h.boundary.css("padding-bottom")) : l - g - parseInt(h.boundary.css("margin-top")), a = c + p > l - r - (h.top < 0 ? 0 : h.top) ? l - r - (c + p) : a
                        }
                        if (h.currentTop != a) {
                            if (h.element.css({
                                    position: "fixed",
                                    top: a,
                                    width: h.getWidthFrom.length ? h.getWidthFrom.width() : h.element.width(),
                                    left: h.wrapper.offset().left
                                }), !h.init && (h.element.addClass(h.options.clsinit), location.hash && c > 0 && h.options.target)) {
                                var w = t.$(location.hash);
                                w.length && setTimeout(function (t, i) {
                                    return function () {
                                        i.element.width();
                                        var e = t.offset(),
                                            s = e.top + t.outerHeight(),
                                            n = i.element.offset(),
                                            o = i.element.outerHeight(),
                                            a = n.top + o;
                                        n.top < s && e.top < a && (c = e.top - o - i.options.target, window.scrollTo(0, c))
                                    }
                                }(w, h), 0)
                            }
                            h.element.addClass(h.options.clsactive).removeClass(h.options.clsinactive), h.element.trigger("active.uk.sticky"), h.element.css("margin", ""), h.options.animation && h.init && !t.Utils.isInView(h.wrapper) && h.element.addClass(h.options.animation), h.currentTop = a
                        }
                    } else null !== h.currentTop && h.reset();
                    h.init = !0
                }
    }
    var e = t.$win,
        s = t.$doc,
        n = [],
        o = 1;
    return t.component("sticky", {
        defaults: {
            top: 0,
            bottom: 0,
            animation: "",
            clsinit: "uk-sticky-init",
            clsactive: "uk-active",
            clsinactive: "",
            getWidthFrom: "",
            showup: !1,
            boundary: !1,
            media: !1,
            target: !1,
            disabled: !1
        },
        boot: function () {
            t.$doc.on("scrolling.uk.document", function (t, e) {
                e && e.dir && (o = e.dir.y, i())
            }), t.$win.on("resize orientationchange", t.Utils.debounce(function () {
                if (n.length) {
                    for (var t = 0; t < n.length; t++) n[t].reset(!0);
                    i()
                }
            }, 100)), t.ready(function (e) {
                setTimeout(function () {
                    t.$("[data-uk-sticky]", e).each(function () {
                        var i = t.$(this);
                        i.data("sticky") || t.sticky(i, t.Utils.options(i.attr("data-uk-sticky")))
                    }), i()
                }, 0)
            })
        },
        init: function () {
            var i, a = t.$('<div class="uk-sticky-placeholder"></div>'),
                r = this.options.boundary;
            this.wrapper = this.element.css("margin", 0).wrap(a).parent(), this.computeWrapper(), r && (r === !0 || "!" === r[0] ? (r = r === !0 ? this.wrapper.parent() : this.wrapper.closest(r.substr(1)), i = !0) : "string" == typeof r && (r = t.$(r))), this.sticky = {
                self: this,
                options: this.options,
                element: this.element,
                currentTop: null,
                wrapper: this.wrapper,
                init: !1,
                getWidthFrom: t.$(this.options.getWidthFrom || this.wrapper),
                boundary: r,
                boundtoparent: i,
                top: 0,
                calcTop: function () {
                    var i = this.options.top;
                    if (this.options.top && "string" == typeof this.options.top)
                        if (this.options.top.match(/^(-|)(\d+)vh$/)) i = window.innerHeight * parseInt(this.options.top, 10) / 100;
                        else {
                            var e = t.$(this.options.top).first();
                            e.length && e.is(":visible") && (i = -1 * (e.offset().top + e.outerHeight() - this.wrapper.offset().top))
                        }
                    this.top = i
                },
                reset: function (i) {
                    this.calcTop();
                    var e = function () {
                        this.element.css({
                            position: "",
                            top: "",
                            width: "",
                            left: "",
                            margin: "0"
                        }), this.element.removeClass([this.options.animation, "uk-animation-reverse", this.options.clsactive].join(" ")), this.element.addClass(this.options.clsinactive), this.element.trigger("inactive.uk.sticky"), this.currentTop = null, this.animate = !1
                    }.bind(this);
                    !i && this.options.animation && t.support.animation && !t.Utils.isInView(this.wrapper) ? (this.animate = !0, this.element.removeClass(this.options.animation).one(t.support.animation.end, function () {
                        e()
                    }).width(), this.element.addClass(this.options.animation + " uk-animation-reverse")) : e()
                },
                check: function () {
                    if (this.options.disabled) return !1;
                    if (this.options.media) switch (typeof this.options.media) {
                        case "number":
                            if (window.innerWidth < this.options.media) return !1;
                            break;
                        case "string":
                            if (window.matchMedia && !window.matchMedia(this.options.media).matches) return !1
                    }
                    var i = e.scrollTop(),
                        n = s.height(),
                        a = n - window.innerHeight,
                        r = i > a ? a - i : 0,
                        p = this.wrapper.offset().top,
                        h = p - this.top - r,
                        c = i >= h;
                    return c && this.options.showup && (1 == o && (c = !1), -1 == o && !this.element.hasClass(this.options.clsactive) && t.Utils.isInView(this.wrapper) && (c = !1)), c
                }
            }, this.sticky.calcTop(), n.push(this.sticky)
        },
        update: function () {
            i(this.sticky)
        },
        enable: function () {
            this.options.disabled = !1, this.update()
        },
        disable: function (t) {
            this.options.disabled = !0, this.sticky.reset(t)
        },
        computeWrapper: function () {
            this.wrapper.css({
                height: -1 == ["absolute", "fixed"].indexOf(this.element.css("position")) ? this.element.outerHeight() : "",
                "float": "none" != this.element.css("float") ? this.element.css("float") : "",
                margin: this.element.css("margin")
            }), "fixed" == this.element.css("position") && this.element.css({
                width: this.sticky.getWidthFrom.length ? this.sticky.getWidthFrom.width() : this.element.width()
            })
        }
    }), t.sticky
});
! function (t) {
    var i;
    window.UIkit && (i = t(UIkit)), "function" == typeof define && define.amd && define("uikit-tooltip", ["uikit"], function () {
        return i || t(UIkit)
    })
}(function (t) {
    "use strict";
    var i, o, e;
    return t.component("tooltip", {
        defaults: {
            offset: 5,
            pos: "top",
            animation: !1,
            delay: 0,
            cls: "",
            activeClass: "uk-active",
            src: function (t) {
                var i = t.attr("title");
                return void 0 !== i && t.data("cached-title", i).removeAttr("title"), t.data("cached-title")
            }
        },
        tip: "",
        boot: function () {
            t.$html.on("mouseenter.tooltip.uikit focus.tooltip.uikit", "[data-uk-tooltip]", function (i) {
                var o = t.$(this);
                o.data("tooltip") || (t.tooltip(o, t.Utils.options(o.attr("data-uk-tooltip"))), o.trigger("mouseenter"))
            })
        },
        init: function () {
            var o = this;
            i || (i = t.$('<div class="uk-tooltip"></div>').appendTo("body")), this.on({
                focus: function (t) {
                    o.show()
                },
                blur: function (t) {
                    o.hide()
                },
                mouseenter: function (t) {
                    o.show()
                },
                mouseleave: function (t) {
                    o.hide()
                }
            })
        },
        show: function () {
            if (this.tip = "function" == typeof this.options.src ? this.options.src(this.element) : this.options.src, o && clearTimeout(o), e && clearTimeout(e), "string" == typeof this.tip ? this.tip.length : 0) {
                i.stop().css({
                    top: -2e3,
                    visibility: "hidden"
                }).removeClass(this.options.activeClass).show(), i.html('<div class="uk-tooltip-inner">' + this.tip + "</div>");
                var s = this,
                    n = t.$.extend({}, this.element.offset(), {
                        width: this.element[0].offsetWidth,
                        height: this.element[0].offsetHeight
                    }),
                    l = i[0].offsetWidth,
                    f = i[0].offsetHeight,
                    p = "function" == typeof this.options.offset ? this.options.offset.call(this.element) : this.options.offset,
                    a = "function" == typeof this.options.pos ? this.options.pos.call(this.element) : this.options.pos,
                    h = a.split("-"),
                    c = {
                        display: "none",
                        visibility: "visible",
                        top: n.top + n.height + f,
                        left: n.left
                    };
                if ("fixed" == t.$html.css("position") || "fixed" == t.$body.css("position")) {
                    var r = t.$("body").offset(),
                        d = t.$("html").offset(),
                        u = {
                            top: d.top + r.top,
                            left: d.left + r.left
                        };
                    n.left -= u.left, n.top -= u.top
                }
                "left" != h[0] && "right" != h[0] || "right" != t.langdirection || (h[0] = "left" == h[0] ? "right" : "left");
                var m = {
                    bottom: {
                        top: n.top + n.height + p,
                        left: n.left + n.width / 2 - l / 2
                    },
                    top: {
                        top: n.top - f - p,
                        left: n.left + n.width / 2 - l / 2
                    },
                    left: {
                        top: n.top + n.height / 2 - f / 2,
                        left: n.left - l - p
                    },
                    right: {
                        top: n.top + n.height / 2 - f / 2,
                        left: n.left + n.width + p
                    }
                };
                t.$.extend(c, m[h[0]]), 2 == h.length && (c.left = "left" == h[1] ? n.left : n.left + n.width - l);
                var v = this.checkBoundary(c.left, c.top, l, f);
                if (v) {
                    switch (v) {
                        case "x":
                            a = 2 == h.length ? h[0] + "-" + (c.left < 0 ? "left" : "right") : c.left < 0 ? "right" : "left";
                            break;
                        case "y":
                            a = 2 == h.length ? (c.top < 0 ? "bottom" : "top") + "-" + h[1] : c.top < 0 ? "bottom" : "top";
                            break;
                        case "xy":
                            a = 2 == h.length ? (c.top < 0 ? "bottom" : "top") + "-" + (c.left < 0 ? "left" : "right") : c.left < 0 ? "right" : "left"
                    }
                    h = a.split("-"), t.$.extend(c, m[h[0]]), 2 == h.length && (c.left = "left" == h[1] ? n.left : n.left + n.width - l)
                }
                c.left -= t.$body.position().left, o = setTimeout(function () {
                    i.css(c).attr("class", ["uk-tooltip", "uk-tooltip-" + a, s.options.cls].join(" ")), s.options.animation ? i.css({
                        opacity: 0,
                        display: "block"
                    }).addClass(s.options.activeClass).animate({
                        opacity: 1
                    }, parseInt(s.options.animation, 10) || 400) : i.show().addClass(s.options.activeClass), o = !1, e = setInterval(function () {
                        s.element.is(":visible") || s.hide()
                    }, 150)
                }, parseInt(this.options.delay, 10) || 0)
            }
        },
        hide: function () {
            if (!this.element.is("input") || this.element[0] !== document.activeElement)
                if (o && clearTimeout(o), e && clearTimeout(e), i.stop(), this.options.animation) {
                    var t = this;
                    i.fadeOut(parseInt(this.options.animation, 10) || 400, function () {
                        i.removeClass(t.options.activeClass)
                    })
                } else i.hide().removeClass(this.options.activeClass)
        },
        content: function () {
            return this.tip
        },
        checkBoundary: function (i, o, e, s) {
            var n = "";
            return (0 > i || i - t.$win.scrollLeft() + e > window.innerWidth) && (n += "x"), (0 > o || o - t.$win.scrollTop() + s > window.innerHeight) && (n += "y"), n
        }
    }), t.tooltip
});
! function (t) {
    var i;
    window.UIkit && (i = t(UIkit)), "function" == typeof define && define.amd && define("uikit-accordion", ["uikit"], function () {
        return i || t(UIkit)
    })
}(function (t) {
    "use strict";

    function i(i) {
        var o = t.$(i),
            e = "auto";
        if (o.is(":visible")) e = o.outerHeight();
        else {
            var a = {
                position: o.css("position"),
                visibility: o.css("visibility"),
                display: o.css("display")
            };
            e = o.css({
                position: "absolute",
                visibility: "hidden",
                display: "block"
            }).outerHeight(), o.css(a)
        }
        return e
    }
    return t.component("accordion", {
        defaults: {
            showfirst: !0,
            collapse: !0,
            animate: !0,
            easing: "swing",
            duration: 300,
            toggle: ".uk-accordion-title",
            containers: ".uk-accordion-content",
            clsactive: "uk-active"
        },
        boot: function () {
            t.ready(function (i) {
                setTimeout(function () {
                    t.$("[data-uk-accordion]", i).each(function () {
                        var i = t.$(this);
                        i.data("accordion") || t.accordion(i, t.Utils.options(i.attr("data-uk-accordion")))
                    })
                }, 0)
            })
        },
        init: function () {
            var i = this;
            this.element.on("click.uikit.accordion", this.options.toggle, function (o) {
                o.preventDefault(), i.toggleItem(t.$(this).data("wrapper"), i.options.animate, i.options.collapse)
            }), this.update(), this.options.showfirst && this.toggleItem(this.toggle.eq(0).data("wrapper"), !1, !1)
        },
        toggleItem: function (o, e, a) {
            var n = this;
            o.data("toggle").toggleClass(this.options.clsactive), o.data("content").toggleClass(this.options.clsactive);
            var s = o.data("toggle").hasClass(this.options.clsactive);
            a && (this.toggle.not(o.data("toggle")).removeClass(this.options.clsactive), this.content.not(o.data("content")).removeClass(this.options.clsactive).parent().stop().css("overflow", "hidden").animate({
                height: 0
            }, {
                easing: this.options.easing,
                duration: e ? this.options.duration : 0
            }).attr("aria-expanded", "false")), o.stop().css("overflow", "hidden"), e ? o.animate({
                height: s ? i(o.data("content")) : 0
            }, {
                easing: this.options.easing,
                duration: this.options.duration,
                complete: function () {
                    s && (o.css({
                        overflow: "",
                        height: "auto"
                    }), t.Utils.checkDisplay(o.data("content"))), n.trigger("display.uk.check")
                }
            }) : (o.height(s ? "auto" : 0), s && (o.css({
                overflow: ""
            }), t.Utils.checkDisplay(o.data("content"))), this.trigger("display.uk.check")), o.attr("aria-expanded", s), this.element.trigger("toggle.uk.accordion", [s, o.data("toggle"), o.data("content")])
        },
        update: function () {
            var i, o, e, a = this;
            this.toggle = this.find(this.options.toggle), this.content = this.find(this.options.containers), this.content.each(function (n) {
                i = t.$(this), i.parent().data("wrapper") ? o = i.parent() : (o = t.$(this).wrap('<div data-wrapper="true" style="overflow:hidden;height:0;position:relative;"></div>').parent(), o.attr("aria-expanded", "false")), e = a.toggle.eq(n), o.data("toggle", e), o.data("content", i), e.data("wrapper", o), i.data("wrapper", o)
            }), this.element.trigger("update.uk.accordion", [this])
        }
    }), t.accordion
});
! function (t) {
    var e;
    window.UIkit && (e = t(UIkit)), "function" == typeof define && define.amd && define("uikit-datepicker", ["uikit"], function () {
        return e || t(UIkit)
    })
}(function (t) {
    "use strict";
    var e, n, a = !1;
    return t.component("datepicker", {
        defaults: {
            mobile: !1,
            weekstart: 1,
            i18n: {
                months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                weekdays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
            },
            format: "YYYY-MM-DD",
            offsettop: 5,
            maxDate: !1,
            minDate: !1,
            pos: "auto",
            template: function (e, n) {
                var a, s = "";
                if (s += '<div class="uk-datepicker-nav">', s += '<a href="" class="uk-datepicker-previous"></a>', s += '<a href="" class="uk-datepicker-next"></a>', t.formSelect) {
                    var i, r, o, u, c = (new Date).getFullYear(),
                        d = [];
                    for (a = 0; a < n.i18n.months.length; a++) a == e.month ? d.push('<option value="' + a + '" selected>' + n.i18n.months[a] + "</option>") : d.push('<option value="' + a + '">' + n.i18n.months[a] + "</option>");
                    for (i = '<span class="uk-form-select">' + n.i18n.months[e.month] + '<select class="update-picker-month">' + d.join("") + "</select></span>", d = [], o = e.minDate ? e.minDate.year() : c - 50, u = e.maxDate ? e.maxDate.year() : c + 20, a = o; u >= a; a++) a == e.year ? d.push('<option value="' + a + '" selected>' + a + "</option>") : d.push('<option value="' + a + '">' + a + "</option>");
                    r = '<span class="uk-form-select">' + e.year + '<select class="update-picker-year">' + d.join("") + "</select></span>", s += '<div class="uk-datepicker-heading">' + i + " " + r + "</div>"
                } else s += '<div class="uk-datepicker-heading">' + n.i18n.months[e.month] + " " + e.year + "</div>";
                for (s += "</div>", s += '<table class="uk-datepicker-table">', s += "<thead>", a = 0; a < e.weekdays.length; a++) e.weekdays[a] && (s += "<th>" + e.weekdays[a] + "</th>");
                for (s += "</thead>", s += "<tbody>", a = 0; a < e.days.length; a++)
                    if (e.days[a] && e.days[a].length) {
                        s += "<tr>";
                        for (var l = 0; l < e.days[a].length; l++)
                            if (e.days[a][l]) {
                                var h = e.days[a][l],
                                    f = [];
                                h.inmonth || f.push("uk-datepicker-table-muted"), h.selected && f.push("uk-active"), h.disabled && f.push("uk-datepicker-date-disabled uk-datepicker-table-muted"), s += '<td><a href="" class="' + f.join(" ") + '" data-date="' + h.day.format() + '">' + h.day.format("D") + "</a></td>"
                            }
                        s += "</tr>"
                    }
                return s += "</tbody>", s += "</table>"
            }
        },
        boot: function () {
            t.$win.on("resize orientationchange", function () {
                a && a.hide()
            }), t.$html.on("focus.datepicker.uikit", "[data-uk-datepicker]", function (e) {
                var n = t.$(this);
                n.data("datepicker") || (e.preventDefault(), t.datepicker(n, t.Utils.options(n.attr("data-uk-datepicker"))), n.trigger("focus"))
            }), t.$html.on("click focus", "*", function (n) {
                var s = t.$(n.target);
                !a || s[0] == e[0] || s.data("datepicker") || s.parents(".uk-datepicker:first").length || a.hide()
            })
        },
        init: function () {
            if (!t.support.touch || "date" != this.element.attr("type") || this.options.mobile) {
                var s = this;
                this.current = this.element.val() ? n(this.element.val(), this.options.format) : n(), this.on("click focus", function () {
                    a !== s && s.pick(this.value ? this.value : s.options.minDate ? s.options.minDate : "")
                }).on("change", function () {
                    s.element.val() && !n(s.element.val(), s.options.format).isValid() && s.element.val(n().format(s.options.format))
                }), e || (e = t.$('<div class="uk-dropdown uk-datepicker"></div>'), e.on("click", ".uk-datepicker-next, .uk-datepicker-previous, [data-date]", function (e) {
                    e.stopPropagation(), e.preventDefault();
                    var s = t.$(this);
                    return s.hasClass("uk-datepicker-date-disabled") ? !1 : void(s.is("[data-date]") ? (a.current = n(s.data("date")), a.element.val(a.current.format(a.options.format)).trigger("change"), a.hide()) : a.add(s.hasClass("uk-datepicker-next") ? 1 : -1, "months"))
                }), e.on("change", ".update-picker-month, .update-picker-year", function () {
                    var e = t.$(this);
                    a[e.is(".update-picker-year") ? "setYear" : "setMonth"](Number(e.val()))
                }), e.appendTo("body"))
            }
        },
        pick: function (s) {
            var i = this.element.offset(),
                r = {
                    left: i.left,
                    right: ""
                };
            this.current = isNaN(s) ? n(s, this.options.format) : n(), this.initdate = this.current.format("YYYY-MM-DD"), this.update(), "right" == t.langdirection && (r.right = window.innerWidth - (r.left + this.element.outerWidth()), r.left = "");
            var o = i.top - this.element.outerHeight() + this.element.height() - this.options.offsettop - e.outerHeight(),
                u = i.top + this.element.outerHeight() + this.options.offsettop;
            r.top = u, "top" == this.options.pos ? r.top = o : "auto" == this.options.pos && window.innerHeight - u - e.outerHeight() < 0 && o >= 0 && (r.top = o), e.css(r).show(), this.trigger("show.uk.datepicker"), a = this
        },
        add: function (t, e) {
            this.current.add(t, e), this.update()
        },
        setMonth: function (t) {
            this.current.month(t), this.update()
        },
        setYear: function (t) {
            this.current.year(t), this.update()
        },
        update: function () {
            var t = this.getRows(this.current.year(), this.current.month()),
                n = this.options.template(t, this.options);
            e.html(n), this.trigger("update.uk.datepicker")
        },
        getRows: function (t, e) {
            var a = this.options,
                s = n().format("YYYY-MM-DD"),
                i = [31, t % 4 === 0 && t % 100 !== 0 || t % 400 === 0 ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][e],
                r = new Date(t, e, 1, 12).getDay(),
                o = {
                    month: e,
                    year: t,
                    weekdays: [],
                    days: [],
                    maxDate: !1,
                    minDate: !1
                },
                u = [];
            a.maxDate !== !1 && (o.maxDate = isNaN(a.maxDate) ? n(a.maxDate, a.format) : n().add(a.maxDate, "days")), a.minDate !== !1 && (o.minDate = isNaN(a.minDate) ? n(a.minDate, a.format) : n().add(a.minDate - 1, "days")), o.weekdays = function () {
                for (var t = 0, e = []; 7 > t; t++) {
                    for (var n = t + (a.weekstart || 0); n >= 7;) n -= 7;
                    e.push(a.i18n.weekdays[n])
                }
                return e
            }(), a.weekstart && a.weekstart > 0 && (r -= a.weekstart, 0 > r && (r += 7));
            for (var c = i + r, d = c; d > 7;) d -= 7;
            c += 7 - d;
            for (var l, h, f, m, _, p = 0, y = 0; c > p; p++) l = new Date(t, e, 1 + (p - r), 12), h = o.minDate && o.minDate > l || o.maxDate && l > o.maxDate, _ = !(r > p || p >= i + r), l = n(l), f = this.initdate == l.format("YYYY-MM-DD"), m = s == l.format("YYYY-MM-DD"), u.push({
                selected: f,
                today: m,
                disabled: h,
                day: l,
                inmonth: _
            }), 7 === ++y && (o.days.push(u), u = [], y = 0);
            return o
        },
        hide: function () {
            a && a === this && (e.hide(), a = !1, this.trigger("hide.uk.datepicker"))
        }
    }), n = function (t) {
        function e(t, e, n) {
            switch (arguments.length) {
                case 2:
                    return null != t ? t : e;
                case 3:
                    return null != t ? t : null != e ? e : n;
                default:
                    throw new Error("Implement me")
            }
        }

        function n(t, e) {
            return Yt.call(t, e)
        }

        function a() {
            return {
                empty: !1,
                unusedTokens: [],
                unusedInput: [],
                overflow: -2,
                charsLeftOver: 0,
                nullInput: !1,
                invalidMonth: null,
                invalidFormat: !1,
                userInvalidated: !1,
                iso: !1
            }
        }

        function s(t) {
            Dt.suppressDeprecationWarnings === !1 && "undefined" != typeof console && console.warn && console.warn("Deprecation warning: " + t)
        }

        function i(t, e) {
            var n = !0;
            return h(function () {
                return n && (s(t), n = !1), e.apply(this, arguments)
            }, e)
        }

        function r(t, e) {
            fe[t] || (s(e), fe[t] = !0)
        }

        function o(t, e) {
            return function (n) {
                return _(t.call(this, n), e)
            }
        }

        function u(t, e) {
            return function (n) {
                return this.localeData().ordinal(t.call(this, n), e)
            }
        }

        function c() {}

        function d(t, e) {
            e !== !1 && F(t), f(this, t), this._d = new Date(+t._d)
        }

        function l(t) {
            var e = v(t),
                n = e.year || 0,
                a = e.quarter || 0,
                s = e.month || 0,
                i = e.week || 0,
                r = e.day || 0,
                o = e.hour || 0,
                u = e.minute || 0,
                c = e.second || 0,
                d = e.millisecond || 0;
            this._milliseconds = +d + 1e3 * c + 6e4 * u + 36e5 * o, this._days = +r + 7 * i, this._months = +s + 3 * a + 12 * n, this._data = {}, this._locale = Dt.localeData(), this._bubble()
        }

        function h(t, e) {
            for (var a in e) n(e, a) && (t[a] = e[a]);
            return n(e, "toString") && (t.toString = e.toString), n(e, "valueOf") && (t.valueOf = e.valueOf), t
        }

        function f(t, e) {
            var n, a, s;
            if ("undefined" != typeof e._isAMomentObject && (t._isAMomentObject = e._isAMomentObject), "undefined" != typeof e._i && (t._i = e._i), "undefined" != typeof e._f && (t._f = e._f), "undefined" != typeof e._l && (t._l = e._l), "undefined" != typeof e._strict && (t._strict = e._strict), "undefined" != typeof e._tzm && (t._tzm = e._tzm), "undefined" != typeof e._isUTC && (t._isUTC = e._isUTC), "undefined" != typeof e._offset && (t._offset = e._offset), "undefined" != typeof e._pf && (t._pf = e._pf), "undefined" != typeof e._locale && (t._locale = e._locale), Ft.length > 0)
                for (n in Ft) a = Ft[n], s = e[a], "undefined" != typeof s && (t[a] = s);
            return t
        }

        function m(t) {
            return 0 > t ? Math.ceil(t) : Math.floor(t)
        }

        function _(t, e, n) {
            for (var a = "" + Math.abs(t), s = t >= 0; a.length < e;) a = "0" + a;
            return (s ? n ? "+" : "" : "-") + a
        }

        function p(t, e) {
            var n = {
                milliseconds: 0,
                months: 0
            };
            return n.months = e.month() - t.month() + 12 * (e.year() - t.year()), t.clone().add(n.months, "M").isAfter(e) && --n.months, n.milliseconds = +e - +t.clone().add(n.months, "M"), n
        }

        function y(t, e) {
            var n;
            return e = I(e, t), t.isBefore(e) ? n = p(t, e) : (n = p(e, t), n.milliseconds = -n.milliseconds, n.months = -n.months), n
        }

        function D(t, e) {
            return function (n, a) {
                var s, i;
                return null === a || isNaN(+a) || (r(e, "moment()." + e + "(period, number) is deprecated. Please use moment()." + e + "(number, period)."), i = n, n = a, a = i), n = "string" == typeof n ? +n : n, s = Dt.duration(n, a), g(this, s, t), this
            }
        }

        function g(t, e, n, a) {
            var s = e._milliseconds,
                i = e._days,
                r = e._months;
            a = null == a ? !0 : a, s && t._d.setTime(+t._d + s * n), i && ft(t, "Date", ht(t, "Date") + i * n), r && lt(t, ht(t, "Month") + r * n), a && Dt.updateOffset(t, i || r)
        }

        function k(t) {
            return "[object Array]" === Object.prototype.toString.call(t)
        }

        function M(t) {
            return "[object Date]" === Object.prototype.toString.call(t) || t instanceof Date
        }

        function Y(t, e, n) {
            var a, s = Math.min(t.length, e.length),
                i = Math.abs(t.length - e.length),
                r = 0;
            for (a = 0; s > a; a++)(n && t[a] !== e[a] || !n && S(t[a]) !== S(e[a])) && r++;
            return r + i
        }

        function w(t) {
            if (t) {
                var e = t.toLowerCase().replace(/(.)s$/, "$1");
                t = re[t] || oe[e] || e
            }
            return t
        }

        function v(t) {
            var e, a, s = {};
            for (a in t) n(t, a) && (e = w(a), e && (s[e] = t[a]));
            return s
        }

        function b(e) {
            var n, a;
            if (0 === e.indexOf("week")) n = 7, a = "day";
            else {
                if (0 !== e.indexOf("month")) return;
                n = 12, a = "month"
            }
            Dt[e] = function (s, i) {
                var r, o, u = Dt._locale[e],
                    c = [];
                if ("number" == typeof s && (i = s, s = t), o = function (t) {
                        var e = Dt().utc().set(a, t);
                        return u.call(Dt._locale, e, s || "")
                    }, null != i) return o(i);
                for (r = 0; n > r; r++) c.push(o(r));
                return c
            }
        }

        function S(t) {
            var e = +t,
                n = 0;
            return 0 !== e && isFinite(e) && (n = e >= 0 ? Math.floor(e) : Math.ceil(e)), n
        }

        function T(t, e) {
            return new Date(Date.UTC(t, e + 1, 0)).getUTCDate()
        }

        function O(t, e, n) {
            return ot(Dt([t, 11, 31 + e - n]), e, n).week
        }

        function W(t) {
            return U(t) ? 366 : 365
        }

        function U(t) {
            return t % 4 === 0 && t % 100 !== 0 || t % 400 === 0
        }

        function F(t) {
            var e;
            t._a && -2 === t._pf.overflow && (e = t._a[vt] < 0 || t._a[vt] > 11 ? vt : t._a[bt] < 1 || t._a[bt] > T(t._a[wt], t._a[vt]) ? bt : t._a[St] < 0 || t._a[St] > 23 ? St : t._a[Tt] < 0 || t._a[Tt] > 59 ? Tt : t._a[Ot] < 0 || t._a[Ot] > 59 ? Ot : t._a[Wt] < 0 || t._a[Wt] > 999 ? Wt : -1, t._pf._overflowDayOfYear && (wt > e || e > bt) && (e = bt), t._pf.overflow = e)
        }

        function G(t) {
            return null == t._isValid && (t._isValid = !isNaN(t._d.getTime()) && t._pf.overflow < 0 && !t._pf.empty && !t._pf.invalidMonth && !t._pf.nullInput && !t._pf.invalidFormat && !t._pf.userInvalidated, t._strict && (t._isValid = t._isValid && 0 === t._pf.charsLeftOver && 0 === t._pf.unusedTokens.length)), t._isValid
        }

        function C(t) {
            return t ? t.toLowerCase().replace("_", "-") : t
        }

        function z(t) {
            for (var e, n, a, s, i = 0; i < t.length;) {
                for (s = C(t[i]).split("-"), e = s.length, n = C(t[i + 1]), n = n ? n.split("-") : null; e > 0;) {
                    if (a = x(s.slice(0, e).join("-"))) return a;
                    if (n && n.length >= e && Y(s, n, !0) >= e - 1) break;
                    e--
                }
                i++
            }
            return null
        }

        function x(t) {
            var e = null;
            if (!Ut[t] && Gt) try {
                e = Dt.locale(), require("./locale/" + t), Dt.locale(e)
            } catch (n) {}
            return Ut[t]
        }

        function I(t, e) {
            return e._isUTC ? Dt(t).zone(e._offset || 0) : Dt(t).local()
        }

        function H(t) {
            return t.match(/\[[\s\S]/) ? t.replace(/^\[|\]$/g, "") : t.replace(/\\/g, "")
        }

        function L(t) {
            var e, n, a = t.match(It);
            for (e = 0, n = a.length; n > e; e++) he[a[e]] ? a[e] = he[a[e]] : a[e] = H(a[e]);
            return function (s) {
                var i = "";
                for (e = 0; n > e; e++) i += a[e] instanceof Function ? a[e].call(s, t) : a[e];
                return i
            }
        }

        function P(t, e) {
            return t.isValid() ? (e = A(e, t.localeData()), ue[e] || (ue[e] = L(e)), ue[e](t)) : t.localeData().invalidDate()
        }

        function A(t, e) {
            function n(t) {
                return e.longDateFormat(t) || t
            }
            var a = 5;
            for (Ht.lastIndex = 0; a >= 0 && Ht.test(t);) t = t.replace(Ht, n), Ht.lastIndex = 0, a -= 1;
            return t
        }

        function N(t, e) {
            var n, a = e._strict;
            switch (t) {
                case "Q":
                    return qt;
                case "DDDD":
                    return Rt;
                case "YYYY":
                case "GGGG":
                case "gggg":
                    return a ? Xt : At;
                case "Y":
                case "G":
                case "g":
                    return Kt;
                case "YYYYYY":
                case "YYYYY":
                case "GGGGG":
                case "ggggg":
                    return a ? Bt : Nt;
                case "S":
                    if (a) return qt;
                case "SS":
                    if (a) return Qt;
                case "SSS":
                    if (a) return Rt;
                case "DDD":
                    return Pt;
                case "MMM":
                case "MMMM":
                case "dd":
                case "ddd":
                case "dddd":
                    return jt;
                case "a":
                case "A":
                    return e._locale._meridiemParse;
                case "X":
                    return Vt;
                case "Z":
                case "ZZ":
                    return Et;
                case "T":
                    return $t;
                case "SSSS":
                    return Zt;
                case "MM":
                case "DD":
                case "YY":
                case "GG":
                case "gg":
                case "HH":
                case "hh":
                case "mm":
                case "ss":
                case "ww":
                case "WW":
                    return a ? Qt : Lt;
                case "M":
                case "D":
                case "d":
                case "H":
                case "h":
                case "m":
                case "s":
                case "w":
                case "W":
                case "e":
                case "E":
                    return Lt;
                case "Do":
                    return Jt;
                default:
                    return n = new RegExp(R(Q(t.replace("\\", "")), "i"))
            }
        }

        function Z(t) {
            t = t || "";
            var e = t.match(Et) || [],
                n = e[e.length - 1] || [],
                a = (n + "").match(se) || ["-", 0, 0],
                s = +(60 * a[1]) + S(a[2]);
            return "+" === a[0] ? -s : s
        }

        function j(t, e, n) {
            var a, s = n._a;
            switch (t) {
                case "Q":
                    null != e && (s[vt] = 3 * (S(e) - 1));
                    break;
                case "M":
                case "MM":
                    null != e && (s[vt] = S(e) - 1);
                    break;
                case "MMM":
                case "MMMM":
                    a = n._locale.monthsParse(e), null != a ? s[vt] = a : n._pf.invalidMonth = e;
                    break;
                case "D":
                case "DD":
                    null != e && (s[bt] = S(e));
                    break;
                case "Do":
                    null != e && (s[bt] = S(parseInt(e, 10)));
                    break;
                case "DDD":
                case "DDDD":
                    null != e && (n._dayOfYear = S(e));
                    break;
                case "YY":
                    s[wt] = Dt.parseTwoDigitYear(e);
                    break;
                case "YYYY":
                case "YYYYY":
                case "YYYYYY":
                    s[wt] = S(e);
                    break;
                case "a":
                case "A":
                    n._isPm = n._locale.isPM(e);
                    break;
                case "H":
                case "HH":
                case "h":
                case "hh":
                    s[St] = S(e);
                    break;
                case "m":
                case "mm":
                    s[Tt] = S(e);
                    break;
                case "s":
                case "ss":
                    s[Ot] = S(e);
                    break;
                case "S":
                case "SS":
                case "SSS":
                case "SSSS":
                    s[Wt] = S(1e3 * ("0." + e));
                    break;
                case "X":
                    n._d = new Date(1e3 * parseFloat(e));
                    break;
                case "Z":
                case "ZZ":
                    n._useUTC = !0, n._tzm = Z(e);
                    break;
                case "dd":
                case "ddd":
                case "dddd":
                    a = n._locale.weekdaysParse(e), null != a ? (n._w = n._w || {}, n._w.d = a) : n._pf.invalidWeekday = e;
                    break;
                case "w":
                case "ww":
                case "W":
                case "WW":
                case "d":
                case "e":
                case "E":
                    t = t.substr(0, 1);
                case "gggg":
                case "GGGG":
                case "GGGGG":
                    t = t.substr(0, 2), e && (n._w = n._w || {}, n._w[t] = S(e));
                    break;
                case "gg":
                case "GG":
                    n._w = n._w || {}, n._w[t] = Dt.parseTwoDigitYear(e)
            }
        }

        function E(t) {
            var n, a, s, i, r, o, u;
            n = t._w, null != n.GG || null != n.W || null != n.E ? (r = 1, o = 4, a = e(n.GG, t._a[wt], ot(Dt(), 1, 4).year), s = e(n.W, 1), i = e(n.E, 1)) : (r = t._locale._week.dow, o = t._locale._week.doy, a = e(n.gg, t._a[wt], ot(Dt(), r, o).year), s = e(n.w, 1), null != n.d ? (i = n.d, r > i && ++s) : i = null != n.e ? n.e + r : r), u = ut(a, s, i, o, r), t._a[wt] = u.year, t._dayOfYear = u.dayOfYear
        }

        function $(t) {
            var n, a, s, i, r = [];
            if (!t._d) {
                for (s = J(t), t._w && null == t._a[bt] && null == t._a[vt] && E(t), t._dayOfYear && (i = e(t._a[wt], s[wt]), t._dayOfYear > W(i) && (t._pf._overflowDayOfYear = !0), a = at(i, 0, t._dayOfYear), t._a[vt] = a.getUTCMonth(), t._a[bt] = a.getUTCDate()), n = 0; 3 > n && null == t._a[n]; ++n) t._a[n] = r[n] = s[n];
                for (; 7 > n; n++) t._a[n] = r[n] = null == t._a[n] ? 2 === n ? 1 : 0 : t._a[n];
                t._d = (t._useUTC ? at : nt).apply(null, r), null != t._tzm && t._d.setUTCMinutes(t._d.getUTCMinutes() + t._tzm)
            }
        }

        function V(t) {
            var e;
            t._d || (e = v(t._i), t._a = [e.year, e.month, e.day, e.hour, e.minute, e.second, e.millisecond], $(t))
        }

        function J(t) {
            var e = new Date;
            return t._useUTC ? [e.getUTCFullYear(), e.getUTCMonth(), e.getUTCDate()] : [e.getFullYear(), e.getMonth(), e.getDate()]
        }

        function q(t) {
            if (t._f === Dt.ISO_8601) return void B(t);
            t._a = [], t._pf.empty = !0;
            var e, n, a, s, i, r = "" + t._i,
                o = r.length,
                u = 0;
            for (a = A(t._f, t._locale).match(It) || [], e = 0; e < a.length; e++) s = a[e], n = (r.match(N(s, t)) || [])[0], n && (i = r.substr(0, r.indexOf(n)), i.length > 0 && t._pf.unusedInput.push(i), r = r.slice(r.indexOf(n) + n.length), u += n.length), he[s] ? (n ? t._pf.empty = !1 : t._pf.unusedTokens.push(s), j(s, n, t)) : t._strict && !n && t._pf.unusedTokens.push(s);
            t._pf.charsLeftOver = o - u, r.length > 0 && t._pf.unusedInput.push(r), t._isPm && t._a[St] < 12 && (t._a[St] += 12), t._isPm === !1 && 12 === t._a[St] && (t._a[St] = 0), $(t), F(t)
        }

        function Q(t) {
            return t.replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (t, e, n, a, s) {
                return e || n || a || s
            })
        }

        function R(t) {
            return t.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&")
        }

        function X(t) {
            var e, n, s, i, r;
            if (0 === t._f.length) return t._pf.invalidFormat = !0, void(t._d = new Date(NaN));
            for (i = 0; i < t._f.length; i++) r = 0, e = f({}, t), null != t._useUTC && (e._useUTC = t._useUTC), e._pf = a(), e._f = t._f[i], q(e), G(e) && (r += e._pf.charsLeftOver, r += 10 * e._pf.unusedTokens.length, e._pf.score = r, (null == s || s > r) && (s = r, n = e));
            h(t, n || e)
        }

        function B(t) {
            var e, n, a = t._i,
                s = te.exec(a);
            if (s) {
                for (t._pf.iso = !0, e = 0, n = ne.length; n > e; e++)
                    if (ne[e][1].exec(a)) {
                        t._f = ne[e][0] + (s[6] || " ");
                        break
                    }
                for (e = 0, n = ae.length; n > e; e++)
                    if (ae[e][1].exec(a)) {
                        t._f += ae[e][0];
                        break
                    }
                a.match(Et) && (t._f += "Z"), q(t)
            } else t._isValid = !1
        }

        function K(t) {
            B(t), t._isValid === !1 && (delete t._isValid, Dt.createFromInputFallback(t))
        }

        function tt(t, e) {
            var n, a = [];
            for (n = 0; n < t.length; ++n) a.push(e(t[n], n));
            return a
        }

        function et(e) {
            var n, a = e._i;
            a === t ? e._d = new Date : M(a) ? e._d = new Date(+a) : null !== (n = Ct.exec(a)) ? e._d = new Date(+n[1]) : "string" == typeof a ? K(e) : k(a) ? (e._a = tt(a.slice(0), function (t) {
                return parseInt(t, 10)
            }), $(e)) : "object" == typeof a ? V(e) : "number" == typeof a ? e._d = new Date(a) : Dt.createFromInputFallback(e)
        }

        function nt(t, e, n, a, s, i, r) {
            var o = new Date(t, e, n, a, s, i, r);
            return 1970 > t && o.setFullYear(t), o
        }

        function at(t) {
            var e = new Date(Date.UTC.apply(null, arguments));
            return 1970 > t && e.setUTCFullYear(t), e
        }

        function st(t, e) {
            if ("string" == typeof t)
                if (isNaN(t)) {
                    if (t = e.weekdaysParse(t), "number" != typeof t) return null
                } else t = parseInt(t, 10);
            return t
        }

        function it(t, e, n, a, s) {
            return s.relativeTime(e || 1, !!n, t, a)
        }

        function rt(t, e, n) {
            var a = Dt.duration(t).abs(),
                s = Mt(a.as("s")),
                i = Mt(a.as("m")),
                r = Mt(a.as("h")),
                o = Mt(a.as("d")),
                u = Mt(a.as("M")),
                c = Mt(a.as("y")),
                d = s < ce.s && ["s", s] || 1 === i && ["m"] || i < ce.m && ["mm", i] || 1 === r && ["h"] || r < ce.h && ["hh", r] || 1 === o && ["d"] || o < ce.d && ["dd", o] || 1 === u && ["M"] || u < ce.M && ["MM", u] || 1 === c && ["y"] || ["yy", c];
            return d[2] = e, d[3] = +t > 0, d[4] = n, it.apply({}, d)
        }

        function ot(t, e, n) {
            var a, s = n - e,
                i = n - t.day();
            return i > s && (i -= 7), s - 7 > i && (i += 7), a = Dt(t).add(i, "d"), {
                week: Math.ceil(a.dayOfYear() / 7),
                year: a.year()
            }
        }

        function ut(t, e, n, a, s) {
            var i, r, o = at(t, 0, 1).getUTCDay();
            return o = 0 === o ? 7 : o, n = null != n ? n : s, i = s - o + (o > a ? 7 : 0) - (s > o ? 7 : 0), r = 7 * (e - 1) + (n - s) + i + 1, {
                year: r > 0 ? t : t - 1,
                dayOfYear: r > 0 ? r : W(t - 1) + r
            }
        }

        function ct(e) {
            var n = e._i,
                a = e._f;
            return e._locale = e._locale || Dt.localeData(e._l), null === n || a === t && "" === n ? Dt.invalid({
                nullInput: !0
            }) : ("string" == typeof n && (e._i = n = e._locale.preparse(n)), Dt.isMoment(n) ? new d(n, !0) : (a ? k(a) ? X(e) : q(e) : et(e), new d(e)))
        }

        function dt(t, e) {
            var n, a;
            if (1 === e.length && k(e[0]) && (e = e[0]), !e.length) return Dt();
            for (n = e[0], a = 1; a < e.length; ++a) e[a][t](n) && (n = e[a]);
            return n
        }

        function lt(t, e) {
            var n;
            return "string" == typeof e && (e = t.localeData().monthsParse(e), "number" != typeof e) ? t : (n = Math.min(t.date(), T(t.year(), e)), t._d["set" + (t._isUTC ? "UTC" : "") + "Month"](e, n), t)
        }

        function ht(t, e) {
            return t._d["get" + (t._isUTC ? "UTC" : "") + e]()
        }

        function ft(t, e, n) {
            return "Month" === e ? lt(t, n) : t._d["set" + (t._isUTC ? "UTC" : "") + e](n)
        }

        function mt(t, e) {
            return function (n) {
                return null != n ? (ft(this, t, n), Dt.updateOffset(this, e), this) : ht(this, t)
            }
        }

        function _t(t) {
            return 400 * t / 146097
        }

        function pt(t) {
            return 146097 * t / 400
        }

        function yt(t) {
            Dt.duration.fn[t] = function () {
                return this._data[t]
            }
        }
        for (var Dt, gt, kt = "2.8.3", Mt = ("undefined" != typeof global ? global : this, Math.round), Yt = Object.prototype.hasOwnProperty, wt = 0, vt = 1, bt = 2, St = 3, Tt = 4, Ot = 5, Wt = 6, Ut = {}, Ft = [], Gt = "undefined" != typeof module && module.exports, Ct = /^\/?Date\((\-?\d+)/i, zt = /(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/, xt = /^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/, It = /(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Q|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,4}|X|zz?|ZZ?|.)/g, Ht = /(\[[^\[]*\])|(\\)?(LT|LL?L?L?|l{1,4})/g, Lt = /\d\d?/, Pt = /\d{1,3}/, At = /\d{1,4}/, Nt = /[+\-]?\d{1,6}/, Zt = /\d+/, jt = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i, Et = /Z|[\+\-]\d\d:?\d\d/gi, $t = /T/i, Vt = /[\+\-]?\d+(\.\d{1,3})?/, Jt = /\d{1,2}/, qt = /\d/, Qt = /\d\d/, Rt = /\d{3}/, Xt = /\d{4}/, Bt = /[+-]?\d{6}/, Kt = /[+-]?\d+/, te = /^\s*(?:[+-]\d{6}|\d{4})-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/, ee = "YYYY-MM-DDTHH:mm:ssZ", ne = [["YYYYYY-MM-DD", /[+-]\d{6}-\d{2}-\d{2}/], ["YYYY-MM-DD", /\d{4}-\d{2}-\d{2}/], ["GGGG-[W]WW-E", /\d{4}-W\d{2}-\d/], ["GGGG-[W]WW", /\d{4}-W\d{2}/], ["YYYY-DDD", /\d{4}-\d{3}/]], ae = [["HH:mm:ss.SSSS", /(T| )\d\d:\d\d:\d\d\.\d+/], ["HH:mm:ss", /(T| )\d\d:\d\d:\d\d/], ["HH:mm", /(T| )\d\d:\d\d/], ["HH", /(T| )\d\d/]], se = /([\+\-]|\d\d)/gi, ie = ("Date|Hours|Minutes|Seconds|Milliseconds".split("|"), {
                Milliseconds: 1,
                Seconds: 1e3,
                Minutes: 6e4,
                Hours: 36e5,
                Days: 864e5,
                Months: 2592e6,
                Years: 31536e6
            }), re = {
                ms: "millisecond",
                s: "second",
                m: "minute",
                h: "hour",
                d: "day",
                D: "date",
                w: "week",
                W: "isoWeek",
                M: "month",
                Q: "quarter",
                y: "year",
                DDD: "dayOfYear",
                e: "weekday",
                E: "isoWeekday",
                gg: "weekYear",
                GG: "isoWeekYear"
            }, oe = {
                dayofyear: "dayOfYear",
                isoweekday: "isoWeekday",
                isoweek: "isoWeek",
                weekyear: "weekYear",
                isoweekyear: "isoWeekYear"
            }, ue = {}, ce = {
                s: 45,
                m: 45,
                h: 22,
                d: 26,
                M: 11
            }, de = "DDD w W M D d".split(" "), le = "M D H h m s w W".split(" "), he = {
                M: function () {
                    return this.month() + 1
                },
                MMM: function (t) {
                    return this.localeData().monthsShort(this, t)
                },
                MMMM: function (t) {
                    return this.localeData().months(this, t)
                },
                D: function () {
                    return this.date()
                },
                DDD: function () {
                    return this.dayOfYear()
                },
                d: function () {
                    return this.day()
                },
                dd: function (t) {
                    return this.localeData().weekdaysMin(this, t)
                },
                ddd: function (t) {
                    return this.localeData().weekdaysShort(this, t)
                },
                dddd: function (t) {
                    return this.localeData().weekdays(this, t)
                },
                w: function () {
                    return this.week()
                },
                W: function () {
                    return this.isoWeek()
                },
                YY: function () {
                    return _(this.year() % 100, 2)
                },
                YYYY: function () {
                    return _(this.year(), 4)
                },
                YYYYY: function () {
                    return _(this.year(), 5)
                },
                YYYYYY: function () {
                    var t = this.year(),
                        e = t >= 0 ? "+" : "-";
                    return e + _(Math.abs(t), 6)
                },
                gg: function () {
                    return _(this.weekYear() % 100, 2)
                },
                gggg: function () {
                    return _(this.weekYear(), 4)
                },
                ggggg: function () {
                    return _(this.weekYear(), 5)
                },
                GG: function () {
                    return _(this.isoWeekYear() % 100, 2)
                },
                GGGG: function () {
                    return _(this.isoWeekYear(), 4)
                },
                GGGGG: function () {
                    return _(this.isoWeekYear(), 5)
                },
                e: function () {
                    return this.weekday()
                },
                E: function () {
                    return this.isoWeekday()
                },
                a: function () {
                    return this.localeData().meridiem(this.hours(), this.minutes(), !0)
                },
                A: function () {
                    return this.localeData().meridiem(this.hours(), this.minutes(), !1)
                },
                H: function () {
                    return this.hours()
                },
                h: function () {
                    return this.hours() % 12 || 12
                },
                m: function () {
                    return this.minutes()
                },
                s: function () {
                    return this.seconds()
                },
                S: function () {
                    return S(this.milliseconds() / 100)
                },
                SS: function () {
                    return _(S(this.milliseconds() / 10), 2)
                },
                SSS: function () {
                    return _(this.milliseconds(), 3)
                },
                SSSS: function () {
                    return _(this.milliseconds(), 3)
                },
                Z: function () {
                    var t = -this.zone(),
                        e = "+";
                    return 0 > t && (t = -t, e = "-"), e + _(S(t / 60), 2) + ":" + _(S(t) % 60, 2)
                },
                ZZ: function () {
                    var t = -this.zone(),
                        e = "+";
                    return 0 > t && (t = -t, e = "-"), e + _(S(t / 60), 2) + _(S(t) % 60, 2)
                },
                z: function () {
                    return this.zoneAbbr()
                },
                zz: function () {
                    return this.zoneName()
                },
                X: function () {
                    return this.unix()
                },
                Q: function () {
                    return this.quarter()
                }
            }, fe = {}, me = ["months", "monthsShort", "weekdays", "weekdaysShort", "weekdaysMin"]; de.length;) gt = de.pop(), he[gt + "o"] = u(he[gt], gt);
        for (; le.length;) gt = le.pop(), he[gt + gt] = o(he[gt], 2);
        he.DDDD = o(he.DDD, 3), h(c.prototype, {
            set: function (t) {
                var e, n;
                for (n in t) e = t[n], "function" == typeof e ? this[n] = e : this["_" + n] = e
            },
            _months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
            months: function (t) {
                return this._months[t.month()]
            },
            _monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
            monthsShort: function (t) {
                return this._monthsShort[t.month()]
            },
            monthsParse: function (t) {
                var e, n, a;
                for (this._monthsParse || (this._monthsParse = []), e = 0; 12 > e; e++)
                    if (this._monthsParse[e] || (n = Dt.utc([2e3, e]), a = "^" + this.months(n, "") + "|^" + this.monthsShort(n, ""), this._monthsParse[e] = new RegExp(a.replace(".", ""), "i")), this._monthsParse[e].test(t)) return e
            },
            _weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
            weekdays: function (t) {
                return this._weekdays[t.day()]
            },
            _weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
            weekdaysShort: function (t) {
                return this._weekdaysShort[t.day()]
            },
            _weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
            weekdaysMin: function (t) {
                return this._weekdaysMin[t.day()]
            },
            weekdaysParse: function (t) {
                var e, n, a;
                for (this._weekdaysParse || (this._weekdaysParse = []), e = 0; 7 > e; e++)
                    if (this._weekdaysParse[e] || (n = Dt([2e3, 1]).day(e), a = "^" + this.weekdays(n, "") + "|^" + this.weekdaysShort(n, "") + "|^" + this.weekdaysMin(n, ""), this._weekdaysParse[e] = new RegExp(a.replace(".", ""), "i")), this._weekdaysParse[e].test(t)) return e
            },
            _longDateFormat: {
                LT: "h:mm A",
                L: "MM/DD/YYYY",
                LL: "MMMM D, YYYY",
                LLL: "MMMM D, YYYY LT",
                LLLL: "dddd, MMMM D, YYYY LT"
            },
            longDateFormat: function (t) {
                var e = this._longDateFormat[t];
                return !e && this._longDateFormat[t.toUpperCase()] && (e = this._longDateFormat[t.toUpperCase()].replace(/MMMM|MM|DD|dddd/g, function (t) {
                    return t.slice(1)
                }), this._longDateFormat[t] = e), e
            },
            isPM: function (t) {
                return "p" === (t + "").toLowerCase().charAt(0)
            },
            _meridiemParse: /[ap]\.?m?\.?/i,
            meridiem: function (t, e, n) {
                return t > 11 ? n ? "pm" : "PM" : n ? "am" : "AM"
            },
            _calendar: {
                sameDay: "[Today at] LT",
                nextDay: "[Tomorrow at] LT",
                nextWeek: "dddd [at] LT",
                lastDay: "[Yesterday at] LT",
                lastWeek: "[Last] dddd [at] LT",
                sameElse: "L"
            },
            calendar: function (t, e) {
                var n = this._calendar[t];
                return "function" == typeof n ? n.apply(e) : n
            },
            _relativeTime: {
                future: "in %s",
                past: "%s ago",
                s: "a few seconds",
                m: "a minute",
                mm: "%d minutes",
                h: "an hour",
                hh: "%d hours",
                d: "a day",
                dd: "%d days",
                M: "a month",
                MM: "%d months",
                y: "a year",
                yy: "%d years"
            },
            relativeTime: function (t, e, n, a) {
                var s = this._relativeTime[n];
                return "function" == typeof s ? s(t, e, n, a) : s.replace(/%d/i, t)
            },
            pastFuture: function (t, e) {
                var n = this._relativeTime[t > 0 ? "future" : "past"];
                return "function" == typeof n ? n(e) : n.replace(/%s/i, e)
            },
            ordinal: function (t) {
                return this._ordinal.replace("%d", t)
            },
            _ordinal: "%d",
            preparse: function (t) {
                return t
            },
            postformat: function (t) {
                return t
            },
            week: function (t) {
                return ot(t, this._week.dow, this._week.doy).week
            },
            _week: {
                dow: 0,
                doy: 6
            },
            _invalidDate: "Invalid date",
            invalidDate: function () {
                return this._invalidDate
            }
        }), Dt = function (e, n, s, i) {
            var r;
            return "boolean" == typeof s && (i = s, s = t), r = {}, r._isAMomentObject = !0, r._i = e, r._f = n, r._l = s, r._strict = i, r._isUTC = !1, r._pf = a(), ct(r)
        }, Dt.suppressDeprecationWarnings = !1, Dt.createFromInputFallback = i("moment construction falls back to js Date. This is discouraged and will be removed in upcoming major release. Please refer to https://github.com/moment/moment/issues/1407 for more info.", function (t) {
            t._d = new Date(t._i)
        }), Dt.min = function () {
            var t = [].slice.call(arguments, 0);
            return dt("isBefore", t)
        }, Dt.max = function () {
            var t = [].slice.call(arguments, 0);
            return dt("isAfter", t)
        }, Dt.utc = function (e, n, s, i) {
            var r;
            return "boolean" == typeof s && (i = s, s = t), r = {}, r._isAMomentObject = !0, r._useUTC = !0, r._isUTC = !0, r._l = s, r._i = e, r._f = n, r._strict = i, r._pf = a(), ct(r).utc()
        }, Dt.unix = function (t) {
            return Dt(1e3 * t)
        }, Dt.duration = function (t, e) {
            var a, s, i, r, o = t,
                u = null;
            return Dt.isDuration(t) ? o = {
                ms: t._milliseconds,
                d: t._days,
                M: t._months
            } : "number" == typeof t ? (o = {}, e ? o[e] = t : o.milliseconds = t) : (u = zt.exec(t)) ? (a = "-" === u[1] ? -1 : 1, o = {
                y: 0,
                d: S(u[bt]) * a,
                h: S(u[St]) * a,
                m: S(u[Tt]) * a,
                s: S(u[Ot]) * a,
                ms: S(u[Wt]) * a
            }) : (u = xt.exec(t)) ? (a = "-" === u[1] ? -1 : 1, i = function (t) {
                var e = t && parseFloat(t.replace(",", "."));
                return (isNaN(e) ? 0 : e) * a
            }, o = {
                y: i(u[2]),
                M: i(u[3]),
                d: i(u[4]),
                h: i(u[5]),
                m: i(u[6]),
                s: i(u[7]),
                w: i(u[8])
            }) : "object" == typeof o && ("from" in o || "to" in o) && (r = y(Dt(o.from), Dt(o.to)), o = {}, o.ms = r.milliseconds, o.M = r.months), s = new l(o), Dt.isDuration(t) && n(t, "_locale") && (s._locale = t._locale), s
        }, Dt.version = kt, Dt.defaultFormat = ee, Dt.ISO_8601 = function () {}, Dt.momentProperties = Ft, Dt.updateOffset = function () {}, Dt.relativeTimeThreshold = function (e, n) {
            return ce[e] === t ? !1 : n === t ? ce[e] : (ce[e] = n, !0)
        }, Dt.lang = i("moment.lang is deprecated. Use moment.locale instead.", function (t, e) {
            return Dt.locale(t, e)
        }), Dt.locale = function (t, e) {
            var n;
            return t && (n = "undefined" != typeof e ? Dt.defineLocale(t, e) : Dt.localeData(t), n && (Dt.duration._locale = Dt._locale = n)), Dt._locale._abbr
        }, Dt.defineLocale = function (t, e) {
            return null !== e ? (e.abbr = t, Ut[t] || (Ut[t] = new c), Ut[t].set(e), Dt.locale(t), Ut[t]) : (delete Ut[t], null)
        }, Dt.langData = i("moment.langData is deprecated. Use moment.localeData instead.", function (t) {
            return Dt.localeData(t)
        }), Dt.localeData = function (t) {
            var e;
            if (t && t._locale && t._locale._abbr && (t = t._locale._abbr), !t) return Dt._locale;
            if (!k(t)) {
                if (e = x(t)) return e;
                t = [t]
            }
            return z(t)
        }, Dt.isMoment = function (t) {
            return t instanceof d || null != t && n(t, "_isAMomentObject")
        }, Dt.isDuration = function (t) {
            return t instanceof l
        };
        for (gt = me.length - 1; gt >= 0; --gt) b(me[gt]);
        Dt.normalizeUnits = function (t) {
            return w(t)
        }, Dt.invalid = function (t) {
            var e = Dt.utc(NaN);
            return null != t ? h(e._pf, t) : e._pf.userInvalidated = !0, e
        }, Dt.parseZone = function () {
            return Dt.apply(null, arguments).parseZone()
        }, Dt.parseTwoDigitYear = function (t) {
            return S(t) + (S(t) > 68 ? 1900 : 2e3)
        }, h(Dt.fn = d.prototype, {
            clone: function () {
                return Dt(this)
            },
            valueOf: function () {
                return +this._d + 6e4 * (this._offset || 0)
            },
            unix: function () {
                return Math.floor(+this / 1e3)
            },
            toString: function () {
                return this.clone().locale("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")
            },
            toDate: function () {
                return this._offset ? new Date(+this) : this._d
            },
            toISOString: function () {
                var t = Dt(this).utc();
                return 0 < t.year() && t.year() <= 9999 ? P(t, "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]") : P(t, "YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]")
            },
            toArray: function () {
                var t = this;
                return [t.year(), t.month(), t.date(), t.hours(), t.minutes(), t.seconds(), t.milliseconds()]
            },
            isValid: function () {
                return G(this)
            },
            isDSTShifted: function () {
                return this._a ? this.isValid() && Y(this._a, (this._isUTC ? Dt.utc(this._a) : Dt(this._a)).toArray()) > 0 : !1
            },
            parsingFlags: function () {
                return h({}, this._pf)
            },
            invalidAt: function () {
                return this._pf.overflow
            },
            utc: function (t) {
                return this.zone(0, t)
            },
            local: function (t) {
                return this._isUTC && (this.zone(0, t), this._isUTC = !1, t && this.add(this._dateTzOffset(), "m")), this
            },
            format: function (t) {
                var e = P(this, t || Dt.defaultFormat);
                return this.localeData().postformat(e)
            },
            add: D(1, "add"),
            subtract: D(-1, "subtract"),
            diff: function (t, e, n) {
                var a, s, i, r = I(t, this),
                    o = 6e4 * (this.zone() - r.zone());
                return e = w(e), "year" === e || "month" === e ? (a = 432e5 * (this.daysInMonth() + r.daysInMonth()), s = 12 * (this.year() - r.year()) + (this.month() - r.month()), i = this - Dt(this).startOf("month") - (r - Dt(r).startOf("month")), i -= 6e4 * (this.zone() - Dt(this).startOf("month").zone() - (r.zone() - Dt(r).startOf("month").zone())), s += i / a, "year" === e && (s /= 12)) : (a = this - r, s = "second" === e ? a / 1e3 : "minute" === e ? a / 6e4 : "hour" === e ? a / 36e5 : "day" === e ? (a - o) / 864e5 : "week" === e ? (a - o) / 6048e5 : a), n ? s : m(s)
            },
            from: function (t, e) {
                return Dt.duration({
                    to: this,
                    from: t
                }).locale(this.locale()).humanize(!e)
            },
            fromNow: function (t) {
                return this.from(Dt(), t)
            },
            calendar: function (t) {
                var e = t || Dt(),
                    n = I(e, this).startOf("day"),
                    a = this.diff(n, "days", !0),
                    s = -6 > a ? "sameElse" : -1 > a ? "lastWeek" : 0 > a ? "lastDay" : 1 > a ? "sameDay" : 2 > a ? "nextDay" : 7 > a ? "nextWeek" : "sameElse";
                return this.format(this.localeData().calendar(s, this))
            },
            isLeapYear: function () {
                return U(this.year())
            },
            isDST: function () {
                return this.zone() < this.clone().month(0).zone() || this.zone() < this.clone().month(5).zone()
            },
            day: function (t) {
                var e = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
                return null != t ? (t = st(t, this.localeData()), this.add(t - e, "d")) : e
            },
            month: mt("Month", !0),
            startOf: function (t) {
                switch (t = w(t)) {
                    case "year":
                        this.month(0);
                    case "quarter":
                    case "month":
                        this.date(1);
                    case "week":
                    case "isoWeek":
                    case "day":
                        this.hours(0);
                    case "hour":
                        this.minutes(0);
                    case "minute":
                        this.seconds(0);
                    case "second":
                        this.milliseconds(0)
                }
                return "week" === t ? this.weekday(0) : "isoWeek" === t && this.isoWeekday(1), "quarter" === t && this.month(3 * Math.floor(this.month() / 3)), this
            },
            endOf: function (t) {
                return t = w(t), this.startOf(t).add(1, "isoWeek" === t ? "week" : t).subtract(1, "ms")
            },
            isAfter: function (t, e) {
                return e = w("undefined" != typeof e ? e : "millisecond"), "millisecond" === e ? (t = Dt.isMoment(t) ? t : Dt(t), +this > +t) : +this.clone().startOf(e) > +Dt(t).startOf(e)
            },
            isBefore: function (t, e) {
                return e = w("undefined" != typeof e ? e : "millisecond"), "millisecond" === e ? (t = Dt.isMoment(t) ? t : Dt(t), +t > +this) : +this.clone().startOf(e) < +Dt(t).startOf(e)
            },
            isSame: function (t, e) {
                return e = w(e || "millisecond"), "millisecond" === e ? (t = Dt.isMoment(t) ? t : Dt(t), +this === +t) : +this.clone().startOf(e) === +I(t, this).startOf(e)
            },
            min: i("moment().min is deprecated, use moment.min instead. https://github.com/moment/moment/issues/1548", function (t) {
                return t = Dt.apply(null, arguments), this > t ? this : t
            }),
            max: i("moment().max is deprecated, use moment.max instead. https://github.com/moment/moment/issues/1548", function (t) {
                return t = Dt.apply(null, arguments), t > this ? this : t
            }),
            zone: function (t, e) {
                var n, a = this._offset || 0;
                return null == t ? this._isUTC ? a : this._dateTzOffset() : ("string" == typeof t && (t = Z(t)), Math.abs(t) < 16 && (t = 60 * t), !this._isUTC && e && (n = this._dateTzOffset()), this._offset = t, this._isUTC = !0, null != n && this.subtract(n, "m"), a !== t && (!e || this._changeInProgress ? g(this, Dt.duration(a - t, "m"), 1, !1) : this._changeInProgress || (this._changeInProgress = !0, Dt.updateOffset(this, !0), this._changeInProgress = null)), this)
            },
            zoneAbbr: function () {
                return this._isUTC ? "UTC" : ""
            },
            zoneName: function () {
                return this._isUTC ? "Coordinated Universal Time" : ""
            },
            parseZone: function () {
                return this._tzm ? this.zone(this._tzm) : "string" == typeof this._i && this.zone(this._i), this
            },
            hasAlignedHourOffset: function (t) {
                return t = t ? Dt(t).zone() : 0, (this.zone() - t) % 60 === 0
            },
            daysInMonth: function () {
                return T(this.year(), this.month())
            },
            dayOfYear: function (t) {
                var e = Mt((Dt(this).startOf("day") - Dt(this).startOf("year")) / 864e5) + 1;
                return null == t ? e : this.add(t - e, "d")
            },
            quarter: function (t) {
                return null == t ? Math.ceil((this.month() + 1) / 3) : this.month(3 * (t - 1) + this.month() % 3)
            },
            weekYear: function (t) {
                var e = ot(this, this.localeData()._week.dow, this.localeData()._week.doy).year;
                return null == t ? e : this.add(t - e, "y")
            },
            isoWeekYear: function (t) {
                var e = ot(this, 1, 4).year;
                return null == t ? e : this.add(t - e, "y")
            },
            week: function (t) {
                var e = this.localeData().week(this);
                return null == t ? e : this.add(7 * (t - e), "d")
            },
            isoWeek: function (t) {
                var e = ot(this, 1, 4).week;
                return null == t ? e : this.add(7 * (t - e), "d")
            },
            weekday: function (t) {
                var e = (this.day() + 7 - this.localeData()._week.dow) % 7;
                return null == t ? e : this.add(t - e, "d")
            },
            isoWeekday: function (t) {
                return null == t ? this.day() || 7 : this.day(this.day() % 7 ? t : t - 7)
            },
            isoWeeksInYear: function () {
                return O(this.year(), 1, 4)
            },
            weeksInYear: function () {
                var t = this.localeData()._week;
                return O(this.year(), t.dow, t.doy)
            },
            get: function (t) {
                return t = w(t), this[t]()
            },
            set: function (t, e) {
                return t = w(t), "function" == typeof this[t] && this[t](e), this
            },
            locale: function (e) {
                var n;
                return e === t ? this._locale._abbr : (n = Dt.localeData(e), null != n && (this._locale = n), this)
            },
            lang: i("moment().lang() is deprecated. Use moment().localeData() instead.", function (e) {
                return e === t ? this.localeData() : this.locale(e)
            }),
            localeData: function () {
                return this._locale
            },
            _dateTzOffset: function () {
                return 15 * Math.round(this._d.getTimezoneOffset() / 15)
            }
        }), Dt.fn.millisecond = Dt.fn.milliseconds = mt("Milliseconds", !1), Dt.fn.second = Dt.fn.seconds = mt("Seconds", !1), Dt.fn.minute = Dt.fn.minutes = mt("Minutes", !1), Dt.fn.hour = Dt.fn.hours = mt("Hours", !0), Dt.fn.date = mt("Date", !0), Dt.fn.dates = i("dates accessor is deprecated. Use date instead.", mt("Date", !0)), Dt.fn.year = mt("FullYear", !0), Dt.fn.years = i("years accessor is deprecated. Use year instead.", mt("FullYear", !0)), Dt.fn.days = Dt.fn.day, Dt.fn.months = Dt.fn.month, Dt.fn.weeks = Dt.fn.week, Dt.fn.isoWeeks = Dt.fn.isoWeek, Dt.fn.quarters = Dt.fn.quarter, Dt.fn.toJSON = Dt.fn.toISOString, h(Dt.duration.fn = l.prototype, {
            _bubble: function () {
                var t, e, n, a = this._milliseconds,
                    s = this._days,
                    i = this._months,
                    r = this._data,
                    o = 0;
                r.milliseconds = a % 1e3, t = m(a / 1e3), r.seconds = t % 60, e = m(t / 60), r.minutes = e % 60, n = m(e / 60), r.hours = n % 24, s += m(n / 24), o = m(_t(s)), s -= m(pt(o)), i += m(s / 30), s %= 30, o += m(i / 12), i %= 12, r.days = s, r.months = i, r.years = o
            },
            abs: function () {
                return this._milliseconds = Math.abs(this._milliseconds), this._days = Math.abs(this._days), this._months = Math.abs(this._months), this._data.milliseconds = Math.abs(this._data.milliseconds), this._data.seconds = Math.abs(this._data.seconds), this._data.minutes = Math.abs(this._data.minutes), this._data.hours = Math.abs(this._data.hours), this._data.months = Math.abs(this._data.months), this._data.years = Math.abs(this._data.years), this
            },
            weeks: function () {
                return m(this.days() / 7)
            },
            valueOf: function () {
                return this._milliseconds + 864e5 * this._days + this._months % 12 * 2592e6 + 31536e6 * S(this._months / 12)
            },
            humanize: function (t) {
                var e = rt(this, !t, this.localeData());
                return t && (e = this.localeData().pastFuture(+this, e)), this.localeData().postformat(e)
            },
            add: function (t, e) {
                var n = Dt.duration(t, e);
                return this._milliseconds += n._milliseconds, this._days += n._days, this._months += n._months, this._bubble(), this
            },
            subtract: function (t, e) {
                var n = Dt.duration(t, e);
                return this._milliseconds -= n._milliseconds, this._days -= n._days, this._months -= n._months, this._bubble(), this
            },
            get: function (t) {
                return t = w(t), this[t.toLowerCase() + "s"]()
            },
            as: function (t) {
                var e, n;
                if (t = w(t), "month" === t || "year" === t) return e = this._days + this._milliseconds / 864e5, n = this._months + 12 * _t(e), "month" === t ? n : n / 12;
                switch (e = this._days + pt(this._months / 12), t) {
                    case "week":
                        return e / 7 + this._milliseconds / 6048e5;
                    case "day":
                        return e + this._milliseconds / 864e5;
                    case "hour":
                        return 24 * e + this._milliseconds / 36e5;
                    case "minute":
                        return 24 * e * 60 + this._milliseconds / 6e4;
                    case "second":
                        return 24 * e * 60 * 60 + this._milliseconds / 1e3;
                    case "millisecond":
                        return Math.floor(24 * e * 60 * 60 * 1e3) + this._milliseconds;
                    default:
                        throw new Error("Unknown unit " + t)
                }
            },
            lang: Dt.fn.lang,
            locale: Dt.fn.locale,
            toIsoString: i("toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)", function () {
                return this.toISOString()
            }),
            toISOString: function () {
                var t = Math.abs(this.years()),
                    e = Math.abs(this.months()),
                    n = Math.abs(this.days()),
                    a = Math.abs(this.hours()),
                    s = Math.abs(this.minutes()),
                    i = Math.abs(this.seconds() + this.milliseconds() / 1e3);
                return this.asSeconds() ? (this.asSeconds() < 0 ? "-" : "") + "P" + (t ? t + "Y" : "") + (e ? e + "M" : "") + (n ? n + "D" : "") + (a || s || i ? "T" : "") + (a ? a + "H" : "") + (s ? s + "M" : "") + (i ? i + "S" : "") : "P0D"
            },
            localeData: function () {
                return this._locale
            }
        }), Dt.duration.fn.toString = Dt.duration.fn.toISOString;
        for (gt in ie) n(ie, gt) && yt(gt.toLowerCase());
        return Dt.duration.fn.asMilliseconds = function () {
            return this.as("ms")
        }, Dt.duration.fn.asSeconds = function () {
            return this.as("s")
        }, Dt.duration.fn.asMinutes = function () {
            return this.as("m")
        }, Dt.duration.fn.asHours = function () {
            return this.as("h")
        }, Dt.duration.fn.asDays = function () {
            return this.as("d")
        }, Dt.duration.fn.asWeeks = function () {
            return this.as("weeks")
        }, Dt.duration.fn.asMonths = function () {
            return this.as("M")
        }, Dt.duration.fn.asYears = function () {
            return this.as("y")
        }, Dt.locale("en", {
            ordinal: function (t) {
                var e = t % 10,
                    n = 1 === S(t % 100 / 10) ? "th" : 1 === e ? "st" : 2 === e ? "nd" : 3 === e ? "rd" : "th";
                return t + n
            }
        }), Dt
    }.call(this), t.Utils.moment = n, t.datepicker
});
! function (t) {
    var i;
    window.UIkit && (i = t(UIkit)), "function" == typeof define && define.amd && define("uikit-grid", ["uikit"], function () {
        return i || t(UIkit)
    })
}(function (t) {
    "use strict";

    function i(t) {
        return e(t)
    }
    t.component("grid", {
        defaults: {
            colwidth: "auto",
            animation: !0,
            duration: 300,
            gutter: 0,
            controls: !1,
            filter: !1
        },
        boot: function () {
            t.ready(function (i) {
                t.$("[data-uk-grid]", i).each(function () {
                    var i = t.$(this);
                    i.data("grid") || t.grid(i, t.Utils.options(i.attr("data-uk-grid")))
                })
            })
        },
        init: function () {
            var i = this,
                e = String(this.options.gutter).trim().split(" ");
            this.gutterv = parseInt(e[0], 10), this.gutterh = parseInt(e[1] || e[0], 10), this.element.css({
                position: "relative"
            }), this.controls = null, this.options.controls && (this.controls = t.$(this.options.controls), this.controls.on("click", "[data-uk-filter]", function (e) {
                e.preventDefault(), i.filter(t.$(this).data("ukFilter"))
            }), this.controls.on("click", "[data-uk-sort]", function (e) {
                e.preventDefault();
                var n = t.$(this).attr("data-uk-sort").split(":");
                i.sort(n[0], n[1])
            })), t.$win.on("load resize orientationchange", t.Utils.debounce(function () {
                i.currentfilter ? i.filter(i.currentfilter) : this.updateLayout()
            }.bind(this), 100)), this.on("display.uk.check", function () {
                i.element.is(":visible") && i.updateLayout()
            }), t.$html.on("changed.uk.dom", function (t) {
                i.updateLayout()
            }), this.options.filter !== !1 ? this.filter(this.options.filter) : this.updateLayout()
        },
        _prepareElements: function () {
            var t, i = this.element.children(":not([data-grid-prepared])");
            i.length && (t = {
                position: "absolute",
                "box-sizing": "border-box",
                width: "auto" == this.options.colwidth ? "" : this.options.colwidth
            }, this.options.gutter && (t["padding-left"] = this.gutterh, t["padding-bottom"] = this.gutterv, this.element.css("margin-left", -1 * this.gutterh)), i.attr("data-grid-prepared", "true").css(t))
        },
        updateLayout: function (e) {
            this._prepareElements(), e = e || this.element.children(":visible");
            var n, r, o, a, s, d, h, u, l = e,
                f = this.element.width() + 2 * this.gutterh + 2,
                c = 0,
                p = 0,
                g = [];
            this.trigger("beforeupdate.uk.grid", [l]), l.each(function (e) {
                for (u = i(this), n = t.$(this), r = u.outerWidth, o = u.outerHeight, c = 0, p = 0, s = 0, h = g.length; h > s; s++) a = g[s], c <= a.aX && (c = a.aX), c + r > f && (c = 0), p <= a.aY && (p = a.aY);
                g.push({
                    ele: n,
                    top: p,
                    left: c,
                    width: r,
                    height: o,
                    aY: p + o,
                    aX: c + r
                })
            });
            var m, v = 0;
            for (s = 0, h = g.length; h > s; s++) {
                for (a = g[s], p = 0, d = 0; s > d; d++) m = g[d], a.left < m.aX && m.left + 1 < a.aX && (p = m.aY);
                a.top = p, a.aY = p + a.height, v = Math.max(v, a.aY)
            }
            v -= this.gutterv, this.options.animation ? (this.element.stop().animate({
                height: v
            }, 100), g.forEach(function (t) {
                t.ele.stop().animate({
                    top: t.top,
                    left: t.left,
                    opacity: 1
                }, this.options.duration)
            }.bind(this))) : (this.element.css("height", v), g.forEach(function (t) {
                t.ele.css({
                    top: t.top,
                    left: t.left,
                    opacity: 1
                })
            }.bind(this))), setTimeout(function () {
                t.$doc.trigger("scrolling.uk.document")
            }, 2 * this.options.duration * (this.options.animation ? 1 : 0)), this.trigger("afterupdate.uk.grid", [l])
        },
        filter: function (i) {
            this.currentfilter = i, i = i || [], "number" == typeof i && (i = i.toString()), "string" == typeof i && (i = i.split(/,/).map(function (t) {
                return t.trim()
            }));
            var e = this,
                n = this.element.children(),
                r = {
                    visible: [],
                    hidden: []
                };
            n.each(function (e) {
                var n = t.$(this),
                    o = n.attr("data-uk-filter"),
                    a = i.length ? !1 : !0;
                o && (o = o.split(/,/).map(function (t) {
                    return t.trim()
                }), i.forEach(function (t) {
                    o.indexOf(t) > -1 && (a = !0)
                })), r[a ? "visible" : "hidden"].push(n)
            }), r.hidden = t.$(r.hidden).map(function () {
                return this[0]
            }), r.visible = t.$(r.visible).map(function () {
                return this[0]
            }), r.hidden.attr("aria-hidden", "true").filter(":visible").fadeOut(this.options.duration), r.visible.attr("aria-hidden", "false").filter(":hidden").css("opacity", 0).show(), e.updateLayout(r.visible), this.controls && this.controls.length && this.controls.find("[data-uk-filter]").removeClass("uk-active").filter('[data-uk-filter="' + i + '"]').addClass("uk-active")
        },
        sort: function (i, e) {
            e = e || 1, "string" == typeof e && (e = "desc" == e.toLowerCase() ? -1 : 1);
            var n = this.element.children();
            n.sort(function (n, r) {
                return n = t.$(n), r = t.$(r), (r.data(i) || "") < (n.data(i) || "") ? e : -1 * e
            }).appendTo(this.element), this.updateLayout(n.filter(":visible")), this.controls && this.controls.length && this.controls.find("[data-uk-sort]").removeClass("uk-active").filter('[data-uk-sort="' + i + ":" + (-1 == e ? "desc" : "asc") + '"]').addClass("uk-active")
        }
    });
    var e = function () {
        function t(t) {
            if (t) {
                if ("string" == typeof u[t]) return t;
                t = t.charAt(0).toUpperCase() + t.slice(1);
                for (var i, e = 0, n = h.length; n > e; e++)
                    if (i = h[e] + t, "string" == typeof u[i]) return i
            }
        }

        function i(t) {
            var i = parseFloat(t),
                e = -1 === t.indexOf("%") && !isNaN(i);
            return e && i
        }

        function e() {}

        function n() {
            for (var t = {
                    width: 0,
                    height: 0,
                    innerWidth: 0,
                    innerHeight: 0,
                    outerWidth: 0,
                    outerHeight: 0
                }, i = 0, e = f.length; e > i; i++) {
                var n = f[i];
                t[n] = 0
            }
            return t
        }

        function r() {
            if (!c) {
                c = !0;
                var e = window.getComputedStyle;
                if (a = function () {
                        var t = e ? function (t) {
                            return e(t, null)
                        } : function (t) {
                            return t.currentStyle
                        };
                        return function (i) {
                            var e = t(i);
                            return e || l("Style returned " + e + ". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"), e
                        }
                    }(), s = t("boxSizing")) {
                    var n = document.createElement("div");
                    n.style.width = "200px", n.style.padding = "1px 2px 3px 4px", n.style.borderStyle = "solid", n.style.borderWidth = "1px 2px 3px 4px", n.style[s] = "border-box";
                    var r = document.body || document.documentElement;
                    r.appendChild(n);
                    var o = a(n);
                    d = 200 === i(o.width), r.removeChild(n)
                }
            }
        }

        function o(t) {
            if (r(), "string" == typeof t && (t = document.querySelector(t)), t && "object" == typeof t && t.nodeType) {
                var e = a(t);
                if ("none" === e.display) return n();
                var o = {};
                o.width = t.offsetWidth, o.height = t.offsetHeight;
                for (var h = o.isBorderBox = !(!s || !e[s] || "border-box" !== e[s]), u = 0, l = f.length; l > u; u++) {
                    var c = f[u],
                        p = e[c],
                        g = parseFloat(p);
                    o[c] = isNaN(g) ? 0 : g
                }
                var m = o.paddingLeft + o.paddingRight,
                    v = o.paddingTop + o.paddingBottom,
                    b = o.marginLeft + o.marginRight,
                    y = o.marginTop + o.marginBottom,
                    k = o.borderLeftWidth + o.borderRightWidth,
                    w = o.borderTopWidth + o.borderBottomWidth,
                    x = h && d,
                    W = i(e.width);
                W !== !1 && (o.width = W + (x ? 0 : m + k));
                var L = i(e.height);
                return L !== !1 && (o.height = L + (x ? 0 : v + w)), o.innerWidth = o.width - (m + k), o.innerHeight = o.height - (v + w), o.outerWidth = o.width + b, o.outerHeight = o.height + y, o
            }
        }
        var a, s, d, h = "Webkit Moz ms Ms O".split(" "),
            u = document.documentElement.style,
            l = "undefined" == typeof console ? e : function (t) {
                console.error(t)
            },
            f = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"],
            c = !1;
        return o
    }()
});
! function (i) {
    var t;
    window.UIkit && (t = i(UIkit)), "function" == typeof define && define.amd && define("uikit-lightbox", ["uikit"], function () {
        return t || i(UIkit)
    })
}(function (i) {
    "use strict";

    function t(t) {
        return e ? (e.lightbox = t, e) : (e = i.$(['<div class="uk-modal">', '<div class="uk-modal-dialog uk-modal-dialog-lightbox uk-slidenav-position" style="margin-left:auto;margin-right:auto;width:200px;height:200px;top:' + Math.abs(window.innerHeight / 2 - 200) + 'px;">', '<a href="#" class="uk-modal-close uk-close uk-close-alt"></a>', '<div class="uk-lightbox-content"></div>', '<div class="uk-modal-spinner uk-hidden"></div>', "</div>", "</div>"].join("")).appendTo("body"), e.dialog = e.find(".uk-modal-dialog:first"), e.content = e.find(".uk-lightbox-content:first"), e.loader = e.find(".uk-modal-spinner:first"), e.closer = e.find(".uk-close.uk-close-alt"), e.modal = i.modal(e, {
            modal: !1
        }), e.on("swipeRight swipeLeft", function (i) {
            e.lightbox["swipeLeft" == i.type ? "next" : "previous"]()
        }).on("click", "[data-lightbox-previous], [data-lightbox-next]", function (t) {
            t.preventDefault(), e.lightbox[i.$(this).is("[data-lightbox-next]") ? "next" : "previous"]()
        }), e.on("hide.uk.modal", function (i) {
            e.content.html("")
        }), i.$win.on("load resize orientationchange", i.Utils.debounce(function (t) {
            e.is(":visible") && !i.Utils.isFullscreen() && e.lightbox.fitSize()
        }.bind(this), 100)), e.lightbox = t, e)
    }
    var e, o = {};
    return i.component("lightbox", {
        defaults: {
            group: !1,
            duration: 400,
            keyboard: !0
        },
        index: 0,
        items: !1,
        boot: function () {
            i.$html.on("click", "[data-uk-lightbox]", function (t) {
                t.preventDefault();
                var e = i.$(this);
                e.data("lightbox") || i.lightbox(e, i.Utils.options(e.attr("data-uk-lightbox"))), e.data("lightbox").show(e)
            }), i.$doc.on("keyup", function (i) {
                if (e && e.is(":visible") && e.lightbox.options.keyboard) switch (i.preventDefault(), i.keyCode) {
                    case 37:
                        e.lightbox.previous();
                        break;
                    case 39:
                        e.lightbox.next()
                }
            })
        },
        init: function () {
            var t = [];
            if (this.index = 0, this.siblings = [], this.element && this.element.length) {
                var e = this.options.group ? i.$(['[data-uk-lightbox*="' + this.options.group + '"]', "[data-uk-lightbox*='" + this.options.group + "']"].join(",")) : this.element;
                e.each(function () {
                    var e = i.$(this);
                    t.push({
                        source: e.attr("href"),
                        title: e.attr("data-title") || e.attr("title"),
                        type: e.attr("data-lightbox-type") || "auto",
                        link: e
                    })
                }), this.index = e.index(this.element), this.siblings = t
            } else this.options.group && this.options.group.length && (this.siblings = this.options.group);
            this.trigger("lightbox-init", [this])
        },
        show: function (e) {
            this.modal = t(this), this.modal.dialog.stop(), this.modal.content.stop();
            var o, n, s = this,
                h = i.$.Deferred();
            e = e || 0, "object" == typeof e && this.siblings.forEach(function (i, t) {
                e[0] === i.link[0] && (e = t)
            }), 0 > e ? e = this.siblings.length - e : this.siblings[e] || (e = 0), n = this.siblings[e], o = {
                lightbox: s,
                source: n.source,
                type: n.type,
                index: e,
                promise: h,
                title: n.title,
                item: n,
                meta: {
                    content: "",
                    width: null,
                    height: null
                }
            }, this.index = e, this.modal.content.empty(), this.modal.is(":visible") || (this.modal.content.css({
                width: "",
                height: ""
            }).empty(), this.modal.modal.show()), this.modal.loader.removeClass("uk-hidden"), h.promise().done(function () {
                s.data = o, s.fitSize(o)
            }).fail(function () {
                o.meta.content = '<div class="uk-position-cover uk-flex uk-flex-middle uk-flex-center"><strong>Loading resource failed!</strong></div>', o.meta.width = 400, o.meta.height = 300, s.data = o, s.fitSize(o)
            }), s.trigger("showitem.uk.lightbox", [o])
        },
        fitSize: function () {
            var t = this,
                e = this.data,
                o = this.modal.dialog.outerWidth() - this.modal.dialog.width(),
                n = parseInt(this.modal.dialog.css("margin-top"), 10),
                s = parseInt(this.modal.dialog.css("margin-bottom"), 10),
                h = n + s,
                a = e.meta.content,
                d = t.options.duration;
            this.siblings.length > 1 && (a = [a, '<a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous uk-hidden-touch" data-lightbox-previous></a>', '<a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next uk-hidden-touch" data-lightbox-next></a>'].join(""));
            var l, r, u = i.$("<div>&nbsp;</div>").css({
                    opacity: 0,
                    position: "absolute",
                    top: 0,
                    left: 0,
                    width: "100%",
                    "max-width": t.modal.dialog.css("max-width"),
                    padding: t.modal.dialog.css("padding"),
                    margin: t.modal.dialog.css("margin")
                }),
                c = e.meta.width,
                g = e.meta.height;
            u.appendTo("body").width(), l = u.width(), r = window.innerHeight - h, u.remove(), this.modal.dialog.find(".uk-modal-caption").remove(), e.title && (this.modal.dialog.append('<div class="uk-modal-caption">' + e.title + "</div>"), r -= this.modal.dialog.find(".uk-modal-caption").outerHeight()), l < e.meta.width && (g = Math.floor(g * (l / c)), c = l), g > r && (g = Math.floor(r), c = Math.ceil(e.meta.width * (r / e.meta.height))), this.modal.content.css("opacity", 0).width(c).html(a), "iframe" == e.type && this.modal.content.find("iframe:first").height(g);
            var m = g + o,
                p = Math.floor(window.innerHeight / 2 - m / 2) - h;
            0 > p && (p = 0), this.modal.closer.addClass("uk-hidden"), t.modal.data("mwidth") == c && t.modal.data("mheight") == g && (d = 0), this.modal.dialog.animate({
                width: c + o,
                height: g + o,
                top: p
            }, d, "swing", function () {
                t.modal.loader.addClass("uk-hidden"), t.modal.content.css({
                    width: ""
                }).animate({
                    opacity: 1
                }, function () {
                    t.modal.closer.removeClass("uk-hidden")
                }), t.modal.data({
                    mwidth: c,
                    mheight: g
                })
            })
        },
        next: function () {
            this.show(this.siblings[this.index + 1] ? this.index + 1 : 0)
        },
        previous: function () {
            this.show(this.siblings[this.index - 1] ? this.index - 1 : this.siblings.length - 1)
        }
    }), i.plugin("lightbox", "image", {
        init: function (i) {
            i.on("showitem.uk.lightbox", function (i, t) {
                if ("image" == t.type || t.source && t.source.match(/\.(jpg|jpeg|png|gif|svg)$/i)) {
                    var e = function (i, e, o) {
                        t.meta = {
                            content: '<img class="uk-responsive-width" width="' + e + '" height="' + o + '" src ="' + i + '">',
                            width: e,
                            height: o
                        }, t.type = "image", t.promise.resolve()
                    };
                    if (o[t.source]) e(t.source, o[t.source].width, o[t.source].height);
                    else {
                        var n = new Image;
                        n.onerror = function () {
                            t.promise.reject("Loading image failed")
                        }, n.onload = function () {
                            o[t.source] = {
                                width: n.width,
                                height: n.height
                            }, e(t.source, o[t.source].width, o[t.source].height)
                        }, n.src = t.source
                    }
                }
            })
        }
    }), i.plugin("lightbox", "youtube", {
        init: function (i) {
            var t = /(\/\/.*?youtube\.[a-z]+)\/watch\?v=([^&]+)&?(.*)/,
                e = /youtu\.be\/(.*)/;
            i.on("showitem.uk.lightbox", function (i, n) {
                var s, h, a = function (i, t, e) {
                    n.meta = {
                        content: '<iframe src="//www.youtube.com/embed/' + i + '" width="' + t + '" height="' + e + '" style="max-width:100%;"></iframe>',
                        width: t,
                        height: e
                    }, n.type = "iframe", n.promise.resolve()
                };
                if ((h = n.source.match(t)) && (s = h[2]), (h = n.source.match(e)) && (s = h[1]), s) {
                    if (o[s]) a(s, o[s].width, o[s].height);
                    else {
                        var d = new Image,
                            l = !1;
                        d.onerror = function () {
                            o[s] = {
                                width: 640,
                                height: 320
                            }, a(s, o[s].width, o[s].height)
                        }, d.onload = function () {
                            120 == d.width && 90 == d.height ? l ? (o[s] = {
                                width: 640,
                                height: 320
                            }, a(s, o[s].width, o[s].height)) : (l = !0, d.src = "//img.youtube.com/vi/" + s + "/0.jpg") : (o[s] = {
                                width: d.width,
                                height: d.height
                            }, a(s, d.width, d.height))
                        }, d.src = "//img.youtube.com/vi/" + s + "/maxresdefault.jpg"
                    }
                    i.stopImmediatePropagation()
                }
            })
        }
    }), i.plugin("lightbox", "vimeo", {
        init: function (t) {
            var e, n = /(\/\/.*?)vimeo\.[a-z]+\/([0-9]+).*?/;
            t.on("showitem.uk.lightbox", function (t, s) {
                var h, a = function (i, t, e) {
                    s.meta = {
                        content: '<iframe src="//player.vimeo.com/video/' + i + '" width="' + t + '" height="' + e + '" style="width:100%;box-sizing:border-box;"></iframe>',
                        width: t,
                        height: e
                    }, s.type = "iframe", s.promise.resolve()
                };
                (e = s.source.match(n)) && (h = e[2], o[h] ? a(h, o[h].width, o[h].height) : i.$.ajax({
                    type: "GET",
                    url: "http://vimeo.com/api/oembed.json?url=" + encodeURI(s.source),
                    jsonp: "callback",
                    dataType: "jsonp",
                    success: function (i) {
                        o[h] = {
                            width: i.width,
                            height: i.height
                        }, a(h, o[h].width, o[h].height)
                    }
                }), t.stopImmediatePropagation())
            })
        }
    }), i.plugin("lightbox", "video", {
        init: function (t) {
            t.on("showitem.uk.lightbox", function (t, e) {
                var n = function (i, t, o) {
                    e.meta = {
                        content: '<video class="uk-responsive-width" src="' + i + '" width="' + t + '" height="' + o + '" controls></video>',
                        width: t,
                        height: o
                    }, e.type = "video", e.promise.resolve()
                };
                if ("video" == e.type || e.source.match(/\.(mp4|webm|ogv)$/i))
                    if (o[e.source]) n(e.source, o[e.source].width, o[e.source].height);
                    else var s = i.$('<video style="position:fixed;visibility:hidden;top:-10000px;"></video>').attr("src", e.source).appendTo("body"),
                        h = setInterval(function () {
                            s[0].videoWidth && (clearInterval(h), o[e.source] = {
                                width: s[0].videoWidth,
                                height: s[0].videoHeight
                            }, n(e.source, o[e.source].width, o[e.source].height), s.remove())
                        }, 20)
            })
        }
    }), i.lightbox.create = function (t, e) {
        if (t) {
            var o, n = [];
            return t.forEach(function (t) {
                n.push(i.$.extend({
                    source: "",
                    title: "",
                    type: "auto",
                    link: !1
                }, "string" == typeof t ? {
                    source: t
                } : t))
            }), o = i.lightbox(i.$.extend({}, e, {
                group: n
            }))
        }
    }, i.lightbox
});
! function (t) {
    var e;
    window.UIkit && (e = t(UIkit)), "function" == typeof define && define.amd && define("uikit-parallax", ["uikit"], function () {
        return e || t(UIkit)
    })
}(function (t) {
    "use strict";

    function e(e, a, r) {
        var n, i, s, o, c, p, l, d = new Image;
        return i = e.element.css({
            "background-size": "cover",
            "background-repeat": "no-repeat"
        }), n = i.css("background-image").replace(/^url\(/g, "").replace(/\)$/g, "").replace(/("|')/g, ""), o = function () {
            var t = i.width(),
                n = i.height(),
                o = "bg" == a ? r.diff : r.diff / 100 * n;
            return n += o, t += Math.ceil(o * c), t - o > s.w && n < s.h ? e.element.css({
                "background-size": ""
            }) : (n > t / c ? (p = Math.ceil(n * c), l = n, n > window.innerHeight && (p = 1.2 * p, l = 1.2 * l)) : (p = t, l = Math.ceil(t / c)), void i.css({
                "background-size": p + "px " + l + "px"
            }).data("bgsize", {
                w: p,
                h: l
            }))
        }, d.onerror = function () {}, d.onload = function () {
            s = {
                w: d.width,
                h: d.height
            }, c = d.width / d.height, t.$win.on("load resize orientationchange", t.Utils.debounce(function () {
                o()
            }, 50)), o()
        }, d.src = n, !0
    }

    function a(t, e, a) {
        return t = n(t), e = n(e), a = a || 0, r(t, e, a)
    }

    function r(t, e, a) {
        var r = "rgba(" + parseInt(t[0] + a * (e[0] - t[0]), 10) + "," + parseInt(t[1] + a * (e[1] - t[1]), 10) + "," + parseInt(t[2] + a * (e[2] - t[2]), 10) + "," + (t && e ? parseFloat(t[3] + a * (e[3] - t[3])) : 1);
        return r += ")"
    }

    function n(t) {
        var e, a;
        return a = (e = /#([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})/.exec(t)) ? [parseInt(e[1], 16), parseInt(e[2], 16), parseInt(e[3], 16), 1] : (e = /#([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])/.exec(t)) ? [17 * parseInt(e[1], 16), 17 * parseInt(e[2], 16), 17 * parseInt(e[3], 16), 1] : (e = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(t)) ? [parseInt(e[1]), parseInt(e[2]), parseInt(e[3]), 1] : (e = /rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9\.]*)\s*\)/.exec(t)) ? [parseInt(e[1], 10), parseInt(e[2], 10), parseInt(e[3], 10), parseFloat(e[4])] : l[t] || [255, 255, 255, 0]
    }
    var i = [],
        s = !1,
        o = 0,
        c = window.innerHeight,
        p = function () {
            o = t.$win.scrollTop(), window.requestAnimationFrame(function () {
                for (var t = 0; t < i.length; t++) i[t].process()
            })
        };
    t.component("parallax", {
        defaults: {
            velocity: .5,
            target: !1,
            viewport: !1,
            media: !1
        },
        boot: function () {
            s = function () {
                var t, e = document.createElement("div"),
                    a = {
                        WebkitTransform: "-webkit-transform",
                        MSTransform: "-ms-transform",
                        MozTransform: "-moz-transform",
                        Transform: "transform"
                    };
                document.body.insertBefore(e, null);
                for (var r in a) void 0 !== e.style[r] && (e.style[r] = "translate3d(1px,1px,1px)", t = window.getComputedStyle(e).getPropertyValue(a[r]));
                return document.body.removeChild(e), void 0 !== t && t.length > 0 && "none" !== t
            }(), t.$doc.on("scrolling.uk.document", p), t.$win.on("load resize orientationchange", t.Utils.debounce(function () {
                c = window.innerHeight, p()
            }, 50)), t.ready(function (e) {
                t.$("[data-uk-parallax]", e).each(function () {
                    var e = t.$(this);
                    e.data("parallax") || t.parallax(e, t.Utils.options(e.attr("data-uk-parallax")))
                })
            })
        },
        init: function () {
            this.base = this.options.target ? t.$(this.options.target) : this.element, this.props = {}, this.velocity = this.options.velocity || 1;
            var e = ["target", "velocity", "viewport", "plugins", "media"];
            Object.keys(this.options).forEach(function (t) {
                if (-1 === e.indexOf(t)) {
                    var a, r, n, i, s = String(this.options[t]).split(",");
                    t.match(/color/i) ? (a = s[1] ? s[0] : this._getStartValue(t), r = s[1] ? s[1] : s[0], a || (a = "rgba(255,255,255,0)")) : (a = parseFloat(s[1] ? s[0] : this._getStartValue(t)), r = parseFloat(s[1] ? s[1] : s[0]), i = r > a ? r - a : a - r, n = r > a ? 1 : -1), this.props[t] = {
                        start: a,
                        end: r,
                        dir: n,
                        diff: i
                    }
                }
            }.bind(this)), i.push(this)
        },
        process: function () {
            if (this.options.media) switch (typeof this.options.media) {
                case "number":
                    if (window.innerWidth < this.options.media) return !1;
                    break;
                case "string":
                    if (window.matchMedia && !window.matchMedia(this.options.media).matches) return !1
            }
            var t = this.percentageInViewport();
            this.options.viewport !== !1 && (t = 0 === this.options.viewport ? 1 : t / this.options.viewport), this.update(t)
        },
        percentageInViewport: function () {
            var t, e, a, r = this.base.offset().top,
                n = this.base.outerHeight();
            return r > o + c ? a = 0 : o > r + n ? a = 1 : c > r + n ? a = (c > o ? o : o - c) / (r + n) : (t = o + c - r, e = Math.round(t / ((c + n) / 100)), a = e / 100), a
        },
        update: function (t) {
            var r, n, i = {
                    transform: ""
                },
                o = t * (1 - (this.velocity - this.velocity * t));
            0 > o && (o = 0), o > 1 && (o = 1), (void 0 === this._percent || this._percent != o) && (Object.keys(this.props).forEach(function (c) {
                switch (r = this.props[c], 0 === t ? n = r.start : 1 === t ? n = r.end : void 0 !== r.diff && (n = r.start + r.diff * o * r.dir), "bg" != c && "bgp" != c || this._bgcover || (this._bgcover = e(this, c, r)), c) {
                    case "x":
                        i.transform += s ? " translate3d(" + n + "px, 0, 0)" : " translateX(" + n + "px)";
                        break;
                    case "xp":
                        i.transform += s ? " translate3d(" + n + "%, 0, 0)" : " translateX(" + n + "%)";
                        break;
                    case "y":
                        i.transform += s ? " translate3d(0, " + n + "px, 0)" : " translateY(" + n + "px)";
                        break;
                    case "yp":
                        i.transform += s ? " translate3d(0, " + n + "%, 0)" : " translateY(" + n + "%)";
                        break;
                    case "rotate":
                        i.transform += " rotate(" + n + "deg)";
                        break;
                    case "scale":
                        i.transform += " scale(" + n + ")";
                        break;
                    case "bg":
                        i["background-position"] = "50% " + n + "px";
                        break;
                    case "bgp":
                        i["background-position"] = "50% " + n + "%";
                        break;
                    case "color":
                    case "background-color":
                    case "border-color":
                        i[c] = a(r.start, r.end, o);
                        break;
                    default:
                        i[c] = n
                }
            }.bind(this)), this.element.css(i), this._percent = o)
        },
        _getStartValue: function (t) {
            var e = 0;
            switch (t) {
                case "scale":
                    e = 1;
                    break;
                default:
                    e = this.element.css(t)
            }
            return e || 0
        }
    });
    var l = {
        black: [0, 0, 0, 1],
        blue: [0, 0, 255, 1],
        brown: [165, 42, 42, 1],
        cyan: [0, 255, 255, 1],
        fuchsia: [255, 0, 255, 1],
        gold: [255, 215, 0, 1],
        green: [0, 128, 0, 1],
        indigo: [75, 0, 130, 1],
        khaki: [240, 230, 140, 1],
        lime: [0, 255, 0, 1],
        magenta: [255, 0, 255, 1],
        maroon: [128, 0, 0, 1],
        navy: [0, 0, 128, 1],
        olive: [128, 128, 0, 1],
        orange: [255, 165, 0, 1],
        pink: [255, 192, 203, 1],
        purple: [128, 0, 128, 1],
        violet: [128, 0, 128, 1],
        red: [255, 0, 0, 1],
        silver: [192, 192, 192, 1],
        white: [255, 255, 255, 1],
        yellow: [255, 255, 0, 1],
        transparent: [255, 255, 255, 0]
    };
    return t.parallax
});
! function (t) {
    var i;
    window.UIkit && (i = t(UIkit)), "function" == typeof define && define.amd && define("uikit-slider", ["uikit"], function () {
        return i || t(UIkit)
    })
}(function (t) {
    "use strict";
    var i, e, s, n, a = {};
    return t.component("slider", {
        defaults: {
            center: !1,
            threshold: 10,
            infinite: !0,
            autoplay: !1,
            autoplayInterval: 7e3,
            pauseOnHover: !0,
            activecls: "uk-active"
        },
        boot: function () {
            t.ready(function (i) {
                setTimeout(function () {
                    t.$("[data-uk-slider]", i).each(function () {
                        var i = t.$(this);
                        i.data("slider") || t.slider(i, t.Utils.options(i.attr("data-uk-slider")))
                    })
                }, 0)
            })
        },
        init: function () {
            var o = this;
            this.container = this.element.find(".uk-slider"), this.focus = 0, t.$win.on("resize load", t.Utils.debounce(function () {
                o.resize(!0)
            }, 100)), this.on("click.uikit.slider", "[data-uk-slider-item]", function (i) {
                i.preventDefault();
                var e = t.$(this).attr("data-uk-slider-item");
                if (o.focus != e) switch (o.stop(), e) {
                    case "next":
                    case "previous":
                        o["next" == e ? "next" : "previous"]();
                        break;
                    default:
                        o.updateFocus(parseInt(e, 10))
                }
            }), this.container.on({
                "touchstart mousedown": function (h) {
                    h.originalEvent && h.originalEvent.touches && (h = h.originalEvent.touches[0]), h.button && 2 == h.button || !o.active || (o.stop(), s = t.$(h.target).is("a") ? t.$(h.target) : t.$(h.target).parents("a:first"), n = !1, s.length && s.one("click", function (t) {
                        n && t.preventDefault()
                    }), e = function (t) {
                        n = !0, i = o, a = {
                            touchx: parseInt(t.pageX, 10),
                            dir: 1,
                            focus: o.focus,
                            base: o.options.center ? "center" : "area"
                        }, t.originalEvent && t.originalEvent.touches && (t = t.originalEvent.touches[0]), i.element.data({
                            "pointer-start": {
                                x: parseInt(t.pageX, 10),
                                y: parseInt(t.pageY, 10)
                            },
                            "pointer-pos-start": o.pos
                        }), o.container.addClass("uk-drag"), e = !1
                    }, e.x = parseInt(h.pageX, 10), e.threshold = o.options.threshold)
                },
                mouseenter: function () {
                    o.options.pauseOnHover && (o.hovering = !0)
                },
                mouseleave: function () {
                    o.hovering = !1
                }
            }), this.resize(!0), this.on("display.uk.check", function () {
                o.element.is(":visible") && o.resize(!0)
            }), this.element.find("a,img").attr("draggable", "false"), this.options.autoplay && this.start()
        },
        resize: function (i) {
            var e, s, n, a, o = this,
                h = 0,
                r = 0;
            return this.items = this.container.children().filter(":visible"), this.vp = this.element[0].getBoundingClientRect().width, this.container.css({
                "min-width": "",
                "min-height": ""
            }), this.items.each(function (i) {
                e = t.$(this), a = e.css({
                    left: "",
                    width: ""
                })[0].getBoundingClientRect(), s = a.width, n = e.width(), r = Math.max(r, a.height), e.css({
                    left: h,
                    width: s
                }).data({
                    idx: i,
                    left: h,
                    width: s,
                    cwidth: n,
                    area: h + s,
                    center: h - (o.vp / 2 - n / 2)
                }), h += s
            }), this.container.css({
                "min-width": h,
                "min-height": r
            }), this.options.infinite && (h <= 2 * this.vp || this.items.length < 5) && !this.itemsResized ? (this.container.children().each(function (t) {
                o.container.append(o.items.eq(t).clone(!0).attr("id", ""))
            }).each(function (t) {
                o.container.append(o.items.eq(t).clone(!0).attr("id", ""))
            }), this.itemsResized = !0, this.resize()) : (this.cw = h, this.pos = 0, this.active = h >= this.vp, this.container.css({
                "-ms-transform": "",
                "-webkit-transform": "",
                transform: ""
            }), void(i && this.updateFocus(this.focus)))
        },
        updatePos: function (t) {
            this.pos = t, this.container.css({
                "-ms-transform": "translateX(" + t + "px)",
                "-webkit-transform": "translateX(" + t + "px)",
                transform: "translateX(" + t + "px)"
            })
        },
        updateFocus: function (i, e) {
            if (this.active) {
                e = e || (i > this.focus ? 1 : -1);
                var s, n, a = this.items.eq(i);
                if (this.options.infinite && this.infinite(i, e), this.options.center) this.updatePos(-1 * a.data("center")), this.items.filter("." + this.options.activecls).removeClass(this.options.activecls), a.addClass(this.options.activecls);
                else if (this.options.infinite) this.updatePos(-1 * a.data("left"));
                else {
                    for (s = 0, n = i; n < this.items.length; n++) s += this.items.eq(n).data("width");
                    if (s > this.vp) this.updatePos(-1 * a.data("left"));
                    else if (1 == e) {
                        for (s = 0, n = this.items.length - 1; n >= 0; n--) {
                            if (s += this.items.eq(n).data("width"), s == this.vp) {
                                i = n;
                                break
                            }
                            if (s > this.vp) {
                                i = n < this.items.length - 1 ? n + 1 : n;
                                break
                            }
                        }
                        s > this.vp ? this.updatePos(-1 * (this.container.width() - this.vp)) : this.updatePos(-1 * this.items.eq(i).data("left"))
                    }
                }
                var o = this.items.eq(i).data("left");
                this.items.removeClass("uk-slide-before uk-slide-after").each(function (e) {
                    e !== i && t.$(this).addClass(t.$(this).data("left") < o ? "uk-slide-before" : "uk-slide-after")
                }), this.focus = i, this.trigger("focusitem.uk.slider", [i, this.items.eq(i), this])
            }
        },
        next: function () {
            var t = this.items[this.focus + 1] ? this.focus + 1 : this.options.infinite ? 0 : this.focus;
            this.updateFocus(t, 1)
        },
        previous: function () {
            var t = this.items[this.focus - 1] ? this.focus - 1 : this.options.infinite ? this.items[this.focus - 1] ? this.items - 1 : this.items.length - 1 : this.focus;
            this.updateFocus(t, -1)
        },
        start: function () {
            this.stop();
            var t = this;
            this.interval = setInterval(function () {
                t.hovering || t.next()
            }, this.options.autoplayInterval)
        },
        stop: function () {
            this.interval && clearInterval(this.interval)
        },
        infinite: function (t, i) {
            var e, s = this,
                n = this.items.eq(t),
                a = t,
                o = [],
                h = 0;
            if (1 == i) {
                for (e = 0; e < this.items.length && (a != t && (h += this.items.eq(a).data("width"), o.push(this.items.eq(a))), !(h > this.vp)); e++) a = a + 1 == this.items.length ? 0 : a + 1;
                o.length && o.forEach(function (t) {
                    var i = n.data("area");
                    t.css({
                        left: i
                    }).data({
                        left: i,
                        area: i + t.data("width"),
                        center: i - (s.vp / 2 - t.data("cwidth") / 2)
                    }), n = t
                })
            } else {
                for (e = this.items.length - 1; e > -1 && (h += this.items.eq(a).data("width"), a != t && o.push(this.items.eq(a)), !(h > this.vp)); e--) a = a - 1 == -1 ? this.items.length - 1 : a - 1;
                o.length && o.forEach(function (t) {
                    var i = n.data("left") - t.data("width");
                    t.css({
                        left: i
                    }).data({
                        left: i,
                        area: i + t.data("width"),
                        center: i - (s.vp / 2 - t.data("cwidth") / 2)
                    }), n = t
                })
            }
        }
    }), t.$doc.on("mousemove.uikit.slider touchmove.uikit.slider", function (t) {
        if (t.originalEvent && t.originalEvent.touches && (t = t.originalEvent.touches[0]), e && Math.abs(t.pageX - e.x) > e.threshold && (window.getSelection().toString() ? i = e = !1 : e(t)), i) {
            var s, n, o, h, r, c, f, u, d, l;
            if (t.clientX || t.clientY ? s = t.clientX : (t.pageX || t.pageY) && (s = t.pageX - document.body.scrollLeft - document.documentElement.scrollLeft), r = a.focus, n = s - i.element.data("pointer-start").x, o = i.element.data("pointer-pos-start") + n, h = s > i.element.data("pointer-start").x ? -1 : 1, c = i.items.eq(a.focus), 1 == h)
                for (f = c.data("left") + Math.abs(n), u = 0, d = a.focus; u < i.items.length; u++) {
                    if (l = i.items.eq(d), d != a.focus && l.data("left") < f && l.data("area") > f) {
                        r = d;
                        break
                    }
                    d = d + 1 == i.items.length ? 0 : d + 1
                } else
                    for (f = c.data("left") - Math.abs(n), u = 0, d = a.focus; u < i.items.length; u++) {
                        if (l = i.items.eq(d), d != a.focus && l.data("area") <= c.data("left") && l.data("center") < f) {
                            r = d;
                            break
                        }
                        d = d - 1 == -1 ? i.items.length - 1 : d - 1
                    }
            i.options.infinite && r != a._focus && i.infinite(r, h), i.updatePos(o), a.dir = h, a._focus = r, a.touchx = parseInt(t.pageX, 10), a.diff = f
        }
    }), t.$doc.on("mouseup.uikit.slider touchend.uikit.slider", function (t) {
        if (i) {
            i.container.removeClass("uk-drag"), i.items.eq(a.focus);
            var s, n, o, h = !1;
            if (1 == a.dir)
                for (n = 0, o = a.focus; n < i.items.length; n++) {
                    if (s = i.items.eq(o), o != a.focus && s.data("left") > a.diff) {
                        h = o;
                        break
                    }
                    o = o + 1 == i.items.length ? 0 : o + 1
                } else
                    for (n = 0, o = a.focus; n < i.items.length; n++) {
                        if (s = i.items.eq(o), o != a.focus && s.data("left") < a.diff) {
                            h = o;
                            break
                        }
                        o = o - 1 == -1 ? i.items.length - 1 : o - 1
                    }
            i.updateFocus(h !== !1 ? h : a._focus)
        }
        i = e = !1
    }), t.slider
});
! function (t) {
    var i;
    window.UIkit && (i = t(UIkit)), "function" == typeof define && define.amd && define("uikit-slideset", ["uikit"], function () {
        return i || t(UIkit)
    })
}(function (t) {
    "use strict";

    function i(i, e, s, n) {
        var a, o, r, l, h = t.$.Deferred(),
            u = this.options.delay === !1 ? Math.floor(this.options.duration / 2) : this.options.delay,
            d = this;
        if (n = n || 1, this.element.css("min-height", this.element.height()), s[0] === e[0]) return h.resolve(), h.promise();
        if ("object" == typeof i ? (a = i[0], o = i[1] || i[0]) : (a = i, o = a), r = function () {
                if (e && e.length && e.hide().removeClass(o + " uk-animation-reverse").css({
                        opacity: "",
                        "animation-delay": "",
                        animation: ""
                    }), !s.length) return void h.resolve();
                for (l = 0; l < s.length; l++) s.eq(1 == n ? l : s.length - l - 1).css("animation-delay", l * u + "ms");
                var i = function () {
                    s.removeClass("" + a).css({
                        opacity: "",
                        display: "",
                        "animation-delay": "",
                        animation: ""
                    }), h.resolve(), d.element.css("min-height", ""), i = function () {}
                };
                s.addClass(a)[1 == n ? "last" : "first"]().one(t.support.animation.end, i).end().css("display", ""), setTimeout(function () {
                    i()
                }, s.length * u * 2)
            }, s.length && s.css("animation-duration", this.options.duration + "ms"), e && e.length)
            for (e.css("animation-duration", this.options.duration + "ms")[1 == n ? "last" : "first"]().one(t.support.animation.end, function () {
                    r()
                }), l = 0; l < e.length; l++) ! function (i, e) {
                setTimeout(function () {
                    e.css("display", "none").css("display", "").css("opacity", 0).on(t.support.animation.end, function () {
                        e.removeClass(o)
                    }).addClass(o + " uk-animation-reverse")
                }.bind(this), l * u)
            }(l, e.eq(1 == n ? l : e.length - l - 1));
        else r();
        return h.promise()
    }

    function e(t, i) {
        var e, s = 0,
            n = -1,
            a = t.length || 0,
            o = [];
        if (1 > i) return null;
        for (; a > s;) e = s % i, e ? o[n][e] = t[s] : o[++n] = [t[s]], s++;
        for (s = 0, a = o.length; a > s;) o[s] = jQuery(o[s]), s++;
        return o
    }
    var s;
    t.component("slideset", {
        defaults: {
            "default": 1,
            animation: "fade",
            duration: 200,
            filter: "",
            delay: !1,
            controls: !1,
            autoplay: !1,
            autoplayInterval: 7e3,
            pauseOnHover: !0
        },
        sets: [],
        boot: function () {
            t.ready(function (i) {
                t.$("[data-uk-slideset]", i).each(function () {
                    var i = t.$(this);
                    i.data("slideset") || t.slideset(i, t.Utils.options(i.attr("data-uk-slideset")))
                })
            })
        },
        init: function () {
            var i = this;
            this.activeSet = !1, this.list = this.element.find(".uk-slideset"), this.nav = this.element.find(".uk-slideset-nav"), this.controls = this.options.controls ? t.$(this.options.controls) : this.element, t.$win.on("resize load", t.Utils.debounce(function () {
                i.updateSets()
            }, 100)), i.list.addClass("uk-grid-width-1-" + i.options["default"]), ["xlarge", "large", "medium", "small"].forEach(function (t) {
                i.options[t] && i.list.addClass("uk-grid-width-" + t + "-1-" + i.options[t])
            }), this.on("click.uikit.slideset", "[data-uk-slideset-item]", function (e) {
                if (e.preventDefault(), !i.animating) {
                    var s = t.$(this).attr("data-uk-slideset-item");
                    if (i.activeSet !== s) switch (s) {
                        case "next":
                        case "previous":
                            i["next" == s ? "next" : "previous"]();
                            break;
                        default:
                            i.show(parseInt(s, 10))
                    }
                }
            }), this.controls.on("click.uikit.slideset", "[data-uk-filter]", function (e) {
                var s = t.$(this);
                s.parent().hasClass("uk-slideset") || (e.preventDefault(), i.animating || i.currentFilter == s.attr("data-uk-filter") || (i.updateFilter(s.attr("data-uk-filter")), i._hide().then(function () {
                    i.updateSets(!0, !0)
                })))
            }), this.on("swipeRight swipeLeft", function (t) {
                i["swipeLeft" == t.type ? "next" : "previous"]()
            }), this.updateFilter(this.options.filter), this.updateSets(), this.element.on({
                mouseenter: function () {
                    i.options.pauseOnHover && (i.hovering = !0)
                },
                mouseleave: function () {
                    i.hovering = !1
                }
            }), this.options.autoplay && this.start()
        },
        updateSets: function (t, i) {
            var s, n = this.visible;
            if (this.visible = this.getVisibleOnCurrenBreakpoint(), n != this.visible || i) {
                for (this.children = this.list.children().hide(), this.items = this.getItems(), this.sets = e(this.items, this.visible), s = 0; s < this.sets.length; s++) this.sets[s].css({
                    display: "none"
                });
                if (this.nav.length && this.nav.empty()) {
                    for (s = 0; s < this.sets.length; s++) this.nav.append('<li data-uk-slideset-item="' + s + '"><a></a></li>');
                    this.nav[1 == this.nav.children().length ? "addClass" : "removeClass"]("uk-invisible")
                }
                this.activeSet = !1, this.show(0, !t)
            }
        },
        updateFilter: function (i) {
            var e, s = this;
            this.currentFilter = i, this.controls.find("[data-uk-filter]").each(function () {
                e = t.$(this), e.parent().hasClass("uk-slideset") || (e.attr("data-uk-filter") == s.currentFilter ? e.addClass("uk-active") : e.removeClass("uk-active"))
            })
        },
        getVisibleOnCurrenBreakpoint: function () {
            var i = null,
                e = t.$('<div style="position:absolute;height:1px;top:-1000px;width:100px"><div></div></div>').appendTo("body"),
                s = e.children().eq(0),
                n = this.options;
            return ["xlarge", "large", "medium", "small"].forEach(function (t) {
                n[t] && !i && (e.attr("class", "uk-grid-width-" + t + "-1-2").width(), 50 == s.width() && (i = t))
            }), e.remove(), this.options[i] || this.options["default"]
        },
        getItems: function () {
            var i, e = [];
            return this.currentFilter ? (i = this.currentFilter || [], "string" == typeof i && (i = i.split(/,/).map(function (t) {
                return t.trim()
            })), this.children.each(function (s) {
                var n = t.$(this),
                    a = n.attr("data-uk-filter"),
                    o = i.length ? !1 : !0;
                a && (a = a.split(/,/).map(function (t) {
                    return t.trim()
                }), i.forEach(function (t) {
                    a.indexOf(t) > -1 && (o = !0)
                })), o && e.push(n[0])
            }), e = t.$(e)) : e = this.list.children(), e
        },
        show: function (i, e, n) {
            var a = this;
            if (this.activeSet !== i && !this.animating) {
                n = n || (i < this.activeSet ? -1 : 1);
                var o = this.sets[this.activeSet] || [],
                    r = this.sets[i],
                    l = this._getAnimation();
                (e || !t.support.animation) && (l = s.none), this.animating = !0, this.nav.length && this.nav.children().removeClass("uk-active").eq(i).addClass("uk-active"), l.apply(a, [o, r, n]).then(function () {
                    t.Utils.checkDisplay(r, !0), a.children.hide().removeClass("uk-active"), r.addClass("uk-active").css({
                        display: "",
                        opacity: ""
                    }), a.animating = !1, a.activeSet = i, t.Utils.checkDisplay(r, !0), a.trigger("show.uk.slideset", [r])
                })
            }
        },
        _getAnimation: function () {
            var i = s[this.options.animation] || s.none;
            return t.support.animation || (i = s.none), i
        },
        _hide: function () {
            var t = this,
                i = this.sets[this.activeSet] || [],
                e = this._getAnimation();
            return this.animating = !0, e.apply(t, [i, [], 1]).then(function () {
                t.animating = !1
            })
        },
        next: function () {
            this.show(this.sets[this.activeSet + 1] ? this.activeSet + 1 : 0, !1, 1)
        },
        previous: function () {
            this.show(this.sets[this.activeSet - 1] ? this.activeSet - 1 : this.sets.length - 1, !1, -1)
        },
        start: function () {
            this.stop();
            var t = this;
            this.interval = setInterval(function () {
                t.hovering || t.animating || t.next()
            }, this.options.autoplayInterval)
        },
        stop: function () {
            this.interval && clearInterval(this.interval)
        }
    }), s = {
        none: function () {
            var i = t.$.Deferred();
            return i.resolve(), i.promise()
        },
        fade: function (t, e) {
            return i.apply(this, ["uk-animation-fade", t, e])
        },
        "slide-bottom": function (t, e) {
            return i.apply(this, ["uk-animation-slide-bottom", t, e])
        },
        "slide-top": function (t, e) {
            return i.apply(this, ["uk-animation-slide-top", t, e])
        },
        "slide-vertical": function (t, e, s) {
            var n = ["uk-animation-slide-top", "uk-animation-slide-bottom"];
            return -1 == s && n.reverse(), i.apply(this, [n, t, e])
        },
        "slide-horizontal": function (t, e, s) {
            var n = ["uk-animation-slide-right", "uk-animation-slide-left"];
            return -1 == s && n.reverse(), i.apply(this, [n, t, e, s])
        },
        scale: function (t, e) {
            return i.apply(this, ["uk-animation-scale-up", t, e])
        }
    }, t.slideset.animations = s
});
! function (i) {
    var t;
    window.UIkit && (t = i(UIkit)), "function" == typeof define && define.amd && define("uikit-slideshow", ["uikit"], function () {
        return t || i(UIkit)
    })
}(function (i) {
    "use strict";
    var t, s = 0;
    i.component("slideshow", {
        defaults: {
            animation: "fade",
            duration: 500,
            height: "auto",
            start: 0,
            autoplay: !1,
            autoplayInterval: 7e3,
            videoautoplay: !0,
            videomute: !0,
            slices: 15,
            pauseOnHover: !0,
            kenburns: !1,
            kenburnsanimations: ["uk-animation-middle-left", "uk-animation-top-right", "uk-animation-bottom-left", "uk-animation-top-center", "", "uk-animation-bottom-right"]
        },
        current: !1,
        interval: null,
        hovering: !1,
        boot: function () {
            i.ready(function (t) {
                i.$("[data-uk-slideshow]", t).each(function () {
                    var t = i.$(this);
                    t.data("slideshow") || i.slideshow(t, i.Utils.options(t.attr("data-uk-slideshow")))
                })
            })
        },
        init: function () {
            var t, e, a = this;
            this.container = this.element.hasClass("uk-slideshow") ? this.element : i.$(this.find(".uk-slideshow")), this.slides = this.container.children(), this.slidesCount = this.slides.length, this.current = this.options.start, this.animating = !1, this.triggers = this.find("[data-uk-slideshow-item]"), this.fixFullscreen = navigator.userAgent.match(/(iPad|iPhone|iPod)/g) && this.container.hasClass("uk-slideshow-fullscreen"), this.options.kenburns && (e = this.options.kenburns === !0 ? "15s" : this.options.kenburns, String(e).match(/(ms|s)$/) || (e += "ms"), "string" == typeof this.options.kenburnsanimations && (this.options.kenburnsanimations = this.options.kenburnsanimations.split(","))), this.slides.each(function (n) {
                var o = i.$(this),
                    r = o.children("img,video,iframe").eq(0);
                if (o.data("media", r), o.data("sizer", r), r.length) {
                    var d;
                    switch (r[0].nodeName) {
                        case "IMG":
                            var u = i.$('<div class="uk-cover-background uk-position-cover"></div>').css({
                                "background-image": "url(" + r.attr("src") + ")"
                            });
                            r.attr("width") && r.attr("height") && (d = i.$("<canvas></canvas>").attr({
                                width: r.attr("width"),
                                height: r.attr("height")
                            }), r.replaceWith(d), r = d, d = void 0), r.css({
                                width: "100%",
                                height: "auto",
                                opacity: 0
                            }), o.prepend(u).data("cover", u);
                            break;
                        case "IFRAME":
                            var h = r[0].src,
                                c = "sw-" + ++s;
                            r.attr("src", "").on("load", function () {
                                if ((n !== a.current || n == a.current && !a.options.videoautoplay) && a.pausemedia(r), a.options.videomute) {
                                    a.mutemedia(r);
                                    var i = setInterval(function (t) {
                                        return function () {
                                            a.mutemedia(r), ++t >= 4 && clearInterval(i)
                                        }
                                    }(0), 250)
                                }
                            }).data("slideshow", a).attr("data-player-id", c).attr("src", [h, h.indexOf("?") > -1 ? "&" : "?", "enablejsapi=1&api=1&player_id=" + c].join("")).addClass("uk-position-absolute"), i.support.touch || r.css("pointer-events", "none"), d = !0, i.cover && (i.cover(r), r.attr("data-uk-cover", "{}"));
                            break;
                        case "VIDEO":
                            r.addClass("uk-cover-object uk-position-absolute"), d = !0, a.options.videomute && a.mutemedia(r)
                    }
                    if (d) {
                        t = i.$("<canvas></canvas>").attr({
                            width: r[0].width,
                            height: r[0].height
                        });
                        var l = i.$('<img style="width:100%;height:auto;">').attr("src", t[0].toDataURL());
                        o.prepend(l), o.data("sizer", l)
                    }
                } else o.data("sizer", o);
                a.hasKenBurns(o) && o.data("cover").css({
                    "-webkit-animation-duration": e,
                    "animation-duration": e
                })
            }), this.on("click.uikit.slideshow", "[data-uk-slideshow-item]", function (t) {
                t.preventDefault();
                var s = i.$(this).attr("data-uk-slideshow-item");
                if (a.current != s) {
                    switch (s) {
                        case "next":
                        case "previous":
                            a["next" == s ? "next" : "previous"]();
                            break;
                        default:
                            a.show(parseInt(s, 10))
                    }
                    a.stop()
                }
            }), this.slides.attr("aria-hidden", "true").eq(this.current).addClass("uk-active").attr("aria-hidden", "false"), this.triggers.filter('[data-uk-slideshow-item="' + this.current + '"]').addClass("uk-active"), i.$win.on("resize load", i.Utils.debounce(function () {
                a.resize(), a.fixFullscreen && (a.container.css("height", window.innerHeight), a.slides.css("height", window.innerHeight))
            }, 100)), setTimeout(function () {
                a.resize()
            }, 80), this.options.autoplay && this.start(), this.options.videoautoplay && this.slides.eq(this.current).data("media") && this.playmedia(this.slides.eq(this.current).data("media")), this.options.kenburns && this.applyKenBurns(this.slides.eq(this.current)), this.container.on({
                mouseenter: function () {
                    a.options.pauseOnHover && (a.hovering = !0)
                },
                mouseleave: function () {
                    a.hovering = !1
                }
            }), this.on("swipeRight swipeLeft", function (i) {
                a["swipeLeft" == i.type ? "next" : "previous"]()
            }), this.on("display.uk.check", function () {
                a.element.is(":visible") && (a.resize(), a.fixFullscreen && (a.container.css("height", window.innerHeight), a.slides.css("height", window.innerHeight)))
            })
        },
        resize: function () {
            if (!this.container.hasClass("uk-slideshow-fullscreen")) {
                var t = this.options.height;
                "auto" === this.options.height && (t = 0, this.slides.css("height", "").each(function () {
                    t = Math.max(t, i.$(this).height())
                })), this.container.css("height", t), this.slides.css("height", t)
            }
        },
        show: function (s, e) {
            if (!this.animating && this.current != s) {
                this.animating = !0;
                var a = this,
                    n = this.slides.eq(this.current),
                    o = this.slides.eq(s),
                    r = e ? e : this.current < s ? 1 : -1,
                    d = n.data("media"),
                    u = t[this.options.animation] ? this.options.animation : "fade",
                    h = o.data("media"),
                    c = function () {
                        a.animating && (d && d.is("video,iframe") && a.pausemedia(d), h && h.is("video,iframe") && a.playmedia(h), o.addClass("uk-active").attr("aria-hidden", "false"), n.removeClass("uk-active").attr("aria-hidden", "true"), a.animating = !1, a.current = s, i.Utils.checkDisplay(o, '[class*="uk-animation-"]:not(.uk-cover-background.uk-position-cover)'), a.trigger("show.uk.slideshow", [o, n, a]))
                    };
                a.applyKenBurns(o), i.support.animation || (u = "none"), n = i.$(n), o = i.$(o), a.trigger("beforeshow.uk.slideshow", [o, n, a]), t[u].apply(this, [n, o, r]).then(c), a.triggers.removeClass("uk-active"), a.triggers.filter('[data-uk-slideshow-item="' + s + '"]').addClass("uk-active")
            }
        },
        applyKenBurns: function (i) {
            if (this.hasKenBurns(i)) {
                var t = this.options.kenburnsanimations,
                    s = this.kbindex || 0;
                i.data("cover").attr("class", "uk-cover-background uk-position-cover").width(), i.data("cover").addClass(["uk-animation-scale", "uk-animation-reverse", t[s].trim()].join(" ")), this.kbindex = t[s + 1] ? s + 1 : 0
            }
        },
        hasKenBurns: function (i) {
            return this.options.kenburns && i.data("cover")
        },
        next: function () {
            this.show(this.slides[this.current + 1] ? this.current + 1 : 0, 1)
        },
        previous: function () {
            this.show(this.slides[this.current - 1] ? this.current - 1 : this.slides.length - 1, -1)
        },
        start: function () {
            this.stop();
            var i = this;
            this.interval = setInterval(function () {
                i.hovering || i.next()
            }, this.options.autoplayInterval)
        },
        stop: function () {
            this.interval && clearInterval(this.interval)
        },
        playmedia: function (i) {
            if (i && i[0]) switch (i[0].nodeName) {
                case "VIDEO":
                    this.options.videomute || (i[0].muted = !1), i[0].play();
                    break;
                case "IFRAME":
                    this.options.videomute || i[0].contentWindow.postMessage('{ "event": "command", "func": "unmute", "method":"setVolume", "value":1}', "*"), i[0].contentWindow.postMessage('{ "event": "command", "func": "playVideo", "method":"play"}', "*")
            }
        },
        pausemedia: function (i) {
            switch (i[0].nodeName) {
                case "VIDEO":
                    i[0].pause();
                    break;
                case "IFRAME":
                    i[0].contentWindow.postMessage('{ "event": "command", "func": "pauseVideo", "method":"pause"}', "*")
            }
        },
        mutemedia: function (i) {
            switch (i[0].nodeName) {
                case "VIDEO":
                    i[0].muted = !0;
                    break;
                case "IFRAME":
                    i[0].contentWindow.postMessage('{ "event": "command", "func": "mute", "method":"setVolume", "value":0}', "*")
            }
        }
    }), t = {
        none: function () {
            var t = i.$.Deferred();
            return t.resolve(), t.promise()
        },
        scroll: function (t, s, e) {
            var a = i.$.Deferred();
            return t.css("animation-duration", this.options.duration + "ms"), s.css("animation-duration", this.options.duration + "ms"), s.css("opacity", 1).one(i.support.animation.end, function () {
                t.removeClass(-1 == e ? "uk-slideshow-scroll-backward-out" : "uk-slideshow-scroll-forward-out"), s.css("opacity", "").removeClass(-1 == e ? "uk-slideshow-scroll-backward-in" : "uk-slideshow-scroll-forward-in"), a.resolve()
            }.bind(this)), t.addClass(-1 == e ? "uk-slideshow-scroll-backward-out" : "uk-slideshow-scroll-forward-out"), s.addClass(-1 == e ? "uk-slideshow-scroll-backward-in" : "uk-slideshow-scroll-forward-in"), s.width(), a.promise()
        },
        swipe: function (t, s, e) {
            var a = i.$.Deferred();
            return t.css("animation-duration", this.options.duration + "ms"), s.css("animation-duration", this.options.duration + "ms"), s.css("opacity", 1).one(i.support.animation.end, function () {
                t.removeClass(-1 === e ? "uk-slideshow-swipe-backward-out" : "uk-slideshow-swipe-forward-out"), s.css("opacity", "").removeClass(-1 === e ? "uk-slideshow-swipe-backward-in" : "uk-slideshow-swipe-forward-in"), a.resolve()
            }.bind(this)), t.addClass(-1 == e ? "uk-slideshow-swipe-backward-out" : "uk-slideshow-swipe-forward-out"), s.addClass(-1 == e ? "uk-slideshow-swipe-backward-in" : "uk-slideshow-swipe-forward-in"), s.width(), a.promise()
        },
        scale: function (t, s, e) {
            var a = i.$.Deferred();
            return t.css("animation-duration", this.options.duration + "ms"), s.css("animation-duration", this.options.duration + "ms"), s.css("opacity", 1), t.one(i.support.animation.end, function () {
                t.removeClass("uk-slideshow-scale-out"), s.css("opacity", ""), a.resolve()
            }.bind(this)), t.addClass("uk-slideshow-scale-out"), t.width(), a.promise()
        },
        fade: function (t, s, e) {
            var a = i.$.Deferred();
            return t.css("animation-duration", this.options.duration + "ms"), s.css("animation-duration", this.options.duration + "ms"), s.css("opacity", 1), s.data("cover") || s.data("placeholder") || s.css("opacity", 1).one(i.support.animation.end, function () {
                s.removeClass("uk-slideshow-fade-in")
            }).addClass("uk-slideshow-fade-in"), t.one(i.support.animation.end, function () {
                t.removeClass("uk-slideshow-fade-out"), s.css("opacity", ""), a.resolve()
            }.bind(this)), t.addClass("uk-slideshow-fade-out"), t.width(), a.promise()
        }
    }, i.slideshow.animations = t, window.addEventListener("message", function (t) {
        var s, e = t.data;
        if ("string" == typeof e) try {
            e = JSON.parse(e)
        } catch (a) {
            e = {}
        }
        t.origin && t.origin.indexOf("vimeo") > -1 && "ready" == e.event && e.player_id && (s = i.$('[data-player-id="' + e.player_id + '"]'), s.length && s.data("slideshow").mutemedia(s))
    }, !1)
});
! function (i) {
    var t;
    window.UIkit && (t = i(UIkit)), "function" == typeof define && define.amd && define("uikit-slideshow-fx", ["uikit"], function () {
        return t || i(UIkit)
    })
}(function (i) {
    "use strict";
    var t = i.slideshow.animations;
    i.$.extend(i.slideshow.animations, {
        slice: function (e, s, n, o) {
            if (!e.data("cover")) return t.fade.apply(this, arguments);
            for (var r, a = i.$.Deferred(), c = Math.ceil(this.element.width() / this.options.slices), h = s.data("cover").css("background-image"), d = i.$("<li></li>").css({
                    top: 0,
                    left: 0,
                    width: this.container.width(),
                    height: this.container.height(),
                    opacity: 1,
                    zIndex: 15
                }), p = d.width(), l = d.height(), u = "slice-up" == o ? l : "0", f = 0; f < this.options.slices; f++) {
                "slice-up-down" == o && (u = (f % 2 + 2) % 2 == 0 ? "0" : l);
                var m, x = f == this.options.slices - 1 ? c : c,
                    v = "rect(0px, " + x * (f + 1) + "px, " + l + "px, " + c * f + "px)";
                m = "rect(0px, " + x * (f + 1) + "px, 0px, " + c * f + "px)", ("slice-up" == o || "slice-up-down" == o && (f % 2 + 2) % 2 == 0) && (m = "rect(" + l + "px, " + x * (f + 1) + "px, " + l + "px, " + c * f + "px)"), r = i.$('<div class="uk-cover-background"></div>').css({
                    position: "absolute",
                    top: 0,
                    left: 0,
                    width: p,
                    height: l,
                    "background-image": h,
                    clip: m,
                    opacity: 0,
                    transition: "all " + this.options.duration + "ms ease-in-out " + 60 * f + "ms",
                    "-webkit-transition": "all " + this.options.duration + "ms ease-in-out " + 60 * f + "ms"
                }).data("clip", v), d.append(r)
            }
            return this.container.append(d), d.children().last().on(i.support.transition.end, function () {
                d.remove(), a.resolve()
            }), d.width(), d.children().each(function () {
                var t = i.$(this);
                t.css({
                    clip: t.data("clip"),
                    opacity: 1
                })
            }), a.promise()
        },
        "slice-up": function (i, e, s) {
            return t.slice.apply(this, [i, e, s, "slice-up"])
        },
        "slice-down": function (i, e, s) {
            return t.slice.apply(this, [i, e, s, "slice-down"])
        },
        "slice-up-down": function (i, e, s) {
            return t.slice.apply(this, [i, e, s, "slice-up-down"])
        },
        fold: function (e, s, n) {
            if (!s.data("cover")) return t.fade.apply(this, arguments);
            for (var o, r = i.$.Deferred(), a = Math.ceil(this.element.width() / this.options.slices), c = s.data("cover").css("background-image"), h = i.$("<li></li>").css({
                    width: s.width(),
                    height: s.height(),
                    opacity: 1,
                    zIndex: 15
                }), d = s.width(), p = s.height(), l = 0; l < this.options.slices; l++) o = i.$('<div class="uk-cover-background"></div>').css({
                position: "absolute",
                top: 0,
                left: 0,
                width: d,
                height: p,
                "background-image": c,
                "transform-origin": a * l + "px 0 0",
                clip: "rect(0px, " + a * (l + 1) + "px, " + p + "px, " + a * l + "px)",
                opacity: 0,
                transform: "scaleX(0.000001)",
                transition: "all " + this.options.duration + "ms ease-in-out " + (100 + 60 * l) + "ms",
                "-webkit-transition": "all " + this.options.duration + "ms ease-in-out " + (100 + 60 * l) + "ms"
            }), h.prepend(o);
            return this.container.append(h), h.width(), h.children().first().on(i.support.transition.end, function () {
                h.remove(), r.resolve()
            }).end().css({
                transform: "scaleX(1)",
                opacity: 1
            }), r.promise()
        },
        puzzle: function (s, n, o) {
            if (!n.data("cover")) return t.fade.apply(this, arguments);
            for (var r, a, c, h = i.$.Deferred(), d = this, p = Math.round(this.options.slices / 2), l = Math.round(n.width() / p), u = Math.round(n.height() / l), f = Math.round(n.height() / u) + 1, m = n.data("cover").css("background-image"), x = i.$("<li></li>").css({
                    width: this.container.width(),
                    height: this.container.height(),
                    opacity: 1,
                    zIndex: 15
                }), v = this.container.width(), g = this.container.height(), w = 0; u > w; w++)
                for (var b = 0; p > b; b++) c = b == p - 1 ? l + 2 : l, a = [f * w + "px", c * (b + 1) + "px", f * (w + 1) + "px", l * b + "px"], r = i.$('<div class="uk-cover-background"></div>').css({
                    position: "absolute",
                    top: 0,
                    left: 0,
                    opacity: 0,
                    width: v,
                    height: g,
                    "background-image": m,
                    clip: "rect(" + a.join(",") + ")",
                    "-webkit-transform": "translateZ(0)",
                    transform: "translateZ(0)"
                }), x.append(r);
            this.container.append(x);
            var k = e(x.children());
            return k.each(function (t) {
                i.$(this).css({
                    transition: "all " + d.options.duration + "ms ease-in-out " + (50 + 25 * t) + "ms",
                    "-webkit-transition": "all " + d.options.duration + "ms ease-in-out " + (50 + 25 * t) + "ms"
                })
            }).last().on(i.support.transition.end, function () {
                x.remove(), h.resolve()
            }), x.width(), k.css({
                opacity: 1
            }), h.promise()
        },
        boxes: function (e, s, n, o) {
            if (!s.data("cover")) return t.fade.apply(this, arguments);
            for (var r, a, c, h, d = i.$.Deferred(), p = Math.round(this.options.slices / 2), l = Math.round(s.width() / p), u = Math.round(s.height() / l), f = Math.round(s.height() / u) + 1, m = s.data("cover").css("background-image"), x = i.$("<li></li>").css({
                    width: s.width(),
                    height: s.height(),
                    opacity: 1,
                    zIndex: 15
                }), v = s.width(), g = s.height(), w = 0; u > w; w++)
                for (h = 0; p > h; h++) c = h == p - 1 ? l + 2 : l, a = [f * w + "px", c * (h + 1) + "px", f * (w + 1) + "px", l * h + "px"], r = i.$('<div class="uk-cover-background"></div>').css({
                    position: "absolute",
                    top: 0,
                    left: 0,
                    opacity: 1,
                    width: v,
                    height: g,
                    "background-image": m,
                    "transform-origin": a[3] + " " + a[0] + " 0",
                    clip: "rect(" + a.join(",") + ")",
                    "-webkit-transform": "scale(0.0000000000000001)",
                    transform: "scale(0.0000000000000001)"
                }), x.append(r);
            this.container.append(x);
            var b, k = 0,
                y = 0,
                $ = 0,
                I = [[]],
                M = x.children();
            for ("boxes-reverse" == o && (M = [].reverse.apply(M)), M.each(function () {
                    I[k][y] = i.$(this), y++, y == p && (k++, y = 0, I[k] = [])
                }), h = 0, b = 0; p * u > h; h++) {
                b = h;
                for (var z = 0; u > z; z++) b >= 0 && p > b && I[z][b].css({
                    transition: "all " + this.options.duration + "ms linear " + (50 + $) + "ms",
                    "-webkit-transition": "all " + this.options.duration + "ms linear " + (50 + $) + "ms"
                }), b--;
                $ += 100
            }
            return M.last().on(i.support.transition.end, function () {
                x.remove(), d.resolve()
            }), x.width(), M.css({
                "-webkit-transform": "scale(1)",
                transform: "scale(1)"
            }), d.promise()
        },
        "boxes-reverse": function (i, e, s) {
            return t.boxes.apply(this, [i, e, s, "boxes-reverse"])
        },
        "random-fx": function () {
            var i = ["slice-up", "fold", "puzzle", "slice-down", "boxes", "slice-up-down", "boxes-reverse"];
            return this.fxIndex = (void 0 === this.fxIndex ? -1 : this.fxIndex) + 1, i[this.fxIndex] || (this.fxIndex = 0), t[i[this.fxIndex]].apply(this, arguments)
        }
    });
    var e = function (i) {
        for (var t, e, s = i.length; s; t = parseInt(Math.random() * s), e = i[--s], i[s] = i[t], i[t] = e);
        return i
    };
    return i.slideshow.animations
});
! function (t) {
    var e;
    window.UIkit && (e = t(UIkit)), "function" == typeof define && define.amd && define("uikit-timepicker", ["uikit"], function () {
        return e || t(UIkit)
    })
}(function (t) {
    "use strict";

    function e(t, e) {
        t = t || 0, e = e || 24;
        var i, o, a = {
            "12h": [],
            "24h": []
        };
        for (i = t, o = ""; e > i; i++) o = "" + i, 10 > i && (o = "0" + o), a["24h"].push({
            value: o + ":00"
        }), a["24h"].push({
            value: o + ":30"
        }), 0 === i && (o = 12, a["12h"].push({
            value: o + ":00 AM"
        }), a["12h"].push({
            value: o + ":30 AM"
        })), i > 0 && 13 > i && 12 !== i && (a["12h"].push({
            value: o + ":00 AM"
        }), a["12h"].push({
            value: o + ":30 AM"
        })), i >= 12 && (o -= 12, 0 === o && (o = 12), 10 > o && (o = "0" + String(o)), a["12h"].push({
            value: o + ":00 PM"
        }), a["12h"].push({
            value: o + ":30 PM"
        }));
        return a
    }
    t.component("timepicker", {
        defaults: {
            format: "24h",
            delay: 0,
            start: 0,
            end: 24
        },
        boot: function () {
            t.$html.on("focus.timepicker.uikit", "[data-uk-timepicker]", function (e) {
                var i = t.$(this);
                if (!i.data("timepicker")) {
                    var o = t.timepicker(i, t.Utils.options(i.attr("data-uk-timepicker")));
                    setTimeout(function () {
                        o.autocomplete.input.focus()
                    }, 40)
                }
            })
        },
        init: function () {
            var i, o = this,
                a = e(this.options.start, this.options.end);
            this.options.minLength = 0, this.options.template = '<ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results">{{~items}}<li data-value="{{$item.value}}"><a>{{$item.value}}</a></li>{{/items}}</ul>', this.options.source = function (t) {
                t(a[o.options.format] || a["12h"])
            }, this.element.is("input") ? (this.element.wrap('<div class="uk-autocomplete"></div>'), i = this.element.parent()) : i = this.element.addClass("uk-autocomplete"), this.autocomplete = t.autocomplete(i, this.options), this.autocomplete.dropdown.addClass("uk-dropdown-small uk-dropdown-scrollable"), this.autocomplete.on("show.uk.autocomplete", function () {
                var t = o.autocomplete.dropdown.find('[data-value="' + o.autocomplete.input.val() + '"]');
                setTimeout(function () {
                    o.autocomplete.pick(t, !0)
                }, 10)
            }), this.autocomplete.input.on("focus", function () {
                o.autocomplete.value = Math.random(), o.autocomplete.triggercomplete()
            }).on("blur", t.Utils.debounce(function () {
                o.checkTime()
            }, 100)), this.element.data("timepicker", this)
        },
        checkTime: function () {
            var t, e, i, o, a = "AM",
                u = this.autocomplete.input.val();
            "12h" == this.options.format ? (t = u.split(" "), e = t[0].split(":"), a = t[1]) : e = u.split(":"), i = parseInt(e[0], 10), o = parseInt(e[1], 10), isNaN(i) && (i = 0), isNaN(o) && (o = 0), "12h" == this.options.format ? (i > 12 ? i = 12 : 0 > i && (i = 12), "am" === a || "a" === a ? a = "AM" : ("pm" === a || "p" === a) && (a = "PM"), "AM" !== a && "PM" !== a && (a = "AM")) : i >= 24 ? i = 23 : 0 > i && (i = 0), 0 > o ? o = 0 : o >= 60 && (o = 0), this.autocomplete.input.val(this.formatTime(i, o, a)).trigger("change")
        },
        formatTime: function (t, e, i) {
            return t = 10 > t ? "0" + t : t, e = 10 > e ? "0" + e : e, t + ":" + e + ("12h" == this.options.format ? " " + i : "")
        }
    })
});
! function (t) {
    var e = {};
    t.fn.socialButtons = function (a) {
        return a = t.extend({
            wrapper: '<div class="tm-socialbuttons uk-clearfix">'
        }, a), a.twitter || a.plusone || a.facebook ? (a.twitter && !e.twitter && (e.twitter = t.getScript("//platform.twitter.com/widgets.js")), a.plusone && !e.plusone && (e.plusone = t.getScript("//apis.google.com/js/plusone.js")), window.FB || !a.facebook || e.facebook || (t("body").append('<div id="fb-root"></div>'), function (t, e, a) {
            var o, n = t.getElementsByTagName(e)[0];
            t.getElementById(a) || (o = t.createElement(e), o.id = a, o.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0", n.parentNode.insertBefore(o, n))
        }(document, "script", "facebook-jssdk"), e.facebook = !0), this.each(function () {
            var e = t(this).data("permalink"),
                o = t(a.wrapper).appendTo(this);
            a.twitter && o.append('<div><a href="http://twitter.com/share" class="twitter-share-button" data-url="' + e + '" data-count="none">Tweet</a></div>'), a.plusone && o.append('<div><div class="g-plusone" data-size="medium" data-annotation="none" data-href="' + e + '"></div></div>'), a.facebook && o.append('<div><div class="fb-like" data-href="' + e + '" data-layout="button_count" data-action="like" data-width="100" data-show-faces="false" data-share="false"></div></div>')
        })) : this
    }, t(function () {
        window.MooTools && Element.prototype.hide && (Element.prototype.hide = function () {})
    })
}(jQuery);
jQuery(function ($) {
    "use strict";
    var config = $('html').data('config') || {},
        preloader = $('.tm-preload');
    $('article[data-permalink]').socialButtons(config);
    $(".uk-offcanvas-bar .uk-parent").removeClass("uk-open").children("a").attr("href", "#");
    $(document).ready(function () {
        var browser_width1 = $(window).width();
        $(".tm-navbar").find("li.uk-parent > ul").each(function (index) {
            var offset1 = $(this).offset();
            var xwidth1 = offset1.left + $(this).width();
            if (xwidth1 >= browser_width1) {
                $(this).addClass("uk-dropdown-flip");
            }
        });
    })
    $(window).resize(function () {
        var browser_width = $(window).width();
        $(".tm-navbar").find("li.uk-parent > ul").removeClass("uk-dropdown-flip");
        $(".tm-navbar").find("li.uk-parent > ul").each(function (index) {
            var offset = $(this).offset();
            var xwidth = offset.left + $(this).width();
            if (xwidth >= browser_width) {
                $(this).addClass("uk-dropdown-flip");
            }
        });
    });
    $(window).on("load", function () {
        preloader.removeClass('loading').fadeOut(500, function () {})
    }), $(window).on("beforeunload", function () {
        preloader.addClass('loading').fadeIn(300, function () {})
    });
});